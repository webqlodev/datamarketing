/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 17);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * jQuery JavaScript Library v3.3.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2018-01-20T17:24Z
 */
( function( global, factory ) {

	"use strict";

	if ( typeof module === "object" && typeof module.exports === "object" ) {

		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
// enough that all such attempts are guarded in a try block.
"use strict";

var arr = [];

var document = window.document;

var getProto = Object.getPrototypeOf;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var fnToString = hasOwn.toString;

var ObjectFunctionString = fnToString.call( Object );

var support = {};

var isFunction = function isFunction( obj ) {

      // Support: Chrome <=57, Firefox <=52
      // In some browsers, typeof returns "function" for HTML <object> elements
      // (i.e., `typeof document.createElement( "object" ) === "function"`).
      // We don't want to classify *any* DOM node as a function.
      return typeof obj === "function" && typeof obj.nodeType !== "number";
  };


var isWindow = function isWindow( obj ) {
		return obj != null && obj === obj.window;
	};




	var preservedScriptAttributes = {
		type: true,
		src: true,
		noModule: true
	};

	function DOMEval( code, doc, node ) {
		doc = doc || document;

		var i,
			script = doc.createElement( "script" );

		script.text = code;
		if ( node ) {
			for ( i in preservedScriptAttributes ) {
				if ( node[ i ] ) {
					script[ i ] = node[ i ];
				}
			}
		}
		doc.head.appendChild( script ).parentNode.removeChild( script );
	}


function toType( obj ) {
	if ( obj == null ) {
		return obj + "";
	}

	// Support: Android <=2.3 only (functionish RegExp)
	return typeof obj === "object" || typeof obj === "function" ?
		class2type[ toString.call( obj ) ] || "object" :
		typeof obj;
}
/* global Symbol */
// Defining this global in .eslintrc.json would create a danger of using the global
// unguarded in another place, it seems safer to define global only for this module



var
	version = "3.3.1",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android <=4.0 only
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {

		// Return all the elements in a clean array
		if ( num == null ) {
			return slice.call( this );
		}

		// Return just the one element from the set
		return num < 0 ? this[ num + this.length ] : this[ num ];
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = Array.isArray( copy ) ) ) ) {

					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && Array.isArray( src ) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject( src ) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isPlainObject: function( obj ) {
		var proto, Ctor;

		// Detect obvious negatives
		// Use toString instead of jQuery.type to catch host objects
		if ( !obj || toString.call( obj ) !== "[object Object]" ) {
			return false;
		}

		proto = getProto( obj );

		// Objects with no prototype (e.g., `Object.create( null )`) are plain
		if ( !proto ) {
			return true;
		}

		// Objects with prototype are plain iff they were constructed by a global Object function
		Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
		return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
	},

	isEmptyObject: function( obj ) {

		/* eslint-disable no-unused-vars */
		// See https://github.com/eslint/eslint/issues/6125
		var name;

		for ( name in obj ) {
			return false;
		}
		return true;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		DOMEval( code );
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// Support: Android <=4.0 only
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	// Support: Android <=4.0 only, PhantomJS 1 only
	// push.apply(_, arraylike) throws on ancient WebKit
	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: real iOS 8.2 only (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = toType( obj );

	if ( isFunction( obj ) || isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.3.3
 * https://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2016-08-08
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// https://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,

	// CSS escapes
	// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// CSS string/identifier serialization
	// https://drafts.csswg.org/cssom/#common-serializing-idioms
	rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
	fcssescape = function( ch, asCodePoint ) {
		if ( asCodePoint ) {

			// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
			if ( ch === "\0" ) {
				return "\uFFFD";
			}

			// Control characters and (dependent upon position) numbers get escaped as code points
			return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
		}

		// Other potentially-special ASCII characters get backslash-escaped
		return "\\" + ch;
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	},

	disabledAncestor = addCombinator(
		function( elem ) {
			return elem.disabled === true && ("form" in elem || "label" in elem);
		},
		{ dir: "parentNode", next: "legend" }
	);

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

				// ID selector
				if ( (m = match[1]) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( (elem = context.getElementById( m )) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && (elem = newContext.getElementById( m )) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[2] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( (m = match[3]) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!compilerCache[ selector + " " ] &&
				(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {

				if ( nodeType !== 1 ) {
					newContext = context;
					newSelector = selector;

				// qSA looks outside Element context, which is not what we want
				// Thanks to Andrew Dupont for this workaround technique
				// Support: IE <=8
				// Exclude object elements
				} else if ( context.nodeName.toLowerCase() !== "object" ) {

					// Capture the context ID, setting it first if necessary
					if ( (nid = context.getAttribute( "id" )) ) {
						nid = nid.replace( rcssescape, fcssescape );
					} else {
						context.setAttribute( "id", (nid = expando) );
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					while ( i-- ) {
						groups[i] = "#" + nid + " " + toSelector( groups[i] );
					}
					newSelector = groups.join( "," );

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;
				}

				if ( newSelector ) {
					try {
						push.apply( results,
							newContext.querySelectorAll( newSelector )
						);
						return results;
					} catch ( qsaError ) {
					} finally {
						if ( nid === expando ) {
							context.removeAttribute( "id" );
						}
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created element and returns a boolean result
 */
function assert( fn ) {
	var el = document.createElement("fieldset");

	try {
		return !!fn( el );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( el.parentNode ) {
			el.parentNode.removeChild( el );
		}
		// release memory in IE
		el = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			a.sourceIndex - b.sourceIndex;

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for :enabled/:disabled
 * @param {Boolean} disabled true for :disabled; false for :enabled
 */
function createDisabledPseudo( disabled ) {

	// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
	return function( elem ) {

		// Only certain elements can match :enabled or :disabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
		if ( "form" in elem ) {

			// Check for inherited disabledness on relevant non-disabled elements:
			// * listed form-associated elements in a disabled fieldset
			//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
			// * option elements in a disabled optgroup
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
			// All such elements have a "form" property.
			if ( elem.parentNode && elem.disabled === false ) {

				// Option elements defer to a parent optgroup if present
				if ( "label" in elem ) {
					if ( "label" in elem.parentNode ) {
						return elem.parentNode.disabled === disabled;
					} else {
						return elem.disabled === disabled;
					}
				}

				// Support: IE 6 - 11
				// Use the isDisabled shortcut property to check for disabled fieldset ancestors
				return elem.isDisabled === disabled ||

					// Where there is no isDisabled, check manually
					/* jshint -W018 */
					elem.isDisabled !== !disabled &&
						disabledAncestor( elem ) === disabled;
			}

			return elem.disabled === disabled;

		// Try to winnow out elements that can't be disabled before trusting the disabled property.
		// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
		// even exist on them, let alone have a boolean value.
		} else if ( "label" in elem ) {
			return elem.disabled === disabled;
		}

		// Remaining elements are neither :enabled nor :disabled
		return false;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, subWindow,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9-11, Edge
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	if ( preferredDoc !== document &&
		(subWindow = document.defaultView) && subWindow.top !== subWindow ) {

		// Support: IE 11, Edge
		if ( subWindow.addEventListener ) {
			subWindow.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( subWindow.attachEvent ) {
			subWindow.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( el ) {
		el.className = "i";
		return !el.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( el ) {
		el.appendChild( document.createComment("") );
		return !el.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programmatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( el ) {
		docElem.appendChild( el ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	});

	// ID filter and find
	if ( support.getById ) {
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var elem = context.getElementById( id );
				return elem ? [ elem ] : [];
			}
		};
	} else {
		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};

		// Support: IE 6 - 7 only
		// getElementById is not reliable as a find shortcut
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var node, i, elems,
					elem = context.getElementById( id );

				if ( elem ) {

					// Verify the id attribute
					node = elem.getAttributeNode("id");
					if ( node && node.value === id ) {
						return [ elem ];
					}

					// Fall back on getElementsByName
					elems = context.getElementsByName( id );
					i = 0;
					while ( (elem = elems[i++]) ) {
						node = elem.getAttributeNode("id");
						if ( node && node.value === id ) {
							return [ elem ];
						}
					}
				}

				return [];
			}
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See https://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( el ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// https://bugs.jquery.com/ticket/12359
			docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( el.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !el.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !el.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibling-combinator selector` fails
			if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( el ) {
			el.innerHTML = "<a href='' disabled='disabled'></a>" +
				"<select disabled='disabled'><option/></select>";

			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement("input");
			input.setAttribute( "type", "hidden" );
			el.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( el.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( el.querySelectorAll(":enabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: IE9-11+
			// IE's :disabled selector does not pick up the children of disabled fieldsets
			docElem.appendChild( el ).disabled = true;
			if ( el.querySelectorAll(":disabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			el.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( el ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( el, "*" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( el, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === document ? -1 :
				b === document ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		!compilerCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.escape = function( sel ) {
	return (sel + "").replace( rcssescape, fcssescape );
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || (node[ expando ] = {});

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								(outerCache[ node.uniqueID ] = {});

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {
							// Use previously-cached element index if available
							if ( useCache ) {
								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || (node[ expando ] = {});

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] || (node[ expando ] = {});

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												(outerCache[ node.uniqueID ] = {});

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": createDisabledPseudo( false ),
		"disabled": createDisabledPseudo( true ),

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		skip = combinator.next,
		key = skip || dir,
		checkNonElements = base && key === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
			return false;
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

						if ( skip && skip === elem.nodeName.toLowerCase() ) {
							elem = elem[ dir ] || elem;
						} else if ( (oldCache = uniqueCache[ key ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ key ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
			return false;
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context === document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					if ( !context && elem.ownerDocument !== document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context || document, xml) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( el ) {
	// Should return 1, but returns 4 (following)
	return el.compareDocumentPosition( document.createElement("fieldset") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( el ) {
	el.innerHTML = "<a href='#'></a>";
	return el.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( el ) {
	el.innerHTML = "<input/>";
	el.firstChild.setAttribute( "value", "" );
	return el.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( el ) {
	return el.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;

// Deprecated
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;
jQuery.escapeSelector = Sizzle.escape;




var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;



function nodeName( elem, name ) {

  return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();

};
var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );



// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			return !!qualifier.call( elem, i, elem ) !== not;
		} );
	}

	// Single element
	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );
	}

	// Arraylike of elements (jQuery, arguments, Array)
	if ( typeof qualifier !== "string" ) {
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
		} );
	}

	// Filtered directly for both simple and complex selectors
	return jQuery.filter( qualifier, elements, not );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	if ( elems.length === 1 && elem.nodeType === 1 ) {
		return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
	}

	return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
		return elem.nodeType === 1;
	} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i, ret,
			len = this.length,
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		ret = this.pushStack( [] );

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		return len > 1 ? jQuery.uniqueSort( ret ) : ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	// Shortcut simple #id case for speed
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					if ( elem ) {

						// Inject the element directly into the jQuery object
						this[ 0 ] = elem;
						this.length = 1;
					}
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			targets = typeof selectors !== "string" && jQuery( selectors );

		// Positional selectors never match, since there's no _selection_ context
		if ( !rneedsContext.test( selectors ) ) {
			for ( ; i < l; i++ ) {
				for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

					// Always skip document fragments
					if ( cur.nodeType < 11 && ( targets ?
						targets.index( cur ) > -1 :

						// Don't pass non-elements to Sizzle
						cur.nodeType === 1 &&
							jQuery.find.matchesSelector( cur, selectors ) ) ) {

						matched.push( cur );
						break;
					}
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
        if ( nodeName( elem, "iframe" ) ) {
            return elem.contentDocument;
        }

        // Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
        // Treat the template element as a regular one in browsers that
        // don't support it.
        if ( nodeName( elem, "template" ) ) {
            elem = elem.content || elem;
        }

        return jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = locked || options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && toType( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory && !firing ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


function Identity( v ) {
	return v;
}
function Thrower( ex ) {
	throw ex;
}

function adoptValue( value, resolve, reject, noValue ) {
	var method;

	try {

		// Check for promise aspect first to privilege synchronous behavior
		if ( value && isFunction( ( method = value.promise ) ) ) {
			method.call( value ).done( resolve ).fail( reject );

		// Other thenables
		} else if ( value && isFunction( ( method = value.then ) ) ) {
			method.call( value, resolve, reject );

		// Other non-thenables
		} else {

			// Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
			// * false: [ value ].slice( 0 ) => resolve( value )
			// * true: [ value ].slice( 1 ) => resolve()
			resolve.apply( undefined, [ value ].slice( noValue ) );
		}

	// For Promises/A+, convert exceptions into rejections
	// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
	// Deferred#then to conditionally suppress rejection.
	} catch ( value ) {

		// Support: Android 4.0 only
		// Strict mode functions invoked without .call/.apply get global-object context
		reject.apply( undefined, [ value ] );
	}
}

jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, callbacks,
				// ... .then handlers, argument index, [final state]
				[ "notify", "progress", jQuery.Callbacks( "memory" ),
					jQuery.Callbacks( "memory" ), 2 ],
				[ "resolve", "done", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 0, "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 1, "rejected" ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				"catch": function( fn ) {
					return promise.then( null, fn );
				},

				// Keep pipe for back-compat
				pipe: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;

					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {

							// Map tuples (progress, done, fail) to arguments (done, fail, progress)
							var fn = isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

							// deferred.progress(function() { bind to newDefer or newDefer.notify })
							// deferred.done(function() { bind to newDefer or newDefer.resolve })
							// deferred.fail(function() { bind to newDefer or newDefer.reject })
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},
				then: function( onFulfilled, onRejected, onProgress ) {
					var maxDepth = 0;
					function resolve( depth, deferred, handler, special ) {
						return function() {
							var that = this,
								args = arguments,
								mightThrow = function() {
									var returned, then;

									// Support: Promises/A+ section 2.3.3.3.3
									// https://promisesaplus.com/#point-59
									// Ignore double-resolution attempts
									if ( depth < maxDepth ) {
										return;
									}

									returned = handler.apply( that, args );

									// Support: Promises/A+ section 2.3.1
									// https://promisesaplus.com/#point-48
									if ( returned === deferred.promise() ) {
										throw new TypeError( "Thenable self-resolution" );
									}

									// Support: Promises/A+ sections 2.3.3.1, 3.5
									// https://promisesaplus.com/#point-54
									// https://promisesaplus.com/#point-75
									// Retrieve `then` only once
									then = returned &&

										// Support: Promises/A+ section 2.3.4
										// https://promisesaplus.com/#point-64
										// Only check objects and functions for thenability
										( typeof returned === "object" ||
											typeof returned === "function" ) &&
										returned.then;

									// Handle a returned thenable
									if ( isFunction( then ) ) {

										// Special processors (notify) just wait for resolution
										if ( special ) {
											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special )
											);

										// Normal processors (resolve) also hook into progress
										} else {

											// ...and disregard older resolution values
											maxDepth++;

											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special ),
												resolve( maxDepth, deferred, Identity,
													deferred.notifyWith )
											);
										}

									// Handle all other returned values
									} else {

										// Only substitute handlers pass on context
										// and multiple values (non-spec behavior)
										if ( handler !== Identity ) {
											that = undefined;
											args = [ returned ];
										}

										// Process the value(s)
										// Default process is resolve
										( special || deferred.resolveWith )( that, args );
									}
								},

								// Only normal processors (resolve) catch and reject exceptions
								process = special ?
									mightThrow :
									function() {
										try {
											mightThrow();
										} catch ( e ) {

											if ( jQuery.Deferred.exceptionHook ) {
												jQuery.Deferred.exceptionHook( e,
													process.stackTrace );
											}

											// Support: Promises/A+ section 2.3.3.3.4.1
											// https://promisesaplus.com/#point-61
											// Ignore post-resolution exceptions
											if ( depth + 1 >= maxDepth ) {

												// Only substitute handlers pass on context
												// and multiple values (non-spec behavior)
												if ( handler !== Thrower ) {
													that = undefined;
													args = [ e ];
												}

												deferred.rejectWith( that, args );
											}
										}
									};

							// Support: Promises/A+ section 2.3.3.3.1
							// https://promisesaplus.com/#point-57
							// Re-resolve promises immediately to dodge false rejection from
							// subsequent errors
							if ( depth ) {
								process();
							} else {

								// Call an optional hook to record the stack, in case of exception
								// since it's otherwise lost when execution goes async
								if ( jQuery.Deferred.getStackHook ) {
									process.stackTrace = jQuery.Deferred.getStackHook();
								}
								window.setTimeout( process );
							}
						};
					}

					return jQuery.Deferred( function( newDefer ) {

						// progress_handlers.add( ... )
						tuples[ 0 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onProgress ) ?
									onProgress :
									Identity,
								newDefer.notifyWith
							)
						);

						// fulfilled_handlers.add( ... )
						tuples[ 1 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onFulfilled ) ?
									onFulfilled :
									Identity
							)
						);

						// rejected_handlers.add( ... )
						tuples[ 2 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onRejected ) ?
									onRejected :
									Thrower
							)
						);
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 5 ];

			// promise.progress = list.add
			// promise.done = list.add
			// promise.fail = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(
					function() {

						// state = "resolved" (i.e., fulfilled)
						// state = "rejected"
						state = stateString;
					},

					// rejected_callbacks.disable
					// fulfilled_callbacks.disable
					tuples[ 3 - i ][ 2 ].disable,

					// rejected_handlers.disable
					// fulfilled_handlers.disable
					tuples[ 3 - i ][ 3 ].disable,

					// progress_callbacks.lock
					tuples[ 0 ][ 2 ].lock,

					// progress_handlers.lock
					tuples[ 0 ][ 3 ].lock
				);
			}

			// progress_handlers.fire
			// fulfilled_handlers.fire
			// rejected_handlers.fire
			list.add( tuple[ 3 ].fire );

			// deferred.notify = function() { deferred.notifyWith(...) }
			// deferred.resolve = function() { deferred.resolveWith(...) }
			// deferred.reject = function() { deferred.rejectWith(...) }
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
				return this;
			};

			// deferred.notifyWith = list.fireWith
			// deferred.resolveWith = list.fireWith
			// deferred.rejectWith = list.fireWith
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( singleValue ) {
		var

			// count of uncompleted subordinates
			remaining = arguments.length,

			// count of unprocessed arguments
			i = remaining,

			// subordinate fulfillment data
			resolveContexts = Array( i ),
			resolveValues = slice.call( arguments ),

			// the master Deferred
			master = jQuery.Deferred(),

			// subordinate callback factory
			updateFunc = function( i ) {
				return function( value ) {
					resolveContexts[ i ] = this;
					resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( !( --remaining ) ) {
						master.resolveWith( resolveContexts, resolveValues );
					}
				};
			};

		// Single- and empty arguments are adopted like Promise.resolve
		if ( remaining <= 1 ) {
			adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject,
				!remaining );

			// Use .then() to unwrap secondary thenables (cf. gh-3000)
			if ( master.state() === "pending" ||
				isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {

				return master.then();
			}
		}

		// Multiple arguments are aggregated like Promise.all array elements
		while ( i-- ) {
			adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
		}

		return master.promise();
	}
} );


// These usually indicate a programmer mistake during development,
// warn about them ASAP rather than swallowing them by default.
var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

jQuery.Deferred.exceptionHook = function( error, stack ) {

	// Support: IE 8 - 9 only
	// Console exists when dev tools are open, which can happen at any time
	if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
		window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
	}
};




jQuery.readyException = function( error ) {
	window.setTimeout( function() {
		throw error;
	} );
};




// The deferred used on DOM ready
var readyList = jQuery.Deferred();

jQuery.fn.ready = function( fn ) {

	readyList
		.then( fn )

		// Wrap jQuery.readyException in a function so that the lookup
		// happens at the time of error handling instead of callback
		// registration.
		.catch( function( error ) {
			jQuery.readyException( error );
		} );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );
	}
} );

jQuery.ready.then = readyList.then;

// The ready event handler and self cleanup method
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

// Catch cases where $(document).ready() is called
// after the browser event has already occurred.
// Support: IE <=9 - 10 only
// Older IE sometimes signals "interactive" too soon
if ( document.readyState === "complete" ||
	( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

	// Handle it asynchronously to allow scripts the opportunity to delay ready
	window.setTimeout( jQuery.ready );

} else {

	// Use the handy event callback
	document.addEventListener( "DOMContentLoaded", completed );

	// A fallback to window.onload, that will always work
	window.addEventListener( "load", completed );
}




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( toType( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	if ( chainable ) {
		return elems;
	}

	// Gets
	if ( bulk ) {
		return fn.call( elems );
	}

	return len ? fn( elems[ 0 ], key ) : emptyGet;
};


// Matches dashed string for camelizing
var rmsPrefix = /^-ms-/,
	rdashAlpha = /-([a-z])/g;

// Used by camelCase as callback to replace()
function fcamelCase( all, letter ) {
	return letter.toUpperCase();
}

// Convert dashed to camelCase; used by the css and data modules
// Support: IE <=9 - 11, Edge 12 - 15
// Microsoft forgot to hump their vendor prefix (#9572)
function camelCase( string ) {
	return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
}
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	cache: function( owner ) {

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		// Always use camelCase key (gh-2257)
		if ( typeof data === "string" ) {
			cache[ camelCase( data ) ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ camelCase( prop ) ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :

			// Always use camelCase key (gh-2257)
			owner[ this.expando ] && owner[ this.expando ][ camelCase( key ) ];
	},
	access: function( owner, key, value ) {

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			return this.get( owner, key );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key !== undefined ) {

			// Support array or space separated string of keys
			if ( Array.isArray( key ) ) {

				// If key is an array of keys...
				// We always set camelCase keys, so remove that.
				key = key.map( camelCase );
			} else {
				key = camelCase( key );

				// If a key with the spaces exists, use it.
				// Otherwise, create an array by matching non-whitespace
				key = key in cache ?
					[ key ] :
					( key.match( rnothtmlwhite ) || [] );
			}

			i = key.length;

			while ( i-- ) {
				delete cache[ key[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <=35 - 45
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function getData( data ) {
	if ( data === "true" ) {
		return true;
	}

	if ( data === "false" ) {
		return false;
	}

	if ( data === "null" ) {
		return null;
	}

	// Only convert to a number if it doesn't change the string
	if ( data === +data + "" ) {
		return +data;
	}

	if ( rbrace.test( data ) ) {
		return JSON.parse( data );
	}

	return data;
}

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = getData( data );
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE 11 only
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// The key will always be camelCased in Data
				data = dataUser.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each( function() {

				// We always store the camelCased key
				dataUser.set( this, key, value );
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || Array.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHiddenWithinTree = function( elem, el ) {

		// isHiddenWithinTree might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;

		// Inline style trumps all
		return elem.style.display === "none" ||
			elem.style.display === "" &&

			// Otherwise, check computed style
			// Support: Firefox <=43 - 45
			// Disconnected elements can have computed display: none, so first confirm that elem is
			// in the document.
			jQuery.contains( elem.ownerDocument, elem ) &&

			jQuery.css( elem, "display" ) === "none";
	};

var swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};




function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted, scale,
		maxIterations = 20,
		currentValue = tween ?
			function() {
				return tween.cur();
			} :
			function() {
				return jQuery.css( elem, prop, "" );
			},
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Support: Firefox <=54
		// Halve the iteration target value to prevent interference from CSS upper bounds (gh-2144)
		initial = initial / 2;

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		while ( maxIterations-- ) {

			// Evaluate and update our best guess (doubling guesses that zero out).
			// Finish if the scale equals or crosses 1 (making the old*new product non-positive).
			jQuery.style( elem, prop, initialInUnit + unit );
			if ( ( 1 - scale ) * ( 1 - ( scale = currentValue() / initial || 0.5 ) ) <= 0 ) {
				maxIterations = 0;
			}
			initialInUnit = initialInUnit / scale;

		}

		initialInUnit = initialInUnit * 2;
		jQuery.style( elem, prop, initialInUnit + unit );

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}


var defaultDisplayMap = {};

function getDefaultDisplay( elem ) {
	var temp,
		doc = elem.ownerDocument,
		nodeName = elem.nodeName,
		display = defaultDisplayMap[ nodeName ];

	if ( display ) {
		return display;
	}

	temp = doc.body.appendChild( doc.createElement( nodeName ) );
	display = jQuery.css( temp, "display" );

	temp.parentNode.removeChild( temp );

	if ( display === "none" ) {
		display = "block";
	}
	defaultDisplayMap[ nodeName ] = display;

	return display;
}

function showHide( elements, show ) {
	var display, elem,
		values = [],
		index = 0,
		length = elements.length;

	// Determine new display value for elements that need to change
	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		display = elem.style.display;
		if ( show ) {

			// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
			// check is required in this first loop unless we have a nonempty display value (either
			// inline or about-to-be-restored)
			if ( display === "none" ) {
				values[ index ] = dataPriv.get( elem, "display" ) || null;
				if ( !values[ index ] ) {
					elem.style.display = "";
				}
			}
			if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
				values[ index ] = getDefaultDisplay( elem );
			}
		} else {
			if ( display !== "none" ) {
				values[ index ] = "none";

				// Remember what we're overwriting
				dataPriv.set( elem, "display", display );
			}
		}
	}

	// Set the display of the elements in a second loop to avoid constant reflow
	for ( index = 0; index < length; index++ ) {
		if ( values[ index ] != null ) {
			elements[ index ].style.display = values[ index ];
		}
	}

	return elements;
}

jQuery.fn.extend( {
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHiddenWithinTree( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]+)/i );

var rscriptType = ( /^$|^module$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// Support: IE <=9 only
	option: [ 1, "<select multiple='multiple'>", "</select>" ],

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

// Support: IE <=9 only
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {

	// Support: IE <=9 - 11 only
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret;

	if ( typeof context.getElementsByTagName !== "undefined" ) {
		ret = context.getElementsByTagName( tag || "*" );

	} else if ( typeof context.querySelectorAll !== "undefined" ) {
		ret = context.querySelectorAll( tag || "*" );

	} else {
		ret = [];
	}

	if ( tag === undefined || tag && nodeName( context, tag ) ) {
		return jQuery.merge( [ context ], ret );
	}

	return ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, contains, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( toType( elem ) === "object" ) {

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		contains = jQuery.contains( elem.ownerDocument, elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( contains ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0 - 4.3 only
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Android <=4.1 only
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE <=11 only
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
} )();
var documentElement = document.documentElement;



var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE <=9 only
// See #13393 for more info
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Ensure that invalid selectors throw exceptions at attach time
		// Evaluate against documentElement in case elem is a non-element node (e.g., document)
		if ( selector ) {
			jQuery.find.matchesSelector( documentElement, selector );
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = {};
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( nativeEvent ) {

		// Make a writable jQuery.Event from the native event object
		var event = jQuery.event.fix( nativeEvent );

		var i, j, ret, matched, handleObj, handlerQueue,
			args = new Array( arguments.length ),
			handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;

		for ( i = 1; i < arguments.length; i++ ) {
			args[ i ] = arguments[ i ];
		}

		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, handleObj, sel, matchedHandlers, matchedSelectors,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		if ( delegateCount &&

			// Support: IE <=9
			// Black-hole SVG <use> instance trees (trac-13180)
			cur.nodeType &&

			// Support: Firefox <=42
			// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
			// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
			// Support: IE 11 only
			// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
			!( event.type === "click" && event.button >= 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
					matchedHandlers = [];
					matchedSelectors = {};
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matchedSelectors[ sel ] === undefined ) {
							matchedSelectors[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matchedSelectors[ sel ] ) {
							matchedHandlers.push( handleObj );
						}
					}
					if ( matchedHandlers.length ) {
						handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		cur = this;
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	addProp: function( name, hook ) {
		Object.defineProperty( jQuery.Event.prototype, name, {
			enumerable: true,
			configurable: true,

			get: isFunction( hook ) ?
				function() {
					if ( this.originalEvent ) {
							return hook( this.originalEvent );
					}
				} :
				function() {
					if ( this.originalEvent ) {
							return this.originalEvent[ name ];
					}
				},

			set: function( value ) {
				Object.defineProperty( this, name, {
					enumerable: true,
					configurable: true,
					writable: true,
					value: value
				} );
			}
		} );
	},

	fix: function( originalEvent ) {
		return originalEvent[ jQuery.expando ] ?
			originalEvent :
			new jQuery.Event( originalEvent );
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {

			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {

			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android <=2.3 only
				src.returnValue === false ?
			returnTrue :
			returnFalse;

		// Create target properties
		// Support: Safari <=6 - 7 only
		// Target should not be a text node (#504, #13143)
		this.target = ( src.target && src.target.nodeType === 3 ) ?
			src.target.parentNode :
			src.target;

		this.currentTarget = src.currentTarget;
		this.relatedTarget = src.relatedTarget;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || Date.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Includes all common event props including KeyEvent and MouseEvent specific props
jQuery.each( {
	altKey: true,
	bubbles: true,
	cancelable: true,
	changedTouches: true,
	ctrlKey: true,
	detail: true,
	eventPhase: true,
	metaKey: true,
	pageX: true,
	pageY: true,
	shiftKey: true,
	view: true,
	"char": true,
	charCode: true,
	key: true,
	keyCode: true,
	button: true,
	buttons: true,
	clientX: true,
	clientY: true,
	offsetX: true,
	offsetY: true,
	pointerId: true,
	pointerType: true,
	screenX: true,
	screenY: true,
	targetTouches: true,
	toElement: true,
	touches: true,

	which: function( event ) {
		var button = event.button;

		// Add which for key events
		if ( event.which == null && rkeyEvent.test( event.type ) ) {
			return event.charCode != null ? event.charCode : event.keyCode;
		}

		// Add which for click: 1 === left; 2 === middle; 3 === right
		if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
			if ( button & 1 ) {
				return 1;
			}

			if ( button & 2 ) {
				return 3;
			}

			if ( button & 4 ) {
				return 2;
			}

			return 0;
		}

		return event.which;
	}
}, jQuery.event.addProp );

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {

	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var

	/* eslint-disable max-len */

	// See https://github.com/eslint/eslint/issues/3229
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,

	/* eslint-enable */

	// Support: IE <=10 - 11, Edge 12 - 13 only
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Prefer a tbody over its parent table for containing new rows
function manipulationTarget( elem, content ) {
	if ( nodeName( elem, "table" ) &&
		nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

		return jQuery( elem ).children( "tbody" )[ 0 ] || elem;
	}

	return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	if ( ( elem.type || "" ).slice( 0, 5 ) === "true/" ) {
		elem.type = elem.type.slice( 5 );
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.access( src );
		pdataCur = dataPriv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = concat.apply( [], args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		valueIsFunction = isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( valueIsFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( valueIsFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android <=4.0 only, PhantomJS 1 only
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src && ( node.type || "" ).toLowerCase()  !== "module" ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl ) {
								jQuery._evalUrl( node.src );
							}
						} else {
							DOMEval( node.textContent.replace( rcleanScript, "" ), doc, node );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html.replace( rxhtmlTag, "<$1></$2>" );
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {
	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: Android <=4.0 only, PhantomJS 1 only
			// .get() because push.apply(_, arraylike) throws on ancient WebKit
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );
var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};

var rboxStyle = new RegExp( cssExpand.join( "|" ), "i" );



( function() {

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {

		// This is a singleton, we need to execute it only once
		if ( !div ) {
			return;
		}

		container.style.cssText = "position:absolute;left:-11111px;width:60px;" +
			"margin-top:1px;padding:0;border:0";
		div.style.cssText =
			"position:relative;display:block;box-sizing:border-box;overflow:scroll;" +
			"margin:auto;border:1px;padding:1px;" +
			"width:60%;top:1%";
		documentElement.appendChild( container ).appendChild( div );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";

		// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
		reliableMarginLeftVal = roundPixelMeasures( divStyle.marginLeft ) === 12;

		// Support: Android 4.0 - 4.3 only, Safari <=9.1 - 10.1, iOS <=7.0 - 9.3
		// Some styles come back with percentage values, even though they shouldn't
		div.style.right = "60%";
		pixelBoxStylesVal = roundPixelMeasures( divStyle.right ) === 36;

		// Support: IE 9 - 11 only
		// Detect misreporting of content dimensions for box-sizing:border-box elements
		boxSizingReliableVal = roundPixelMeasures( divStyle.width ) === 36;

		// Support: IE 9 only
		// Detect overflow:scroll screwiness (gh-3699)
		div.style.position = "absolute";
		scrollboxSizeVal = div.offsetWidth === 36 || "absolute";

		documentElement.removeChild( container );

		// Nullify the div so it wouldn't be stored in the memory and
		// it will also be a sign that checks already performed
		div = null;
	}

	function roundPixelMeasures( measure ) {
		return Math.round( parseFloat( measure ) );
	}

	var pixelPositionVal, boxSizingReliableVal, scrollboxSizeVal, pixelBoxStylesVal,
		reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE <=9 - 11 only
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	jQuery.extend( support, {
		boxSizingReliable: function() {
			computeStyleTests();
			return boxSizingReliableVal;
		},
		pixelBoxStyles: function() {
			computeStyleTests();
			return pixelBoxStylesVal;
		},
		pixelPosition: function() {
			computeStyleTests();
			return pixelPositionVal;
		},
		reliableMarginLeft: function() {
			computeStyleTests();
			return reliableMarginLeftVal;
		},
		scrollboxSize: function() {
			computeStyleTests();
			return scrollboxSizeVal;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,

		// Support: Firefox 51+
		// Retrieving style before computed somehow
		// fixes an issue with getting wrong values
		// on detached elements
		style = elem.style;

	computed = computed || getStyles( elem );

	// getPropertyValue is needed for:
	//   .css('filter') (IE 9 only, #12537)
	//   .css('--customProperty) (#3144)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];

		if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// https://drafts.csswg.org/cssom/#resolved-values
		if ( !support.pixelBoxStyles() && rnumnonpx.test( ret ) && rboxStyle.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE <=9 - 11 only
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rcustomProp = /^--/,
	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style;

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in emptyStyle ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

// Return a property mapped along what jQuery.cssProps suggests or to
// a vendor prefixed property.
function finalPropName( name ) {
	var ret = jQuery.cssProps[ name ];
	if ( !ret ) {
		ret = jQuery.cssProps[ name ] = vendorPropName( name ) || name;
	}
	return ret;
}

function setPositiveNumber( elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function boxModelAdjustment( elem, dimension, box, isBorderBox, styles, computedVal ) {
	var i = dimension === "width" ? 1 : 0,
		extra = 0,
		delta = 0;

	// Adjustment may not be necessary
	if ( box === ( isBorderBox ? "border" : "content" ) ) {
		return 0;
	}

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin
		if ( box === "margin" ) {
			delta += jQuery.css( elem, box + cssExpand[ i ], true, styles );
		}

		// If we get here with a content-box, we're seeking "padding" or "border" or "margin"
		if ( !isBorderBox ) {

			// Add padding
			delta += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// For "border" or "margin", add border
			if ( box !== "padding" ) {
				delta += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );

			// But still keep track of it otherwise
			} else {
				extra += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}

		// If we get here with a border-box (content + padding + border), we're seeking "content" or
		// "padding" or "margin"
		} else {

			// For "content", subtract padding
			if ( box === "content" ) {
				delta -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// For "content" or "padding", subtract border
			if ( box !== "margin" ) {
				delta -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	// Account for positive content-box scroll gutter when requested by providing computedVal
	if ( !isBorderBox && computedVal >= 0 ) {

		// offsetWidth/offsetHeight is a rounded sum of content, padding, scroll gutter, and border
		// Assuming integer scroll gutter, subtract the rest and round down
		delta += Math.max( 0, Math.ceil(
			elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
			computedVal -
			delta -
			extra -
			0.5
		) );
	}

	return delta;
}

function getWidthOrHeight( elem, dimension, extra ) {

	// Start with computed style
	var styles = getStyles( elem ),
		val = curCSS( elem, dimension, styles ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
		valueIsBorderBox = isBorderBox;

	// Support: Firefox <=54
	// Return a confounding non-pixel value or feign ignorance, as appropriate.
	if ( rnumnonpx.test( val ) ) {
		if ( !extra ) {
			return val;
		}
		val = "auto";
	}

	// Check for style in case a browser which returns unreliable values
	// for getComputedStyle silently falls back to the reliable elem.style
	valueIsBorderBox = valueIsBorderBox &&
		( support.boxSizingReliable() || val === elem.style[ dimension ] );

	// Fall back to offsetWidth/offsetHeight when value is "auto"
	// This happens for inline elements with no explicit setting (gh-3571)
	// Support: Android <=4.1 - 4.3 only
	// Also use offsetWidth/offsetHeight for misreported inline dimensions (gh-3602)
	if ( val === "auto" ||
		!parseFloat( val ) && jQuery.css( elem, "display", false, styles ) === "inline" ) {

		val = elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ];

		// offsetWidth/offsetHeight provide border-box values
		valueIsBorderBox = true;
	}

	// Normalize "" and auto
	val = parseFloat( val ) || 0;

	// Adjust for the element's box model
	return ( val +
		boxModelAdjustment(
			elem,
			dimension,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles,

			// Provide the current computed size to request scroll gutter calculation (gh-3589)
			val
		)
	) + "px";
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name ),
			style = elem.style;

		// Make sure that we're working with the right name. We don't
		// want to query the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			if ( type === "number" ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				if ( isCustomProp ) {
					style.setProperty( name, value );
				} else {
					style[ name ] = value;
				}
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name );

		// Make sure that we're working with the right name. We don't
		// want to modify the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}

		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( i, dimension ) {
	jQuery.cssHooks[ dimension ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&

					// Support: Safari 8+
					// Table columns in Safari have non-zero offsetWidth & zero
					// getBoundingClientRect().width unless display is changed.
					// Support: IE <=11 only
					// Running getBoundingClientRect on a disconnected node
					// in IE throws an error.
					( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, dimension, extra );
						} ) :
						getWidthOrHeight( elem, dimension, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = getStyles( elem ),
				isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
				subtract = extra && boxModelAdjustment(
					elem,
					dimension,
					extra,
					isBorderBox,
					styles
				);

			// Account for unreliable border-box dimensions by comparing offset* to computed and
			// faking a content-box to get border and padding (gh-3699)
			if ( isBorderBox && support.scrollboxSize() === styles.position ) {
				subtract -= Math.ceil(
					elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
					parseFloat( styles[ dimension ] ) -
					boxModelAdjustment( elem, dimension, "border", false, styles ) -
					0.5
				);
			}

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ dimension ] = value;
				value = jQuery.css( elem, dimension );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( prefix !== "margin" ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( Array.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 &&
				( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
					jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9 only
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, inProgress,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

function schedule() {
	if ( inProgress ) {
		if ( document.hidden === false && window.requestAnimationFrame ) {
			window.requestAnimationFrame( schedule );
		} else {
			window.setTimeout( schedule, jQuery.fx.interval );
		}

		jQuery.fx.tick();
	}
}

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = Date.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
		isBox = "width" in props || "height" in props,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHiddenWithinTree( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Queue-skipping animations hijack the fx hooks
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Detect show/hide animations
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.test( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// Pretend to be hidden if this is a "show" and
				// there is still data from a stopped show/hide
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;

				// Ignore all other no-op show/hide data
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	// Bail out if this is a no-op like .hide().hide()
	propTween = !jQuery.isEmptyObject( props );
	if ( !propTween && jQuery.isEmptyObject( orig ) ) {
		return;
	}

	// Restrict "overflow" and "display" styles during box animations
	if ( isBox && elem.nodeType === 1 ) {

		// Support: IE <=9 - 11, Edge 12 - 15
		// Record all 3 overflow attributes because IE does not infer the shorthand
		// from identically-valued overflowX and overflowY and Edge just mirrors
		// the overflowX value there.
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Identify a display type, preferring old show/hide data over the CSS cascade
		restoreDisplay = dataShow && dataShow.display;
		if ( restoreDisplay == null ) {
			restoreDisplay = dataPriv.get( elem, "display" );
		}
		display = jQuery.css( elem, "display" );
		if ( display === "none" ) {
			if ( restoreDisplay ) {
				display = restoreDisplay;
			} else {

				// Get nonempty value(s) by temporarily forcing visibility
				showHide( [ elem ], true );
				restoreDisplay = elem.style.display || restoreDisplay;
				display = jQuery.css( elem, "display" );
				showHide( [ elem ] );
			}
		}

		// Animate inline elements as inline-block
		if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
			if ( jQuery.css( elem, "float" ) === "none" ) {

				// Restore the original display value at the end of pure show/hide animations
				if ( !propTween ) {
					anim.done( function() {
						style.display = restoreDisplay;
					} );
					if ( restoreDisplay == null ) {
						display = style.display;
						restoreDisplay = display === "none" ? "" : display;
					}
				}
				style.display = "inline-block";
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// Implement show/hide animations
	propTween = false;
	for ( prop in orig ) {

		// General show/hide setup for this element animation
		if ( !propTween ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
			}

			// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}

			// Show elements before animating them
			if ( hidden ) {
				showHide( [ elem ], true );
			}

			/* eslint-disable no-loop-func */

			anim.done( function() {

			/* eslint-enable no-loop-func */

				// The final step of a "hide" animation is actually hiding the element
				if ( !hidden ) {
					showHide( [ elem ] );
				}
				dataPriv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			} );
		}

		// Per-property setup
		propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
		if ( !( prop in dataShow ) ) {
			dataShow[ prop ] = propTween.start;
			if ( hidden ) {
				propTween.end = propTween.start;
				propTween.start = 0;
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( Array.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3 only
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			// If there's more to do, yield
			if ( percent < 1 && length ) {
				return remaining;
			}

			// If this was an empty animation, synthesize a final progress notification
			if ( !length ) {
				deferred.notifyWith( elem, [ animation, 1, 0 ] );
			}

			// Resolve the animation and report its conclusion
			deferred.resolveWith( elem, [ animation ] );
			return false;
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					result.stop.bind( result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	// Attach callbacks from options
	animation
		.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	return animation;
}

jQuery.Animation = jQuery.extend( Animation, {

	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnothtmlwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !isFunction( easing ) && easing
	};

	// Go to the end state if fx are off
	if ( jQuery.fx.off ) {
		opt.duration = 0;

	} else {
		if ( typeof opt.duration !== "number" ) {
			if ( opt.duration in jQuery.fx.speeds ) {
				opt.duration = jQuery.fx.speeds[ opt.duration ];

			} else {
				opt.duration = jQuery.fx.speeds._default;
			}
		}
	}

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = Date.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Run the timer and safely remove it when done (allowing for external removal)
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	jQuery.fx.start();
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( inProgress ) {
		return;
	}

	inProgress = true;
	schedule();
};

jQuery.fx.stop = function() {
	inProgress = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: Android <=4.3 only
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE <=11 only
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: IE <=11 only
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// Attribute hooks are determined by the lowercase version
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name,
			i = 0,

			// Attribute names can contain non-HTML whitespace characters
			// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
			attrNames = value && value.match( rnothtmlwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};

jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle,
			lowercaseName = name.toLowerCase();

		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ lowercaseName ];
			attrHandle[ lowercaseName ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				lowercaseName :
				null;
			attrHandle[ lowercaseName ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// Support: IE <=9 - 11 only
				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				if ( tabindex ) {
					return parseInt( tabindex, 10 );
				}

				if (
					rfocusable.test( elem.nodeName ) ||
					rclickable.test( elem.nodeName ) &&
					elem.href
				) {
					return 0;
				}

				return -1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
// eslint rule "no-unused-expressions" is disabled for this code
// since it considers such accessions noop
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




	// Strip and collapse whitespace according to HTML spec
	// https://infra.spec.whatwg.org/#strip-and-collapse-ascii-whitespace
	function stripAndCollapse( value ) {
		var tokens = value.match( rnothtmlwhite ) || [];
		return tokens.join( " " );
	}


function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

function classesToArray( value ) {
	if ( Array.isArray( value ) ) {
		return value;
	}
	if ( typeof value === "string" ) {
		return value.match( rnothtmlwhite ) || [];
	}
	return [];
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value,
			isValidValue = type === "string" || Array.isArray( value );

		if ( typeof stateVal === "boolean" && isValidValue ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( isValidValue ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = classesToArray( value );

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
					return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, valueIsFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				// Handle most common string cases
				if ( typeof ret === "string" ) {
					return ret.replace( rreturn, "" );
				}

				// Handle cases where value is null/undef or number
				return ret == null ? "" : ret;
			}

			return;
		}

		valueIsFunction = isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( valueIsFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( Array.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE <=10 - 11 only
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					stripAndCollapse( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option, i,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one",
					values = one ? null : [],
					max = one ? index + 1 : options.length;

				if ( index < 0 ) {
					i = max;

				} else {
					i = one ? index : 0;
				}

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// Support: IE <=9 only
					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							!option.disabled &&
							( !option.parentNode.disabled ||
								!nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					/* eslint-disable no-cond-assign */

					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}

					/* eslint-enable no-cond-assign */
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( Array.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


support.focusin = "onfocusin" in window;


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	stopPropagationCallback = function( e ) {
		e.stopPropagation();
	};

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special, lastElement,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = lastElement = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {
			lastElement = cur;
			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && isFunction( elem[ type ] ) && !isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;

					if ( event.isPropagationStopped() ) {
						lastElement.addEventListener( type, stopPropagationCallback );
					}

					elem[ type ]();

					if ( event.isPropagationStopped() ) {
						lastElement.removeEventListener( type, stopPropagationCallback );
					}

					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


// Support: Firefox <=44
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = Date.now();

var rquery = ( /\?/ );



// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE 9 - 11 only
	// IE throws on parseFromString with invalid input.
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( Array.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && toType( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, valueOrFunction ) {

			// If value is a function, invoke it and use its return value
			var value = isFunction( valueOrFunction ) ?
				valueOrFunction() :
				valueOrFunction;

			s[ s.length ] = encodeURIComponent( key ) + "=" +
				encodeURIComponent( value == null ? "" : value );
		};

	// If an array was passed in, assume that it is an array of form elements.
	if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( i, elem ) {
			var val = jQuery( this ).val();

			if ( val == null ) {
				return null;
			}

			if ( Array.isArray( val ) ) {
				return jQuery.map( val, function( val ) {
					return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
				} );
			}

			return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


var
	r20 = /%20/g,
	rhash = /#.*$/,
	rantiCache = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];

		if ( isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",

		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": JSON.parse,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// Request state (becomes false upon send and true upon completion)
			completed,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// uncached part of the url
			uncached,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( completed ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return completed ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					if ( completed == null ) {
						name = requestHeadersNames[ name.toLowerCase() ] =
							requestHeadersNames[ name.toLowerCase() ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( completed == null ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( completed ) {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						} else {

							// Lazy-add the new callbacks in a way that preserves old ones
							for ( code in map ) {
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR );

		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE <=8 - 11, Edge 12 - 15
			// IE throws exception on accessing the href property if url is malformed,
			// e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE <=8 - 11 only
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( completed ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		// Remove hash to simplify url manipulation
		cacheURL = s.url.replace( rhash, "" );

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// Remember the hash so we can put it back
			uncached = s.url.slice( cacheURL.length );

			// If data is available and should be processed, append data to url
			if ( s.data && ( s.processData || typeof s.data === "string" ) ) {
				cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add or update anti-cache param if needed
			if ( s.cache === false ) {
				cacheURL = cacheURL.replace( rantiCache, "$1" );
				uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce++ ) + uncached;
			}

			// Put hash and anti-cache on the URL that will be requested (gh-1732)
			s.url = cacheURL + uncached;

		// Change '%20' to '+' if this is encoded form body content (gh-2658)
		} else if ( s.data && s.processData &&
			( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
			s.data = s.data.replace( r20, "+" );
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		completeDeferred.add( s.complete );
		jqXHR.done( s.success );
		jqXHR.fail( s.error );

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( completed ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				completed = false;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Rethrow post-completion exceptions
				if ( completed ) {
					throw e;
				}

				// Propagate others as results
				done( -1, e );
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Ignore repeat invocations
			if ( completed ) {
				return;
			}

			completed = true;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );


jQuery._evalUrl = function( url ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		cache: true,
		async: false,
		global: false,
		"throws": true
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( this[ 0 ] ) {
			if ( isFunction( html ) ) {
				html = html.call( this[ 0 ] );
			}

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var htmlIsFunction = isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( htmlIsFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function( selector ) {
		this.parent( selector ).not( "body" ).each( function() {
			jQuery( this ).replaceWith( this.childNodes );
		} );
		return this;
	}
} );


jQuery.expr.pseudos.hidden = function( elem ) {
	return !jQuery.expr.pseudos.visible( elem );
};
jQuery.expr.pseudos.visible = function( elem ) {
	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE <=9 only
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.ontimeout =
									xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE <=9 only
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE <=9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = xhr.ontimeout = callback( "error" );

				// Support: IE 9 only
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
jQuery.ajaxPrefilter( function( s ) {
	if ( s.crossDomain ) {
		s.contents.script = false;
	}
} );

// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" ).prop( {
					charset: s.scriptCharset,
					src: s.url
				} ).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Support: Safari 8 only
// In Safari 8 documents created via document.implementation.createHTMLDocument
// collapse sibling forms: the second one becomes a child of the first one.
// Because of that, this security measure has to be disabled in Safari 8.
// https://bugs.webkit.org/show_bug.cgi?id=137337
support.createHTMLDocument = ( function() {
	var body = document.implementation.createHTMLDocument( "" ).body;
	body.innerHTML = "<form></form><form></form>";
	return body.childNodes.length === 2;
} )();


// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( typeof data !== "string" ) {
		return [];
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}

	var base, parsed, scripts;

	if ( !context ) {

		// Stop scripts or inline event handlers from being executed immediately
		// by using document.implementation
		if ( support.createHTMLDocument ) {
			context = document.implementation.createHTMLDocument( "" );

			// Set the base href for the created document
			// so any parsed elements with URLs
			// are based on the document's URL (gh-2965)
			base = context.createElement( "base" );
			base.href = document.location.href;
			context.head.appendChild( base );
		} else {
			context = document;
		}
	}

	parsed = rsingleTag.exec( data );
	scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = stripAndCollapse( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.expr.pseudos.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {

	// offset() relates an element's border box to the document origin
	offset: function( options ) {

		// Preserve chaining for setter
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var rect, win,
			elem = this[ 0 ];

		if ( !elem ) {
			return;
		}

		// Return zeros for disconnected and hidden (display: none) elements (gh-2310)
		// Support: IE <=11 only
		// Running getBoundingClientRect on a
		// disconnected node in IE throws an error
		if ( !elem.getClientRects().length ) {
			return { top: 0, left: 0 };
		}

		// Get document-relative position by adding viewport scroll to viewport-relative gBCR
		rect = elem.getBoundingClientRect();
		win = elem.ownerDocument.defaultView;
		return {
			top: rect.top + win.pageYOffset,
			left: rect.left + win.pageXOffset
		};
	},

	// position() relates an element's margin box to its offset parent's padding box
	// This corresponds to the behavior of CSS absolute positioning
	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset, doc,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// position:fixed elements are offset from the viewport, which itself always has zero offset
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume position:fixed implies availability of getBoundingClientRect
			offset = elem.getBoundingClientRect();

		} else {
			offset = this.offset();

			// Account for the *real* offset parent, which can be the document or its root element
			// when a statically positioned element is identified
			doc = elem.ownerDocument;
			offsetParent = elem.offsetParent || doc.documentElement;
			while ( offsetParent &&
				( offsetParent === doc.body || offsetParent === doc.documentElement ) &&
				jQuery.css( offsetParent, "position" ) === "static" ) {

				offsetParent = offsetParent.parentNode;
			}
			if ( offsetParent && offsetParent !== elem && offsetParent.nodeType === 1 ) {

				// Incorporate borders into its offset, since they are outside its content origin
				parentOffset = jQuery( offsetParent ).offset();
				parentOffset.top += jQuery.css( offsetParent, "borderTopWidth", true );
				parentOffset.left += jQuery.css( offsetParent, "borderLeftWidth", true );
			}
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {

			// Coalesce documents and windows
			var win;
			if ( isWindow( elem ) ) {
				win = elem;
			} else if ( elem.nodeType === 9 ) {
				win = elem.defaultView;
			}

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari <=7 - 9.1, Chrome <=37 - 49
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( isWindow( elem ) ) {

					// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
					return funcName.indexOf( "outer" ) === 0 ?
						elem[ "inner" + name ] :
						elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable );
		};
	} );
} );


jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup contextmenu" ).split( " " ),
	function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
} );

jQuery.fn.extend( {
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );




jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	}
} );

// Bind a function to a context, optionally partially applying any
// arguments.
// jQuery.proxy is deprecated to promote standards (specifically Function#bind)
// However, it is not slated for removal any time soon
jQuery.proxy = function( fn, context ) {
	var tmp, args, proxy;

	if ( typeof context === "string" ) {
		tmp = fn[ context ];
		context = fn;
		fn = tmp;
	}

	// Quick check to determine if target is callable, in the spec
	// this throws a TypeError, but we will just return undefined.
	if ( !isFunction( fn ) ) {
		return undefined;
	}

	// Simulated bind
	args = slice.call( arguments, 2 );
	proxy = function() {
		return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
	};

	// Set the guid of unique handler to the same of original handler, so it can be removed
	proxy.guid = fn.guid = fn.guid || jQuery.guid++;

	return proxy;
};

jQuery.holdReady = function( hold ) {
	if ( hold ) {
		jQuery.readyWait++;
	} else {
		jQuery.ready( true );
	}
};
jQuery.isArray = Array.isArray;
jQuery.parseJSON = JSON.parse;
jQuery.nodeName = nodeName;
jQuery.isFunction = isFunction;
jQuery.isWindow = isWindow;
jQuery.camelCase = camelCase;
jQuery.type = toType;

jQuery.now = Date.now;

jQuery.isNumeric = function( obj ) {

	// As of jQuery 3.0, isNumeric is limited to
	// strings and numbers (primitives or objects)
	// that can be coerced to finite numbers (gh-2662)
	var type = jQuery.type( obj );
	return ( type === "number" || type === "string" ) &&

		// parseFloat NaNs numeric-cast false positives ("")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		!isNaN( obj - parseFloat( obj ) );
};




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( true ) {
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
		return jQuery;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
}




var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;
} );


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {/*! jQuery UI - v1.9.2 - 2014-03-21
* http://jqueryui.com
* Includes: jquery.ui.effect.js
* Copyright 2014 jQuery Foundation and other contributors; Licensed MIT */

jQuery.effects || function (e, t) {
  var i = e.uiBackCompat !== !1,
      a = "ui-effects-";e.effects = { effect: {} }, function (t, i) {
    function a(e, t, i) {
      var a = c[t.type] || {};return null == e ? i || !t.def ? null : t.def : (e = a.floor ? ~~e : parseFloat(e), isNaN(e) ? t.def : a.mod ? (e + a.mod) % a.mod : 0 > e ? 0 : e > a.max ? a.max : e);
    }function s(e) {
      var a = u(),
          s = a._rgba = [];return e = e.toLowerCase(), m(l, function (t, n) {
        var r,
            o = n.re.exec(e),
            h = o && n.parse(o),
            l = n.space || "rgba";return h ? (r = a[l](h), a[d[l].cache] = r[d[l].cache], s = a._rgba = r._rgba, !1) : i;
      }), s.length ? ("0,0,0,0" === s.join() && t.extend(s, r.transparent), a) : r[e];
    }function n(e, t, i) {
      return i = (i + 1) % 1, 1 > 6 * i ? e + 6 * (t - e) * i : 1 > 2 * i ? t : 2 > 3 * i ? e + 6 * (t - e) * (2 / 3 - i) : e;
    }var r,
        o = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor".split(" "),
        h = /^([\-+])=\s*(\d+\.?\d*)/,
        l = [{ re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/, parse: function parse(e) {
        return [e[1], e[2], e[3], e[4]];
      } }, { re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/, parse: function parse(e) {
        return [2.55 * e[1], 2.55 * e[2], 2.55 * e[3], e[4]];
      } }, { re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/, parse: function parse(e) {
        return [parseInt(e[1], 16), parseInt(e[2], 16), parseInt(e[3], 16)];
      } }, { re: /#([a-f0-9])([a-f0-9])([a-f0-9])/, parse: function parse(e) {
        return [parseInt(e[1] + e[1], 16), parseInt(e[2] + e[2], 16), parseInt(e[3] + e[3], 16)];
      } }, { re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/, space: "hsla", parse: function parse(e) {
        return [e[1], e[2] / 100, e[3] / 100, e[4]];
      } }],
        u = t.Color = function (e, i, a, s) {
      return new t.Color.fn.parse(e, i, a, s);
    },
        d = { rgba: { props: { red: { idx: 0, type: "byte" }, green: { idx: 1, type: "byte" }, blue: { idx: 2, type: "byte" } } }, hsla: { props: { hue: { idx: 0, type: "degrees" }, saturation: { idx: 1, type: "percent" }, lightness: { idx: 2, type: "percent" } } } },
        c = { "byte": { floor: !0, max: 255 }, percent: { max: 1 }, degrees: { mod: 360, floor: !0 } },
        p = u.support = {},
        f = t("<p>")[0],
        m = t.each;f.style.cssText = "background-color:rgba(1,1,1,.5)", p.rgba = f.style.backgroundColor.indexOf("rgba") > -1, m(d, function (e, t) {
      t.cache = "_" + e, t.props.alpha = { idx: 3, type: "percent", def: 1 };
    }), u.fn = t.extend(u.prototype, { parse: function parse(n, o, h, l) {
        if (n === i) return this._rgba = [null, null, null, null], this;(n.jquery || n.nodeType) && (n = t(n).css(o), o = i);var c = this,
            p = t.type(n),
            f = this._rgba = [];return o !== i && (n = [n, o, h, l], p = "array"), "string" === p ? this.parse(s(n) || r._default) : "array" === p ? (m(d.rgba.props, function (e, t) {
          f[t.idx] = a(n[t.idx], t);
        }), this) : "object" === p ? (n instanceof u ? m(d, function (e, t) {
          n[t.cache] && (c[t.cache] = n[t.cache].slice());
        }) : m(d, function (t, i) {
          var s = i.cache;m(i.props, function (e, t) {
            if (!c[s] && i.to) {
              if ("alpha" === e || null == n[e]) return;c[s] = i.to(c._rgba);
            }c[s][t.idx] = a(n[e], t, !0);
          }), c[s] && 0 > e.inArray(null, c[s].slice(0, 3)) && (c[s][3] = 1, i.from && (c._rgba = i.from(c[s])));
        }), this) : i;
      }, is: function is(e) {
        var t = u(e),
            a = !0,
            s = this;return m(d, function (e, n) {
          var r,
              o = t[n.cache];return o && (r = s[n.cache] || n.to && n.to(s._rgba) || [], m(n.props, function (e, t) {
            return null != o[t.idx] ? a = o[t.idx] === r[t.idx] : i;
          })), a;
        }), a;
      }, _space: function _space() {
        var e = [],
            t = this;return m(d, function (i, a) {
          t[a.cache] && e.push(i);
        }), e.pop();
      }, transition: function transition(e, t) {
        var i = u(e),
            s = i._space(),
            n = d[s],
            r = 0 === this.alpha() ? u("transparent") : this,
            o = r[n.cache] || n.to(r._rgba),
            h = o.slice();return i = i[n.cache], m(n.props, function (e, s) {
          var n = s.idx,
              r = o[n],
              l = i[n],
              u = c[s.type] || {};null !== l && (null === r ? h[n] = l : (u.mod && (l - r > u.mod / 2 ? r += u.mod : r - l > u.mod / 2 && (r -= u.mod)), h[n] = a((l - r) * t + r, s)));
        }), this[s](h);
      }, blend: function blend(e) {
        if (1 === this._rgba[3]) return this;var i = this._rgba.slice(),
            a = i.pop(),
            s = u(e)._rgba;return u(t.map(i, function (e, t) {
          return (1 - a) * s[t] + a * e;
        }));
      }, toRgbaString: function toRgbaString() {
        var e = "rgba(",
            i = t.map(this._rgba, function (e, t) {
          return null == e ? t > 2 ? 1 : 0 : e;
        });return 1 === i[3] && (i.pop(), e = "rgb("), e + i.join() + ")";
      }, toHslaString: function toHslaString() {
        var e = "hsla(",
            i = t.map(this.hsla(), function (e, t) {
          return null == e && (e = t > 2 ? 1 : 0), t && 3 > t && (e = Math.round(100 * e) + "%"), e;
        });return 1 === i[3] && (i.pop(), e = "hsl("), e + i.join() + ")";
      }, toHexString: function toHexString(e) {
        var i = this._rgba.slice(),
            a = i.pop();return e && i.push(~~(255 * a)), "#" + t.map(i, function (e) {
          return e = (e || 0).toString(16), 1 === e.length ? "0" + e : e;
        }).join("");
      }, toString: function toString() {
        return 0 === this._rgba[3] ? "transparent" : this.toRgbaString();
      } }), u.fn.parse.prototype = u.fn, d.hsla.to = function (e) {
      if (null == e[0] || null == e[1] || null == e[2]) return [null, null, null, e[3]];var t,
          i,
          a = e[0] / 255,
          s = e[1] / 255,
          n = e[2] / 255,
          r = e[3],
          o = Math.max(a, s, n),
          h = Math.min(a, s, n),
          l = o - h,
          u = o + h,
          d = .5 * u;return t = h === o ? 0 : a === o ? 60 * (s - n) / l + 360 : s === o ? 60 * (n - a) / l + 120 : 60 * (a - s) / l + 240, i = 0 === d || 1 === d ? d : .5 >= d ? l / u : l / (2 - u), [Math.round(t) % 360, i, d, null == r ? 1 : r];
    }, d.hsla.from = function (e) {
      if (null == e[0] || null == e[1] || null == e[2]) return [null, null, null, e[3]];var t = e[0] / 360,
          i = e[1],
          a = e[2],
          s = e[3],
          r = .5 >= a ? a * (1 + i) : a + i - a * i,
          o = 2 * a - r;return [Math.round(255 * n(o, r, t + 1 / 3)), Math.round(255 * n(o, r, t)), Math.round(255 * n(o, r, t - 1 / 3)), s];
    }, m(d, function (e, s) {
      var n = s.props,
          r = s.cache,
          o = s.to,
          l = s.from;u.fn[e] = function (e) {
        if (o && !this[r] && (this[r] = o(this._rgba)), e === i) return this[r].slice();var s,
            h = t.type(e),
            d = "array" === h || "object" === h ? e : arguments,
            c = this[r].slice();return m(n, function (e, t) {
          var i = d["object" === h ? e : t.idx];null == i && (i = c[t.idx]), c[t.idx] = a(i, t);
        }), l ? (s = u(l(c)), s[r] = c, s) : u(c);
      }, m(n, function (i, a) {
        u.fn[i] || (u.fn[i] = function (s) {
          var n,
              r = t.type(s),
              o = "alpha" === i ? this._hsla ? "hsla" : "rgba" : e,
              l = this[o](),
              u = l[a.idx];return "undefined" === r ? u : ("function" === r && (s = s.call(this, u), r = t.type(s)), null == s && a.empty ? this : ("string" === r && (n = h.exec(s), n && (s = u + parseFloat(n[2]) * ("+" === n[1] ? 1 : -1))), l[a.idx] = s, this[o](l)));
        });
      });
    }), m(o, function (e, i) {
      t.cssHooks[i] = { set: function set(e, a) {
          var n,
              r,
              o = "";if ("string" !== t.type(a) || (n = s(a))) {
            if (a = u(n || a), !p.rgba && 1 !== a._rgba[3]) {
              for (r = "backgroundColor" === i ? e.parentNode : e; ("" === o || "transparent" === o) && r && r.style;) {
                try {
                  o = t.css(r, "backgroundColor"), r = r.parentNode;
                } catch (h) {}
              }a = a.blend(o && "transparent" !== o ? o : "_default");
            }a = a.toRgbaString();
          }try {
            e.style[i] = a;
          } catch (l) {}
        } }, t.fx.step[i] = function (e) {
        e.colorInit || (e.start = u(e.elem, i), e.end = u(e.end), e.colorInit = !0), t.cssHooks[i].set(e.elem, e.start.transition(e.end, e.pos));
      };
    }), t.cssHooks.borderColor = { expand: function expand(e) {
        var t = {};return m(["Top", "Right", "Bottom", "Left"], function (i, a) {
          t["border" + a + "Color"] = e;
        }), t;
      } }, r = t.Color.names = { aqua: "#00ffff", black: "#000000", blue: "#0000ff", fuchsia: "#ff00ff", gray: "#808080", green: "#008000", lime: "#00ff00", maroon: "#800000", navy: "#000080", olive: "#808000", purple: "#800080", red: "#ff0000", silver: "#c0c0c0", teal: "#008080", white: "#ffffff", yellow: "#ffff00", transparent: [null, null, null, 0], _default: "#ffffff" };
  }(jQuery), function () {
    function i() {
      var t,
          i,
          a = this.ownerDocument.defaultView ? this.ownerDocument.defaultView.getComputedStyle(this, null) : this.currentStyle,
          s = {};if (a && a.length && a[0] && a[a[0]]) for (i = a.length; i--;) {
        t = a[i], "string" == typeof a[t] && (s[e.camelCase(t)] = a[t]);
      } else for (t in a) {
        "string" == typeof a[t] && (s[t] = a[t]);
      }return s;
    }function a(t, i) {
      var a,
          s,
          r = {};for (a in i) {
        s = i[a], t[a] !== s && (n[a] || (e.fx.step[a] || !isNaN(parseFloat(s))) && (r[a] = s));
      }return r;
    }var s = ["add", "remove", "toggle"],
        n = { border: 1, borderBottom: 1, borderColor: 1, borderLeft: 1, borderRight: 1, borderTop: 1, borderWidth: 1, margin: 1, padding: 1 };e.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function (t, i) {
      e.fx.step[i] = function (e) {
        ("none" !== e.end && !e.setAttr || 1 === e.pos && !e.setAttr) && (jQuery.style(e.elem, i, e.end), e.setAttr = !0);
      };
    }), e.effects.animateClass = function (t, n, r, o) {
      var h = e.speed(n, r, o);return this.queue(function () {
        var n,
            r = e(this),
            o = r.attr("class") || "",
            l = h.children ? r.find("*").andSelf() : r;l = l.map(function () {
          var t = e(this);return { el: t, start: i.call(this) };
        }), n = function n() {
          e.each(s, function (e, i) {
            t[i] && r[i + "Class"](t[i]);
          });
        }, n(), l = l.map(function () {
          return this.end = i.call(this.el[0]), this.diff = a(this.start, this.end), this;
        }), r.attr("class", o), l = l.map(function () {
          var t = this,
              i = e.Deferred(),
              a = jQuery.extend({}, h, { queue: !1, complete: function complete() {
              i.resolve(t);
            } });return this.el.animate(this.diff, a), i.promise();
        }), e.when.apply(e, l.get()).done(function () {
          n(), e.each(arguments, function () {
            var t = this.el;e.each(this.diff, function (e) {
              t.css(e, "");
            });
          }), h.complete.call(r[0]);
        });
      });
    }, e.fn.extend({ _addClass: e.fn.addClass, addClass: function addClass(t, i, a, s) {
        return i ? e.effects.animateClass.call(this, { add: t }, i, a, s) : this._addClass(t);
      }, _removeClass: e.fn.removeClass, removeClass: function removeClass(t, i, a, s) {
        return i ? e.effects.animateClass.call(this, { remove: t }, i, a, s) : this._removeClass(t);
      }, _toggleClass: e.fn.toggleClass, toggleClass: function toggleClass(i, a, s, n, r) {
        return "boolean" == typeof a || a === t ? s ? e.effects.animateClass.call(this, a ? { add: i } : { remove: i }, s, n, r) : this._toggleClass(i, a) : e.effects.animateClass.call(this, { toggle: i }, a, s, n);
      }, switchClass: function switchClass(t, i, a, s, n) {
        return e.effects.animateClass.call(this, { add: i, remove: t }, a, s, n);
      } });
  }(), function () {
    function s(t, i, a, s) {
      return e.isPlainObject(t) && (i = t, t = t.effect), t = { effect: t }, null == i && (i = {}), e.isFunction(i) && (s = i, a = null, i = {}), ("number" == typeof i || e.fx.speeds[i]) && (s = a, a = i, i = {}), e.isFunction(a) && (s = a, a = null), i && e.extend(t, i), a = a || i.duration, t.duration = e.fx.off ? 0 : "number" == typeof a ? a : a in e.fx.speeds ? e.fx.speeds[a] : e.fx.speeds._default, t.complete = s || i.complete, t;
    }function n(t) {
      return !t || "number" == typeof t || e.fx.speeds[t] ? !0 : "string" != typeof t || e.effects.effect[t] ? !1 : i && e.effects[t] ? !1 : !0;
    }e.extend(e.effects, { version: "1.9.2", save: function save(e, t) {
        for (var i = 0; t.length > i; i++) {
          null !== t[i] && e.data(a + t[i], e[0].style[t[i]]);
        }
      }, restore: function restore(e, i) {
        var s, n;for (n = 0; i.length > n; n++) {
          null !== i[n] && (s = e.data(a + i[n]), s === t && (s = ""), e.css(i[n], s));
        }
      }, setMode: function setMode(e, t) {
        return "toggle" === t && (t = e.is(":hidden") ? "show" : "hide"), t;
      }, getBaseline: function getBaseline(e, t) {
        var i, a;switch (e[0]) {case "top":
            i = 0;break;case "middle":
            i = .5;break;case "bottom":
            i = 1;break;default:
            i = e[0] / t.height;}switch (e[1]) {case "left":
            a = 0;break;case "center":
            a = .5;break;case "right":
            a = 1;break;default:
            a = e[1] / t.width;}return { x: a, y: i };
      }, createWrapper: function createWrapper(t) {
        if (t.parent().is(".ui-effects-wrapper")) return t.parent();var i = { width: t.outerWidth(!0), height: t.outerHeight(!0), "float": t.css("float") },
            a = e("<div></div>").addClass("ui-effects-wrapper").css({ fontSize: "100%", background: "transparent", border: "none", margin: 0, padding: 0 }),
            s = { width: t.width(), height: t.height() },
            n = document.activeElement;try {
          n.id;
        } catch (r) {
          n = document.body;
        }return t.wrap(a), (t[0] === n || e.contains(t[0], n)) && e(n).focus(), a = t.parent(), "static" === t.css("position") ? (a.css({ position: "relative" }), t.css({ position: "relative" })) : (e.extend(i, { position: t.css("position"), zIndex: t.css("z-index") }), e.each(["top", "left", "bottom", "right"], function (e, a) {
          i[a] = t.css(a), isNaN(parseInt(i[a], 10)) && (i[a] = "auto");
        }), t.css({ position: "relative", top: 0, left: 0, right: "auto", bottom: "auto" })), t.css(s), a.css(i).show();
      }, removeWrapper: function removeWrapper(t) {
        var i = document.activeElement;return t.parent().is(".ui-effects-wrapper") && (t.parent().replaceWith(t), (t[0] === i || e.contains(t[0], i)) && e(i).focus()), t;
      }, setTransition: function setTransition(t, i, a, s) {
        return s = s || {}, e.each(i, function (e, i) {
          var n = t.cssUnit(i);n[0] > 0 && (s[i] = n[0] * a + n[1]);
        }), s;
      } }), e.fn.extend({ effect: function effect() {
        function t(t) {
          function i() {
            e.isFunction(n) && n.call(s[0]), e.isFunction(t) && t();
          }var s = e(this),
              n = a.complete,
              r = a.mode;(s.is(":hidden") ? "hide" === r : "show" === r) ? i() : o.call(s[0], a, i);
        }var a = s.apply(this, arguments),
            n = a.mode,
            r = a.queue,
            o = e.effects.effect[a.effect],
            h = !o && i && e.effects[a.effect];return e.fx.off || !o && !h ? n ? this[n](a.duration, a.complete) : this.each(function () {
          a.complete && a.complete.call(this);
        }) : o ? r === !1 ? this.each(t) : this.queue(r || "fx", t) : h.call(this, { options: a, duration: a.duration, callback: a.complete, mode: a.mode });
      }, _show: e.fn.show, show: function show(e) {
        if (n(e)) return this._show.apply(this, arguments);var t = s.apply(this, arguments);return t.mode = "show", this.effect.call(this, t);
      }, _hide: e.fn.hide, hide: function hide(e) {
        if (n(e)) return this._hide.apply(this, arguments);var t = s.apply(this, arguments);return t.mode = "hide", this.effect.call(this, t);
      }, __toggle: e.fn.toggle, toggle: function toggle(t) {
        if (n(t) || "boolean" == typeof t || e.isFunction(t)) return this.__toggle.apply(this, arguments);var i = s.apply(this, arguments);return i.mode = "toggle", this.effect.call(this, i);
      }, cssUnit: function cssUnit(t) {
        var i = this.css(t),
            a = [];return e.each(["em", "px", "%", "pt"], function (e, t) {
          i.indexOf(t) > 0 && (a = [parseFloat(i), t]);
        }), a;
      } });
  }(), function () {
    var t = {};e.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function (e, i) {
      t[i] = function (t) {
        return Math.pow(t, e + 2);
      };
    }), e.extend(t, { Sine: function Sine(e) {
        return 1 - Math.cos(e * Math.PI / 2);
      }, Circ: function Circ(e) {
        return 1 - Math.sqrt(1 - e * e);
      }, Elastic: function Elastic(e) {
        return 0 === e || 1 === e ? e : -Math.pow(2, 8 * (e - 1)) * Math.sin((80 * (e - 1) - 7.5) * Math.PI / 15);
      }, Back: function Back(e) {
        return e * e * (3 * e - 2);
      }, Bounce: function Bounce(e) {
        for (var t, i = 4; ((t = Math.pow(2, --i)) - 1) / 11 > e;) {}return 1 / Math.pow(4, 3 - i) - 7.5625 * Math.pow((3 * t - 2) / 22 - e, 2);
      } }), e.each(t, function (t, i) {
      e.easing["easeIn" + t] = i, e.easing["easeOut" + t] = function (e) {
        return 1 - i(1 - e);
      }, e.easing["easeInOut" + t] = function (e) {
        return .5 > e ? i(2 * e) / 2 : 1 - i(-2 * e + 2) / 2;
      };
    });
  }();
}(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');
__webpack_require__(8);
__webpack_require__(15);
__webpack_require__(11);
//require('./js/jquery.fullpage.extensions.min.js');
__webpack_require__(1);
__webpack_require__(13);
__webpack_require__(10);
__webpack_require__(1);
//require('./js/jquery.cardslider.min.js');
__webpack_require__(16);
window.Dropzone = __webpack_require__(9);
//require('./js/ben.js');
__webpack_require__(7);
window.bodymovin = __webpack_require__(6);
__webpack_require__(14);
__webpack_require__(12);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

!function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = e() : t.bodymovin = e();
}(window, function () {
  function ProjectInterface() {
    return {};
  }function roundValues(t) {
    bm_rnd = t ? Math.round : function (t) {
      return t;
    };
  }function roundTo2Decimals(t) {
    return Math.round(1e4 * t) / 1e4;
  }function roundTo3Decimals(t) {
    return Math.round(100 * t) / 100;
  }function styleDiv(t) {
    t.style.position = "absolute", t.style.top = 0, t.style.left = 0, t.style.display = "block", t.style.transformOrigin = t.style.webkitTransformOrigin = "0 0", t.style.backfaceVisibility = t.style.webkitBackfaceVisibility = "visible", t.style.transformStyle = t.style.webkitTransformStyle = t.style.mozTransformStyle = "preserve-3d";
  }function styleUnselectableDiv(t) {
    t.style.userSelect = "none", t.style.MozUserSelect = "none", t.style.webkitUserSelect = "none", t.style.oUserSelect = "none";
  }function BMEnterFrameEvent(t, e, r, s) {
    this.type = t, this.currentTime = e, this.totalTime = r, this.direction = 0 > s ? -1 : 1;
  }function BMCompleteEvent(t, e) {
    this.type = t, this.direction = 0 > e ? -1 : 1;
  }function BMCompleteLoopEvent(t, e, r, s) {
    this.type = t, this.currentLoop = e, this.totalLoops = r, this.direction = 0 > s ? -1 : 1;
  }function BMSegmentStartEvent(t, e, r) {
    this.type = t, this.firstFrame = e, this.totalFrames = r;
  }function BMDestroyEvent(t, e) {
    this.type = t, this.target = e;
  }function _addEventListener(t, e) {
    this._cbs[t] || (this._cbs[t] = []), this._cbs[t].push(e);
  }function _removeEventListener(t, e) {
    if (e) {
      if (this._cbs[t]) {
        for (var r = 0, s = this._cbs[t].length; s > r;) {
          this._cbs[t][r] === e && (this._cbs[t].splice(r, 1), r -= 1, s -= 1), r += 1;
        }this._cbs[t].length || (this._cbs[t] = null);
      }
    } else this._cbs[t] = null;
  }function _triggerEvent(t, e) {
    if (this._cbs[t]) for (var r = this._cbs[t].length, s = 0; r > s; s++) {
      this._cbs[t][s](e);
    }
  }function randomString(t, e) {
    void 0 === e && (e = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");var r,
        s = "";for (r = t; r > 0; --r) {
      s += e[Math.round(Math.random() * (e.length - 1))];
    }return s;
  }function HSVtoRGB(t, e, r) {
    var s, i, a, n, o, h, l, p;switch (1 === arguments.length && (e = t.s, r = t.v, t = t.h), n = Math.floor(6 * t), o = 6 * t - n, h = r * (1 - e), l = r * (1 - o * e), p = r * (1 - (1 - o) * e), n % 6) {case 0:
        s = r, i = p, a = h;break;case 1:
        s = l, i = r, a = h;break;case 2:
        s = h, i = r, a = p;break;case 3:
        s = h, i = l, a = r;break;case 4:
        s = p, i = h, a = r;break;case 5:
        s = r, i = h, a = l;}return [s, i, a];
  }function RGBtoHSV(t, e, r) {
    1 === arguments.length && (e = t.g, r = t.b, t = t.r);var s,
        i = Math.max(t, e, r),
        a = Math.min(t, e, r),
        n = i - a,
        o = 0 === i ? 0 : n / i,
        h = i / 255;switch (i) {case a:
        s = 0;break;case t:
        s = e - r + n * (r > e ? 6 : 0), s /= 6 * n;break;case e:
        s = r - t + 2 * n, s /= 6 * n;break;case r:
        s = t - e + 4 * n, s /= 6 * n;}return [s, o, h];
  }function addSaturationToRGB(t, e) {
    var r = RGBtoHSV(255 * t[0], 255 * t[1], 255 * t[2]);return r[1] += e, r[1] > 1 ? r[1] = 1 : r[1] <= 0 && (r[1] = 0), HSVtoRGB(r[0], r[1], r[2]);
  }function addBrightnessToRGB(t, e) {
    var r = RGBtoHSV(255 * t[0], 255 * t[1], 255 * t[2]);return r[2] += e, r[2] > 1 ? r[2] = 1 : r[2] < 0 && (r[2] = 0), HSVtoRGB(r[0], r[1], r[2]);
  }function addHueToRGB(t, e) {
    var r = RGBtoHSV(255 * t[0], 255 * t[1], 255 * t[2]);return r[0] += e / 360, r[0] > 1 ? r[0] -= 1 : r[0] < 0 && (r[0] += 1), HSVtoRGB(r[0], r[1], r[2]);
  }function componentToHex(t) {
    var e = t.toString(16);return 1 == e.length ? "0" + e : e;
  }function fillToRgba(t, e) {
    if (!cachedColors[t]) {
      var r = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(t);cachedColors[t] = parseInt(r[1], 16) + "," + parseInt(r[2], 16) + "," + parseInt(r[3], 16);
    }return "rgba(" + cachedColors[t] + "," + e + ")";
  }function RenderedFrame(t, e) {
    this.tr = t, this.o = e;
  }function LetterProps(t, e, r, s, i, a) {
    this.o = t, this.sw = e, this.sc = r, this.fc = s, this.m = i, this.props = a;
  }function iterateDynamicProperties(t) {
    var e,
        r = this.dynamicProperties;for (e = 0; r > e; e += 1) {
      this.dynamicProperties[e].getValue(t);
    }
  }function reversePath(t) {
    var e,
        r,
        s = [],
        i = [],
        a = [],
        n = {},
        o = 0;t.c && (s[0] = t.o[0], i[0] = t.i[0], a[0] = t.v[0], o = 1), r = t.i.length;var h = r - 1;for (e = o; r > e; e += 1) {
      s.push(t.o[h]), i.push(t.i[h]), a.push(t.v[h]), h -= 1;
    }return n.i = s, n.o = i, n.v = a, n;
  }function Matrix() {}function matrixManagerFunction() {
    var t = new Matrix(),
        e = function e(_e, r, s, i, a) {
      return t.reset().translate(i, a).rotate(_e).scale(r, s).toCSS();
    },
        r = function r(t) {
      return e(t.tr.r[2], t.tr.s[0], t.tr.s[1], t.tr.p[0], t.tr.p[1]);
    };return { getMatrix: r };
  }function createElement(t, e, r) {
    if (!e) {
      var s = Object.create(t.prototype, r),
          i = {};return s && "[object Function]" === i.toString.call(s.init) && s.init(), s;
    }e.prototype = Object.create(t.prototype), e.prototype.constructor = e, e.prototype._parent = t.prototype;
  }function extendPrototype(t, e) {
    for (var r in t.prototype) {
      t.prototype.hasOwnProperty(r) && (e.prototype[r] = t.prototype[r]);
    }
  }function bezFunction() {
    function t(t, e, r, s, i, a) {
      var n = t * s + e * i + r * a - i * s - a * t - r * e;return n > -1e-4 && 1e-4 > n;
    }function e(e, r, s, i, a, n, o, h, l) {
      return t(e, r, i, a, o, h) && t(e, s, i, n, o, l);
    }function r(t) {
      this.segmentLength = 0, this.points = new Array(t);
    }function s(t, e) {
      this.partialLength = t, this.point = e;
    }function i(t, e) {
      var r = e.segments,
          s = r.length,
          i = bm_floor((s - 1) * t),
          a = t * e.addedLength,
          n = 0;if (a == r[i].l) return r[i].p;for (var o = r[i].l > a ? -1 : 1, h = !0; h;) {
        r[i].l <= a && r[i + 1].l > a ? (n = (a - r[i].l) / (r[i + 1].l - r[i].l), h = !1) : i += o, (0 > i || i >= s - 1) && (h = !1);
      }return r[i].p + (r[i + 1].p - r[i].p) * n;
    }function a() {
      this.pt1 = new Array(2), this.pt2 = new Array(2), this.pt3 = new Array(2), this.pt4 = new Array(2);
    }function n(t, e, r, s, n, o, h) {
      var l = new a();n = 0 > n ? 0 : n > 1 ? 1 : n;var p = i(n, h);o = o > 1 ? 1 : o;var m,
          f = i(o, h),
          c = t.length,
          d = 1 - p,
          u = 1 - f;for (m = 0; c > m; m += 1) {
        l.pt1[m] = Math.round(1e3 * (d * d * d * t[m] + (p * d * d + d * p * d + d * d * p) * r[m] + (p * p * d + d * p * p + p * d * p) * s[m] + p * p * p * e[m])) / 1e3, l.pt3[m] = Math.round(1e3 * (d * d * u * t[m] + (p * d * u + d * p * u + d * d * f) * r[m] + (p * p * u + d * p * f + p * d * f) * s[m] + p * p * f * e[m])) / 1e3, l.pt4[m] = Math.round(1e3 * (d * u * u * t[m] + (p * u * u + d * f * u + d * u * f) * r[m] + (p * f * u + d * f * f + p * u * f) * s[m] + p * f * f * e[m])) / 1e3, l.pt2[m] = Math.round(1e3 * (u * u * u * t[m] + (f * u * u + u * f * u + u * u * f) * r[m] + (f * f * u + u * f * f + f * u * f) * s[m] + f * f * f * e[m])) / 1e3;
      }return l;
    }var o = (Math, function () {
      function t(t, e) {
        this.l = t, this.p = e;
      }return function (e, r, s, i) {
        var a,
            n,
            o,
            h,
            l,
            p,
            m = defaultCurveSegments,
            f = 0,
            c = [],
            d = [],
            u = { addedLength: 0, segments: [] };for (o = s.length, a = 0; m > a; a += 1) {
          for (l = a / (m - 1), p = 0, n = 0; o > n; n += 1) {
            h = bm_pow(1 - l, 3) * e[n] + 3 * bm_pow(1 - l, 2) * l * s[n] + 3 * (1 - l) * bm_pow(l, 2) * i[n] + bm_pow(l, 3) * r[n], c[n] = h, null !== d[n] && (p += bm_pow(c[n] - d[n], 2)), d[n] = c[n];
          }p && (p = bm_sqrt(p), f += p), u.segments.push(new t(f, l));
        }return u.addedLength = f, u;
      };
    }()),
        h = function () {
      var e = {};return function (i) {
        var a = i.s,
            n = i.e,
            o = i.to,
            h = i.ti,
            l = (a.join("_") + "_" + n.join("_") + "_" + o.join("_") + "_" + h.join("_")).replace(/\./g, "p");if (e[l]) return void (i.bezierData = e[l]);var p,
            m,
            f,
            c,
            d,
            u,
            y,
            g = defaultCurveSegments,
            v = 0,
            b = null;2 === a.length && (a[0] != n[0] || a[1] != n[1]) && t(a[0], a[1], n[0], n[1], a[0] + o[0], a[1] + o[1]) && t(a[0], a[1], n[0], n[1], n[0] + h[0], n[1] + h[1]) && (g = 2);var E = new r(g);for (f = o.length, p = 0; g > p; p += 1) {
          for (y = new Array(f), d = p / (g - 1), u = 0, m = 0; f > m; m += 1) {
            c = bm_pow(1 - d, 3) * a[m] + 3 * bm_pow(1 - d, 2) * d * (a[m] + o[m]) + 3 * (1 - d) * bm_pow(d, 2) * (n[m] + h[m]) + bm_pow(d, 3) * n[m], y[m] = c, null !== b && (u += bm_pow(y[m] - b[m], 2));
          }u = bm_sqrt(u), v += u, E.points[p] = new s(u, y), b = y;
        }E.segmentLength = v, i.bezierData = E, e[l] = E;
      };
    }();return { getBezierLength: o, getNewSegment: n, buildBezierData: h, pointOnLine2D: t, pointOnLine3D: e };
  }function dataFunctionManager() {
    function t(i, a, o) {
      var h,
          l,
          p,
          m,
          f,
          c,
          d,
          u,
          y = i.length;for (m = 0; y > m; m += 1) {
        if (h = i[m], "ks" in h && !h.completed) {
          if (h.completed = !0, h.tt && (i[m - 1].td = h.tt), l = [], p = -1, h.hasMask) {
            var g = h.masksProperties;for (c = g.length, f = 0; c > f; f += 1) {
              if (g[f].pt.k.i) s(g[f].pt.k);else for (u = g[f].pt.k.length, d = 0; u > d; d += 1) {
                g[f].pt.k[d].s && s(g[f].pt.k[d].s[0]), g[f].pt.k[d].e && s(g[f].pt.k[d].e[0]);
              }
            }
          }0 === h.ty ? (h.layers = e(h.refId, a), t(h.layers, a, o)) : 4 === h.ty ? r(h.shapes) : 5 == h.ty && n(h, o);
        }
      }
    }function e(t, e) {
      for (var r = 0, s = e.length; s > r;) {
        if (e[r].id === t) return e[r].layers;r += 1;
      }
    }function r(t) {
      var e,
          i,
          a,
          n = t.length,
          o = !1;for (e = n - 1; e >= 0; e -= 1) {
        if ("sh" == t[e].ty) {
          if (t[e].ks.k.i) s(t[e].ks.k);else for (a = t[e].ks.k.length, i = 0; a > i; i += 1) {
            t[e].ks.k[i].s && s(t[e].ks.k[i].s[0]), t[e].ks.k[i].e && s(t[e].ks.k[i].e[0]);
          }o = !0;
        } else "gr" == t[e].ty && r(t[e].it);
      }
    }function s(t) {
      var e,
          r = t.i.length;for (e = 0; r > e; e += 1) {
        t.i[e][0] += t.v[e][0], t.i[e][1] += t.v[e][1], t.o[e][0] += t.v[e][0], t.o[e][1] += t.v[e][1];
      }
    }function i(t, e) {
      var r = e ? e.split(".") : [100, 100, 100];return t[0] > r[0] ? !0 : r[0] > t[0] ? !1 : t[1] > r[1] ? !0 : r[1] > t[1] ? !1 : t[2] > r[2] ? !0 : r[2] > t[2] ? !1 : void 0;
    }function a(e, r) {
      e.__complete || (h(e), o(e), l(e), t(e.layers, e.assets, r), e.__complete = !0);
    }function n(t, e) {
      var r,
          s,
          i = t.t.d.k,
          a = i.length;for (s = 0; a > s; s += 1) {
        var n = t.t.d.k[s].s;r = [];var o,
            h,
            l,
            p,
            m,
            f,
            c,
            d = 0,
            u = t.t.m.g,
            y = 0,
            g = 0,
            v = 0,
            b = [],
            E = 0,
            P = 0,
            x = e.getFontByName(n.f),
            S = 0,
            C = x.fStyle.split(" "),
            k = "normal",
            M = "normal";for (h = C.length, o = 0; h > o; o += 1) {
          "italic" === C[o].toLowerCase() ? M = "italic" : "bold" === C[o].toLowerCase() ? k = "700" : "black" === C[o].toLowerCase() ? k = "900" : "medium" === C[o].toLowerCase() ? k = "500" : "regular" === C[o].toLowerCase() || "normal" === C[o].toLowerCase() ? k = "400" : ("light" === C[o].toLowerCase() || "thin" === C[o].toLowerCase()) && (k = "200");
        }if (n.fWeight = k, n.fStyle = M, h = n.t.length, n.sz) {
          var A = n.sz[0],
              D = -1;for (o = 0; h > o; o += 1) {
            l = !1, " " === n.t.charAt(o) ? D = o : 13 === n.t.charCodeAt(o) && (E = 0, l = !0), e.chars ? (c = e.getCharData(n.t.charAt(o), x.fStyle, x.fFamily), S = l ? 0 : c.w * n.s / 100) : S = e.measureText(n.t.charAt(o), n.f, n.s), E + S > A ? (-1 === D ? (n.t = n.t.substr(0, o) + "\r" + n.t.substr(o), h += 1) : (o = D, n.t = n.t.substr(0, o) + "\r" + n.t.substr(o + 1)), D = -1, E = 0) : E += S;
          }h = n.t.length;
        }for (E = 0, S = 0, o = 0; h > o; o += 1) {
          if (l = !1, " " === n.t.charAt(o) ? p = "\xa0" : 13 === n.t.charCodeAt(o) ? (b.push(E), P = E > P ? E : P, E = 0, p = "", l = !0, v += 1) : p = n.t.charAt(o), e.chars ? (c = e.getCharData(n.t.charAt(o), x.fStyle, e.getFontByName(n.f).fFamily), S = l ? 0 : c.w * n.s / 100) : S = e.measureText(p, n.f, n.s), E += S, r.push({ l: S, an: S, add: y, n: l, anIndexes: [], val: p, line: v }), 2 == u) {
            if (y += S, "" == p || "\xa0" == p || o == h - 1) {
              for (("" == p || "\xa0" == p) && (y -= S); o >= g;) {
                r[g].an = y, r[g].ind = d, r[g].extra = S, g += 1;
              }d += 1, y = 0;
            }
          } else if (3 == u) {
            if (y += S, "" == p || o == h - 1) {
              for ("" == p && (y -= S); o >= g;) {
                r[g].an = y, r[g].ind = d, r[g].extra = S, g += 1;
              }y = 0, d += 1;
            }
          } else r[d].ind = d, r[d].extra = 0, d += 1;
        }if (n.l = r, P = E > P ? E : P, b.push(E), n.sz) n.boxWidth = n.sz[0], n.justifyOffset = 0;else switch (n.boxWidth = P, n.j) {case 1:
            n.justifyOffset = -n.boxWidth;break;case 2:
            n.justifyOffset = -n.boxWidth / 2;break;default:
            n.justifyOffset = 0;}n.lineWidths = b;var T = t.t.a;f = T.length;var w,
            F,
            I = [];for (m = 0; f > m; m += 1) {
          for (T[m].a.sc && (n.strokeColorAnim = !0), T[m].a.sw && (n.strokeWidthAnim = !0), (T[m].a.fc || T[m].a.fh || T[m].a.fs || T[m].a.fb) && (n.fillColorAnim = !0), F = 0, w = T[m].s.b, o = 0; h > o; o += 1) {
            r[o].anIndexes[m] = F, (1 == w && "" != r[o].val || 2 == w && "" != r[o].val && "\xa0" != r[o].val || 3 == w && (r[o].n || "\xa0" == r[o].val || o == h - 1) || 4 == w && (r[o].n || o == h - 1)) && (1 === T[m].s.rn && I.push(F), F += 1);
          }t.t.a[m].s.totalChars = F;var _,
              V = -1;if (1 === T[m].s.rn) for (o = 0; h > o; o += 1) {
            V != r[o].anIndexes[m] && (V = r[o].anIndexes[m], _ = I.splice(Math.floor(Math.random() * I.length), 1)[0]), r[o].anIndexes[m] = _;
          }
        }0 !== f || "m" in t.t.p || (t.singleShape = !0), n.yOffset = n.lh || 1.2 * n.s, n.ascent = x.ascent * n.s / 100;
      }
    }var o = function () {
      function t(t) {
        var e = t.t.d;t.t.d = { k: [{ s: e, t: 0 }] };
      }function e(e) {
        var r,
            s = e.length;for (r = 0; s > r; r += 1) {
          5 === e[r].ty && t(e[r]);
        }
      }var r = [4, 4, 14];return function (t) {
        if (i(r, t.v) && (e(t.layers), t.assets)) {
          var s,
              a = t.assets.length;for (s = 0; a > s; s += 1) {
            t.assets[s].layers && e(t.assets[s].layers);
          }
        }
      };
    }(),
        h = function () {
      function t(e) {
        var r,
            s,
            i,
            a = e.length;for (r = 0; a > r; r += 1) {
          if ("gr" === e[r].ty) t(e[r].it);else if ("fl" === e[r].ty || "st" === e[r].ty) if (e[r].c.k && e[r].c.k[0].i) for (i = e[r].c.k.length, s = 0; i > s; s += 1) {
            e[r].c.k[s].s && (e[r].c.k[s].s[0] /= 255, e[r].c.k[s].s[1] /= 255, e[r].c.k[s].s[2] /= 255, e[r].c.k[s].s[3] /= 255), e[r].c.k[s].e && (e[r].c.k[s].e[0] /= 255, e[r].c.k[s].e[1] /= 255, e[r].c.k[s].e[2] /= 255, e[r].c.k[s].e[3] /= 255);
          } else e[r].c.k[0] /= 255, e[r].c.k[1] /= 255, e[r].c.k[2] /= 255, e[r].c.k[3] /= 255;
        }
      }function e(e) {
        var r,
            s = e.length;for (r = 0; s > r; r += 1) {
          4 === e[r].ty && t(e[r].shapes);
        }
      }var r = [4, 1, 9];return function (t) {
        if (i(r, t.v) && (e(t.layers), t.assets)) {
          var s,
              a = t.assets.length;for (s = 0; a > s; s += 1) {
            t.assets[s].layers && e(t.assets[s].layers);
          }
        }
      };
    }(),
        l = function () {
      function t(e) {
        var r,
            s,
            i,
            a = e.length,
            n = !1;for (r = a - 1; r >= 0; r -= 1) {
          if ("sh" == e[r].ty) {
            if (e[r].ks.k.i) e[r].ks.k.c = e[r].closed;else for (i = e[r].ks.k.length, s = 0; i > s; s += 1) {
              e[r].ks.k[s].s && (e[r].ks.k[s].s[0].c = e[r].closed), e[r].ks.k[s].e && (e[r].ks.k[s].e[0].c = e[r].closed);
            }n = !0;
          } else "gr" == e[r].ty && t(e[r].it);
        }
      }function e(e) {
        var r,
            s,
            i,
            a,
            n,
            o,
            h = e.length;for (s = 0; h > s; s += 1) {
          if (r = e[s], r.hasMask) {
            var l = r.masksProperties;for (a = l.length, i = 0; a > i; i += 1) {
              if (l[i].pt.k.i) l[i].pt.k.c = l[i].cl;else for (o = l[i].pt.k.length, n = 0; o > n; n += 1) {
                l[i].pt.k[n].s && (l[i].pt.k[n].s[0].c = l[i].cl), l[i].pt.k[n].e && (l[i].pt.k[n].e[0].c = l[i].cl);
              }
            }
          }4 === r.ty && t(r.shapes);
        }
      }var r = [4, 4, 18];return function (t) {
        if (i(r, t.v) && (e(t.layers), t.assets)) {
          var s,
              a = t.assets.length;for (s = 0; a > s; s += 1) {
            t.assets[s].layers && e(t.assets[s].layers);
          }
        }
      };
    }(),
        p = {};return p.completeData = a, p;
  }function ShapePath() {
    this.c = !1, this._length = 0, this._maxLength = 8, this.v = Array.apply(null, { length: this._maxLength }), this.o = Array.apply(null, { length: this._maxLength }), this.i = Array.apply(null, { length: this._maxLength });
  }function ShapeModifier() {}function TrimModifier() {}function RoundCornersModifier() {}function RepeaterModifier() {}function ShapeCollection() {
    this._length = 0, this._maxLength = 4, this.shapes = Array.apply(null, { length: this._maxLength });
  }function BaseRenderer() {}function SVGRenderer(t, e) {
    this.animationItem = t, this.layers = null, this.renderedFrame = -1, this.globalData = { frameNum: -1 }, this.renderConfig = { preserveAspectRatio: e && e.preserveAspectRatio || "xMidYMid meet", progressiveLoad: e && e.progressiveLoad || !1 }, this.elements = [], this.pendingElements = [], this.destroyed = !1;
  }function MaskElement(t, e, r) {
    this.dynamicProperties = [], this.data = t, this.element = e, this.globalData = r, this.paths = [], this.storedData = [], this.masksProperties = this.data.masksProperties, this.viewData = new Array(this.masksProperties.length), this.maskElement = null, this.firstFrame = !0;var s,
        i,
        a,
        n,
        o,
        h,
        l,
        p,
        m = this.globalData.defs,
        f = this.masksProperties.length,
        c = this.masksProperties,
        d = 0,
        u = [],
        y = randomString(10),
        g = "clipPath",
        v = "clip-path";for (s = 0; f > s; s++) {
      if (("a" !== c[s].mode && "n" !== c[s].mode || c[s].inv || 100 !== c[s].o.k) && (g = "mask", v = "mask"), "s" != c[s].mode && "i" != c[s].mode || 0 != d ? o = null : (o = document.createElementNS(svgNS, "rect"), o.setAttribute("fill", "#ffffff"), o.setAttribute("width", this.element.comp.data.w), o.setAttribute("height", this.element.comp.data.h), u.push(o)), i = document.createElementNS(svgNS, "path"), "n" != c[s].mode) {
        if (d += 1, "s" == c[s].mode ? i.setAttribute("fill", "#000000") : i.setAttribute("fill", "#ffffff"), i.setAttribute("clip-rule", "nonzero"), 0 !== c[s].x.k) {
          g = "mask", v = "mask", p = PropertyFactory.getProp(this.element, c[s].x, 0, null, this.dynamicProperties);var b = "fi_" + randomString(10);h = document.createElementNS(svgNS, "filter"), h.setAttribute("id", b), l = document.createElementNS(svgNS, "feMorphology"), l.setAttribute("operator", "dilate"), l.setAttribute("in", "SourceGraphic"), l.setAttribute("radius", "0"), h.appendChild(l), m.appendChild(h), "s" == c[s].mode ? i.setAttribute("stroke", "#000000") : i.setAttribute("stroke", "#ffffff");
        } else l = null, p = null;if (this.storedData[s] = { elem: i, x: p, expan: l, lastPath: "", lastOperator: "", filterId: b, lastRadius: 0 }, "i" == c[s].mode) {
          n = u.length;var E = document.createElementNS(svgNS, "g");for (a = 0; n > a; a += 1) {
            E.appendChild(u[a]);
          }var P = document.createElementNS(svgNS, "mask");P.setAttribute("mask-type", "alpha"), P.setAttribute("id", y + "_" + d), P.appendChild(i), m.appendChild(P), E.setAttribute("mask", "url(#" + y + "_" + d + ")"), u.length = 0, u.push(E);
        } else u.push(i);c[s].inv && !this.solidPath && (this.solidPath = this.createLayerSolidPath()), this.viewData[s] = { elem: i, lastPath: "", op: PropertyFactory.getProp(this.element, c[s].o, 0, .01, this.dynamicProperties), prop: ShapePropertyFactory.getShapeProp(this.element, c[s], 3, this.dynamicProperties, null) }, o && (this.viewData[s].invRect = o), this.viewData[s].prop.k || this.drawPath(c[s], this.viewData[s].prop.v, this.viewData[s]);
      } else this.viewData[s] = { op: PropertyFactory.getProp(this.element, c[s].o, 0, .01, this.dynamicProperties), prop: ShapePropertyFactory.getShapeProp(this.element, c[s], 3, this.dynamicProperties, null), elem: i }, m.appendChild(i);
    }for (this.maskElement = document.createElementNS(svgNS, g), f = u.length, s = 0; f > s; s += 1) {
      this.maskElement.appendChild(u[s]);
    }this.maskElement.setAttribute("id", y), d > 0 && this.element.maskedElement.setAttribute(v, "url(#" + y + ")"), m.appendChild(this.maskElement);
  }function BaseElement() {}function SVGBaseElement(t, e, r, s, i) {
    this.globalData = r, this.comp = s, this.data = t, this.matteElement = null, this.transformedElement = null, this.parentContainer = e, this.layerId = i ? i.layerId : "ly_" + randomString(10), this.placeholder = i, this.init();
  }function ITextElement(t, e, r, s) {}function SVGTextElement(t, e, r, s, i) {
    this.textSpans = [], this.renderType = "svg", this._parent.constructor.call(this, t, e, r, s, i);
  }function SVGTintFilter(t, e) {
    this.filterManager = e;var r = document.createElementNS(svgNS, "feColorMatrix");if (r.setAttribute("type", "matrix"), r.setAttribute("color-interpolation-filters", "linearRGB"), r.setAttribute("values", "0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"), r.setAttribute("result", "f1"), t.appendChild(r), r = document.createElementNS(svgNS, "feColorMatrix"), r.setAttribute("type", "matrix"), r.setAttribute("color-interpolation-filters", "sRGB"), r.setAttribute("values", "1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"), r.setAttribute("result", "f2"), t.appendChild(r), this.matrixFilter = r, 100 !== e.effectElements[2].p.v || e.effectElements[2].p.k) {
      var s = document.createElementNS(svgNS, "feMerge");t.appendChild(s);var i;i = document.createElementNS(svgNS, "feMergeNode"), i.setAttribute("in", "SourceGraphic"), s.appendChild(i), i = document.createElementNS(svgNS, "feMergeNode"), i.setAttribute("in", "f2"), s.appendChild(i);
    }
  }function SVGFillFilter(t, e) {
    this.filterManager = e;var r = document.createElementNS(svgNS, "feColorMatrix");r.setAttribute("type", "matrix"), r.setAttribute("color-interpolation-filters", "sRGB"), r.setAttribute("values", "1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"), t.appendChild(r), this.matrixFilter = r;
  }function SVGStrokeEffect(t, e) {
    this.initialized = !1, this.filterManager = e, this.elem = t, this.paths = [];
  }function SVGTritoneFilter(t, e) {
    this.filterManager = e;var r = document.createElementNS(svgNS, "feColorMatrix");r.setAttribute("type", "matrix"), r.setAttribute("color-interpolation-filters", "linearRGB"), r.setAttribute("values", "0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"), r.setAttribute("result", "f1"), t.appendChild(r);var s = document.createElementNS(svgNS, "feComponentTransfer");s.setAttribute("color-interpolation-filters", "sRGB"), t.appendChild(s), this.matrixFilter = s;var i = document.createElementNS(svgNS, "feFuncR");i.setAttribute("type", "table"), s.appendChild(i), this.feFuncR = i;var a = document.createElementNS(svgNS, "feFuncG");a.setAttribute("type", "table"), s.appendChild(a), this.feFuncG = a;var n = document.createElementNS(svgNS, "feFuncB");n.setAttribute("type", "table"), s.appendChild(n), this.feFuncB = n;
  }function SVGProLevelsFilter(t, e) {
    this.filterManager = e;var r = this.filterManager.effectElements,
        s = document.createElementNS(svgNS, "feComponentTransfer");(r[9].p.k || 0 !== r[9].p.v || r[10].p.k || 1 !== r[10].p.v || r[11].p.k || 1 !== r[11].p.v || r[12].p.k || 0 !== r[12].p.v || r[13].p.k || 1 !== r[13].p.v) && (this.feFuncR = this.createFeFunc("feFuncR", s)), (r[16].p.k || 0 !== r[16].p.v || r[17].p.k || 1 !== r[17].p.v || r[18].p.k || 1 !== r[18].p.v || r[19].p.k || 0 !== r[19].p.v || r[20].p.k || 1 !== r[20].p.v) && (this.feFuncG = this.createFeFunc("feFuncG", s)), (r[23].p.k || 0 !== r[23].p.v || r[24].p.k || 1 !== r[24].p.v || r[25].p.k || 1 !== r[25].p.v || r[26].p.k || 0 !== r[26].p.v || r[27].p.k || 1 !== r[27].p.v) && (this.feFuncB = this.createFeFunc("feFuncB", s)), (r[30].p.k || 0 !== r[30].p.v || r[31].p.k || 1 !== r[31].p.v || r[32].p.k || 1 !== r[32].p.v || r[33].p.k || 0 !== r[33].p.v || r[34].p.k || 1 !== r[34].p.v) && (this.feFuncA = this.createFeFunc("feFuncA", s)), (this.feFuncR || this.feFuncG || this.feFuncB || this.feFuncA) && (s.setAttribute("color-interpolation-filters", "sRGB"), t.appendChild(s), s = document.createElementNS(svgNS, "feComponentTransfer")), (r[2].p.k || 0 !== r[2].p.v || r[3].p.k || 1 !== r[3].p.v || r[4].p.k || 1 !== r[4].p.v || r[5].p.k || 0 !== r[5].p.v || r[6].p.k || 1 !== r[6].p.v) && (s.setAttribute("color-interpolation-filters", "sRGB"), t.appendChild(s), this.feFuncRComposed = this.createFeFunc("feFuncR", s), this.feFuncGComposed = this.createFeFunc("feFuncG", s), this.feFuncBComposed = this.createFeFunc("feFuncB", s));
  }function SVGDropShadowEffect(t, e) {
    t.setAttribute("x", "-100%"), t.setAttribute("y", "-100%"), t.setAttribute("width", "400%"), t.setAttribute("height", "400%"), this.filterManager = e;var r = document.createElementNS(svgNS, "feGaussianBlur");r.setAttribute("in", "SourceAlpha"), r.setAttribute("result", "drop_shadow_1"), r.setAttribute("stdDeviation", "0"), this.feGaussianBlur = r, t.appendChild(r);var s = document.createElementNS(svgNS, "feOffset");s.setAttribute("dx", "25"), s.setAttribute("dy", "0"), s.setAttribute("in", "drop_shadow_1"), s.setAttribute("result", "drop_shadow_2"), this.feOffset = s, t.appendChild(s);var i = document.createElementNS(svgNS, "feFlood");i.setAttribute("flood-color", "#00ff00"), i.setAttribute("flood-opacity", "1"), i.setAttribute("result", "drop_shadow_3"), this.feFlood = i, t.appendChild(i);var a = document.createElementNS(svgNS, "feComposite");a.setAttribute("in", "drop_shadow_3"), a.setAttribute("in2", "drop_shadow_2"), a.setAttribute("operator", "in"), a.setAttribute("result", "drop_shadow_4"), t.appendChild(a);var n = document.createElementNS(svgNS, "feMerge");t.appendChild(n);var o;o = document.createElementNS(svgNS, "feMergeNode"), n.appendChild(o), o = document.createElementNS(svgNS, "feMergeNode"), o.setAttribute("in", "SourceGraphic"), this.feMergeNode = o, this.feMerge = n, this.originalNodeAdded = !1, n.appendChild(o);
  }function SVGEffects(t) {
    var e,
        r = t.data.ef.length,
        s = randomString(10),
        i = filtersFactory.createFilter(s),
        a = 0;this.filters = [];var n;for (e = 0; r > e; e += 1) {
      20 === t.data.ef[e].ty ? (a += 1, n = new SVGTintFilter(i, t.effects.effectElements[e]), this.filters.push(n)) : 21 === t.data.ef[e].ty ? (a += 1, n = new SVGFillFilter(i, t.effects.effectElements[e]), this.filters.push(n)) : 22 === t.data.ef[e].ty ? (n = new SVGStrokeEffect(t, t.effects.effectElements[e]), this.filters.push(n)) : 23 === t.data.ef[e].ty ? (a += 1, n = new SVGTritoneFilter(i, t.effects.effectElements[e]), this.filters.push(n)) : 24 === t.data.ef[e].ty ? (a += 1, n = new SVGProLevelsFilter(i, t.effects.effectElements[e]), this.filters.push(n)) : 25 === t.data.ef[e].ty && (a += 1, n = new SVGDropShadowEffect(i, t.effects.effectElements[e]), this.filters.push(n));
    }a && (t.globalData.defs.appendChild(i), t.layerElement.setAttribute("filter", "url(#" + s + ")"));
  }function ICompElement(t, e, r, s, i) {
    this._parent.constructor.call(this, t, e, r, s, i), this.layers = t.layers, this.supports3d = !0, this.completeLayers = !1, this.pendingElements = [], this.elements = this.layers ? Array.apply(null, { length: this.layers.length }) : [], this.data.tm && (this.tm = PropertyFactory.getProp(this, this.data.tm, 0, r.frameRate, this.dynamicProperties)), this.data.xt ? (this.layerElement = document.createElementNS(svgNS, "g"), this.buildAllItems()) : r.progressiveLoad || this.buildAllItems();
  }function IImageElement(t, e, r, s, i) {
    this.assetData = r.getAssetData(t.refId), this._parent.constructor.call(this, t, e, r, s, i);
  }function IShapeElement(t, e, r, s, i) {
    this.shapes = [], this.shapesData = t.shapes, this.stylesList = [], this.viewData = [], this.shapeModifiers = [], this._parent.constructor.call(this, t, e, r, s, i);
  }function ISolidElement(t, e, r, s, i) {
    this._parent.constructor.call(this, t, e, r, s, i);
  }function CanvasRenderer(t, e) {
    this.animationItem = t, this.renderConfig = { clearCanvas: e && void 0 !== e.clearCanvas ? e.clearCanvas : !0, context: e && e.context || null, progressiveLoad: e && e.progressiveLoad || !1, preserveAspectRatio: e && e.preserveAspectRatio || "xMidYMid meet" }, this.renderConfig.dpr = e && e.dpr || 1, this.animationItem.wrapper && (this.renderConfig.dpr = e && e.dpr || window.devicePixelRatio || 1), this.renderedFrame = -1, this.globalData = { frameNum: -1 }, this.contextData = { saved: Array.apply(null, { length: 15 }), savedOp: Array.apply(null, { length: 15 }), cArrPos: 0, cTr: new Matrix(), cO: 1 };var r,
        s = 15;for (r = 0; s > r; r += 1) {
      this.contextData.saved[r] = Array.apply(null, { length: 16 });
    }this.elements = [], this.pendingElements = [], this.transformMat = new Matrix(), this.completeLayers = !1;
  }function HybridRenderer(t) {
    this.animationItem = t, this.layers = null, this.renderedFrame = -1, this.globalData = { frameNum: -1 }, this.pendingElements = [], this.elements = [], this.threeDElements = [], this.destroyed = !1, this.camera = null, this.supports3d = !0;
  }function CVBaseElement(t, e, r) {
    this.globalData = r, this.data = t, this.comp = e, this.canvasContext = r.canvasContext, this.init();
  }function CVCompElement(t, e, r) {
    this._parent.constructor.call(this, t, e, r);var s = {};for (var i in r) {
      r.hasOwnProperty(i) && (s[i] = r[i]);
    }s.renderer = this, s.compHeight = this.data.h, s.compWidth = this.data.w, this.renderConfig = { clearCanvas: !0 }, this.contextData = { saved: Array.apply(null, { length: 15 }), savedOp: Array.apply(null, { length: 15 }), cArrPos: 0, cTr: new Matrix(), cO: 1 }, this.completeLayers = !1;var a,
        n = 15;for (a = 0; n > a; a += 1) {
      this.contextData.saved[a] = Array.apply(null, { length: 16 });
    }this.transformMat = new Matrix(), this.parentGlobalData = this.globalData;var o = document.createElement("canvas");s.canvasContext = o.getContext("2d"), this.canvasContext = s.canvasContext, o.width = this.data.w, o.height = this.data.h, this.canvas = o, this.globalData = s, this.layers = t.layers, this.pendingElements = [], this.elements = Array.apply(null, { length: this.layers.length }), this.data.tm && (this.tm = PropertyFactory.getProp(this, this.data.tm, 0, r.frameRate, this.dynamicProperties)), (this.data.xt || !r.progressiveLoad) && this.buildAllItems();
  }function CVImageElement(t, e, r) {
    this.assetData = r.getAssetData(t.refId), this._parent.constructor.call(this, t, e, r), this.globalData.addPendingElement();
  }function CVMaskElement(t, e) {
    this.data = t, this.element = e, this.dynamicProperties = [], this.masksProperties = this.data.masksProperties, this.viewData = new Array(this.masksProperties.length);var r,
        s = this.masksProperties.length;for (r = 0; s > r; r++) {
      this.viewData[r] = ShapePropertyFactory.getShapeProp(this.element, this.masksProperties[r], 3, this.dynamicProperties, null);
    }
  }function CVShapeElement(t, e, r) {
    this.shapes = [], this.stylesList = [], this.viewData = [], this.shapeModifiers = [], this.shapesData = t.shapes, this.firstFrame = !0, this._parent.constructor.call(this, t, e, r);
  }function CVSolidElement(t, e, r) {
    this._parent.constructor.call(this, t, e, r);
  }function CVTextElement(t, e, r) {
    this.textSpans = [], this.yOffset = 0, this.fillColorAnim = !1, this.strokeColorAnim = !1, this.strokeWidthAnim = !1, this.stroke = !1, this.fill = !1, this.justifyOffset = 0, this.currentRender = null, this.renderType = "canvas", this.values = { fill: "rgba(0,0,0,0)", stroke: "rgba(0,0,0,0)", sWidth: 0, fValue: "" }, this._parent.constructor.call(this, t, e, r);
  }function HBaseElement(t, e, r, s, i) {
    this.globalData = r, this.comp = s, this.data = t, this.matteElement = null, this.parentContainer = e, this.layerId = i ? i.layerId : "ly_" + randomString(10), this.placeholder = i, this.init();
  }function HSolidElement(t, e, r, s, i) {
    this._parent.constructor.call(this, t, e, r, s, i);
  }function HCompElement(t, e, r, s, i) {
    this._parent.constructor.call(this, t, e, r, s, i), this.layers = t.layers, this.supports3d = !0, this.completeLayers = !1, this.pendingElements = [], this.elements = Array.apply(null, { length: this.layers.length }), this.data.tm && (this.tm = PropertyFactory.getProp(this, this.data.tm, 0, r.frameRate, this.dynamicProperties)), this.data.hasMask && (this.supports3d = !1), this.data.xt && (this.layerElement = document.createElement("div")), this.buildAllItems();
  }function HShapeElement(t, e, r, s, i) {
    this.shapes = [], this.shapeModifiers = [], this.shapesData = t.shapes, this.stylesList = [], this.viewData = [], this._parent.constructor.call(this, t, e, r, s, i), this.addedTransforms = { mdf: !1, mats: [this.finalTransform.mat] }, this.currentBBox = { x: 999999, y: -999999, h: 0, w: 0 };
  }function HTextElement(t, e, r, s, i) {
    this.textSpans = [], this.textPaths = [], this.currentBBox = { x: 999999, y: -999999, h: 0, w: 0 }, this.renderType = "svg", this.isMasked = !1, this._parent.constructor.call(this, t, e, r, s, i);
  }function HImageElement(t, e, r, s, i) {
    this.assetData = r.getAssetData(t.refId), this._parent.constructor.call(this, t, e, r, s, i);
  }function HCameraElement(t, e, r, s, i) {
    if (this._parent.constructor.call(this, t, e, r, s, i), this.pe = PropertyFactory.getProp(this, t.pe, 0, 0, this.dynamicProperties), t.ks.p.s ? (this.px = PropertyFactory.getProp(this, t.ks.p.x, 1, 0, this.dynamicProperties), this.py = PropertyFactory.getProp(this, t.ks.p.y, 1, 0, this.dynamicProperties), this.pz = PropertyFactory.getProp(this, t.ks.p.z, 1, 0, this.dynamicProperties)) : this.p = PropertyFactory.getProp(this, t.ks.p, 1, 0, this.dynamicProperties), t.ks.a && (this.a = PropertyFactory.getProp(this, t.ks.a, 1, 0, this.dynamicProperties)), t.ks.or.k.length) {
      var a,
          n = t.ks.or.k.length;for (a = 0; n > a; a += 1) {
        t.ks.or.k[a].to = null, t.ks.or.k[a].ti = null;
      }
    }this.or = PropertyFactory.getProp(this, t.ks.or, 1, degToRads, this.dynamicProperties), this.or.sh = !0, this.rx = PropertyFactory.getProp(this, t.ks.rx, 0, degToRads, this.dynamicProperties), this.ry = PropertyFactory.getProp(this, t.ks.ry, 0, degToRads, this.dynamicProperties), this.rz = PropertyFactory.getProp(this, t.ks.rz, 0, degToRads, this.dynamicProperties), this.mat = new Matrix();
  }function SliderEffect(t, e, r) {
    this.p = PropertyFactory.getProp(e, t.v, 0, 0, r);
  }function AngleEffect(t, e, r) {
    this.p = PropertyFactory.getProp(e, t.v, 0, 0, r);
  }function ColorEffect(t, e, r) {
    this.p = PropertyFactory.getProp(e, t.v, 1, 0, r);
  }function PointEffect(t, e, r) {
    this.p = PropertyFactory.getProp(e, t.v, 1, 0, r);
  }function LayerIndexEffect(t, e, r) {
    this.p = PropertyFactory.getProp(e, t.v, 0, 0, r);
  }function MaskIndexEffect(t, e, r) {
    this.p = PropertyFactory.getProp(e, t.v, 0, 0, r);
  }function CheckboxEffect(t, e, r) {
    this.p = PropertyFactory.getProp(e, t.v, 0, 0, r);
  }function NoValueEffect() {
    this.p = {};
  }function EffectsManager(t, e, r) {
    var s = t.ef;this.effectElements = [];var i,
        a,
        n = s.length;for (i = 0; n > i; i++) {
      a = new GroupEffect(s[i], e, r), this.effectElements.push(a);
    }
  }function GroupEffect(t, e, r) {
    this.dynamicProperties = [], this.init(t, e, this.dynamicProperties), this.dynamicProperties.length && r.push(this);
  }function play(t) {
    animationManager.play(t);
  }function pause(t) {
    animationManager.pause(t);
  }function togglePause(t) {
    animationManager.togglePause(t);
  }function setSpeed(t, e) {
    animationManager.setSpeed(t, e);
  }function setDirection(t, e) {
    animationManager.setDirection(t, e);
  }function stop(t) {
    animationManager.stop(t);
  }function moveFrame(t) {
    animationManager.moveFrame(t);
  }function searchAnimations() {
    standalone === !0 ? animationManager.searchAnimations(animationData, standalone, renderer) : animationManager.searchAnimations();
  }function registerAnimation(t) {
    return animationManager.registerAnimation(t);
  }function resize() {
    animationManager.resize();
  }function start() {
    animationManager.start();
  }function goToAndStop(t, e, r) {
    animationManager.goToAndStop(t, e, r);
  }function setSubframeRendering(t) {
    subframeEnabled = t;
  }function loadAnimation(t) {
    return standalone === !0 && (t.animationData = JSON.parse(animationData)), animationManager.loadAnimation(t);
  }function destroy(t) {
    return animationManager.destroy(t);
  }function setQuality(t) {
    if ("string" == typeof t) switch (t) {case "high":
        defaultCurveSegments = 200;break;case "medium":
        defaultCurveSegments = 50;break;case "low":
        defaultCurveSegments = 10;} else !isNaN(t) && t > 1 && (defaultCurveSegments = t);roundValues(defaultCurveSegments >= 50 ? !1 : !0);
  }function installPlugin(t, e) {
    "expressions" === t && (expressionsPlugin = e);
  }function getFactory(t) {
    switch (t) {case "propertyFactory":
        return PropertyFactory;case "shapePropertyFactory":
        return ShapePropertyFactory;case "matrix":
        return Matrix;}
  }function checkReady() {
    "complete" === document.readyState && (clearInterval(readyStateCheckInterval), searchAnimations());
  }function getQueryVariable(t) {
    for (var e = queryString.split("&"), r = 0; r < e.length; r++) {
      var s = e[r].split("=");if (decodeURIComponent(s[0]) == t) return decodeURIComponent(s[1]);
    }
  }var svgNS = "http://www.w3.org/2000/svg",
      subframeEnabled = !0,
      expressionsPlugin,
      isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent),
      cachedColors = {},
      bm_rounder = Math.round,
      bm_rnd,
      bm_pow = Math.pow,
      bm_sqrt = Math.sqrt,
      bm_abs = Math.abs,
      bm_floor = Math.floor,
      bm_max = Math.max,
      bm_min = Math.min,
      blitter = 10,
      BMMath = {};!function () {
    var t,
        e = Object.getOwnPropertyNames(Math),
        r = e.length;for (t = 0; r > t; t += 1) {
      BMMath[e[t]] = Math[e[t]];
    }
  }(), BMMath.random = Math.random, BMMath.abs = function (t) {
    var e = typeof t === "undefined" ? "undefined" : _typeof(t);if ("object" === e && t.length) {
      var r,
          s = Array.apply(null, { length: t.length }),
          i = t.length;for (r = 0; i > r; r += 1) {
        s[r] = Math.abs(t[r]);
      }return s;
    }return Math.abs(t);
  };var defaultCurveSegments = 150,
      degToRads = Math.PI / 180,
      roundCorner = .5519;roundValues(!1);var rgbToHex = function () {
    var t,
        e,
        r = [];for (t = 0; 256 > t; t += 1) {
      e = t.toString(16), r[t] = 1 == e.length ? "0" + e : e;
    }return function (t, e, s) {
      return 0 > t && (t = 0), 0 > e && (e = 0), 0 > s && (s = 0), "#" + r[t] + r[e] + r[s];
    };
  }(),
      fillColorToString = function () {
    var t = [];return function (e, r) {
      return void 0 !== r && (e[3] = r), t[e[0]] || (t[e[0]] = {}), t[e[0]][e[1]] || (t[e[0]][e[1]] = {}), t[e[0]][e[1]][e[2]] || (t[e[0]][e[1]][e[2]] = {}), t[e[0]][e[1]][e[2]][e[3]] || (t[e[0]][e[1]][e[2]][e[3]] = "rgba(" + e.join(",") + ")"), t[e[0]][e[1]][e[2]][e[3]];
    };
  }(),
      Matrix = function () {
    function t() {
      return this.props[0] = 1, this.props[1] = 0, this.props[2] = 0, this.props[3] = 0, this.props[4] = 0, this.props[5] = 1, this.props[6] = 0, this.props[7] = 0, this.props[8] = 0, this.props[9] = 0, this.props[10] = 1, this.props[11] = 0, this.props[12] = 0, this.props[13] = 0, this.props[14] = 0, this.props[15] = 1, this;
    }function e(t) {
      if (0 === t) return this;var e = Math.cos(t),
          r = Math.sin(t);return this._t(e, -r, 0, 0, r, e, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }function r(t) {
      if (0 === t) return this;var e = Math.cos(t),
          r = Math.sin(t);return this._t(1, 0, 0, 0, 0, e, -r, 0, 0, r, e, 0, 0, 0, 0, 1);
    }function s(t) {
      if (0 === t) return this;var e = Math.cos(t),
          r = Math.sin(t);return this._t(e, 0, r, 0, 0, 1, 0, 0, -r, 0, e, 0, 0, 0, 0, 1);
    }function i(t) {
      if (0 === t) return this;var e = Math.cos(t),
          r = Math.sin(t);return this._t(e, -r, 0, 0, r, e, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }function a(t, e) {
      return this._t(1, e, t, 1, 0, 0);
    }function n(t, e) {
      return this.shear(Math.tan(t), Math.tan(e));
    }function o(t, e) {
      var r = Math.cos(e),
          s = Math.sin(e);return this._t(r, s, 0, 0, -s, r, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)._t(1, 0, 0, 0, Math.tan(t), 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)._t(r, -s, 0, 0, s, r, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }function h(t, e, r) {
      return r = isNaN(r) ? 1 : r, 1 == t && 1 == e && 1 == r ? this : this._t(t, 0, 0, 0, 0, e, 0, 0, 0, 0, r, 0, 0, 0, 0, 1);
    }function l(t, e, r, s, i, a, n, o, h, l, p, m, f, c, d, u) {
      return this.props[0] = t, this.props[1] = e, this.props[2] = r, this.props[3] = s, this.props[4] = i, this.props[5] = a, this.props[6] = n, this.props[7] = o, this.props[8] = h, this.props[9] = l, this.props[10] = p, this.props[11] = m, this.props[12] = f, this.props[13] = c, this.props[14] = d, this.props[15] = u, this;
    }function p(t, e, r) {
      return r = r || 0, 0 !== t || 0 !== e || 0 !== r ? this._t(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, t, e, r, 1) : this;
    }function m(t, e, r, s, i, a, n, o, h, l, p, m, f, c, d, u) {
      if (1 === t && 0 === e && 0 === r && 0 === s && 0 === i && 1 === a && 0 === n && 0 === o && 0 === h && 0 === l && 1 === p && 0 === m) return (0 !== f || 0 !== c || 0 !== d) && (this.props[12] = this.props[12] * t + this.props[13] * i + this.props[14] * h + this.props[15] * f, this.props[13] = this.props[12] * e + this.props[13] * a + this.props[14] * l + this.props[15] * c, this.props[14] = this.props[12] * r + this.props[13] * n + this.props[14] * p + this.props[15] * d, this.props[15] = this.props[12] * s + this.props[13] * o + this.props[14] * m + this.props[15] * u), this;var y = this.props[0],
          g = this.props[1],
          v = this.props[2],
          b = this.props[3],
          E = this.props[4],
          P = this.props[5],
          x = this.props[6],
          S = this.props[7],
          C = this.props[8],
          k = this.props[9],
          M = this.props[10],
          A = this.props[11],
          D = this.props[12],
          T = this.props[13],
          w = this.props[14],
          F = this.props[15];return this.props[0] = y * t + g * i + v * h + b * f, this.props[1] = y * e + g * a + v * l + b * c, this.props[2] = y * r + g * n + v * p + b * d, this.props[3] = y * s + g * o + v * m + b * u, this.props[4] = E * t + P * i + x * h + S * f, this.props[5] = E * e + P * a + x * l + S * c, this.props[6] = E * r + P * n + x * p + S * d, this.props[7] = E * s + P * o + x * m + S * u, this.props[8] = C * t + k * i + M * h + A * f, this.props[9] = C * e + k * a + M * l + A * c, this.props[10] = C * r + k * n + M * p + A * d, this.props[11] = C * s + k * o + M * m + A * u, this.props[12] = D * t + T * i + w * h + F * f, this.props[13] = D * e + T * a + w * l + F * c, this.props[14] = D * r + T * n + w * p + F * d, this.props[15] = D * s + T * o + w * m + F * u, this;
    }function f(t) {
      var e;for (e = 0; 16 > e; e += 1) {
        t.props[e] = this.props[e];
      }
    }function c(t) {
      var e;for (e = 0; 16 > e; e += 1) {
        this.props[e] = t[e];
      }
    }function d(t, e, r) {
      return { x: t * this.props[0] + e * this.props[4] + r * this.props[8] + this.props[12], y: t * this.props[1] + e * this.props[5] + r * this.props[9] + this.props[13], z: t * this.props[2] + e * this.props[6] + r * this.props[10] + this.props[14] };
    }function u(t, e, r) {
      return t * this.props[0] + e * this.props[4] + r * this.props[8] + this.props[12];
    }function y(t, e, r) {
      return t * this.props[1] + e * this.props[5] + r * this.props[9] + this.props[13];
    }function g(t, e, r) {
      return t * this.props[2] + e * this.props[6] + r * this.props[10] + this.props[14];
    }function v(t) {
      var e,
          r = this.props[0] * this.props[5] - this.props[1] * this.props[4],
          s = this.props[5] / r,
          i = -this.props[1] / r,
          a = -this.props[4] / r,
          n = this.props[0] / r,
          o = (this.props[4] * this.props[13] - this.props[5] * this.props[12]) / r,
          h = -(this.props[0] * this.props[13] - this.props[1] * this.props[12]) / r,
          l = t.length,
          p = [];for (e = 0; l > e; e += 1) {
        p[e] = [t[e][0] * s + t[e][1] * a + o, t[e][0] * i + t[e][1] * n + h, 0];
      }return p;
    }function b(t, e, r, s) {
      if (s && 2 === s) {
        var i = point_pool.newPoint();return i[0] = t * this.props[0] + e * this.props[4] + r * this.props[8] + this.props[12], i[1] = t * this.props[1] + e * this.props[5] + r * this.props[9] + this.props[13], i;
      }return [t * this.props[0] + e * this.props[4] + r * this.props[8] + this.props[12], t * this.props[1] + e * this.props[5] + r * this.props[9] + this.props[13], t * this.props[2] + e * this.props[6] + r * this.props[10] + this.props[14]];
    }function E(t, e) {
      return bm_rnd(t * this.props[0] + e * this.props[4] + this.props[12]) + "," + bm_rnd(t * this.props[1] + e * this.props[5] + this.props[13]);
    }function P() {
      return [this.props[0], this.props[1], this.props[2], this.props[3], this.props[4], this.props[5], this.props[6], this.props[7], this.props[8], this.props[9], this.props[10], this.props[11], this.props[12], this.props[13], this.props[14], this.props[15]];
    }function x() {
      return isSafari ? "matrix3d(" + roundTo2Decimals(this.props[0]) + "," + roundTo2Decimals(this.props[1]) + "," + roundTo2Decimals(this.props[2]) + "," + roundTo2Decimals(this.props[3]) + "," + roundTo2Decimals(this.props[4]) + "," + roundTo2Decimals(this.props[5]) + "," + roundTo2Decimals(this.props[6]) + "," + roundTo2Decimals(this.props[7]) + "," + roundTo2Decimals(this.props[8]) + "," + roundTo2Decimals(this.props[9]) + "," + roundTo2Decimals(this.props[10]) + "," + roundTo2Decimals(this.props[11]) + "," + roundTo2Decimals(this.props[12]) + "," + roundTo2Decimals(this.props[13]) + "," + roundTo2Decimals(this.props[14]) + "," + roundTo2Decimals(this.props[15]) + ")" : (this.cssParts[1] = this.props.join(","), this.cssParts.join(""));
    }function S() {
      return "matrix(" + this.props[0] + "," + this.props[1] + "," + this.props[4] + "," + this.props[5] + "," + this.props[12] + "," + this.props[13] + ")";
    }function C() {
      return "" + this.toArray();
    }return function () {
      this.reset = t, this.rotate = e, this.rotateX = r, this.rotateY = s, this.rotateZ = i, this.skew = n, this.skewFromAxis = o, this.shear = a, this.scale = h, this.setTransform = l, this.translate = p, this.transform = m, this.applyToPoint = d, this.applyToX = u, this.applyToY = y, this.applyToZ = g, this.applyToPointArray = b, this.applyToPointStringified = E, this.toArray = P, this.toCSS = x, this.to2dCSS = S, this.toString = C, this.clone = f, this.cloneFromProps = c, this.inversePoints = v, this._t = this.transform, this.props = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1], this.cssParts = ["matrix3d(", "", ")"];
    };
  }();!function (t, e) {
    function r(r, l, p) {
      var c = [];l = 1 == l ? { entropy: !0 } : l || {};var v = n(a(l.entropy ? [r, h(t)] : null == r ? o() : r, 3), c),
          b = new s(c),
          E = function E() {
        for (var t = b.g(f), e = u, r = 0; y > t;) {
          t = (t + r) * m, e *= m, r = b.g(1);
        }for (; t >= g;) {
          t /= 2, e /= 2, r >>>= 1;
        }return (t + r) / e;
      };return E.int32 = function () {
        return 0 | b.g(4);
      }, E.quick = function () {
        return b.g(4) / 4294967296;
      }, E["double"] = E, n(h(b.S), t), (l.pass || p || function (t, r, s, a) {
        return a && (a.S && i(a, b), t.state = function () {
          return i(b, {});
        }), s ? (e[d] = t, r) : t;
      })(E, v, "global" in l ? l.global : this == e, l.state);
    }function s(t) {
      var e,
          r = t.length,
          s = this,
          i = 0,
          a = s.i = s.j = 0,
          n = s.S = [];for (r || (t = [r++]); m > i;) {
        n[i] = i++;
      }for (i = 0; m > i; i++) {
        n[i] = n[a = v & a + t[i % r] + (e = n[i])], n[a] = e;
      }(s.g = function (t) {
        for (var e, r = 0, i = s.i, a = s.j, n = s.S; t--;) {
          e = n[i = v & i + 1], r = r * m + n[v & (n[i] = n[a = v & a + e]) + (n[a] = e)];
        }return s.i = i, s.j = a, r;
      })(m);
    }function i(t, e) {
      return e.i = t.i, e.j = t.j, e.S = t.S.slice(), e;
    }function a(t, e) {
      var r,
          s = [],
          i = typeof t === "undefined" ? "undefined" : _typeof(t);if (e && "object" == i) for (r in t) {
        try {
          s.push(a(t[r], e - 1));
        } catch (n) {}
      }return s.length ? s : "string" == i ? t : t + "\x00";
    }function n(t, e) {
      for (var r, s = t + "", i = 0; i < s.length;) {
        e[v & i] = v & (r ^= 19 * e[v & i]) + s.charCodeAt(i++);
      }return h(e);
    }function o() {
      try {
        if (l) return h(l.randomBytes(m));var e = new Uint8Array(m);return (p.crypto || p.msCrypto).getRandomValues(e), h(e);
      } catch (r) {
        var s = p.navigator,
            i = s && s.plugins;return [+new Date(), p, i, p.screen, h(t)];
      }
    }function h(t) {
      return String.fromCharCode.apply(0, t);
    }var l,
        p = this,
        m = 256,
        f = 6,
        c = 52,
        d = "random",
        u = e.pow(m, f),
        y = e.pow(2, c),
        g = 2 * y,
        v = m - 1;e["seed" + d] = r, n(e.random(), t);
  }([], BMMath);var BezierFactory = function () {
    function t(t, e, r, s, i) {
      var a = i || ("bez_" + t + "_" + e + "_" + r + "_" + s).replace(/\./g, "p");if (p[a]) return p[a];var n = new h([t, e, r, s]);return p[a] = n, n;
    }function e(t, e) {
      return 1 - 3 * e + 3 * t;
    }function r(t, e) {
      return 3 * e - 6 * t;
    }function s(t) {
      return 3 * t;
    }function i(t, i, a) {
      return ((e(i, a) * t + r(i, a)) * t + s(i)) * t;
    }function a(t, i, a) {
      return 3 * e(i, a) * t * t + 2 * r(i, a) * t + s(i);
    }function n(t, e, r, s, a) {
      var n,
          o,
          h = 0;do {
        o = e + (r - e) / 2, n = i(o, s, a) - t, n > 0 ? r = o : e = o;
      } while (Math.abs(n) > c && ++h < d);return o;
    }function o(t, e, r, s) {
      for (var n = 0; m > n; ++n) {
        var o = a(e, r, s);if (0 === o) return e;var h = i(e, r, s) - t;e -= h / o;
      }return e;
    }function h(t) {
      this._p = t, this._mSampleValues = g ? new Float32Array(u) : new Array(u), this._precomputed = !1, this.get = this.get.bind(this);
    }var l = {};l.getBezierEasing = t;var p = {},
        m = 4,
        f = .001,
        c = 1e-7,
        d = 10,
        u = 11,
        y = 1 / (u - 1),
        g = "function" == typeof Float32Array;return h.prototype = { get: function get(t) {
        var e = this._p[0],
            r = this._p[1],
            s = this._p[2],
            a = this._p[3];return this._precomputed || this._precompute(), e === r && s === a ? t : 0 === t ? 0 : 1 === t ? 1 : i(this._getTForX(t), r, a);
      }, _precompute: function _precompute() {
        var t = this._p[0],
            e = this._p[1],
            r = this._p[2],
            s = this._p[3];this._precomputed = !0, (t !== e || r !== s) && this._calcSampleValues();
      }, _calcSampleValues: function _calcSampleValues() {
        for (var t = this._p[0], e = this._p[2], r = 0; u > r; ++r) {
          this._mSampleValues[r] = i(r * y, t, e);
        }
      }, _getTForX: function _getTForX(t) {
        for (var e = this._p[0], r = this._p[2], s = this._mSampleValues, i = 0, h = 1, l = u - 1; h !== l && s[h] <= t; ++h) {
          i += y;
        }--h;var p = (t - s[h]) / (s[h + 1] - s[h]),
            m = i + p * y,
            c = a(m, e, r);return c >= f ? o(t, m, e, r) : 0 === c ? m : n(t, i, i + y, e, r);
      } }, l;
  }(),
      MatrixManager = matrixManagerFunction;!function () {
    for (var t = 0, e = ["ms", "moz", "webkit", "o"], r = 0; r < e.length && !window.requestAnimationFrame; ++r) {
      window.requestAnimationFrame = window[e[r] + "RequestAnimationFrame"], window.cancelAnimationFrame = window[e[r] + "CancelAnimationFrame"] || window[e[r] + "CancelRequestAnimationFrame"];
    }window.requestAnimationFrame || (window.requestAnimationFrame = function (e, r) {
      var s = new Date().getTime(),
          i = Math.max(0, 16 - (s - t)),
          a = window.setTimeout(function () {
        e(s + i);
      }, i);return t = s + i, a;
    }), window.cancelAnimationFrame || (window.cancelAnimationFrame = function (t) {
      clearTimeout(t);
    });
  }();var bez = bezFunction(),
      dataManager = dataFunctionManager(),
      FontManager = function () {
    function t(t, e) {
      var r = document.createElement("span");r.style.fontFamily = e;var s = document.createElement("span");s.innerHTML = "giItT1WQy@!-/#", r.style.position = "absolute", r.style.left = "-10000px", r.style.top = "-10000px", r.style.fontSize = "300px", r.style.fontVariant = "normal", r.style.fontStyle = "normal", r.style.fontWeight = "normal", r.style.letterSpacing = "0", r.appendChild(s), document.body.appendChild(r);var i = s.offsetWidth;return s.style.fontFamily = t + ", " + e, { node: s, w: i, parent: r };
    }function e() {
      var t,
          r,
          s,
          i = this.fonts.length,
          a = i;for (t = 0; i > t; t += 1) {
        if (this.fonts[t].loaded) a -= 1;else if ("t" === this.fonts[t].fOrigin) {
          if (window.Typekit && window.Typekit.load && 0 === this.typekitLoaded) {
            this.typekitLoaded = 1;try {
              window.Typekit.load({ async: !0, active: function () {
                  this.typekitLoaded = 2;
                }.bind(this) });
            } catch (n) {}
          }2 === this.typekitLoaded && (this.fonts[t].loaded = !0);
        } else "n" === this.fonts[t].fOrigin ? this.fonts[t].loaded = !0 : (r = this.fonts[t].monoCase.node, s = this.fonts[t].monoCase.w, r.offsetWidth !== s ? (a -= 1, this.fonts[t].loaded = !0) : (r = this.fonts[t].sansCase.node, s = this.fonts[t].sansCase.w, r.offsetWidth !== s && (a -= 1, this.fonts[t].loaded = !0)), this.fonts[t].loaded && (this.fonts[t].sansCase.parent.parentNode.removeChild(this.fonts[t].sansCase.parent), this.fonts[t].monoCase.parent.parentNode.removeChild(this.fonts[t].monoCase.parent)));
      }0 !== a && Date.now() - this.initTime < h ? setTimeout(e.bind(this), 20) : setTimeout(function () {
        this.loaded = !0;
      }.bind(this), 0);
    }function r(t, e) {
      var r = document.createElementNS(svgNS, "text");r.style.fontSize = "100px", r.style.fontFamily = e.fFamily, r.textContent = "1", e.fClass ? (r.style.fontFamily = "inherit", r.className = e.fClass) : r.style.fontFamily = e.fFamily, t.appendChild(r);var s = document.createElement("canvas").getContext("2d");return s.font = "100px " + e.fFamily, s;
    }function s(s, i) {
      if (!s) return void (this.loaded = !0);if (this.chars) return this.loaded = !0, void (this.fonts = s.list);var a,
          n = s.list,
          o = n.length;for (a = 0; o > a; a += 1) {
        if (n[a].loaded = !1, n[a].monoCase = t(n[a].fFamily, "monospace"), n[a].sansCase = t(n[a].fFamily, "sans-serif"), n[a].fPath) {
          if ("p" === n[a].fOrigin) {
            var h = document.createElement("style");h.type = "text/css", h.innerHTML = "@font-face {font-family: " + n[a].fFamily + "; font-style: normal; src: url('" + n[a].fPath + "');}", i.appendChild(h);
          } else if ("g" === n[a].fOrigin) {
            var l = document.createElement("link");l.type = "text/css", l.rel = "stylesheet", l.href = n[a].fPath, i.appendChild(l);
          } else if ("t" === n[a].fOrigin) {
            var p = document.createElement("script");p.setAttribute("src", n[a].fPath), i.appendChild(p);
          }
        } else n[a].loaded = !0;n[a].helper = r(i, n[a]), this.fonts.push(n[a]);
      }e.bind(this)();
    }function i(t) {
      if (t) {
        this.chars || (this.chars = []);var e,
            r,
            s,
            i = t.length,
            a = this.chars.length;for (e = 0; i > e; e += 1) {
          for (r = 0, s = !1; a > r;) {
            this.chars[r].style === t[e].style && this.chars[r].fFamily === t[e].fFamily && this.chars[r].ch === t[e].ch && (s = !0), r += 1;
          }s || (this.chars.push(t[e]), a += 1);
        }
      }
    }function a(t, e, r) {
      for (var s = 0, i = this.chars.length; i > s;) {
        if (this.chars[s].ch === t && this.chars[s].style === e && this.chars[s].fFamily === r) return this.chars[s];s += 1;
      }
    }function n(t, e, r) {
      var s = this.getFontByName(e),
          i = s.helper;return i.measureText(t).width * r / 100;
    }function o(t) {
      for (var e = 0, r = this.fonts.length; r > e;) {
        if (this.fonts[e].fName === t) return this.fonts[e];e += 1;
      }return "sans-serif";
    }var h = 5e3,
        l = function l() {
      this.fonts = [], this.chars = null, this.typekitLoaded = 0, this.loaded = !1, this.initTime = Date.now();
    };return l.prototype.addChars = i, l.prototype.addFonts = s, l.prototype.getCharData = a, l.prototype.getFontByName = o, l.prototype.measureText = n, l;
  }(),
      PropertyFactory = function () {
    function t() {
      if (this.elem.globalData.frameId !== this.frameId) {
        this.mdf = !1;var t = this.comp.renderedFrame - this.offsetTime;if (!(t === this.lastFrame || this.lastFrame !== l && (this.lastFrame >= this.keyframes[this.keyframes.length - 1].t - this.offsetTime && t >= this.keyframes[this.keyframes.length - 1].t - this.offsetTime || this.lastFrame < this.keyframes[0].t - this.offsetTime && t < this.keyframes[0].t - this.offsetTime))) {
          for (var e, r, s = this.lastFrame < t ? this._lastIndex : 0, i = this.keyframes.length - 1, a = !0; a;) {
            if (e = this.keyframes[s], r = this.keyframes[s + 1], s == i - 1 && t >= r.t - this.offsetTime) {
              e.h && (e = r);break;
            }if (r.t - this.offsetTime > t) break;i - 1 > s ? s += 1 : a = !1;
          }this._lastIndex = s;var n, o, h, p, m, f;if (e.to) {
            e.bezierData || bez.buildBezierData(e);var c = e.bezierData;if (t >= r.t - this.offsetTime || t < e.t - this.offsetTime) {
              var d = t >= r.t - this.offsetTime ? c.points.length - 1 : 0;for (o = c.points[d].point.length, n = 0; o > n; n += 1) {
                this.pv[n] = c.points[d].point[n], this.v[n] = this.mult ? this.pv[n] * this.mult : this.pv[n], this.lastPValue[n] !== this.pv[n] && (this.mdf = !0, this.lastPValue[n] = this.pv[n]);
              }this._lastBezierData = null;
            } else {
              e.__fnct ? f = e.__fnct : (f = BezierFactory.getBezierEasing(e.o.x, e.o.y, e.i.x, e.i.y, e.n).get, e.__fnct = f), h = f((t - (e.t - this.offsetTime)) / (r.t - this.offsetTime - (e.t - this.offsetTime)));var u,
                  y = c.segmentLength * h,
                  g = this.lastFrame < t && this._lastBezierData === c ? this._lastAddedLength : 0;for (m = this.lastFrame < t && this._lastBezierData === c ? this._lastPoint : 0, a = !0, p = c.points.length; a;) {
                if (g += c.points[m].partialLength, 0 === y || 0 === h || m == c.points.length - 1) {
                  for (o = c.points[m].point.length, n = 0; o > n; n += 1) {
                    this.pv[n] = c.points[m].point[n], this.v[n] = this.mult ? this.pv[n] * this.mult : this.pv[n], this.lastPValue[n] !== this.pv[n] && (this.mdf = !0, this.lastPValue[n] = this.pv[n]);
                  }break;
                }if (y >= g && y < g + c.points[m + 1].partialLength) {
                  for (u = (y - g) / c.points[m + 1].partialLength, o = c.points[m].point.length, n = 0; o > n; n += 1) {
                    this.pv[n] = c.points[m].point[n] + (c.points[m + 1].point[n] - c.points[m].point[n]) * u, this.v[n] = this.mult ? this.pv[n] * this.mult : this.pv[n], this.lastPValue[n] !== this.pv[n] && (this.mdf = !0, this.lastPValue[n] = this.pv[n]);
                  }break;
                }p - 1 > m ? m += 1 : a = !1;
              }this._lastPoint = m, this._lastAddedLength = g - c.points[m].partialLength, this._lastBezierData = c;
            }
          } else {
            var v, b, E, P, x;for (i = e.s.length, s = 0; i > s; s += 1) {
              if (1 !== e.h && (t >= r.t - this.offsetTime ? h = 1 : t < e.t - this.offsetTime ? h = 0 : (e.o.x instanceof Array ? (e.__fnct || (e.__fnct = []), e.__fnct[s] ? f = e.__fnct[s] : (v = e.o.x[s] || e.o.x[0], b = e.o.y[s] || e.o.y[0], E = e.i.x[s] || e.i.x[0], P = e.i.y[s] || e.i.y[0], f = BezierFactory.getBezierEasing(v, b, E, P).get, e.__fnct[s] = f)) : e.__fnct ? f = e.__fnct : (v = e.o.x, b = e.o.y, E = e.i.x, P = e.i.y, f = BezierFactory.getBezierEasing(v, b, E, P).get, e.__fnct = f), h = f((t - (e.t - this.offsetTime)) / (r.t - this.offsetTime - (e.t - this.offsetTime))))), this.sh && 1 !== e.h) {
                var S = e.s[s],
                    C = e.e[s];-180 > S - C ? S += 360 : S - C > 180 && (S -= 360), x = S + (C - S) * h;
              } else x = 1 === e.h ? e.s[s] : e.s[s] + (e.e[s] - e.s[s]) * h;1 === i ? (this.v = this.mult ? x * this.mult : x, this.pv = x, this.lastPValue != this.pv && (this.mdf = !0, this.lastPValue = this.pv)) : (this.v[s] = this.mult ? x * this.mult : x, this.pv[s] = x, this.lastPValue[s] !== this.pv[s] && (this.mdf = !0, this.lastPValue[s] = this.pv[s]));
            }
          }
        }this.lastFrame = t, this.frameId = this.elem.globalData.frameId;
      }
    }function e() {}function r(t, r, s) {
      this.mult = s, this.v = s ? r.k * s : r.k, this.pv = r.k, this.mdf = !1, this.comp = t.comp, this.k = !1, this.kf = !1, this.vel = 0, this.getValue = e;
    }function s(t, r, s) {
      this.mult = s, this.data = r, this.mdf = !1, this.comp = t.comp, this.k = !1, this.kf = !1, this.frameId = -1, this.v = Array.apply(null, { length: r.k.length }), this.pv = Array.apply(null, { length: r.k.length }), this.lastValue = Array.apply(null, { length: r.k.length });var i = Array.apply(null, { length: r.k.length });this.vel = i.map(function () {
        return 0;
      });var a,
          n = r.k.length;for (a = 0; n > a; a += 1) {
        this.v[a] = s ? r.k[a] * s : r.k[a], this.pv[a] = r.k[a];
      }this.getValue = e;
    }function i(e, r, s) {
      this.keyframes = r.k, this.offsetTime = e.data.st, this.lastValue = -99999, this.lastPValue = -99999, this.frameId = -1, this._lastIndex = 0, this.k = !0, this.kf = !0, this.data = r, this.mult = s, this.elem = e, this.comp = e.comp, this.lastFrame = l, this.v = s ? r.k[0].s[0] * s : r.k[0].s[0], this.pv = r.k[0].s[0], this.getValue = t;
    }function a(e, r, s) {
      var i,
          a,
          n,
          o,
          h,
          p = r.k.length;for (i = 0; p - 1 > i; i += 1) {
        r.k[i].to && r.k[i].s && r.k[i].e && (a = r.k[i].s, n = r.k[i].e, o = r.k[i].to, h = r.k[i].ti, (2 === a.length && (a[0] !== n[0] || a[1] !== n[1]) && bez.pointOnLine2D(a[0], a[1], n[0], n[1], a[0] + o[0], a[1] + o[1]) && bez.pointOnLine2D(a[0], a[1], n[0], n[1], n[0] + h[0], n[1] + h[1]) || 3 === a.length && (a[0] !== n[0] || a[1] !== n[1] || a[2] !== n[2]) && bez.pointOnLine3D(a[0], a[1], a[2], n[0], n[1], n[2], a[0] + o[0], a[1] + o[1], a[2] + o[2]) && bez.pointOnLine3D(a[0], a[1], a[2], n[0], n[1], n[2], n[0] + h[0], n[1] + h[1], n[2] + h[2])) && (r.k[i].to = null, r.k[i].ti = null));
      }this.keyframes = r.k, this.offsetTime = e.data.st, this.k = !0, this.kf = !0, this.mult = s, this.elem = e, this.comp = e.comp, this.getValue = t, this.frameId = -1, this._lastIndex = 0, this.v = Array.apply(null, { length: r.k[0].s.length }), this.pv = Array.apply(null, { length: r.k[0].s.length }), this.lastValue = Array.apply(null, { length: r.k[0].s.length }), this.lastPValue = Array.apply(null, { length: r.k[0].s.length }), this.lastFrame = l;
    }function n(t, e, n, o, h) {
      var l;if (2 === n) l = new p(t, e, h);else if (0 === e.a) l = 0 === n ? new r(t, e, o) : new s(t, e, o);else if (1 === e.a) l = 0 === n ? new i(t, e, o) : new a(t, e, o);else if (e.k.length) {
        if ("number" == typeof e.k[0]) l = new s(t, e, o);else switch (n) {case 0:
            l = new i(t, e, o);break;case 1:
            l = new a(t, e, o);}
      } else l = new r(t, e, o);return l.k && h.push(l), l;
    }function o(t, e, r, s) {
      return new f(t, e, r, s);
    }function h(t, e, r) {
      return new c(t, e, r);
    }var l = -999999,
        p = function () {
      function t() {
        return ExpressionValue(this.p);
      }function e() {
        return ExpressionValue(this.px);
      }function r() {
        return ExpressionValue(this.py);
      }function s() {
        return ExpressionValue(this.a);
      }function i() {
        return ExpressionValue(this.or);
      }function a() {
        return ExpressionValue(this.r, 1 / degToRads);
      }function n() {
        return ExpressionValue(this.s, 100);
      }function o() {
        return ExpressionValue(this.o, 100);
      }function h() {
        return ExpressionValue(this.sk);
      }function l() {
        return ExpressionValue(this.sa);
      }function p(t) {
        var e,
            r = this.dynamicProperties.length;for (e = 0; r > e; e += 1) {
          this.dynamicProperties[e].getValue(), this.dynamicProperties[e].mdf && (this.mdf = !0);
        }this.a && t.translate(-this.a.v[0], -this.a.v[1], this.a.v[2]), this.s && t.scale(this.s.v[0], this.s.v[1], this.s.v[2]), this.r ? t.rotate(-this.r.v) : t.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]), this.data.p.s ? this.data.p.z ? t.translate(this.px.v, this.py.v, -this.pz.v) : t.translate(this.px.v, this.py.v, 0) : t.translate(this.p.v[0], this.p.v[1], -this.p.v[2]);
      }function m() {
        if (this.elem.globalData.frameId !== this.frameId) {
          this.mdf = !1;var t,
              e = this.dynamicProperties.length;for (t = 0; e > t; t += 1) {
            this.dynamicProperties[t].getValue(), this.dynamicProperties[t].mdf && (this.mdf = !0);
          }if (this.mdf) {
            if (this.v.reset(), this.a && this.v.translate(-this.a.v[0], -this.a.v[1], this.a.v[2]), this.s && this.v.scale(this.s.v[0], this.s.v[1], this.s.v[2]), this.sk && this.v.skewFromAxis(-this.sk.v, this.sa.v), this.r ? this.v.rotate(-this.r.v) : this.v.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]), this.autoOriented && this.p.keyframes && this.p.getValueAtTime) {
              var r, s;this.p.lastFrame + this.p.offsetTime <= this.p.keyframes[0].t ? (r = this.p.getValueAtTime((this.p.keyframes[0].t + .01) / this.elem.globalData.frameRate, 0), s = this.p.getValueAtTime(this.p.keyframes[0].t / this.elem.globalData.frameRate, 0)) : this.p.lastFrame + this.p.offsetTime >= this.p.keyframes[this.p.keyframes.length - 1].t ? (r = this.p.getValueAtTime(this.p.keyframes[this.p.keyframes.length - 1].t / this.elem.globalData.frameRate, 0), s = this.p.getValueAtTime((this.p.keyframes[this.p.keyframes.length - 1].t - .01) / this.elem.globalData.frameRate, 0)) : (r = this.p.pv, s = this.p.getValueAtTime((this.p.lastFrame + this.p.offsetTime - .01) / this.elem.globalData.frameRate, this.p.offsetTime)), this.v.rotate(-Math.atan2(r[1] - s[1], r[0] - s[0]));
            }this.data.p.s ? this.data.p.z ? this.v.translate(this.px.v, this.py.v, -this.pz.v) : this.v.translate(this.px.v, this.py.v, 0) : this.v.translate(this.p.v[0], this.p.v[1], -this.p.v[2]);
          }this.frameId = this.elem.globalData.frameId;
        }
      }function f() {
        this.inverted = !0, this.iv = new Matrix(), this.k || (this.data.p.s ? this.iv.translate(this.px.v, this.py.v, -this.pz.v) : this.iv.translate(this.p.v[0], this.p.v[1], -this.p.v[2]), this.r ? this.iv.rotate(-this.r.v) : this.iv.rotateX(-this.rx.v).rotateY(-this.ry.v).rotateZ(this.rz.v), this.s && this.iv.scale(this.s.v[0], this.s.v[1], 1), this.a && this.iv.translate(-this.a.v[0], -this.a.v[1], this.a.v[2]));
      }function c() {}return function (d, u, y) {
        this.elem = d, this.frameId = -1, this.type = "transform", this.dynamicProperties = [], this.mdf = !1, this.data = u, this.getValue = m, this.applyToMatrix = p, this.setInverted = f, this.autoOrient = c, this.v = new Matrix(), u.p.s ? (this.px = PropertyFactory.getProp(d, u.p.x, 0, 0, this.dynamicProperties), this.py = PropertyFactory.getProp(d, u.p.y, 0, 0, this.dynamicProperties), u.p.z && (this.pz = PropertyFactory.getProp(d, u.p.z, 0, 0, this.dynamicProperties))) : this.p = PropertyFactory.getProp(d, u.p, 1, 0, this.dynamicProperties), u.r ? this.r = PropertyFactory.getProp(d, u.r, 0, degToRads, this.dynamicProperties) : u.rx && (this.rx = PropertyFactory.getProp(d, u.rx, 0, degToRads, this.dynamicProperties), this.ry = PropertyFactory.getProp(d, u.ry, 0, degToRads, this.dynamicProperties), this.rz = PropertyFactory.getProp(d, u.rz, 0, degToRads, this.dynamicProperties), this.or = PropertyFactory.getProp(d, u.or, 1, degToRads, this.dynamicProperties)), u.sk && (this.sk = PropertyFactory.getProp(d, u.sk, 0, degToRads, this.dynamicProperties), this.sa = PropertyFactory.getProp(d, u.sa, 0, degToRads, this.dynamicProperties)), u.a && (this.a = PropertyFactory.getProp(d, u.a, 1, 0, this.dynamicProperties)), u.s && (this.s = PropertyFactory.getProp(d, u.s, 1, .01, this.dynamicProperties)), this.o = u.o ? PropertyFactory.getProp(d, u.o, 0, .01, y) : { mdf: !1, v: 1 }, this.dynamicProperties.length ? y.push(this) : (this.a && this.v.translate(-this.a.v[0], -this.a.v[1], this.a.v[2]), this.s && this.v.scale(this.s.v[0], this.s.v[1], this.s.v[2]), this.sk && this.v.skewFromAxis(-this.sk.v, this.sa.v), this.r ? this.v.rotate(-this.r.v) : this.v.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]), this.data.p.s ? u.p.z ? this.v.translate(this.px.v, this.py.v, -this.pz.v) : this.v.translate(this.px.v, this.py.v, 0) : this.v.translate(this.p.v[0], this.p.v[1], -this.p.v[2])), Object.defineProperty(this, "position", { get: t }), Object.defineProperty(this, "xPosition", { get: e }), Object.defineProperty(this, "yPosition", { get: r }), Object.defineProperty(this, "orientation", { get: i }), Object.defineProperty(this, "anchorPoint", { get: s }), Object.defineProperty(this, "rotation", { get: a }), Object.defineProperty(this, "scale", { get: n }), Object.defineProperty(this, "opacity", { get: o }), Object.defineProperty(this, "skew", { get: h }), Object.defineProperty(this, "skewAxis", { get: l });
      };
    }(),
        m = function () {
      function t(t) {
        if (this.prop.getValue(), this.cmdf = !1, this.omdf = !1, this.prop.mdf || t) {
          var e,
              r,
              s,
              i = 4 * this.data.p;for (e = 0; i > e; e += 1) {
            r = e % 4 === 0 ? 100 : 255, s = Math.round(this.prop.v[e] * r), this.c[e] !== s && (this.c[e] = s, this.cmdf = !0);
          }if (this.o.length) for (i = this.prop.v.length, e = 4 * this.data.p; i > e; e += 1) {
            r = e % 2 === 0 ? 100 : 1, s = e % 2 === 0 ? Math.round(100 * this.prop.v[e]) : this.prop.v[e], this.o[e - 4 * this.data.p] !== s && (this.o[e - 4 * this.data.p] = s, this.omdf = !0);
          }
        }
      }function e(e, r, s) {
        this.prop = n(e, r.k, 1, null, []), this.data = r, this.k = this.prop.k, this.c = Array.apply(null, { length: 4 * r.p });var i = r.k.k[0].s ? r.k.k[0].s.length - 4 * r.p : r.k.k.length - 4 * r.p;this.o = Array.apply(null, { length: i }), this.cmdf = !1, this.omdf = !1, this.getValue = t, this.prop.k && s.push(this), this.getValue(!0);
      }return function (t, r, s) {
        return new e(t, r, s);
      };
    }(),
        f = function () {
      function t(t) {
        var e = 0,
            r = this.dataProps.length;if (this.elem.globalData.frameId !== this.frameId || t) {
          for (this.mdf = !1, this.frameId = this.elem.globalData.frameId; r > e;) {
            if (this.dataProps[e].p.mdf) {
              this.mdf = !0;break;
            }e += 1;
          }if (this.mdf || t) for ("svg" === this.renderer && (this.dasharray = ""), e = 0; r > e; e += 1) {
            "o" != this.dataProps[e].n ? "svg" === this.renderer ? this.dasharray += " " + this.dataProps[e].p.v : this.dasharray[e] = this.dataProps[e].p.v : this.dashoffset = this.dataProps[e].p.v;
          }
        }
      }return function (e, r, s, i) {
        this.elem = e, this.frameId = -1, this.dataProps = new Array(r.length), this.renderer = s, this.mdf = !1, this.k = !1, this.dasharray = "svg" === this.renderer ? "" : new Array(r.length - 1), this.dashoffset = 0;var a,
            n,
            o = r.length;for (a = 0; o > a; a += 1) {
          n = PropertyFactory.getProp(e, r[a].v, 0, 0, i), this.k = n.k ? !0 : this.k, this.dataProps[a] = { n: r[a].n, p: n };
        }this.getValue = t, this.k ? i.push(this) : this.getValue(!0);
      };
    }(),
        c = function () {
      function t() {
        if (this.dynamicProperties.length) {
          var t,
              e = this.dynamicProperties.length;for (t = 0; e > t; t += 1) {
            this.dynamicProperties[t].getValue(), this.dynamicProperties[t].mdf && (this.mdf = !0);
          }
        }var r = this.data.totalChars,
            s = 2 === this.data.r ? 1 : 100 / r,
            i = this.o.v / s,
            a = this.s.v / s + i,
            n = this.e.v / s + i;if (a > n) {
          var o = a;a = n, n = o;
        }this.finalS = a, this.finalE = n;
      }function e(t) {
        var e = BezierFactory.getBezierEasing(this.ne.v / 100, 0, 1 - this.xe.v / 100, 1).get,
            a = 0,
            n = this.finalS,
            o = this.finalE,
            h = this.data.sh;if (2 == h) a = o === n ? t >= o ? 1 : 0 : r(0, s(.5 / (o - n) + (t - n) / (o - n), 1)), a = e(a);else if (3 == h) a = o === n ? t >= o ? 0 : 1 : 1 - r(0, s(.5 / (o - n) + (t - n) / (o - n), 1)), a = e(a);else if (4 == h) o === n ? a = 0 : (a = r(0, s(.5 / (o - n) + (t - n) / (o - n), 1)), .5 > a ? a *= 2 : a = 1 - 2 * (a - .5)), a = e(a);else if (5 == h) {
          if (o === n) a = 0;else {
            var l = o - n;t = s(r(0, t + .5 - n), o - n);var p = -l / 2 + t,
                m = l / 2;a = Math.sqrt(1 - p * p / (m * m));
          }a = e(a);
        } else 6 == h ? (o === n ? a = 0 : (t = s(r(0, t + .5 - n), o - n), a = (1 + Math.cos(Math.PI + 2 * Math.PI * t / (o - n))) / 2), a = e(a)) : (t >= i(n) && (a = 0 > t - n ? 1 - (n - t) : r(0, s(o - t, 1))), a = e(a));return a * this.a.v;
      }var r = Math.max,
          s = Math.min,
          i = Math.floor;return function (r, s, i) {
        this.mdf = !1, this.k = !1, this.data = s, this.dynamicProperties = [], this.getValue = t, this.getMult = e, this.comp = r.comp, this.finalS = 0, this.finalE = 0, this.s = PropertyFactory.getProp(r, s.s || { k: 0 }, 0, 0, this.dynamicProperties), this.e = "e" in s ? PropertyFactory.getProp(r, s.e, 0, 0, this.dynamicProperties) : { v: 2 === s.r ? s.totalChars : 100 }, this.o = PropertyFactory.getProp(r, s.o || { k: 0 }, 0, 0, this.dynamicProperties), this.xe = PropertyFactory.getProp(r, s.xe || { k: 0 }, 0, 0, this.dynamicProperties), this.ne = PropertyFactory.getProp(r, s.ne || { k: 0 }, 0, 0, this.dynamicProperties), this.a = PropertyFactory.getProp(r, s.a, 0, .01, this.dynamicProperties), this.dynamicProperties.length ? i.push(this) : this.getValue();
      };
    }(),
        d = {};return d.getProp = n, d.getDashProp = o, d.getTextSelectorProp = h, d.getGradientProp = m, d;
  }();ShapePath.prototype.setPathData = function (t, e) {
    for (this.c = t; e > this._maxLength;) {
      this.doubleArrayLength();
    }for (var r = 0; e > r;) {
      this.v[r] = point_pool.newPoint(), this.o[r] = point_pool.newPoint(), this.i[r] = point_pool.newPoint(), r += 1;
    }this._length = e;
  }, ShapePath.prototype.doubleArrayLength = function () {
    this.v = this.v.concat(Array.apply(null, { length: this._maxLength })), this.i = this.i.concat(Array.apply(null, { length: this._maxLength })), this.o = this.o.concat(Array.apply(null, { length: this._maxLength })), this._maxLength *= 2;
  }, ShapePath.prototype.setXYAt = function (t, e, r, s, i) {
    var a;switch (this._length = Math.max(this._length, s + 1), this._length >= this._maxLength && this.doubleArrayLength(), r) {case "v":
        a = this.v;break;case "i":
        a = this.i;break;case "o":
        a = this.o;}(!a[s] || a[s] && !i) && (a[s] = point_pool.newPoint()), a[s][0] = t, a[s][1] = e;
  }, ShapePath.prototype.setTripleAt = function (t, e, r, s, i, a, n, o) {
    this.setXYAt(t, e, "v", n, o), this.setXYAt(r, s, "o", n, o), this.setXYAt(i, a, "i", n, o);
  };var ShapePropertyFactory = function () {
    function t() {
      if (this.elem.globalData.frameId !== this.frameId) {
        this.mdf = !1;var t = this.comp.renderedFrame - this.offsetTime;if (this.lastFrame === n || !(this.lastFrame < this.keyframes[0].t - this.offsetTime && t < this.keyframes[0].t - this.offsetTime || this.lastFrame > this.keyframes[this.keyframes.length - 1].t - this.offsetTime && t > this.keyframes[this.keyframes.length - 1].t - this.offsetTime)) {
          var e, r, s;if (t < this.keyframes[0].t - this.offsetTime) e = this.keyframes[0].s[0], s = !0, this._lastIndex = 0;else if (t >= this.keyframes[this.keyframes.length - 1].t - this.offsetTime) e = 1 === this.keyframes[this.keyframes.length - 2].h ? this.keyframes[this.keyframes.length - 1].s[0] : this.keyframes[this.keyframes.length - 2].e[0], s = !0;else {
            for (var i, a, o, h, l, p, m = this.lastFrame < n ? this._lastIndex : 0, f = this.keyframes.length - 1, c = !0; c && (i = this.keyframes[m], a = this.keyframes[m + 1], !(a.t - this.offsetTime > t));) {
              f - 1 > m ? m += 1 : c = !1;
            }s = 1 === i.h, this._lastIndex = m;var d;if (!s) {
              if (t >= a.t - this.offsetTime) d = 1;else if (t < i.t - this.offsetTime) d = 0;else {
                var u;i.__fnct ? u = i.__fnct : (u = BezierFactory.getBezierEasing(i.o.x, i.o.y, i.i.x, i.i.y).get, i.__fnct = u), d = u((t - (i.t - this.offsetTime)) / (a.t - this.offsetTime - (i.t - this.offsetTime)));
              }r = i.e[0];
            }e = i.s[0];
          }h = this.v._length, p = e.i[0].length;var y,
              g = !1;for (o = 0; h > o; o += 1) {
            for (l = 0; p > l; l += 1) {
              s ? (y = e.i[o][l], this.v.i[o][l] !== y && (this.v.i[o][l] = y, this.pv.i[o][l] = y, g = !0), y = e.o[o][l], this.v.o[o][l] !== y && (this.v.o[o][l] = y, this.pv.o[o][l] = y, g = !0), y = e.v[o][l], this.v.v[o][l] !== y && (this.v.v[o][l] = y, this.pv.v[o][l] = y, g = !0)) : (y = e.i[o][l] + (r.i[o][l] - e.i[o][l]) * d, this.v.i[o][l] !== y && (this.v.i[o][l] = y, this.pv.i[o][l] = y, g = !0), y = e.o[o][l] + (r.o[o][l] - e.o[o][l]) * d, this.v.o[o][l] !== y && (this.v.o[o][l] = y, this.pv.o[o][l] = y, g = !0), y = e.v[o][l] + (r.v[o][l] - e.v[o][l]) * d, this.v.v[o][l] !== y && (this.v.v[o][l] = y, this.pv.v[o][l] = y, g = !0));
            }
          }this.mdf = g, this.v.c = e.c, this.paths = this.localShapeCollection;
        }this.lastFrame = t, this.frameId = this.elem.globalData.frameId;
      }
    }function e() {
      return this.v;
    }function r() {
      this.paths = this.localShapeCollection, this.k || (this.mdf = !1);
    }function s(t, s, i) {
      this.comp = t.comp, this.k = !1, this.mdf = !1, this.v = shape_pool.newShape();var a = 3 === i ? s.pt.k : s.ks.k;this.v.v = a.v, this.v.i = a.i, this.v.o = a.o, this.v.c = a.c, this.v._length = this.v.v.length, this.getValue = e, this.pv = shape_pool.clone(this.v), this.localShapeCollection = shapeCollection_pool.newShapeCollection(), this.paths = this.localShapeCollection, this.paths.addShape(this.v), this.reset = r;
    }function i(e, s, i) {
      this.comp = e.comp, this.elem = e, this.offsetTime = e.data.st, this._lastIndex = 0, this.getValue = t, this.keyframes = 3 === i ? s.pt.k : s.ks.k, this.k = !0;{
        var a = this.keyframes[0].s[0].i.length;this.keyframes[0].s[0].i[0].length;
      }this.v = shape_pool.newShape(), this.v.setPathData(this.keyframes[0].s[0].c, a), this.pv = shape_pool.clone(this.v), this.localShapeCollection = shapeCollection_pool.newShapeCollection(), this.paths = this.localShapeCollection, this.paths.addShape(this.v), this.lastFrame = n, this.reset = r;
    }function a(t, e, r, a) {
      var n;if (3 === r || 4 === r) {
        var p = 3 === r ? e.pt : e.ks,
            m = p.k;n = 1 === p.a || m.length ? new i(t, e, r) : new s(t, e, r);
      } else 5 === r ? n = new l(t, e) : 6 === r ? n = new o(t, e) : 7 === r && (n = new h(t, e));return n.k && a.push(n), n;
    }var n = -999999,
        o = function () {
      function t() {
        var t = this.p.v[0],
            e = this.p.v[1],
            r = this.s.v[0] / 2,
            i = this.s.v[1] / 2;3 !== this.d ? (this.v.v[0][0] = t, this.v.v[0][1] = e - i, this.v.v[1][0] = t + r, this.v.v[1][1] = e, this.v.v[2][0] = t, this.v.v[2][1] = e + i, this.v.v[3][0] = t - r, this.v.v[3][1] = e, this.v.i[0][0] = t - r * s, this.v.i[0][1] = e - i, this.v.i[1][0] = t + r, this.v.i[1][1] = e - i * s, this.v.i[2][0] = t + r * s, this.v.i[2][1] = e + i, this.v.i[3][0] = t - r, this.v.i[3][1] = e + i * s, this.v.o[0][0] = t + r * s, this.v.o[0][1] = e - i, this.v.o[1][0] = t + r, this.v.o[1][1] = e + i * s, this.v.o[2][0] = t - r * s, this.v.o[2][1] = e + i, this.v.o[3][0] = t - r, this.v.o[3][1] = e - i * s) : (this.v.v[0][0] = t, this.v.v[0][1] = e - i, this.v.v[1][0] = t - r, this.v.v[1][1] = e, this.v.v[2][0] = t, this.v.v[2][1] = e + i, this.v.v[3][0] = t + r, this.v.v[3][1] = e, this.v.i[0][0] = t + r * s, this.v.i[0][1] = e - i, this.v.i[1][0] = t - r, this.v.i[1][1] = e - i * s, this.v.i[2][0] = t - r * s, this.v.i[2][1] = e + i, this.v.i[3][0] = t + r, this.v.i[3][1] = e + i * s, this.v.o[0][0] = t - r * s, this.v.o[0][1] = e - i, this.v.o[1][0] = t - r, this.v.o[1][1] = e + i * s, this.v.o[2][0] = t + r * s, this.v.o[2][1] = e + i, this.v.o[3][0] = t + r, this.v.o[3][1] = e - i * s);
      }function e(t) {
        var e,
            r = this.dynamicProperties.length;if (this.elem.globalData.frameId !== this.frameId) {
          for (this.mdf = !1, this.frameId = this.elem.globalData.frameId, e = 0; r > e; e += 1) {
            this.dynamicProperties[e].getValue(t), this.dynamicProperties[e].mdf && (this.mdf = !0);
          }this.mdf && this.convertEllToPath();
        }
      }var s = roundCorner;return function (s, i) {
        this.v = shape_pool.newShape(), this.v.setPathData(!0, 4), this.localShapeCollection = shapeCollection_pool.newShapeCollection(), this.paths = this.localShapeCollection, this.localShapeCollection.addShape(this.v), this.d = i.d, this.dynamicProperties = [], this.elem = s, this.comp = s.comp, this.frameId = -1, this.mdf = !1, this.getValue = e, this.convertEllToPath = t, this.reset = r, this.p = PropertyFactory.getProp(s, i.p, 1, 0, this.dynamicProperties), this.s = PropertyFactory.getProp(s, i.s, 1, 0, this.dynamicProperties), this.dynamicProperties.length ? this.k = !0 : this.convertEllToPath();
      };
    }(),
        h = function () {
      function t() {
        var t,
            e = Math.floor(this.pt.v),
            r = 2 * Math.PI / e,
            s = this.or.v,
            i = this.os.v,
            a = 2 * Math.PI * s / (4 * e),
            n = -Math.PI / 2,
            o = 3 === this.data.d ? -1 : 1;for (n += this.r.v, this.v._length = 0, t = 0; e > t; t += 1) {
          var h = s * Math.cos(n),
              l = s * Math.sin(n),
              p = 0 === h && 0 === l ? 0 : l / Math.sqrt(h * h + l * l),
              m = 0 === h && 0 === l ? 0 : -h / Math.sqrt(h * h + l * l);h += +this.p.v[0], l += +this.p.v[1], this.v.setTripleAt(h, l, h - p * a * i * o, l - m * a * i * o, h + p * a * i * o, l + m * a * i * o, t, !0), n += r * o;
        }this.paths.length = 0, this.paths[0] = this.v;
      }function e() {
        var t,
            e,
            r,
            s,
            i = 2 * Math.floor(this.pt.v),
            a = 2 * Math.PI / i,
            n = !0,
            o = this.or.v,
            h = this.ir.v,
            l = this.os.v,
            p = this.is.v,
            m = 2 * Math.PI * o / (2 * i),
            f = 2 * Math.PI * h / (2 * i),
            c = -Math.PI / 2;c += this.r.v;var d = 3 === this.data.d ? -1 : 1;for (this.v._length = 0, t = 0; i > t; t += 1) {
          e = n ? o : h, r = n ? l : p, s = n ? m : f;var u = e * Math.cos(c),
              y = e * Math.sin(c),
              g = 0 === u && 0 === y ? 0 : y / Math.sqrt(u * u + y * y),
              v = 0 === u && 0 === y ? 0 : -u / Math.sqrt(u * u + y * y);u += +this.p.v[0], y += +this.p.v[1], this.v.setTripleAt(u, y, u - g * s * r * d, y - v * s * r * d, u + g * s * r * d, y + v * s * r * d, t, !0), n = !n, c += a * d;
        }
      }function s() {
        if (this.elem.globalData.frameId !== this.frameId) {
          this.mdf = !1, this.frameId = this.elem.globalData.frameId;var t,
              e = this.dynamicProperties.length;for (t = 0; e > t; t += 1) {
            this.dynamicProperties[t].getValue(), this.dynamicProperties[t].mdf && (this.mdf = !0);
          }this.mdf && this.convertToPath();
        }
      }return function (i, a) {
        this.v = shape_pool.newShape(), this.v.setPathData(!0, 0), this.elem = i, this.comp = i.comp, this.data = a, this.frameId = -1, this.d = a.d, this.dynamicProperties = [], this.mdf = !1, this.getValue = s, this.reset = r, 1 === a.sy ? (this.ir = PropertyFactory.getProp(i, a.ir, 0, 0, this.dynamicProperties), this.is = PropertyFactory.getProp(i, a.is, 0, .01, this.dynamicProperties), this.convertToPath = e) : this.convertToPath = t, this.pt = PropertyFactory.getProp(i, a.pt, 0, 0, this.dynamicProperties), this.p = PropertyFactory.getProp(i, a.p, 1, 0, this.dynamicProperties), this.r = PropertyFactory.getProp(i, a.r, 0, degToRads, this.dynamicProperties), this.or = PropertyFactory.getProp(i, a.or, 0, 0, this.dynamicProperties), this.os = PropertyFactory.getProp(i, a.os, 0, .01, this.dynamicProperties), this.localShapeCollection = shapeCollection_pool.newShapeCollection(), this.localShapeCollection.addShape(this.v), this.paths = this.localShapeCollection, this.dynamicProperties.length ? this.k = !0 : this.convertToPath();
      };
    }(),
        l = function () {
      function t(t) {
        if (this.elem.globalData.frameId !== this.frameId) {
          this.mdf = !1, this.frameId = this.elem.globalData.frameId;var e,
              r = this.dynamicProperties.length;for (e = 0; r > e; e += 1) {
            this.dynamicProperties[e].getValue(t), this.dynamicProperties[e].mdf && (this.mdf = !0);
          }this.mdf && this.convertRectToPath();
        }
      }function e() {
        var t = this.p.v[0],
            e = this.p.v[1],
            r = this.s.v[0] / 2,
            s = this.s.v[1] / 2,
            i = bm_min(r, s, this.r.v),
            a = i * (1 - roundCorner);this.v._length = 0, 2 === this.d || 1 === this.d ? (this.v.setTripleAt(t + r, e - s + i, t + r, e - s + i, t + r, e - s + a, 0, !0), this.v.setTripleAt(t + r, e + s - i, t + r, e + s - a, t + r, e + s - i, 1, !0), 0 !== i ? (this.v.setTripleAt(t + r - i, e + s, t + r - i, e + s, t + r - a, e + s, 2, !0), this.v.setTripleAt(t - r + i, e + s, t - r + a, e + s, t - r + i, e + s, 3, !0), this.v.setTripleAt(t - r, e + s - i, t - r, e + s - i, t - r, e + s - a, 4, !0), this.v.setTripleAt(t - r, e - s + i, t - r, e - s + a, t - r, e - s + i, 5, !0), this.v.setTripleAt(t - r + i, e - s, t - r + i, e - s, t - r + a, e - s, 6, !0), this.v.setTripleAt(t + r - i, e - s, t + r - a, e - s, t + r - i, e - s, 7, !0)) : (this.v.setTripleAt(t - r, e + s, t - r + a, e + s, t - r, e + s, 2), this.v.setTripleAt(t - r, e - s, t - r, e - s + a, t - r, e - s, 3))) : (this.v.setTripleAt(t + r, e - s + i, t + r, e - s + a, t + r, e - s + i, 0, !0), 0 !== i ? (this.v.setTripleAt(t + r - i, e - s, t + r - i, e - s, t + r - a, e - s, 1, !0), this.v.setTripleAt(t - r + i, e - s, t - r + a, e - s, t - r + i, e - s, 2, !0), this.v.setTripleAt(t - r, e - s + i, t - r, e - s + i, t - r, e - s + a, 3, !0), this.v.setTripleAt(t - r, e + s - i, t - r, e + s - a, t - r, e + s - i, 4, !0), this.v.setTripleAt(t - r + i, e + s, t - r + i, e + s, t - r + a, e + s, 5, !0), this.v.setTripleAt(t + r - i, e + s, t + r - a, e + s, t + r - i, e + s, 6, !0), this.v.setTripleAt(t + r, e + s - i, t + r, e + s - i, t + r, e + s - a, 7, !0)) : (this.v.setTripleAt(t - r, e - s, t - r + a, e - s, t - r, e - s, 1, !0), this.v.setTripleAt(t - r, e + s, t - r, e + s - a, t - r, e + s, 2, !0), this.v.setTripleAt(t + r, e + s, t + r - a, e + s, t + r, e + s, 3, !0)));
      }return function (s, i) {
        this.v = shape_pool.newShape(), this.v.c = !0, this.localShapeCollection = shapeCollection_pool.newShapeCollection(), this.localShapeCollection.addShape(this.v), this.paths = this.localShapeCollection, this.elem = s, this.comp = s.comp, this.frameId = -1, this.d = i.d, this.dynamicProperties = [], this.mdf = !1, this.getValue = t, this.convertRectToPath = e, this.reset = r, this.p = PropertyFactory.getProp(s, i.p, 1, 0, this.dynamicProperties), this.s = PropertyFactory.getProp(s, i.s, 1, 0, this.dynamicProperties), this.r = PropertyFactory.getProp(s, i.r, 0, 0, this.dynamicProperties), this.dynamicProperties.length ? this.k = !0 : this.convertRectToPath();
      };
    }(),
        p = {};return p.getShapeProp = a, p;
  }(),
      ShapeModifiers = function () {
    function t(t, e) {
      s[t] || (s[t] = e);
    }function e(t, e, r, i) {
      return new s[t](e, r, i);
    }var r = {},
        s = {};return r.registerModifier = t, r.getModifier = e, r;
  }();ShapeModifier.prototype.initModifierProperties = function () {}, ShapeModifier.prototype.addShapeToModifier = function () {}, ShapeModifier.prototype.addShape = function (t) {
    this.closed || (this.shapes.push({ shape: t.sh, data: t, localShapeCollection: shapeCollection_pool.newShapeCollection() }), this.addShapeToModifier(t.sh));
  }, ShapeModifier.prototype.init = function (t, e, r) {
    this.elem = t, this.frameId = -1, this.shapes = [], this.dynamicProperties = [], this.mdf = !1, this.closed = !1, this.k = !1, this.isTrimming = !1, this.comp = t.comp, this.initModifierProperties(t, e), this.dynamicProperties.length ? (this.k = !0, r.push(this)) : this.getValue(!0);
  }, extendPrototype(ShapeModifier, TrimModifier), TrimModifier.prototype.processKeys = function (t) {
    if (this.elem.globalData.frameId !== this.frameId || t) {
      this.mdf = t ? !0 : !1, this.frameId = this.elem.globalData.frameId;var e,
          r = this.dynamicProperties.length;for (e = 0; r > e; e += 1) {
        this.dynamicProperties[e].getValue(), this.dynamicProperties[e].mdf && (this.mdf = !0);
      }if (this.mdf || t) {
        var s = this.o.v % 360 / 360;0 > s && (s += 1);var i = this.s.v + s,
            a = this.e.v + s;if (i > a) {
          var n = i;i = a, a = n;
        }this.sValue = i, this.eValue = a, this.oValue = s;
      }
    }
  }, TrimModifier.prototype.initModifierProperties = function (t, e) {
    this.sValue = 0, this.eValue = 0, this.oValue = 0, this.getValue = this.processKeys, this.s = PropertyFactory.getProp(t, e.s, 0, .01, this.dynamicProperties), this.e = PropertyFactory.getProp(t, e.e, 0, .01, this.dynamicProperties), this.o = PropertyFactory.getProp(t, e.o, 0, 0, this.dynamicProperties), this.m = e.m, this.dynamicProperties.length || this.getValue(!0);
  }, TrimModifier.prototype.getSegmentsLength = function (t) {
    var e,
        r = t.c,
        s = t.v,
        i = t.o,
        a = t.i,
        n = t._length,
        o = [],
        h = 0;for (e = 0; n - 1 > e; e += 1) {
      o[e] = bez.getBezierLength(s[e], s[e + 1], i[e], a[e + 1]), h += o[e].addedLength;
    }return r && (o[e] = bez.getBezierLength(s[e], s[0], i[e], a[0]), h += o[e].addedLength), { lengths: o, totalLength: h };
  }, TrimModifier.prototype.calculateShapeEdges = function (t, e, r, s, i) {
    var a = [];1 >= e ? a.push({ s: t, e: e }) : t >= 1 ? a.push({ s: t - 1, e: e - 1 }) : (a.push({ s: t, e: 1 }), a.push({ s: 0, e: e - 1 }));var n,
        o,
        h = [],
        l = a.length;for (n = 0; l > n; n += 1) {
      if (o = a[n], o.e * i < s || o.s * i > s + r) ;else {
        var p, m;p = o.s * i <= s ? 0 : (o.s * i - s) / r, m = o.e * i >= s + r ? 1 : (o.e * i - s) / r, h.push([p, m]);
      }
    }return h.length || h.push([0, 0]), h;
  }, TrimModifier.prototype.processShapes = function (t) {
    var e,
        r,
        s,
        i,
        a,
        n,
        o,
        h = this.shapes.length,
        l = this.sValue,
        p = this.eValue,
        m = 0;if (p === l) for (r = 0; h > r; r += 1) {
      this.shapes[r].localShapeCollection.releaseShapes(), this.shapes[r].shape.mdf = !0, this.shapes[r].shape.paths = this.shapes[r].localShapeCollection;
    } else if (!(1 === p && 0 === l || 0 === p && 1 === l)) {
      var f,
          c,
          d = [];for (r = 0; h > r; r += 1) {
        if (f = this.shapes[r], f.shape.mdf || this.mdf || t || 2 === this.m) {
          if (e = f.shape.paths, i = e._length, o = 0, !f.shape.mdf && f.pathsData) o = f.totalShapeLength;else {
            for (a = [], s = 0; i > s; s += 1) {
              n = this.getSegmentsLength(e.shapes[s]), a.push(n), o += n.totalLength;
            }f.totalShapeLength = o, f.pathsData = a;
          }m += o, f.shape.mdf = !0;
        } else f.shape.paths = f.localShapeCollection;
      }var s,
          i,
          u = l,
          y = p,
          g = 0;for (r = h - 1; r >= 0; r -= 1) {
        if (f = this.shapes[r], f.shape.mdf) {
          if (c = f.localShapeCollection, c.releaseShapes(), 2 === this.m && h > 1) {
            var v = this.calculateShapeEdges(l, p, f.totalShapeLength, g, m);g += f.totalShapeLength;
          } else v = [[u, y]];for (i = v.length, s = 0; i > s; s += 1) {
            u = v[s][0], y = v[s][1], d.length = 0, 1 >= y ? d.push({ s: f.totalShapeLength * u, e: f.totalShapeLength * y }) : u >= 1 ? d.push({ s: f.totalShapeLength * (u - 1), e: f.totalShapeLength * (y - 1) }) : (d.push({ s: f.totalShapeLength * u, e: f.totalShapeLength }), d.push({ s: 0, e: f.totalShapeLength * (y - 1) }));var b = this.addShapes(f, d[0]);if (d[0].s !== d[0].e) {
              if (d.length > 1) if (f.shape.v.c) {
                var E = b.pop();this.addPaths(b, c), b = this.addShapes(f, d[1], E);
              } else this.addPaths(b, c), b = this.addShapes(f, d[1]);this.addPaths(b, c);
            }
          }f.shape.paths = c;
        }
      }
    }this.dynamicProperties.length || (this.mdf = !1);
  }, TrimModifier.prototype.addPaths = function (t, e) {
    var r,
        s = t.length;for (r = 0; s > r; r += 1) {
      e.addShape(t[r]);
    }
  }, TrimModifier.prototype.addSegment = function (t, e, r, s, i, a, n) {
    i.setXYAt(e[0], e[1], "o", a), i.setXYAt(r[0], r[1], "i", a + 1), n && i.setXYAt(t[0], t[1], "v", a), i.setXYAt(s[0], s[1], "v", a + 1);
  }, TrimModifier.prototype.addShapes = function (t, e, r) {
    var s,
        i,
        a,
        n,
        o,
        h,
        l,
        p,
        m = t.pathsData,
        f = t.shape.paths.shapes,
        c = t.shape.paths._length,
        d = 0,
        u = [],
        y = !0;for (r ? (o = r._length, p = r._length) : (r = shape_pool.newShape(), o = 0, p = 0), u.push(r), s = 0; c > s; s += 1) {
      for (h = m[s].lengths, r.c = f[s].c, a = f[s].c ? h.length : h.length + 1, i = 1; a > i; i += 1) {
        if (n = h[i - 1], d + n.addedLength < e.s) d += n.addedLength, r.c = !1;else {
          if (d > e.e) {
            r.c = !1;break;
          }e.s <= d && e.e >= d + n.addedLength ? (this.addSegment(f[s].v[i - 1], f[s].o[i - 1], f[s].i[i], f[s].v[i], r, o, y), y = !1) : (l = bez.getNewSegment(f[s].v[i - 1], f[s].v[i], f[s].o[i - 1], f[s].i[i], (e.s - d) / n.addedLength, (e.e - d) / n.addedLength, h[i - 1]), this.addSegment(l.pt1, l.pt3, l.pt4, l.pt2, r, o, y), y = !1, r.c = !1), d += n.addedLength, o += 1;
        }
      }if (f[s].c) {
        if (n = h[i - 1], d <= e.e) {
          var g = h[i - 1].addedLength;e.s <= d && e.e >= d + g ? (this.addSegment(f[s].v[i - 1], f[s].o[i - 1], f[s].i[0], f[s].v[0], r, o, y), y = !1) : (l = bez.getNewSegment(f[s].v[i - 1], f[s].v[0], f[s].o[i - 1], f[s].i[0], (e.s - d) / g, (e.e - d) / g, h[i - 1]), this.addSegment(l.pt1, l.pt3, l.pt4, l.pt2, r, o, y), y = !1, r.c = !1);
        } else r.c = !1;d += n.addedLength, o += 1;
      }if (r._length && (r.setXYAt(r.v[p][0], r.v[p][1], "i", p), r.setXYAt(r.v[r._length - 1][0], r.v[r._length - 1][1], "o", r._length - 1)), d > e.e) break;c - 1 > s && (r = shape_pool.newShape(), y = !0, u.push(r), o = 0);
    }return u;
  }, ShapeModifiers.registerModifier("tm", TrimModifier), extendPrototype(ShapeModifier, RoundCornersModifier), RoundCornersModifier.prototype.processKeys = function (t) {
    if (this.elem.globalData.frameId !== this.frameId || t) {
      this.mdf = t ? !0 : !1, this.frameId = this.elem.globalData.frameId;var e,
          r = this.dynamicProperties.length;for (e = 0; r > e; e += 1) {
        this.dynamicProperties[e].getValue(), this.dynamicProperties[e].mdf && (this.mdf = !0);
      }
    }
  }, RoundCornersModifier.prototype.initModifierProperties = function (t, e) {
    this.getValue = this.processKeys, this.rd = PropertyFactory.getProp(t, e.r, 0, null, this.dynamicProperties), this.dynamicProperties.length || this.getValue(!0);
  }, RoundCornersModifier.prototype.processPath = function (t, e) {
    var r = shape_pool.newShape();r.c = t.c;var s,
        i,
        a,
        n,
        o,
        h,
        l,
        p,
        m,
        f,
        c,
        d,
        u,
        y = t._length,
        g = 0;for (s = 0; y > s; s += 1) {
      i = t.v[s], n = t.o[s], a = t.i[s], i[0] === n[0] && i[1] === n[1] && i[0] === a[0] && i[1] === a[1] ? 0 !== s && s !== y - 1 || t.c ? (o = 0 === s ? t.v[y - 1] : t.v[s - 1], h = Math.sqrt(Math.pow(i[0] - o[0], 2) + Math.pow(i[1] - o[1], 2)), l = h ? Math.min(h / 2, e) / h : 0, p = d = i[0] + (o[0] - i[0]) * l, m = u = i[1] - (i[1] - o[1]) * l, f = p - (p - i[0]) * roundCorner, c = m - (m - i[1]) * roundCorner, r.setTripleAt(p, m, f, c, d, u, g), g += 1, o = s === y - 1 ? t.v[0] : t.v[s + 1], h = Math.sqrt(Math.pow(i[0] - o[0], 2) + Math.pow(i[1] - o[1], 2)), l = h ? Math.min(h / 2, e) / h : 0, p = f = i[0] + (o[0] - i[0]) * l, m = c = i[1] + (o[1] - i[1]) * l, d = p - (p - i[0]) * roundCorner, u = m - (m - i[1]) * roundCorner, r.setTripleAt(p, m, f, c, d, u, g), g += 1) : (r.setTripleAt(i[0], i[1], n[0], n[1], a[0], a[1], g), g += 1) : (r.setTripleAt(t.v[s][0], t.v[s][1], t.o[s][0], t.o[s][1], t.i[s][0], t.i[s][1], g), g += 1);
    }return r;
  }, RoundCornersModifier.prototype.processShapes = function (t) {
    var e,
        r,
        s,
        i,
        a = this.shapes.length,
        n = this.rd.v;if (0 !== n) {
      var o, h, l;for (r = 0; a > r; r += 1) {
        if (o = this.shapes[r], h = o.shape.paths, l = o.localShapeCollection, o.shape.mdf || this.mdf || t) for (l.releaseShapes(), o.shape.mdf = !0, e = o.shape.paths.shapes, i = o.shape.paths._length, s = 0; i > s; s += 1) {
          l.addShape(this.processPath(e[s], n));
        }o.shape.paths = o.localShapeCollection;
      }
    }this.dynamicProperties.length || (this.mdf = !1);
  }, ShapeModifiers.registerModifier("rd", RoundCornersModifier), extendPrototype(ShapeModifier, RepeaterModifier), RepeaterModifier.prototype.processKeys = function (t) {
    if (this.elem.globalData.frameId !== this.frameId || t) {
      this.mdf = t ? !0 : !1, this.frameId = this.elem.globalData.frameId;var e,
          r = this.dynamicProperties.length;for (e = 0; r > e; e += 1) {
        this.dynamicProperties[e].getValue(), this.dynamicProperties[e].mdf && (this.mdf = !0);
      }
    }
  }, RepeaterModifier.prototype.initModifierProperties = function (t, e) {
    this.getValue = this.processKeys, this.c = PropertyFactory.getProp(t, e.c, 0, null, this.dynamicProperties), this.o = PropertyFactory.getProp(t, e.o, 0, null, this.dynamicProperties), this.tr = PropertyFactory.getProp(t, e.tr, 2, null, this.dynamicProperties), this.dynamicProperties.length || this.getValue(!0), this.pMatrix = new Matrix(), this.rMatrix = new Matrix(), this.sMatrix = new Matrix(), this.tMatrix = new Matrix(), this.matrix = new Matrix();
  }, RepeaterModifier.prototype.applyTransforms = function (t, e, r, s, i, a) {
    var n = a ? -1 : 1,
        o = s.s.v[0] + (1 - s.s.v[0]) * (1 - i),
        h = s.s.v[1] + (1 - s.s.v[1]) * (1 - i);t.translate(s.p.v[0] * n * i, s.p.v[1] * n * i, s.p.v[2]), e.translate(-s.a.v[0], -s.a.v[1], s.a.v[2]), e.rotate(-s.r.v * n * i), e.translate(s.a.v[0], s.a.v[1], s.a.v[2]), r.translate(-s.a.v[0], -s.a.v[1], s.a.v[2]), r.scale(a ? 1 / o : o, a ? 1 / h : h), r.translate(s.a.v[0], s.a.v[1], s.a.v[2]);
  }, RepeaterModifier.prototype.processShapes = function (t) {
    this.dynamicProperties.length || (this.mdf = !1);var e,
        r,
        s,
        i,
        a,
        n,
        o,
        h,
        l,
        p,
        m,
        f,
        c,
        d,
        u = this.shapes.length,
        y = Math.ceil(this.c.v),
        g = this.o.v,
        v = g % 1,
        b = g > 0 ? Math.floor(g) : Math.ceil(g),
        E = (this.tr.v.props, this.pMatrix.props),
        P = this.rMatrix.props,
        x = this.sMatrix.props,
        S = 0;for (e = 0; u > e; e += 1) {
      if (i = this.shapes[e], a = i.localShapeCollection, i.shape.mdf || this.mdf || t) {
        if (a.releaseShapes(), i.shape.mdf = !0, h = i.shape.paths, l = h.shapes, s = h._length, S = 0, this.pMatrix.reset(), this.rMatrix.reset(), this.sMatrix.reset(), this.tMatrix.reset(), this.matrix.reset(), g > 0) {
          for (; b > S;) {
            this.applyTransforms(this.pMatrix, this.rMatrix, this.sMatrix, this.tr, 1, !1), S += 1;
          }v && (this.applyTransforms(this.pMatrix, this.rMatrix, this.sMatrix, this.tr, v, !1), S += v);
        } else if (0 > b) {
          for (; S > b;) {
            this.applyTransforms(this.pMatrix, this.rMatrix, this.sMatrix, this.tr, 1, !0), S -= 1;
          }v && (this.applyTransforms(this.pMatrix, this.rMatrix, this.sMatrix, this.tr, -v, !0), S -= v);
        }for (r = 0; s > r; r += 1) {
          for (n = l[r], o = 0; y > o; o += 1) {
            if (0 !== o && this.applyTransforms(this.pMatrix, this.rMatrix, this.sMatrix, this.tr, 1, !1), i.data.transformers) {
              for (i.data.lvl = 0, d = 0, m = i.data.elements.length, p = 0; m > p; p += 1) {
                d = Math.max(d, i.data.elements[p].st.lvl);
              }for (c = i.data.transformers, m = c.length, p = m - 1; p >= d; p -= 1) {
                f = c[p].mProps.v.props, this.matrix.transform(f[0], f[1], f[2], f[3], f[4], f[5], f[6], f[7], f[8], f[9], f[10], f[11], f[12], f[13], f[14], f[15]);
              }
            }0 !== S && (this.matrix.transform(P[0], P[1], P[2], P[3], P[4], P[5], P[6], P[7], P[8], P[9], P[10], P[11], P[12], P[13], P[14], P[15]), this.matrix.transform(x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14], x[15]), this.matrix.transform(E[0], E[1], E[2], E[3], E[4], E[5], E[6], E[7], E[8], E[9], E[10], E[11], E[12], E[13], E[14], E[15])), a.addShape(this.processPath(n, this.matrix)), this.matrix.reset(), S += 1;
          }
        }
      }i.shape.paths = a;
    }
  }, RepeaterModifier.prototype.processPath = function (t, e) {
    var r = shape_pool.clone(t, e);return r;
  }, ShapeModifiers.registerModifier("rp", RepeaterModifier), ShapeCollection.prototype.addShape = function (t) {
    this._length === this._maxLength && (this.shapes = this.shapes.concat(Array.apply(null, { length: this._maxLength })), this._maxLength *= 2), this.shapes[this._length] = t, this._length += 1;
  }, ShapeCollection.prototype.releaseShapes = function () {
    var t;for (t = 0; t < this._length; t += 1) {
      shape_pool.release(this.shapes[t]);
    }this._length = 0;
  };var ImagePreloader = function () {
    function t() {
      this.loadedAssets += 1, this.loadedAssets === this.totalImages;
    }function e(t) {
      var e = "";if (this.assetsPath) {
        var r = t.p;-1 !== r.indexOf("images/") && (r = r.split("/")[1]), e = this.assetsPath + r;
      } else e = this.path, e += t.u ? t.u : "", e += t.p;return e;
    }function r(e) {
      var r = document.createElement("img");r.addEventListener("load", t.bind(this), !1), r.addEventListener("error", t.bind(this), !1), r.src = e;
    }function s(t) {
      this.totalAssets = t.length;var s;for (s = 0; s < this.totalAssets; s += 1) {
        t[s].layers || (r.bind(this)(e.bind(this)(t[s])), this.totalImages += 1);
      }
    }function i(t) {
      this.path = t || "";
    }function a(t) {
      this.assetsPath = t || "";
    }return function () {
      this.loadAssets = s, this.setAssetsPath = a, this.setPath = i, this.assetsPath = "", this.path = "", this.totalAssets = 0, this.totalImages = 0, this.loadedAssets = 0;
    };
  }(),
      featureSupport = function () {
    var t = { maskType: !0 };return (/MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) && (t.maskType = !1), t;
  }(),
      filtersFactory = function () {
    function t(t) {
      var e = document.createElementNS(svgNS, "filter");return e.setAttribute("id", t), e.setAttribute("filterUnits", "objectBoundingBox"), e.setAttribute("x", "0%"), e.setAttribute("y", "0%"), e.setAttribute("width", "100%"), e.setAttribute("height", "100%"), e;
    }function e() {
      var t = document.createElementNS(svgNS, "feColorMatrix");return t.setAttribute("type", "matrix"), t.setAttribute("color-interpolation-filters", "sRGB"), t.setAttribute("values", "0 0 0 1 0  0 0 0 1 0  0 0 0 1 0  0 0 0 0 1"), t;
    }var r = {};return r.createFilter = t, r.createAlphaToLuminanceFilter = e, r;
  }(),
      pooling = function () {
    function t(t) {
      return t.concat(Array.apply(null, { length: t.length }));
    }return { "double": t };
  }(),
      point_pool = function () {
    function t() {
      var t;return s ? (s -= 1, t = a[s]) : t = [.1, .1], t;
    }function e(t) {
      s === i && (a = pooling["double"](a), i = 2 * i), a[s] = t, s += 1;
    }var r = { newPoint: t, release: e },
        s = 0,
        i = 8,
        a = Array.apply(null, { length: i });return r;
  }(),
      shape_pool = function () {
    function t() {
      var t;return a ? (a -= 1, t = o[a]) : t = new ShapePath(), t;
    }function e(t) {
      a === n && (o = pooling["double"](o), n = 2 * n);var e,
          r = t._length;for (e = 0; r > e; e += 1) {
        point_pool.release(t.v[e]), point_pool.release(t.i[e]), point_pool.release(t.o[e]);
      }t._length = 0, t.c = !1, o[a] = t, a += 1;
    }function r(t, r) {
      for (; r--;) {
        e(t[r]);
      }
    }function s(e, r) {
      var s,
          i = e._length,
          a = t();a._length = e._length, a.c = e.c;var n;for (s = 0; i > s; s += 1) {
        r ? (n = r.applyToPointArray(e.v[s][0], e.v[s][1], 0, 2), a.setXYAt(n[0], n[1], "v", s), point_pool.release(n), n = r.applyToPointArray(e.o[s][0], e.o[s][1], 0, 2), a.setXYAt(n[0], n[1], "o", s), point_pool.release(n), n = r.applyToPointArray(e.i[s][0], e.i[s][1], 0, 2), a.setXYAt(n[0], n[1], "i", s), point_pool.release(n)) : a.setTripleAt(e.v[s][0], e.v[s][1], e.o[s][0], e.o[s][1], e.i[s][0], e.i[s][1], s);
      }return a;
    }var i = { clone: s, newShape: t, release: e, releaseArray: r },
        a = 0,
        n = 4,
        o = Array.apply(null, { length: n });return i;
  }(),
      shapeCollection_pool = function () {
    function t() {
      var t;return i ? (i -= 1, t = n[i]) : t = new ShapeCollection(), t;
    }function e(t) {
      var e,
          r = t._length;for (e = 0; r > e; e += 1) {
        shape_pool.release(t.shapes[e]);
      }t._length = 0, i === a && (n = pooling["double"](n), a = 2 * a), n[i] = t, i += 1;
    }function r(t, r) {
      e(t), i === a && (n = pooling["double"](n), a = 2 * a), n[i] = t, i += 1;
    }var s = { newShapeCollection: t, release: e, clone: r },
        i = 0,
        a = 4,
        n = Array.apply(null, { length: a });return s;
  }();BaseRenderer.prototype.checkLayers = function (t) {
    var e,
        r,
        s = this.layers.length;for (this.completeLayers = !0, e = s - 1; e >= 0; e--) {
      this.elements[e] || (r = this.layers[e], r.ip - r.st <= t - this.layers[e].st && r.op - r.st > t - this.layers[e].st && this.buildItem(e)), this.completeLayers = this.elements[e] ? this.completeLayers : !1;
    }this.checkPendingElements();
  }, BaseRenderer.prototype.createItem = function (t) {
    switch (t.ty) {case 2:
        return this.createImage(t);case 0:
        return this.createComp(t);case 1:
        return this.createSolid(t);case 4:
        return this.createShape(t);case 5:
        return this.createText(t);case 99:
        return null;}return this.createBase(t);
  }, BaseRenderer.prototype.buildAllItems = function () {
    var t,
        e = this.layers.length;for (t = 0; e > t; t += 1) {
      this.buildItem(t);
    }this.checkPendingElements();
  }, BaseRenderer.prototype.includeLayers = function (t) {
    this.completeLayers = !1;var e,
        r,
        s = t.length,
        i = this.layers.length;for (e = 0; s > e; e += 1) {
      for (r = 0; i > r;) {
        if (this.layers[r].id == t[e].id) {
          this.layers[r] = t[e];break;
        }r += 1;
      }
    }
  }, BaseRenderer.prototype.setProjectInterface = function (t) {
    this.globalData.projectInterface = t;
  }, BaseRenderer.prototype.initItems = function () {
    this.globalData.progressiveLoad || this.buildAllItems();
  }, BaseRenderer.prototype.buildElementParenting = function (t, e, r) {
    r = r || [];for (var s = this.elements, i = this.layers, a = 0, n = i.length; n > a;) {
      i[a].ind == e && (s[a] && s[a] !== !0 ? void 0 !== i[a].parent ? (r.push(s[a]), s[a]._isParent = !0, this.buildElementParenting(t, i[a].parent, r)) : (r.push(s[a]), s[a]._isParent = !0, t.setHierarchy(r)) : (this.buildItem(a), this.addPendingElement(t))), a += 1;
    }
  }, BaseRenderer.prototype.addPendingElement = function (t) {
    this.pendingElements.push(t);
  }, extendPrototype(BaseRenderer, SVGRenderer), SVGRenderer.prototype.createBase = function (t) {
    return new SVGBaseElement(t, this.layerElement, this.globalData, this);
  }, SVGRenderer.prototype.createShape = function (t) {
    return new IShapeElement(t, this.layerElement, this.globalData, this);
  }, SVGRenderer.prototype.createText = function (t) {
    return new SVGTextElement(t, this.layerElement, this.globalData, this);
  }, SVGRenderer.prototype.createImage = function (t) {
    return new IImageElement(t, this.layerElement, this.globalData, this);
  }, SVGRenderer.prototype.createComp = function (t) {
    return new ICompElement(t, this.layerElement, this.globalData, this);
  }, SVGRenderer.prototype.createSolid = function (t) {
    return new ISolidElement(t, this.layerElement, this.globalData, this);
  }, SVGRenderer.prototype.configAnimation = function (t) {
    this.layerElement = document.createElementNS(svgNS, "svg"), this.layerElement.setAttribute("xmlns", "http://www.w3.org/2000/svg"), this.layerElement.setAttribute("width", t.w), this.layerElement.setAttribute("height", t.h), this.layerElement.setAttribute("viewBox", "0 0 " + t.w + " " + t.h), this.layerElement.setAttribute("preserveAspectRatio", this.renderConfig.preserveAspectRatio), this.layerElement.style.width = "100%", this.layerElement.style.height = "100%", this.animationItem.wrapper.appendChild(this.layerElement);var e = document.createElementNS(svgNS, "defs");this.globalData.defs = e, this.layerElement.appendChild(e), this.globalData.getAssetData = this.animationItem.getAssetData.bind(this.animationItem), this.globalData.getAssetsPath = this.animationItem.getAssetsPath.bind(this.animationItem), this.globalData.progressiveLoad = this.renderConfig.progressiveLoad, this.globalData.frameId = 0, this.globalData.nm = t.nm, this.globalData.compSize = { w: t.w, h: t.h }, this.data = t, this.globalData.frameRate = t.fr;var r = document.createElementNS(svgNS, "clipPath"),
        s = document.createElementNS(svgNS, "rect");s.setAttribute("width", t.w), s.setAttribute("height", t.h), s.setAttribute("x", 0), s.setAttribute("y", 0);var i = "animationMask_" + randomString(10);r.setAttribute("id", i), r.appendChild(s);var a = document.createElementNS(svgNS, "g");a.setAttribute("clip-path", "url(#" + i + ")"), this.layerElement.appendChild(a), e.appendChild(r), this.layerElement = a, this.layers = t.layers, this.globalData.fontManager = new FontManager(), this.globalData.fontManager.addChars(t.chars), this.globalData.fontManager.addFonts(t.fonts, e), this.elements = Array.apply(null, { length: t.layers.length });
  }, SVGRenderer.prototype.destroy = function () {
    this.animationItem.wrapper.innerHTML = "", this.layerElement = null, this.globalData.defs = null;var t,
        e = this.layers ? this.layers.length : 0;for (t = 0; e > t; t++) {
      this.elements[t] && this.elements[t].destroy();
    }this.elements.length = 0, this.destroyed = !0, this.animationItem = null;
  }, SVGRenderer.prototype.updateContainerSize = function () {}, SVGRenderer.prototype.buildItem = function (t) {
    var e = this.elements;if (!e[t] && 99 != this.layers[t].ty) {
      e[t] = !0;var r = this.createItem(this.layers[t]);e[t] = r, expressionsPlugin && (0 === this.layers[t].ty && this.globalData.projectInterface.registerComposition(r), r.initExpressions()), this.appendElementInPos(r, t), this.layers[t].tt && (this.elements[t - 1] && this.elements[t - 1] !== !0 ? r.setMatte(e[t - 1].layerId) : (this.buildItem(t - 1), this.addPendingElement(r)));
    }
  }, SVGRenderer.prototype.checkPendingElements = function () {
    for (; this.pendingElements.length;) {
      var t = this.pendingElements.pop();if (t.checkParenting(), t.data.tt) for (var e = 0, r = this.elements.length; r > e;) {
        if (this.elements[e] === t) {
          t.setMatte(this.elements[e - 1].layerId);break;
        }e += 1;
      }
    }
  }, SVGRenderer.prototype.renderFrame = function (t) {
    if (this.renderedFrame != t && !this.destroyed) {
      null === t ? t = this.renderedFrame : this.renderedFrame = t, this.globalData.frameNum = t, this.globalData.frameId += 1, this.globalData.projectInterface.currentFrame = t;var e,
          r = this.layers.length;for (this.completeLayers || this.checkLayers(t), e = r - 1; e >= 0; e--) {
        (this.completeLayers || this.elements[e]) && this.elements[e].prepareFrame(t - this.layers[e].st);
      }for (e = r - 1; e >= 0; e--) {
        (this.completeLayers || this.elements[e]) && this.elements[e].renderFrame();
      }
    }
  }, SVGRenderer.prototype.appendElementInPos = function (t, e) {
    var r = t.getBaseElement();if (r) {
      for (var s, i = 0; e > i;) {
        this.elements[i] && this.elements[i] !== !0 && this.elements[i].getBaseElement() && (s = this.elements[i].getBaseElement()), i += 1;
      }s ? this.layerElement.insertBefore(r, s) : this.layerElement.appendChild(r);
    }
  }, SVGRenderer.prototype.hide = function () {
    this.layerElement.style.display = "none";
  }, SVGRenderer.prototype.show = function () {
    this.layerElement.style.display = "block";
  }, SVGRenderer.prototype.searchExtraCompositions = function (t) {
    var e,
        r = t.length,
        s = document.createElementNS(svgNS, "g");for (e = 0; r > e; e += 1) {
      if (t[e].xt) {
        var i = this.createComp(t[e], s, this.globalData.comp, null);i.initExpressions(), this.globalData.projectInterface.registerComposition(i);
      }
    }
  }, MaskElement.prototype.getMaskProperty = function (t) {
    return this.viewData[t].prop;
  }, MaskElement.prototype.prepareFrame = function () {
    var t,
        e = this.dynamicProperties.length;for (t = 0; e > t; t += 1) {
      this.dynamicProperties[t].getValue();
    }
  }, MaskElement.prototype.renderFrame = function (t) {
    var e,
        r = this.masksProperties.length;for (e = 0; r > e; e++) {
      if ((this.viewData[e].prop.mdf || this.firstFrame) && this.drawPath(this.masksProperties[e], this.viewData[e].prop.v, this.viewData[e]), (this.viewData[e].op.mdf || this.firstFrame) && this.viewData[e].elem.setAttribute("fill-opacity", this.viewData[e].op.v), "n" !== this.masksProperties[e].mode && (this.viewData[e].invRect && (this.element.finalTransform.mProp.mdf || this.firstFrame) && (this.viewData[e].invRect.setAttribute("x", -t.props[12]), this.viewData[e].invRect.setAttribute("y", -t.props[13])), this.storedData[e].x && (this.storedData[e].x.mdf || this.firstFrame))) {
        var s = this.storedData[e].expan;this.storedData[e].x.v < 0 ? ("erode" !== this.storedData[e].lastOperator && (this.storedData[e].lastOperator = "erode", this.storedData[e].elem.setAttribute("filter", "url(#" + this.storedData[e].filterId + ")")), s.setAttribute("radius", -this.storedData[e].x.v)) : ("dilate" !== this.storedData[e].lastOperator && (this.storedData[e].lastOperator = "dilate", this.storedData[e].elem.setAttribute("filter", null)), this.storedData[e].elem.setAttribute("stroke-width", 2 * this.storedData[e].x.v));
      }
    }this.firstFrame = !1;
  }, MaskElement.prototype.getMaskelement = function () {
    return this.maskElement;
  }, MaskElement.prototype.createLayerSolidPath = function () {
    var t = "M0,0 ";return t += " h" + this.globalData.compSize.w, t += " v" + this.globalData.compSize.h, t += " h-" + this.globalData.compSize.w, t += " v-" + this.globalData.compSize.h + " ";
  }, MaskElement.prototype.drawPath = function (t, e, r) {
    var s,
        i,
        a = " M" + e.v[0][0] + "," + e.v[0][1];for (i = e._length, s = 1; i > s; s += 1) {
      a += " C" + bm_rnd(e.o[s - 1][0]) + "," + bm_rnd(e.o[s - 1][1]) + " " + bm_rnd(e.i[s][0]) + "," + bm_rnd(e.i[s][1]) + " " + bm_rnd(e.v[s][0]) + "," + bm_rnd(e.v[s][1]);
    }e.c && i > 1 && (a += " C" + bm_rnd(e.o[s - 1][0]) + "," + bm_rnd(e.o[s - 1][1]) + " " + bm_rnd(e.i[0][0]) + "," + bm_rnd(e.i[0][1]) + " " + bm_rnd(e.v[0][0]) + "," + bm_rnd(e.v[0][1])), r.lastPath !== a && (r.elem && (e.c ? t.inv ? r.elem.setAttribute("d", this.solidPath + a) : r.elem.setAttribute("d", a) : r.elem.setAttribute("d", "")), r.lastPath = a);
  }, MaskElement.prototype.getMask = function (t) {
    for (var e = 0, r = this.masksProperties.length; r > e;) {
      if (this.masksProperties[e].nm === t) return { maskPath: this.viewData[e].prop.pv };e += 1;
    }
  }, MaskElement.prototype.destroy = function () {
    this.element = null, this.globalData = null, this.maskElement = null, this.data = null, this.paths = null, this.masksProperties = null;
  }, BaseElement.prototype.checkMasks = function () {
    if (!this.data.hasMask) return !1;for (var t = 0, e = this.data.masksProperties.length; e > t;) {
      if ("n" !== this.data.masksProperties[t].mode && this.data.masksProperties[t].cl !== !1) return !0;t += 1;
    }return !1;
  }, BaseElement.prototype.checkParenting = function () {
    void 0 !== this.data.parent && this.comp.buildElementParenting(this, this.data.parent);
  }, BaseElement.prototype.prepareFrame = function (t) {
    this.data.ip - this.data.st <= t && this.data.op - this.data.st > t ? this.isVisible !== !0 && (this.elemMdf = !0, this.globalData.mdf = !0, this.isVisible = !0, this.firstFrame = !0, this.data.hasMask && (this.maskManager.firstFrame = !0)) : this.isVisible !== !1 && (this.elemMdf = !0, this.globalData.mdf = !0, this.isVisible = !1);var e,
        r = this.dynamicProperties.length;for (e = 0; r > e; e += 1) {
      (this.isVisible || this._isParent && "transform" === this.dynamicProperties[e].type) && (this.dynamicProperties[e].getValue(), this.dynamicProperties[e].mdf && (this.elemMdf = !0, this.globalData.mdf = !0));
    }return this.data.hasMask && this.isVisible && this.maskManager.prepareFrame(t * this.data.sr), this.currentFrameNum = t * this.data.sr, this.isVisible;
  }, BaseElement.prototype.globalToLocal = function (t) {
    var e = [];e.push(this.finalTransform);for (var r = !0, s = this.comp; r;) {
      s.finalTransform ? (s.data.hasMask && e.splice(0, 0, s.finalTransform), s = s.comp) : r = !1;
    }var i,
        a,
        n = e.length;for (i = 0; n > i; i += 1) {
      a = e[i].mat.applyToPointArray(0, 0, 0), t = [t[0] - a[0], t[1] - a[1], 0];
    }return t;
  }, BaseElement.prototype.initExpressions = function () {
    this.layerInterface = LayerExpressionInterface(this), this.data.hasMask && this.layerInterface.registerMaskInterface(this.maskManager);var t = EffectsExpressionInterface.createEffectsInterface(this, this.layerInterface);this.layerInterface.registerEffectsInterface(t), 0 === this.data.ty || this.data.xt ? this.compInterface = CompExpressionInterface(this) : 4 === this.data.ty ? this.layerInterface.shapeInterface = ShapeExpressionInterface.createShapeInterface(this.shapesData, this.viewData, this.layerInterface) : 5 === this.data.ty && (this.layerInterface.textInterface = TextExpressionInterface(this));
  }, BaseElement.prototype.setBlendMode = function () {
    var t = "";switch (this.data.bm) {case 1:
        t = "multiply";break;case 2:
        t = "screen";break;case 3:
        t = "overlay";break;case 4:
        t = "darken";break;case 5:
        t = "lighten";break;case 6:
        t = "color-dodge";break;case 7:
        t = "color-burn";break;case 8:
        t = "hard-light";break;case 9:
        t = "soft-light";break;case 10:
        t = "difference";break;case 11:
        t = "exclusion";break;case 12:
        t = "hue";break;case 13:
        t = "saturation";break;case 14:
        t = "color";break;case 15:
        t = "luminosity";}var e = this.baseElement || this.layerElement;e.style["mix-blend-mode"] = t;
  }, BaseElement.prototype.init = function () {
    this.data.sr || (this.data.sr = 1), this.dynamicProperties = [], this.data.ef && (this.effects = new EffectsManager(this.data, this, this.dynamicProperties)), this.hidden = !1, this.firstFrame = !0, this.isVisible = !1, this._isParent = !1, this.currentFrameNum = -99999, this.lastNum = -99999, this.data.ks && (this.finalTransform = { mProp: PropertyFactory.getProp(this, this.data.ks, 2, null, this.dynamicProperties), matMdf: !1, opMdf: !1, mat: new Matrix(), opacity: 1 }, this.data.ao && (this.finalTransform.mProp.autoOriented = !0), this.finalTransform.op = this.finalTransform.mProp.o, this.transform = this.finalTransform.mProp, 11 !== this.data.ty && this.createElements(), this.data.hasMask && this.addMasks(this.data)), this.elemMdf = !1;
  }, BaseElement.prototype.getType = function () {
    return this.type;
  }, BaseElement.prototype.resetHierarchy = function () {
    this.hierarchy ? this.hierarchy.length = 0 : this.hierarchy = [];
  }, BaseElement.prototype.getHierarchy = function () {
    return this.hierarchy || (this.hierarchy = []), this.hierarchy;
  }, BaseElement.prototype.setHierarchy = function (t) {
    this.hierarchy = t;
  }, BaseElement.prototype.getLayerSize = function () {
    return 5 === this.data.ty ? {
      w: this.data.textData.width, h: this.data.textData.height } : { w: this.data.width, h: this.data.height };
  }, BaseElement.prototype.hide = function () {}, BaseElement.prototype.mHelper = new Matrix(), createElement(BaseElement, SVGBaseElement), SVGBaseElement.prototype.createElements = function () {
    this.layerElement = document.createElementNS(svgNS, "g"), this.transformedElement = this.layerElement, this.data.hasMask && (this.maskedElement = this.layerElement);var t = null;if (this.data.td) {
      if (3 == this.data.td || 1 == this.data.td) {
        var e = document.createElementNS(svgNS, "mask");if (e.setAttribute("id", this.layerId), e.setAttribute("mask-type", 3 == this.data.td ? "luminance" : "alpha"), e.appendChild(this.layerElement), t = e, this.globalData.defs.appendChild(e), !featureSupport.maskType && 1 == this.data.td) {
          e.setAttribute("mask-type", "luminance");var r = randomString(10),
              s = filtersFactory.createFilter(r);this.globalData.defs.appendChild(s), s.appendChild(filtersFactory.createAlphaToLuminanceFilter());var i = document.createElementNS(svgNS, "g");i.appendChild(this.layerElement), t = i, e.appendChild(i), i.setAttribute("filter", "url(#" + r + ")");
        }
      } else if (2 == this.data.td) {
        var a = document.createElementNS(svgNS, "mask");a.setAttribute("id", this.layerId), a.setAttribute("mask-type", "alpha");var n = document.createElementNS(svgNS, "g");a.appendChild(n);var r = randomString(10),
            s = filtersFactory.createFilter(r),
            o = document.createElementNS(svgNS, "feColorMatrix");o.setAttribute("type", "matrix"), o.setAttribute("color-interpolation-filters", "sRGB"), o.setAttribute("values", "1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 -1 1"), s.appendChild(o), this.globalData.defs.appendChild(s);var h = document.createElementNS(svgNS, "rect");if (h.setAttribute("width", this.comp.data.w), h.setAttribute("height", this.comp.data.h), h.setAttribute("x", "0"), h.setAttribute("y", "0"), h.setAttribute("fill", "#ffffff"), h.setAttribute("opacity", "0"), n.setAttribute("filter", "url(#" + r + ")"), n.appendChild(h), n.appendChild(this.layerElement), t = n, !featureSupport.maskType) {
          a.setAttribute("mask-type", "luminance"), s.appendChild(filtersFactory.createAlphaToLuminanceFilter());var i = document.createElementNS(svgNS, "g");n.appendChild(h), i.appendChild(this.layerElement), t = i, n.appendChild(i);
        }this.globalData.defs.appendChild(a);
      }
    } else (this.data.hasMask || this.data.tt) && this.data.tt ? (this.matteElement = document.createElementNS(svgNS, "g"), this.matteElement.appendChild(this.layerElement), t = this.matteElement, this.baseElement = this.matteElement) : this.baseElement = this.layerElement;if (!this.data.ln && !this.data.cl || 4 !== this.data.ty && 0 !== this.data.ty || (this.data.ln && this.layerElement.setAttribute("id", this.data.ln), this.data.cl && this.layerElement.setAttribute("class", this.data.cl)), 0 === this.data.ty) {
      var l = document.createElementNS(svgNS, "clipPath"),
          p = document.createElementNS(svgNS, "path");p.setAttribute("d", "M0,0 L" + this.data.w + ",0 L" + this.data.w + "," + this.data.h + " L0," + this.data.h + "z");var m = "cp_" + randomString(8);if (l.setAttribute("id", m), l.appendChild(p), this.globalData.defs.appendChild(l), this.checkMasks()) {
        var f = document.createElementNS(svgNS, "g");f.setAttribute("clip-path", "url(#" + m + ")"), f.appendChild(this.layerElement), this.transformedElement = f, t ? t.appendChild(this.transformedElement) : this.baseElement = this.transformedElement;
      } else this.layerElement.setAttribute("clip-path", "url(#" + m + ")");
    }0 !== this.data.bm && this.setBlendMode(), this.layerElement !== this.parentContainer && (this.placeholder = null), this.data.ef && (this.effectsManager = new SVGEffects(this)), this.checkParenting();
  }, SVGBaseElement.prototype.setBlendMode = BaseElement.prototype.setBlendMode, SVGBaseElement.prototype.renderFrame = function (t) {
    if (3 === this.data.ty || this.data.hd || !this.isVisible) return !1;this.lastNum = this.currentFrameNum, this.finalTransform.opMdf = this.firstFrame || this.finalTransform.op.mdf, this.finalTransform.matMdf = this.firstFrame || this.finalTransform.mProp.mdf, this.finalTransform.opacity = this.finalTransform.op.v;var e,
        r = this.finalTransform.mat;if (this.hierarchy) {
      var s = 0,
          i = this.hierarchy.length;if (!this.finalTransform.matMdf) for (; i > s;) {
        if (this.hierarchy[s].finalTransform.mProp.mdf) {
          this.finalTransform.matMdf = !0;break;
        }s += 1;
      }if (this.finalTransform.matMdf) for (e = this.finalTransform.mProp.v.props, r.cloneFromProps(e), s = 0; i > s; s += 1) {
        e = this.hierarchy[s].finalTransform.mProp.v.props, r.transform(e[0], e[1], e[2], e[3], e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15]);
      }
    } else this.isVisible && (r = this.finalTransform.mProp.v);return this.finalTransform.matMdf && this.layerElement && this.transformedElement.setAttribute("transform", r.to2dCSS()), this.finalTransform.opMdf && this.layerElement && this.transformedElement.setAttribute("opacity", this.finalTransform.op.v), this.data.hasMask && this.maskManager.renderFrame(r), this.effectsManager && this.effectsManager.renderFrame(this.firstFrame), this.isVisible;
  }, SVGBaseElement.prototype.destroy = function () {
    this.layerElement = null, this.parentContainer = null, this.matteElement && (this.matteElement = null), this.maskManager && this.maskManager.destroy();
  }, SVGBaseElement.prototype.getBaseElement = function () {
    return this.baseElement;
  }, SVGBaseElement.prototype.addMasks = function (t) {
    this.maskManager = new MaskElement(t, this, this.globalData);
  }, SVGBaseElement.prototype.setMatte = function (t) {
    this.matteElement && this.matteElement.setAttribute("mask", "url(#" + t + ")");
  }, SVGBaseElement.prototype.setMatte = function (t) {
    this.matteElement && this.matteElement.setAttribute("mask", "url(#" + t + ")");
  }, SVGBaseElement.prototype.hide = function () {}, ITextElement.prototype.init = function () {
    this._parent.init.call(this), this.lettersChangedFlag = !1, this.currentTextDocumentData = {};var t = this.data;this.viewData = { m: { a: PropertyFactory.getProp(this, t.t.m.a, 1, 0, this.dynamicProperties) } };var e = this.data.t;if (e.a.length) {
      this.viewData.a = Array.apply(null, { length: e.a.length });var r,
          s,
          i,
          a = e.a.length;for (r = 0; a > r; r += 1) {
        i = e.a[r], s = { a: {}, s: {} }, "r" in i.a && (s.a.r = PropertyFactory.getProp(this, i.a.r, 0, degToRads, this.dynamicProperties)), "rx" in i.a && (s.a.rx = PropertyFactory.getProp(this, i.a.rx, 0, degToRads, this.dynamicProperties)), "ry" in i.a && (s.a.ry = PropertyFactory.getProp(this, i.a.ry, 0, degToRads, this.dynamicProperties)), "sk" in i.a && (s.a.sk = PropertyFactory.getProp(this, i.a.sk, 0, degToRads, this.dynamicProperties)), "sa" in i.a && (s.a.sa = PropertyFactory.getProp(this, i.a.sa, 0, degToRads, this.dynamicProperties)), "s" in i.a && (s.a.s = PropertyFactory.getProp(this, i.a.s, 1, .01, this.dynamicProperties)), "a" in i.a && (s.a.a = PropertyFactory.getProp(this, i.a.a, 1, 0, this.dynamicProperties)), "o" in i.a && (s.a.o = PropertyFactory.getProp(this, i.a.o, 0, .01, this.dynamicProperties)), "p" in i.a && (s.a.p = PropertyFactory.getProp(this, i.a.p, 1, 0, this.dynamicProperties)), "sw" in i.a && (s.a.sw = PropertyFactory.getProp(this, i.a.sw, 0, 0, this.dynamicProperties)), "sc" in i.a && (s.a.sc = PropertyFactory.getProp(this, i.a.sc, 1, 0, this.dynamicProperties)), "fc" in i.a && (s.a.fc = PropertyFactory.getProp(this, i.a.fc, 1, 0, this.dynamicProperties)), "fh" in i.a && (s.a.fh = PropertyFactory.getProp(this, i.a.fh, 0, 0, this.dynamicProperties)), "fs" in i.a && (s.a.fs = PropertyFactory.getProp(this, i.a.fs, 0, .01, this.dynamicProperties)), "fb" in i.a && (s.a.fb = PropertyFactory.getProp(this, i.a.fb, 0, .01, this.dynamicProperties)), "t" in i.a && (s.a.t = PropertyFactory.getProp(this, i.a.t, 0, 0, this.dynamicProperties)), s.s = PropertyFactory.getTextSelectorProp(this, i.s, this.dynamicProperties), s.s.t = i.s.t, this.viewData.a[r] = s;
      }
    } else this.viewData.a = [];e.p && "m" in e.p ? (this.viewData.p = { f: PropertyFactory.getProp(this, e.p.f, 0, 0, this.dynamicProperties), l: PropertyFactory.getProp(this, e.p.l, 0, 0, this.dynamicProperties), r: e.p.r, m: this.maskManager.getMaskProperty(e.p.m) }, this.maskPath = !0) : this.maskPath = !1;
  }, ITextElement.prototype.prepareFrame = function (t) {
    var e = 0,
        r = this.data.t.d.k.length,
        s = this.data.t.d.k[e].s;for (e += 1; r > e && !(this.data.t.d.k[e].t > t);) {
      s = this.data.t.d.k[e].s, e += 1;
    }this.lettersChangedFlag = !1, s !== this.currentTextDocumentData && (this.currentTextDocumentData = s, this.lettersChangedFlag = !0, this.buildNewText()), this._parent.prepareFrame.call(this, t);
  }, ITextElement.prototype.createPathShape = function (t, e) {
    var r,
        s,
        i,
        a,
        n = e.length,
        o = "";for (r = 0; n > r; r += 1) {
      for (i = e[r].ks.k.i.length, a = e[r].ks.k, s = 1; i > s; s += 1) {
        1 == s && (o += " M" + t.applyToPointStringified(a.v[0][0], a.v[0][1])), o += " C" + t.applyToPointStringified(a.o[s - 1][0], a.o[s - 1][1]) + " " + t.applyToPointStringified(a.i[s][0], a.i[s][1]) + " " + t.applyToPointStringified(a.v[s][0], a.v[s][1]);
      }o += " C" + t.applyToPointStringified(a.o[s - 1][0], a.o[s - 1][1]) + " " + t.applyToPointStringified(a.i[0][0], a.i[0][1]) + " " + t.applyToPointStringified(a.v[0][0], a.v[0][1]), o += "z";
    }return o;
  }, ITextElement.prototype.getMeasures = function () {
    var t,
        e,
        r,
        s,
        i = this.mHelper,
        a = this.renderType,
        n = this.data,
        o = this.currentTextDocumentData,
        h = o.l;if (this.maskPath) {
      var l = this.viewData.p.m;if (!this.viewData.p.n || this.viewData.p.mdf) {
        var p = l.v;this.viewData.p.r && (p = reversePath(p));var m = { tLength: 0, segments: [] };s = p.v.length - 1;var f,
            c = 0;for (r = 0; s > r; r += 1) {
          f = { s: p.v[r], e: p.v[r + 1], to: [p.o[r][0] - p.v[r][0], p.o[r][1] - p.v[r][1]], ti: [p.i[r + 1][0] - p.v[r + 1][0], p.i[r + 1][1] - p.v[r + 1][1]] }, bez.buildBezierData(f), m.tLength += f.bezierData.segmentLength, m.segments.push(f), c += f.bezierData.segmentLength;
        }r = s, l.v.c && (f = { s: p.v[r], e: p.v[0], to: [p.o[r][0] - p.v[r][0], p.o[r][1] - p.v[r][1]], ti: [p.i[0][0] - p.v[0][0], p.i[0][1] - p.v[0][1]] }, bez.buildBezierData(f), m.tLength += f.bezierData.segmentLength, m.segments.push(f), c += f.bezierData.segmentLength), this.viewData.p.pi = m;
      }var d,
          u,
          y,
          m = this.viewData.p.pi,
          g = this.viewData.p.f.v,
          v = 0,
          b = 1,
          E = 0,
          P = !0,
          x = m.segments;if (0 > g && l.v.c) for (m.tLength < Math.abs(g) && (g = -Math.abs(g) % m.tLength), v = x.length - 1, y = x[v].bezierData.points, b = y.length - 1; 0 > g;) {
        g += y[b].partialLength, b -= 1, 0 > b && (v -= 1, y = x[v].bezierData.points, b = y.length - 1);
      }y = x[v].bezierData.points, u = y[b - 1], d = y[b];var S,
          C,
          k = d.partialLength;
    }s = h.length, t = 0, e = 0;var M,
        A,
        D,
        T,
        w,
        F = 1.2 * o.s * .714,
        I = !0,
        _ = this.viewData,
        V = Array.apply(null, { length: s });T = _.a.length;var R,
        B,
        N,
        L,
        G,
        O,
        j,
        H,
        z,
        W,
        X,
        Y,
        q,
        U,
        Z,
        J,
        K = -1,
        Q = g,
        $ = v,
        tt = b,
        et = -1,
        rt = 0;for (r = 0; s > r; r += 1) {
      if (i.reset(), O = 1, h[r].n) t = 0, e += o.yOffset, e += I ? 1 : 0, g = Q, I = !1, rt = 0, this.maskPath && (v = $, b = tt, y = x[v].bezierData.points, u = y[b - 1], d = y[b], k = d.partialLength, E = 0), V[r] = this.emptyProp;else {
        if (this.maskPath) {
          if (et !== h[r].line) {
            switch (o.j) {case 1:
                g += c - o.lineWidths[h[r].line];break;case 2:
                g += (c - o.lineWidths[h[r].line]) / 2;}et = h[r].line;
          }K !== h[r].ind && (h[K] && (g += h[K].extra), g += h[r].an / 2, K = h[r].ind), g += _.m.a.v[0] * h[r].an / 200;var st = 0;for (D = 0; T > D; D += 1) {
            M = _.a[D].a, "p" in M && (A = _.a[D].s, B = A.getMult(h[r].anIndexes[D], n.t.a[D].s.totalChars), st += B.length ? M.p.v[0] * B[0] : M.p.v[0] * B);
          }for (P = !0; P;) {
            E + k >= g + st || !y ? (S = (g + st - E) / d.partialLength, L = u.point[0] + (d.point[0] - u.point[0]) * S, G = u.point[1] + (d.point[1] - u.point[1]) * S, i.translate(0, -(_.m.a.v[1] * F / 100) + e), P = !1) : y && (E += d.partialLength, b += 1, b >= y.length && (b = 0, v += 1, x[v] ? y = x[v].bezierData.points : l.v.c ? (b = 0, v = 0, y = x[v].bezierData.points) : (E -= d.partialLength, y = null)), y && (u = d, d = y[b], k = d.partialLength));
          }N = h[r].an / 2 - h[r].add, i.translate(-N, 0, 0);
        } else N = h[r].an / 2 - h[r].add, i.translate(-N, 0, 0), i.translate(-_.m.a.v[0] * h[r].an / 200, -_.m.a.v[1] * F / 100, 0);for (rt += h[r].l / 2, D = 0; T > D; D += 1) {
          M = _.a[D].a, "t" in M && (A = _.a[D].s, B = A.getMult(h[r].anIndexes[D], n.t.a[D].s.totalChars), this.maskPath ? g += B.length ? M.t * B[0] : M.t * B : t += B.length ? M.t.v * B[0] : M.t.v * B);
        }for (rt += h[r].l / 2, o.strokeWidthAnim && (H = o.sw || 0), o.strokeColorAnim && (j = o.sc ? [o.sc[0], o.sc[1], o.sc[2]] : [0, 0, 0]), o.fillColorAnim && (z = [o.fc[0], o.fc[1], o.fc[2]]), D = 0; T > D; D += 1) {
          M = _.a[D].a, "a" in M && (A = _.a[D].s, B = A.getMult(h[r].anIndexes[D], n.t.a[D].s.totalChars), B.length ? i.translate(-M.a.v[0] * B[0], -M.a.v[1] * B[1], M.a.v[2] * B[2]) : i.translate(-M.a.v[0] * B, -M.a.v[1] * B, M.a.v[2] * B));
        }for (D = 0; T > D; D += 1) {
          M = _.a[D].a, "s" in M && (A = _.a[D].s, B = A.getMult(h[r].anIndexes[D], n.t.a[D].s.totalChars), B.length ? i.scale(1 + (M.s.v[0] - 1) * B[0], 1 + (M.s.v[1] - 1) * B[1], 1) : i.scale(1 + (M.s.v[0] - 1) * B, 1 + (M.s.v[1] - 1) * B, 1));
        }for (D = 0; T > D; D += 1) {
          if (M = _.a[D].a, A = _.a[D].s, B = A.getMult(h[r].anIndexes[D], n.t.a[D].s.totalChars), "sk" in M && (B.length ? i.skewFromAxis(-M.sk.v * B[0], M.sa.v * B[1]) : i.skewFromAxis(-M.sk.v * B, M.sa.v * B)), "r" in M && i.rotateZ(B.length ? -M.r.v * B[2] : -M.r.v * B), "ry" in M && i.rotateY(B.length ? M.ry.v * B[1] : M.ry.v * B), "rx" in M && i.rotateX(B.length ? M.rx.v * B[0] : M.rx.v * B), "o" in M && (O += B.length ? (M.o.v * B[0] - O) * B[0] : (M.o.v * B - O) * B), o.strokeWidthAnim && "sw" in M && (H += B.length ? M.sw.v * B[0] : M.sw.v * B), o.strokeColorAnim && "sc" in M) for (W = 0; 3 > W; W += 1) {
            j[W] = Math.round(B.length ? 255 * (j[W] + (M.sc.v[W] - j[W]) * B[0]) : 255 * (j[W] + (M.sc.v[W] - j[W]) * B));
          }if (o.fillColorAnim) {
            if ("fc" in M) for (W = 0; 3 > W; W += 1) {
              z[W] = B.length ? z[W] + (M.fc.v[W] - z[W]) * B[0] : z[W] + (M.fc.v[W] - z[W]) * B;
            }"fh" in M && (z = B.length ? addHueToRGB(z, M.fh.v * B[0]) : addHueToRGB(z, M.fh.v * B)), "fs" in M && (z = B.length ? addSaturationToRGB(z, M.fs.v * B[0]) : addSaturationToRGB(z, M.fs.v * B)), "fb" in M && (z = B.length ? addBrightnessToRGB(z, M.fb.v * B[0]) : addBrightnessToRGB(z, M.fb.v * B));
          }
        }for (D = 0; T > D; D += 1) {
          M = _.a[D].a, "p" in M && (A = _.a[D].s, B = A.getMult(h[r].anIndexes[D], n.t.a[D].s.totalChars), this.maskPath ? B.length ? i.translate(0, M.p.v[1] * B[0], -M.p.v[2] * B[1]) : i.translate(0, M.p.v[1] * B, -M.p.v[2] * B) : B.length ? i.translate(M.p.v[0] * B[0], M.p.v[1] * B[1], -M.p.v[2] * B[2]) : i.translate(M.p.v[0] * B, M.p.v[1] * B, -M.p.v[2] * B));
        }if (o.strokeWidthAnim && (X = 0 > H ? 0 : H), o.strokeColorAnim && (Y = "rgb(" + Math.round(255 * j[0]) + "," + Math.round(255 * j[1]) + "," + Math.round(255 * j[2]) + ")"), o.fillColorAnim && (q = "rgb(" + Math.round(255 * z[0]) + "," + Math.round(255 * z[1]) + "," + Math.round(255 * z[2]) + ")"), this.maskPath) {
          if (n.t.p.p) {
            C = (d.point[1] - u.point[1]) / (d.point[0] - u.point[0]);var it = 180 * Math.atan(C) / Math.PI;d.point[0] < u.point[0] && (it += 180), i.rotate(-it * Math.PI / 180);
          }i.translate(L, G, 0), i.translate(_.m.a.v[0] * h[r].an / 200, _.m.a.v[1] * F / 100, 0), g -= _.m.a.v[0] * h[r].an / 200, h[r + 1] && K !== h[r + 1].ind && (g += h[r].an / 2, g += o.tr / 1e3 * o.s);
        } else {
          switch (i.translate(t, e, 0), o.ps && i.translate(o.ps[0], o.ps[1] + o.ascent, 0), o.j) {case 1:
              i.translate(o.justifyOffset + (o.boxWidth - o.lineWidths[h[r].line]), 0, 0);break;case 2:
              i.translate(o.justifyOffset + (o.boxWidth - o.lineWidths[h[r].line]) / 2, 0, 0);}i.translate(N, 0, 0), i.translate(_.m.a.v[0] * h[r].an / 200, _.m.a.v[1] * F / 100, 0), t += h[r].l + o.tr / 1e3 * o.s;
        }"html" === a ? U = i.toCSS() : "svg" === a ? U = i.to2dCSS() : Z = [i.props[0], i.props[1], i.props[2], i.props[3], i.props[4], i.props[5], i.props[6], i.props[7], i.props[8], i.props[9], i.props[10], i.props[11], i.props[12], i.props[13], i.props[14], i.props[15]], J = O, R = this.renderedLetters[r], !R || R.o === J && R.sw === X && R.sc === Y && R.fc === q ? "svg" !== a && "html" !== a || R && R.m === U ? "canvas" !== a || R && R.props[0] === Z[0] && R.props[1] === Z[1] && R.props[4] === Z[4] && R.props[5] === Z[5] && R.props[12] === Z[12] && R.props[13] === Z[13] ? w = R : (this.lettersChangedFlag = !0, w = new LetterProps(J, X, Y, q, null, Z)) : (this.lettersChangedFlag = !0, w = new LetterProps(J, X, Y, q, U)) : (this.lettersChangedFlag = !0, w = new LetterProps(J, X, Y, q, U, Z)), this.renderedLetters[r] = w;
      }
    }
  }, ITextElement.prototype.emptyProp = new LetterProps(), createElement(SVGBaseElement, SVGTextElement), SVGTextElement.prototype.init = ITextElement.prototype.init, SVGTextElement.prototype.createPathShape = ITextElement.prototype.createPathShape, SVGTextElement.prototype.getMeasures = ITextElement.prototype.getMeasures, SVGTextElement.prototype.prepareFrame = ITextElement.prototype.prepareFrame, SVGTextElement.prototype.createElements = function () {
    this._parent.createElements.call(this), this.data.ln && this.layerElement.setAttribute("id", this.data.ln), this.data.cl && this.layerElement.setAttribute("class", this.data.cl);
  }, SVGTextElement.prototype.buildNewText = function () {
    var t,
        e,
        r = this.currentTextDocumentData;this.renderedLetters = Array.apply(null, { length: this.currentTextDocumentData.l ? this.currentTextDocumentData.l.length : 0 }), r.fc ? this.layerElement.setAttribute("fill", "rgb(" + Math.round(255 * r.fc[0]) + "," + Math.round(255 * r.fc[1]) + "," + Math.round(255 * r.fc[2]) + ")") : this.layerElement.setAttribute("fill", "rgba(0,0,0,0)"), r.sc && (this.layerElement.setAttribute("stroke", "rgb(" + Math.round(255 * r.sc[0]) + "," + Math.round(255 * r.sc[1]) + "," + Math.round(255 * r.sc[2]) + ")"), this.layerElement.setAttribute("stroke-width", r.sw)), this.layerElement.setAttribute("font-size", r.s);var s = this.globalData.fontManager.getFontByName(r.f);if (s.fClass) this.layerElement.setAttribute("class", s.fClass);else {
      this.layerElement.setAttribute("font-family", s.fFamily);var i = r.fWeight,
          a = r.fStyle;this.layerElement.setAttribute("font-style", a), this.layerElement.setAttribute("font-weight", i);
    }var n = r.l || [];if (e = n.length) {
      var o,
          h,
          l = this.mHelper,
          p = "",
          m = this.data.singleShape;if (m) var f = 0,
          c = 0,
          d = r.lineWidths,
          u = r.boxWidth,
          y = !0;var g = 0;for (t = 0; e > t; t += 1) {
        if (this.globalData.fontManager.chars ? m && 0 !== t || (o = this.textSpans[g] ? this.textSpans[g] : document.createElementNS(svgNS, "path")) : o = this.textSpans[g] ? this.textSpans[g] : document.createElementNS(svgNS, "text"), o.style.display = "inherit", o.setAttribute("stroke-linecap", "butt"), o.setAttribute("stroke-linejoin", "round"), o.setAttribute("stroke-miterlimit", "4"), m && n[t].n && (f = 0, c += r.yOffset, c += y ? 1 : 0, y = !1), l.reset(), this.globalData.fontManager.chars && l.scale(r.s / 100, r.s / 100), m) {
          switch (r.ps && l.translate(r.ps[0], r.ps[1] + r.ascent, 0), r.j) {case 1:
              l.translate(r.justifyOffset + (u - d[n[t].line]), 0, 0);break;case 2:
              l.translate(r.justifyOffset + (u - d[n[t].line]) / 2, 0, 0);}l.translate(f, c, 0);
        }if (this.globalData.fontManager.chars) {
          var v,
              b = this.globalData.fontManager.getCharData(r.t.charAt(t), s.fStyle, this.globalData.fontManager.getFontByName(r.f).fFamily);v = b ? b.data : null, v && v.shapes && (h = v.shapes[0].it, m || (p = ""), p += this.createPathShape(l, h), m || o.setAttribute("d", p)), m || this.layerElement.appendChild(o);
        } else o.textContent = n[t].val, o.setAttributeNS("http://www.w3.org/XML/1998/namespace", "xml:space", "preserve"), this.layerElement.appendChild(o), m && o.setAttribute("transform", l.to2dCSS());m && (f += n[t].l, f += r.tr / 1e3 * r.s), this.textSpans[g] = o, g += 1;
      }if (!m) for (; g < this.textSpans.length;) {
        this.textSpans[g].style.display = "none", g += 1;
      }m && this.globalData.fontManager.chars && (o.setAttribute("d", p), this.layerElement.appendChild(o));
    }
  }, SVGTextElement.prototype.hide = function () {
    this.hidden || (this.layerElement.style.display = "none", this.hidden = !0);
  }, SVGTextElement.prototype.renderFrame = function (t) {
    var e = this._parent.renderFrame.call(this, t);if (e === !1) return void this.hide();if (this.hidden && (this.hidden = !1, this.layerElement.style.display = "block"), !this.data.singleShape && (this.getMeasures(), this.lettersChangedFlag)) {
      var r,
          s,
          i = this.renderedLetters,
          a = this.currentTextDocumentData.l;s = a.length;var n;for (r = 0; s > r; r += 1) {
        a[r].n || (n = i[r], this.textSpans[r].setAttribute("transform", n.m), this.textSpans[r].setAttribute("opacity", n.o), n.sw && this.textSpans[r].setAttribute("stroke-width", n.sw), n.sc && this.textSpans[r].setAttribute("stroke", n.sc), n.fc && this.textSpans[r].setAttribute("fill", n.fc));
      }this.firstFrame && (this.firstFrame = !1);
    }
  }, SVGTextElement.prototype.destroy = function () {
    this._parent.destroy.call(this._parent);
  }, SVGTintFilter.prototype.renderFrame = function (t) {
    if (t || this.filterManager.mdf) {
      var e = this.filterManager.effectElements[0].p.v,
          r = this.filterManager.effectElements[1].p.v,
          s = this.filterManager.effectElements[2].p.v / 100;this.matrixFilter.setAttribute("values", r[0] - e[0] + " 0 0 0 " + e[0] + " " + (r[1] - e[1]) + " 0 0 0 " + e[1] + " " + (r[2] - e[2]) + " 0 0 0 " + e[2] + " 0 0 0 " + s + " 0");
    }
  }, SVGFillFilter.prototype.renderFrame = function (t) {
    if (t || this.filterManager.mdf) {
      var e = this.filterManager.effectElements[2].p.v,
          r = this.filterManager.effectElements[6].p.v;this.matrixFilter.setAttribute("values", "0 0 0 0 " + e[0] + " 0 0 0 0 " + e[1] + " 0 0 0 0 " + e[2] + " 0 0 0 " + r + " 0");
    }
  }, SVGStrokeEffect.prototype.initialize = function () {
    var t,
        e,
        r,
        s,
        i = this.elem.layerElement.children || this.elem.layerElement.childNodes;for (1 === this.filterManager.effectElements[1].p.v ? (s = this.elem.maskManager.masksProperties.length, r = 0) : (r = this.filterManager.effectElements[0].p.v - 1, s = r + 1), e = document.createElementNS(svgNS, "g"), e.setAttribute("fill", "none"), e.setAttribute("stroke-linecap", "round"), e.setAttribute("stroke-dashoffset", 1), r; s > r; r += 1) {
      t = document.createElementNS(svgNS, "path"), e.appendChild(t), this.paths.push({ p: t, m: r });
    }if (3 === this.filterManager.effectElements[10].p.v) {
      var a = document.createElementNS(svgNS, "mask"),
          n = "stms_" + randomString(10);a.setAttribute("id", n), a.setAttribute("mask-type", "alpha"), a.appendChild(e), this.elem.globalData.defs.appendChild(a);var o = document.createElementNS(svgNS, "g");o.setAttribute("mask", "url(#" + n + ")"), i[0] && o.appendChild(i[0]), this.elem.layerElement.appendChild(o), this.masker = a, e.setAttribute("stroke", "#fff");
    } else if (1 === this.filterManager.effectElements[10].p.v || 2 === this.filterManager.effectElements[10].p.v) {
      if (2 === this.filterManager.effectElements[10].p.v) for (var i = this.elem.layerElement.children || this.elem.layerElement.childNodes; i.length;) {
        this.elem.layerElement.removeChild(i[0]);
      }this.elem.layerElement.appendChild(e), this.elem.layerElement.removeAttribute("mask"), e.setAttribute("stroke", "#fff");
    }this.initialized = !0, this.pathMasker = e;
  }, SVGStrokeEffect.prototype.renderFrame = function (t) {
    this.initialized || this.initialize();var e,
        r,
        s,
        i = this.paths.length;for (e = 0; i > e; e += 1) {
      if (r = this.elem.maskManager.viewData[this.paths[e].m], s = this.paths[e].p, (t || this.filterManager.mdf || r.prop.mdf) && s.setAttribute("d", r.lastPath), t || this.filterManager.effectElements[9].p.mdf || this.filterManager.effectElements[4].p.mdf || this.filterManager.effectElements[7].p.mdf || this.filterManager.effectElements[8].p.mdf || r.prop.mdf) {
        var a;if (0 !== this.filterManager.effectElements[7].p.v || 100 !== this.filterManager.effectElements[8].p.v) {
          var n = Math.min(this.filterManager.effectElements[7].p.v, this.filterManager.effectElements[8].p.v) / 100,
              o = Math.max(this.filterManager.effectElements[7].p.v, this.filterManager.effectElements[8].p.v) / 100,
              h = s.getTotalLength();a = "0 0 0 " + h * n + " ";var l,
              p = h * (o - n),
              m = 1 + 2 * this.filterManager.effectElements[4].p.v * this.filterManager.effectElements[9].p.v / 100,
              f = Math.floor(p / m);for (l = 0; f > l; l += 1) {
            a += "1 " + 2 * this.filterManager.effectElements[4].p.v * this.filterManager.effectElements[9].p.v / 100 + " ";
          }a += "0 " + 10 * h + " 0 0";
        } else a = "1 " + 2 * this.filterManager.effectElements[4].p.v * this.filterManager.effectElements[9].p.v / 100;s.setAttribute("stroke-dasharray", a);
      }
    }if ((t || this.filterManager.effectElements[4].p.mdf) && this.pathMasker.setAttribute("stroke-width", 2 * this.filterManager.effectElements[4].p.v), (t || this.filterManager.effectElements[6].p.mdf) && this.pathMasker.setAttribute("opacity", this.filterManager.effectElements[6].p.v), (1 === this.filterManager.effectElements[10].p.v || 2 === this.filterManager.effectElements[10].p.v) && (t || this.filterManager.effectElements[3].p.mdf)) {
      var c = this.filterManager.effectElements[3].p.v;this.pathMasker.setAttribute("stroke", "rgb(" + bm_floor(255 * c[0]) + "," + bm_floor(255 * c[1]) + "," + bm_floor(255 * c[2]) + ")");
    }
  }, SVGTritoneFilter.prototype.renderFrame = function (t) {
    if (t || this.filterManager.mdf) {
      var e = this.filterManager.effectElements[0].p.v,
          r = this.filterManager.effectElements[1].p.v,
          s = this.filterManager.effectElements[2].p.v,
          i = s[0] + " " + r[0] + " " + e[0],
          a = s[1] + " " + r[1] + " " + e[1],
          n = s[2] + " " + r[2] + " " + e[2];this.feFuncR.setAttribute("tableValues", i), this.feFuncG.setAttribute("tableValues", a), this.feFuncB.setAttribute("tableValues", n);
    }
  }, SVGProLevelsFilter.prototype.createFeFunc = function (t, e) {
    var r = document.createElementNS(svgNS, t);return r.setAttribute("type", "table"), e.appendChild(r), r;
  }, SVGProLevelsFilter.prototype.getTableValue = function (t, e, r, s, i) {
    for (var a, n, o = 0, h = 256, l = Math.min(t, e), p = Math.max(t, e), m = Array.call(null, { length: h }), f = 0, c = i - s, d = e - t; 256 >= o;) {
      a = o / 256, n = l >= a ? 0 > d ? i : s : a >= p ? 0 > d ? s : i : s + c * Math.pow((a - t) / d, 1 / r), m[f++] = n, o += 256 / (h - 1);
    }return m.join(" ");
  }, SVGProLevelsFilter.prototype.renderFrame = function (t) {
    if (t || this.filterManager.mdf) {
      var e,
          r = this.filterManager.effectElements;this.feFuncRComposed && (t || r[2].p.mdf || r[3].p.mdf || r[4].p.mdf || r[5].p.mdf || r[6].p.mdf) && (e = this.getTableValue(r[2].p.v, r[3].p.v, r[4].p.v, r[5].p.v, r[6].p.v), this.feFuncRComposed.setAttribute("tableValues", e), this.feFuncGComposed.setAttribute("tableValues", e), this.feFuncBComposed.setAttribute("tableValues", e)), this.feFuncR && (t || r[9].p.mdf || r[10].p.mdf || r[11].p.mdf || r[12].p.mdf || r[13].p.mdf) && (e = this.getTableValue(r[9].p.v, r[10].p.v, r[11].p.v, r[12].p.v, r[13].p.v), this.feFuncR.setAttribute("tableValues", e)), this.feFuncG && (t || r[16].p.mdf || r[17].p.mdf || r[18].p.mdf || r[19].p.mdf || r[20].p.mdf) && (e = this.getTableValue(r[16].p.v, r[17].p.v, r[18].p.v, r[19].p.v, r[20].p.v), this.feFuncG.setAttribute("tableValues", e)), this.feFuncB && (t || r[23].p.mdf || r[24].p.mdf || r[25].p.mdf || r[26].p.mdf || r[27].p.mdf) && (e = this.getTableValue(r[23].p.v, r[24].p.v, r[25].p.v, r[26].p.v, r[27].p.v), this.feFuncB.setAttribute("tableValues", e)), this.feFuncA && (t || r[30].p.mdf || r[31].p.mdf || r[32].p.mdf || r[33].p.mdf || r[34].p.mdf) && (e = this.getTableValue(r[30].p.v, r[31].p.v, r[32].p.v, r[33].p.v, r[34].p.v), this.feFuncA.setAttribute("tableValues", e));
    }
  }, SVGDropShadowEffect.prototype.renderFrame = function (t) {
    if (t || this.filterManager.mdf) {
      if ((t || this.filterManager.effectElements[4].p.mdf) && this.feGaussianBlur.setAttribute("stdDeviation", this.filterManager.effectElements[4].p.v / 4), t || this.filterManager.effectElements[0].p.mdf) {
        var e = this.filterManager.effectElements[0].p.v;this.feFlood.setAttribute("flood-color", rgbToHex(Math.round(255 * e[0]), Math.round(255 * e[1]), Math.round(255 * e[2])));
      }if ((t || this.filterManager.effectElements[1].p.mdf) && this.feFlood.setAttribute("flood-opacity", this.filterManager.effectElements[1].p.v / 255), t || this.filterManager.effectElements[2].p.mdf || this.filterManager.effectElements[3].p.mdf) {
        var r = this.filterManager.effectElements[3].p.v,
            s = (this.filterManager.effectElements[2].p.v - 90) * degToRads,
            i = r * Math.cos(s),
            a = r * Math.sin(s);this.feOffset.setAttribute("dx", i), this.feOffset.setAttribute("dy", a);
      }
    }
  }, SVGEffects.prototype.renderFrame = function (t) {
    var e,
        r = this.filters.length;for (e = 0; r > e; e += 1) {
      this.filters[e].renderFrame(t);
    }
  }, createElement(SVGBaseElement, ICompElement), ICompElement.prototype.hide = function () {
    if (!this.hidden) {
      var t,
          e = this.elements.length;for (t = 0; e > t; t += 1) {
        this.elements[t] && this.elements[t].hide();
      }this.hidden = !0;
    }
  }, ICompElement.prototype.prepareFrame = function (t) {
    if (this._parent.prepareFrame.call(this, t), this.isVisible !== !1 || this.data.xt) {
      if (this.tm) {
        var e = this.tm.v;e === this.data.op && (e = this.data.op - 1), this.renderedFrame = e;
      } else this.renderedFrame = t / this.data.sr;var r,
          s = this.elements.length;for (this.completeLayers || this.checkLayers(this.renderedFrame), r = 0; s > r; r += 1) {
        (this.completeLayers || this.elements[r]) && this.elements[r].prepareFrame(this.renderedFrame - this.layers[r].st);
      }
    }
  }, ICompElement.prototype.renderFrame = function (t) {
    var e,
        r = this._parent.renderFrame.call(this, t),
        s = this.layers.length;if (r === !1) return void this.hide();for (this.hidden = !1, e = 0; s > e; e += 1) {
      (this.completeLayers || this.elements[e]) && this.elements[e].renderFrame();
    }this.firstFrame && (this.firstFrame = !1);
  }, ICompElement.prototype.setElements = function (t) {
    this.elements = t;
  }, ICompElement.prototype.getElements = function () {
    return this.elements;
  }, ICompElement.prototype.destroy = function () {
    this._parent.destroy.call(this._parent);var t,
        e = this.layers.length;for (t = 0; e > t; t += 1) {
      this.elements[t] && this.elements[t].destroy();
    }
  }, ICompElement.prototype.checkLayers = SVGRenderer.prototype.checkLayers, ICompElement.prototype.buildItem = SVGRenderer.prototype.buildItem, ICompElement.prototype.buildAllItems = SVGRenderer.prototype.buildAllItems, ICompElement.prototype.buildElementParenting = SVGRenderer.prototype.buildElementParenting, ICompElement.prototype.createItem = SVGRenderer.prototype.createItem, ICompElement.prototype.createImage = SVGRenderer.prototype.createImage, ICompElement.prototype.createComp = SVGRenderer.prototype.createComp, ICompElement.prototype.createSolid = SVGRenderer.prototype.createSolid, ICompElement.prototype.createShape = SVGRenderer.prototype.createShape, ICompElement.prototype.createText = SVGRenderer.prototype.createText, ICompElement.prototype.createBase = SVGRenderer.prototype.createBase, ICompElement.prototype.appendElementInPos = SVGRenderer.prototype.appendElementInPos, ICompElement.prototype.checkPendingElements = SVGRenderer.prototype.checkPendingElements, ICompElement.prototype.addPendingElement = SVGRenderer.prototype.addPendingElement, createElement(SVGBaseElement, IImageElement), IImageElement.prototype.createElements = function () {
    var t = this.globalData.getAssetsPath(this.assetData);this._parent.createElements.call(this), this.innerElem = document.createElementNS(svgNS, "image"), this.innerElem.setAttribute("width", this.assetData.w + "px"), this.innerElem.setAttribute("height", this.assetData.h + "px"), this.innerElem.setAttribute("preserveAspectRatio", "xMidYMid slice"), this.innerElem.setAttributeNS("http://www.w3.org/1999/xlink", "href", t), this.maskedElement = this.innerElem, this.layerElement.appendChild(this.innerElem), this.data.ln && this.layerElement.setAttribute("id", this.data.ln), this.data.cl && this.layerElement.setAttribute("class", this.data.cl);
  }, IImageElement.prototype.hide = function () {
    this.hidden || (this.layerElement.style.display = "none", this.hidden = !0);
  }, IImageElement.prototype.renderFrame = function (t) {
    var e = this._parent.renderFrame.call(this, t);return e === !1 ? void this.hide() : (this.hidden && (this.hidden = !1, this.layerElement.style.display = "block"), void (this.firstFrame && (this.firstFrame = !1)));
  }, IImageElement.prototype.destroy = function () {
    this._parent.destroy.call(this._parent), this.innerElem = null;
  }, createElement(SVGBaseElement, IShapeElement), IShapeElement.prototype.lcEnum = { 1: "butt", 2: "round", 3: "butt" }, IShapeElement.prototype.ljEnum = { 1: "miter", 2: "round", 3: "butt" }, IShapeElement.prototype.buildExpressionInterface = function () {}, IShapeElement.prototype.createElements = function () {
    this._parent.createElements.call(this), this.searchShapes(this.shapesData, this.viewData, this.layerElement, this.dynamicProperties, 0), (!this.data.hd || this.data.td) && styleUnselectableDiv(this.layerElement);
  }, IShapeElement.prototype.setGradientData = function (t, e, r) {
    var s,
        i = "gr_" + randomString(10);s = 1 === e.t ? document.createElementNS(svgNS, "linearGradient") : document.createElementNS(svgNS, "radialGradient"), s.setAttribute("id", i), s.setAttribute("spreadMethod", "pad"), s.setAttribute("gradientUnits", "userSpaceOnUse");var a,
        n,
        o,
        h = [];for (o = 4 * e.g.p, n = 0; o > n; n += 4) {
      a = document.createElementNS(svgNS, "stop"), s.appendChild(a), h.push(a);
    }t.setAttribute("gf" === e.ty ? "fill" : "stroke", "url(#" + i + ")"), this.globalData.defs.appendChild(s), r.gf = s, r.cst = h;
  }, IShapeElement.prototype.setGradientOpacity = function (t, e, r) {
    if (t.g.k.k[0].s && t.g.k.k[0].s.length > 4 * t.g.p || t.g.k.k.length > 4 * t.g.p) {
      var s,
          i,
          a,
          n,
          o = document.createElementNS(svgNS, "mask"),
          h = document.createElementNS(svgNS, "path");o.appendChild(h);var l = "op_" + randomString(10),
          p = "mk_" + randomString(10);o.setAttribute("id", p), s = 1 === t.t ? document.createElementNS(svgNS, "linearGradient") : document.createElementNS(svgNS, "radialGradient"), s.setAttribute("id", l), s.setAttribute("spreadMethod", "pad"), s.setAttribute("gradientUnits", "userSpaceOnUse"), n = t.g.k.k[0].s ? t.g.k.k[0].s.length : t.g.k.k.length;var m = [];for (a = 4 * t.g.p; n > a; a += 2) {
        i = document.createElementNS(svgNS, "stop"), i.setAttribute("stop-color", "rgb(255,255,255)"), s.appendChild(i), m.push(i);
      }return h.setAttribute("gf" === t.ty ? "fill" : "stroke", "url(#" + l + ")"), this.globalData.defs.appendChild(s), this.globalData.defs.appendChild(o), e.of = s, e.ost = m, r.msElem = h, p;
    }
  }, IShapeElement.prototype.searchShapes = function (t, e, r, s, i, a) {
    a = a || [];var n,
        o,
        h,
        l,
        p,
        m = [].concat(a),
        f = t.length - 1,
        c = [],
        d = [];for (n = f; n >= 0; n -= 1) {
      if ("fl" == t[n].ty || "st" == t[n].ty || "gf" == t[n].ty || "gs" == t[n].ty) {
        e[n] = {}, l = { type: t[n].ty, d: "", ld: "", lvl: i, mdf: !1 };var u = document.createElementNS(svgNS, "path");if (e[n].o = PropertyFactory.getProp(this, t[n].o, 0, .01, s), ("st" == t[n].ty || "gs" == t[n].ty) && (u.setAttribute("stroke-linecap", this.lcEnum[t[n].lc] || "round"), u.setAttribute("stroke-linejoin", this.ljEnum[t[n].lj] || "round"), u.setAttribute("fill-opacity", "0"), 1 == t[n].lj && u.setAttribute("stroke-miterlimit", t[n].ml), e[n].w = PropertyFactory.getProp(this, t[n].w, 0, null, s), t[n].d)) {
          var y = PropertyFactory.getDashProp(this, t[n].d, "svg", s);y.k || (u.setAttribute("stroke-dasharray", y.dasharray), u.setAttribute("stroke-dashoffset", y.dashoffset)), e[n].d = y;
        }if ("fl" == t[n].ty || "st" == t[n].ty) e[n].c = PropertyFactory.getProp(this, t[n].c, 1, 255, s), r.appendChild(u);else {
          e[n].g = PropertyFactory.getGradientProp(this, t[n].g, s), 2 == t[n].t && (e[n].h = PropertyFactory.getProp(this, t[n].h, 1, .01, s), e[n].a = PropertyFactory.getProp(this, t[n].a, 1, degToRads, s)), e[n].s = PropertyFactory.getProp(this, t[n].s, 1, null, s), e[n].e = PropertyFactory.getProp(this, t[n].e, 1, null, s), this.setGradientData(u, t[n], e[n], l);

          var g = this.setGradientOpacity(t[n], e[n], l);g && u.setAttribute("mask", "url(#" + g + ")"), e[n].elem = u, r.appendChild(u);
        }2 === t[n].r && u.setAttribute("fill-rule", "evenodd"), t[n].ln && u.setAttribute("id", t[n].ln), t[n].cl && u.setAttribute("class", t[n].cl), l.pElem = u, this.stylesList.push(l), e[n].style = l, c.push(l);
      } else if ("gr" == t[n].ty) {
        e[n] = { it: [] };var v = document.createElementNS(svgNS, "g");r.appendChild(v), e[n].gr = v, this.searchShapes(t[n].it, e[n].it, v, s, i + 1, m);
      } else if ("tr" == t[n].ty) e[n] = { transform: { op: PropertyFactory.getProp(this, t[n].o, 0, .01, s), mProps: PropertyFactory.getProp(this, t[n], 2, null, s) }, elements: [] }, p = e[n].transform, m.push(p);else if ("sh" == t[n].ty || "rc" == t[n].ty || "el" == t[n].ty || "sr" == t[n].ty) {
        e[n] = { elements: [], caches: [], styles: [], transformers: m, lStr: "" };var b = 4;for ("rc" == t[n].ty ? b = 5 : "el" == t[n].ty ? b = 6 : "sr" == t[n].ty && (b = 7), e[n].sh = ShapePropertyFactory.getShapeProp(this, t[n], b, s), e[n].lvl = i, this.shapes.push(e[n].sh), this.addShapeToModifiers(e[n]), h = this.stylesList.length, o = 0; h > o; o += 1) {
          this.stylesList[o].closed || e[n].elements.push({ ty: this.stylesList[o].type, st: this.stylesList[o] });
        }
      } else if ("tm" == t[n].ty || "rd" == t[n].ty || "ms" == t[n].ty || "rp" == t[n].ty) {
        var E = ShapeModifiers.getModifier(t[n].ty);E.init(this, t[n], s), this.shapeModifiers.push(E), d.push(E), e[n] = E;
      }
    }for (f = c.length, n = 0; f > n; n += 1) {
      c[n].closed = !0;
    }for (f = d.length, n = 0; f > n; n += 1) {
      d[n].closed = !0;
    }
  }, IShapeElement.prototype.addShapeToModifiers = function (t) {
    var e,
        r = this.shapeModifiers.length;for (e = 0; r > e; e += 1) {
      this.shapeModifiers[e].addShape(t);
    }
  }, IShapeElement.prototype.renderModifiers = function () {
    if (this.shapeModifiers.length) {
      var t,
          e = this.shapes.length;for (t = 0; e > t; t += 1) {
        this.shapes[t].reset();
      }for (e = this.shapeModifiers.length, t = e - 1; t >= 0; t -= 1) {
        this.shapeModifiers[t].processShapes(this.firstFrame);
      }
    }
  }, IShapeElement.prototype.renderFrame = function (t) {
    var e = this._parent.renderFrame.call(this, t);return e === !1 ? void this.hide() : (this.globalToLocal([0, 0, 0]), this.hidden && (this.layerElement.style.display = "block", this.hidden = !1), this.renderModifiers(), void this.renderShape(null, null, !0, null));
  }, IShapeElement.prototype.hide = function () {
    if (!this.hidden) {
      this.layerElement.style.display = "none";var t,
          e = this.stylesList.length;for (t = e - 1; t >= 0; t -= 1) {
        "0" !== this.stylesList[t].ld && (this.stylesList[t].ld = "0", this.stylesList[t].pElem.style.display = "none", this.stylesList[t].pElem.parentNode && (this.stylesList[t].parent = this.stylesList[t].pElem.parentNode));
      }this.hidden = !0;
    }
  }, IShapeElement.prototype.renderShape = function (t, e, r, s) {
    var i, a;if (!t) for (t = this.shapesData, a = this.stylesList.length, i = 0; a > i; i += 1) {
      this.stylesList[i].d = "", this.stylesList[i].mdf = !1;
    }e || (e = this.viewData), a = t.length - 1;var n;for (i = a; i >= 0; i -= 1) {
      n = t[i].ty, "tr" == n ? ((this.firstFrame || e[i].transform.op.mdf && s) && s.setAttribute("opacity", e[i].transform.op.v), (this.firstFrame || e[i].transform.mProps.mdf && s) && s.setAttribute("transform", e[i].transform.mProps.v.to2dCSS())) : "sh" == n || "el" == n || "rc" == n || "sr" == n ? this.renderPath(t[i], e[i]) : "fl" == n ? this.renderFill(t[i], e[i]) : "gf" == n ? this.renderGradient(t[i], e[i]) : "gs" == n ? (this.renderGradient(t[i], e[i]), this.renderStroke(t[i], e[i])) : "st" == n ? this.renderStroke(t[i], e[i]) : "gr" == n && this.renderShape(t[i].it, e[i].it, !1, e[i].gr);
    }if (r) {
      for (a = this.stylesList.length, i = 0; a > i; i += 1) {
        "0" === this.stylesList[i].ld && (this.stylesList[i].ld = "1", this.stylesList[i].pElem.style.display = "block"), (this.stylesList[i].mdf || this.firstFrame) && (this.stylesList[i].pElem.setAttribute("d", this.stylesList[i].d), this.stylesList[i].msElem && this.stylesList[i].msElem.setAttribute("d", this.stylesList[i].d));
      }this.firstFrame && (this.firstFrame = !1);
    }
  }, IShapeElement.prototype.renderPath = function (t, e) {
    var r,
        s,
        i,
        a,
        n,
        o,
        h,
        l,
        p = e.elements.length,
        m = e.lvl;for (l = 0; p > l; l += 1) {
      o = e.sh.mdf || this.firstFrame, n = "M0 0";var f = e.sh.paths;if (a = f._length, e.elements[l].st.lvl < m) {
        for (var c, d = this.mHelper.reset(), u = m - e.elements[l].st.lvl, y = e.transformers.length - 1; u > 0;) {
          o = e.transformers[y].mProps.mdf || o, c = e.transformers[y].mProps.v.props, d.transform(c[0], c[1], c[2], c[3], c[4], c[5], c[6], c[7], c[8], c[9], c[10], c[11], c[12], c[13], c[14], c[15]), u--, y--;
        }if (o) {
          for (i = 0; a > i; i += 1) {
            if (h = f.shapes[i], h && h._length) {
              for (r = h._length, s = 1; r > s; s += 1) {
                1 == s && (n += " M" + d.applyToPointStringified(h.v[0][0], h.v[0][1])), n += " C" + d.applyToPointStringified(h.o[s - 1][0], h.o[s - 1][1]) + " " + d.applyToPointStringified(h.i[s][0], h.i[s][1]) + " " + d.applyToPointStringified(h.v[s][0], h.v[s][1]);
              }1 == r && (n += " M" + d.applyToPointStringified(h.v[0][0], h.v[0][1])), h.c && (n += " C" + d.applyToPointStringified(h.o[s - 1][0], h.o[s - 1][1]) + " " + d.applyToPointStringified(h.i[0][0], h.i[0][1]) + " " + d.applyToPointStringified(h.v[0][0], h.v[0][1]), n += "z");
            }
          }e.caches[l] = n;
        } else n = e.caches[l];
      } else if (o) {
        for (i = 0; a > i; i += 1) {
          if (h = f.shapes[i], h && h._length) {
            for (r = h._length, s = 1; r > s; s += 1) {
              1 == s && (n += " M" + h.v[0].join(",")), n += " C" + h.o[s - 1].join(",") + " " + h.i[s].join(",") + " " + h.v[s].join(",");
            }1 == r && (n += " M" + h.v[0].join(",")), h.c && r && (n += " C" + h.o[s - 1].join(",") + " " + h.i[0].join(",") + " " + h.v[0].join(","), n += "z");
          }
        }e.caches[l] = n;
      } else n = e.caches[l];e.elements[l].st.d += n, e.elements[l].st.mdf = o || e.elements[l].st.mdf;
    }
  }, IShapeElement.prototype.renderFill = function (t, e) {
    var r = e.style;(e.c.mdf || this.firstFrame) && r.pElem.setAttribute("fill", "rgb(" + bm_floor(e.c.v[0]) + "," + bm_floor(e.c.v[1]) + "," + bm_floor(e.c.v[2]) + ")"), (e.o.mdf || this.firstFrame) && r.pElem.setAttribute("fill-opacity", e.o.v);
  }, IShapeElement.prototype.renderGradient = function (t, e) {
    var r = e.gf,
        s = e.of,
        i = e.s.v,
        a = e.e.v;if (e.o.mdf || this.firstFrame) {
      var n = "gf" === t.ty ? "fill-opacity" : "stroke-opacity";e.elem.setAttribute(n, e.o.v);
    }if (e.s.mdf || this.firstFrame) {
      var o = 1 === t.t ? "x1" : "cx",
          h = "x1" === o ? "y1" : "cy";r.setAttribute(o, i[0]), r.setAttribute(h, i[1]), s && (s.setAttribute(o, i[0]), s.setAttribute(h, i[1]));
    }var l, p, m, f;if (e.g.cmdf || this.firstFrame) {
      l = e.cst;var c = e.g.c;for (m = l.length, p = 0; m > p; p += 1) {
        f = l[p], f.setAttribute("offset", c[4 * p] + "%"), f.setAttribute("stop-color", "rgb(" + c[4 * p + 1] + "," + c[4 * p + 2] + "," + c[4 * p + 3] + ")");
      }
    }if (s && (e.g.omdf || this.firstFrame)) {
      l = e.ost;var d = e.g.o;for (m = l.length, p = 0; m > p; p += 1) {
        f = l[p], f.setAttribute("offset", d[2 * p] + "%"), f.setAttribute("stop-opacity", d[2 * p + 1]);
      }
    }if (1 === t.t) (e.e.mdf || this.firstFrame) && (r.setAttribute("x2", a[0]), r.setAttribute("y2", a[1]), s && (s.setAttribute("x2", a[0]), s.setAttribute("y2", a[1])));else {
      var u;if ((e.s.mdf || e.e.mdf || this.firstFrame) && (u = Math.sqrt(Math.pow(i[0] - a[0], 2) + Math.pow(i[1] - a[1], 2)), r.setAttribute("r", u), s && s.setAttribute("r", u)), e.e.mdf || e.h.mdf || e.a.mdf || this.firstFrame) {
        u || (u = Math.sqrt(Math.pow(i[0] - a[0], 2) + Math.pow(i[1] - a[1], 2)));var y = Math.atan2(a[1] - i[1], a[0] - i[0]),
            g = e.h.v >= 1 ? .99 : e.h.v <= -1 ? -.99 : e.h.v,
            v = u * g,
            b = Math.cos(y + e.a.v) * v + i[0],
            E = Math.sin(y + e.a.v) * v + i[1];r.setAttribute("fx", b), r.setAttribute("fy", E), s && (s.setAttribute("fx", b), s.setAttribute("fy", E));
      }
    }
  }, IShapeElement.prototype.renderStroke = function (t, e) {
    var r = e.style,
        s = e.d;s && s.k && (s.mdf || this.firstFrame) && (r.pElem.setAttribute("stroke-dasharray", s.dasharray), r.pElem.setAttribute("stroke-dashoffset", s.dashoffset)), e.c && (e.c.mdf || this.firstFrame) && r.pElem.setAttribute("stroke", "rgb(" + bm_floor(e.c.v[0]) + "," + bm_floor(e.c.v[1]) + "," + bm_floor(e.c.v[2]) + ")"), (e.o.mdf || this.firstFrame) && r.pElem.setAttribute("stroke-opacity", e.o.v), (e.w.mdf || this.firstFrame) && (r.pElem.setAttribute("stroke-width", e.w.v), r.msElem && r.msElem.setAttribute("stroke-width", e.w.v));
  }, IShapeElement.prototype.destroy = function () {
    this._parent.destroy.call(this._parent), this.shapeData = null, this.viewData = null, this.parentContainer = null, this.placeholder = null;
  }, createElement(SVGBaseElement, ISolidElement), ISolidElement.prototype.createElements = function () {
    this._parent.createElements.call(this);var t = document.createElementNS(svgNS, "rect");t.setAttribute("width", this.data.sw), t.setAttribute("height", this.data.sh), t.setAttribute("fill", this.data.sc), this.layerElement.appendChild(t), this.innerElem = t, this.data.ln && this.layerElement.setAttribute("id", this.data.ln), this.data.cl && this.layerElement.setAttribute("class", this.data.cl);
  }, ISolidElement.prototype.hide = IImageElement.prototype.hide, ISolidElement.prototype.renderFrame = IImageElement.prototype.renderFrame, ISolidElement.prototype.destroy = IImageElement.prototype.destroy;var animationManager = function () {
    function t(t) {
      for (var e = 0, r = t.target; C > e;) {
        x[e].animation === r && (x.splice(e, 1), e -= 1, C -= 1, r.isPaused || s()), e += 1;
      }
    }function e(t, e) {
      if (!t) return null;for (var r = 0; C > r;) {
        if (x[r].elem == t && null !== x[r].elem) return x[r].animation;r += 1;
      }var s = new AnimationItem();return i(s, t), s.setData(t, e), s;
    }function r() {
      M += 1, E();
    }function s() {
      M -= 1, 0 === M && (k = !0);
    }function i(e, i) {
      e.addEventListener("destroy", t), e.addEventListener("_active", r), e.addEventListener("_idle", s), x.push({ elem: i, animation: e }), C += 1;
    }function a(t) {
      var e = new AnimationItem();return i(e, null), e.setParams(t), e;
    }function n(t, e) {
      var r;for (r = 0; C > r; r += 1) {
        x[r].animation.setSpeed(t, e);
      }
    }function o(t, e) {
      var r;for (r = 0; C > r; r += 1) {
        x[r].animation.setDirection(t, e);
      }
    }function h(t) {
      var e;for (e = 0; C > e; e += 1) {
        x[e].animation.play(t);
      }
    }function l(t, e) {
      S = Date.now();var r;for (r = 0; C > r; r += 1) {
        x[r].animation.moveFrame(t, e);
      }
    }function p(t) {
      var e,
          r = t - S;for (e = 0; C > e; e += 1) {
        x[e].animation.advanceTime(r);
      }S = t, k || requestAnimationFrame(p);
    }function m(t) {
      S = t, requestAnimationFrame(p);
    }function f(t) {
      var e;for (e = 0; C > e; e += 1) {
        x[e].animation.pause(t);
      }
    }function c(t, e, r) {
      var s;for (s = 0; C > s; s += 1) {
        x[s].animation.goToAndStop(t, e, r);
      }
    }function d(t) {
      var e;for (e = 0; C > e; e += 1) {
        x[e].animation.stop(t);
      }
    }function u(t) {
      var e;for (e = 0; C > e; e += 1) {
        x[e].animation.togglePause(t);
      }
    }function y(t) {
      var e;for (e = C - 1; e >= 0; e -= 1) {
        x[e].animation.destroy(t);
      }
    }function g(t, r, s) {
      var i,
          a = document.getElementsByClassName("bodymovin"),
          n = a.length;for (i = 0; n > i; i += 1) {
        s && a[i].setAttribute("data-bm-type", s), e(a[i], t);
      }if (r && 0 === n) {
        s || (s = "svg");var o = document.getElementsByTagName("body")[0];o.innerHTML = "";var h = document.createElement("div");h.style.width = "100%", h.style.height = "100%", h.setAttribute("data-bm-type", s), o.appendChild(h), e(h, t);
      }
    }function v() {
      var t;for (t = 0; C > t; t += 1) {
        x[t].animation.resize();
      }
    }function b() {
      requestAnimationFrame(m);
    }function E() {
      k && (k = !1, requestAnimationFrame(m));
    }var P = {},
        x = [],
        S = 0,
        C = 0,
        k = !0,
        M = 0;return setTimeout(b, 0), P.registerAnimation = e, P.loadAnimation = a, P.setSpeed = n, P.setDirection = o, P.play = h, P.moveFrame = l, P.pause = f, P.stop = d, P.togglePause = u, P.searchAnimations = g, P.resize = v, P.start = b, P.goToAndStop = c, P.destroy = y, P;
  }(),
      AnimationItem = function AnimationItem() {
    this._cbs = [], this.name = "", this.path = "", this.isLoaded = !1, this.currentFrame = 0, this.currentRawFrame = 0, this.totalFrames = 0, this.frameRate = 0, this.frameMult = 0, this.playSpeed = 1, this.playDirection = 1, this.pendingElements = 0, this.playCount = 0, this.prerenderFramesFlag = !0, this.animationData = {}, this.layers = [], this.assets = [], this.isPaused = !0, this.autoplay = !1, this.loop = !0, this.renderer = null, this.animationID = randomString(10), this.scaleMode = "fit", this.assetsPath = "", this.timeCompleted = 0, this.segmentPos = 0, this.subframeEnabled = subframeEnabled, this.segments = [], this.pendingSegment = !1, this._idle = !0, this.projectInterface = ProjectInterface();
  };AnimationItem.prototype.setParams = function (t) {
    var e = this;t.context && (this.context = t.context), (t.wrapper || t.container) && (this.wrapper = t.wrapper || t.container);var r = t.animType ? t.animType : t.renderer ? t.renderer : "svg";switch (r) {case "canvas":
        this.renderer = new CanvasRenderer(this, t.rendererSettings);break;case "svg":
        this.renderer = new SVGRenderer(this, t.rendererSettings);break;case "hybrid":case "html":default:
        this.renderer = new HybridRenderer(this, t.rendererSettings);}if (this.renderer.setProjectInterface(this.projectInterface), this.animType = r, "" === t.loop || null === t.loop || (this.loop = t.loop === !1 ? !1 : t.loop === !0 ? !0 : parseInt(t.loop)), this.autoplay = "autoplay" in t ? t.autoplay : !0, this.name = t.name ? t.name : "", this.prerenderFramesFlag = "prerender" in t ? t.prerender : !0, this.autoloadSegments = t.hasOwnProperty("autoloadSegments") ? t.autoloadSegments : !0, t.animationData) e.configAnimation(t.animationData);else if (t.path) {
      "json" != t.path.substr(-4) && ("/" != t.path.substr(-1, 1) && (t.path += "/"), t.path += "data.json");var s = new XMLHttpRequest();this.path = -1 != t.path.lastIndexOf("\\") ? t.path.substr(0, t.path.lastIndexOf("\\") + 1) : t.path.substr(0, t.path.lastIndexOf("/") + 1), this.assetsPath = t.assetsPath, this.fileName = t.path.substr(t.path.lastIndexOf("/") + 1), this.fileName = this.fileName.substr(0, this.fileName.lastIndexOf(".json")), s.open("GET", t.path, !0), s.send(), s.onreadystatechange = function () {
        if (4 == s.readyState) if (200 == s.status) e.configAnimation(JSON.parse(s.responseText));else try {
          var t = JSON.parse(s.responseText);e.configAnimation(t);
        } catch (r) {}
      };
    }
  }, AnimationItem.prototype.setData = function (t, e) {
    var r = { wrapper: t, animationData: e ? "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? e : JSON.parse(e) : null },
        s = t.attributes;r.path = s.getNamedItem("data-animation-path") ? s.getNamedItem("data-animation-path").value : s.getNamedItem("data-bm-path") ? s.getNamedItem("data-bm-path").value : s.getNamedItem("bm-path") ? s.getNamedItem("bm-path").value : "", r.animType = s.getNamedItem("data-anim-type") ? s.getNamedItem("data-anim-type").value : s.getNamedItem("data-bm-type") ? s.getNamedItem("data-bm-type").value : s.getNamedItem("bm-type") ? s.getNamedItem("bm-type").value : s.getNamedItem("data-bm-renderer") ? s.getNamedItem("data-bm-renderer").value : s.getNamedItem("bm-renderer") ? s.getNamedItem("bm-renderer").value : "canvas";var i = s.getNamedItem("data-anim-loop") ? s.getNamedItem("data-anim-loop").value : s.getNamedItem("data-bm-loop") ? s.getNamedItem("data-bm-loop").value : s.getNamedItem("bm-loop") ? s.getNamedItem("bm-loop").value : "";"" === i || (r.loop = "false" === i ? !1 : "true" === i ? !0 : parseInt(i));var a = s.getNamedItem("data-anim-autoplay") ? s.getNamedItem("data-anim-autoplay").value : s.getNamedItem("data-bm-autoplay") ? s.getNamedItem("data-bm-autoplay").value : s.getNamedItem("bm-autoplay") ? s.getNamedItem("bm-autoplay").value : !0;r.autoplay = "false" !== a, r.name = s.getNamedItem("data-name") ? s.getNamedItem("data-name").value : s.getNamedItem("data-bm-name") ? s.getNamedItem("data-bm-name").value : s.getNamedItem("bm-name") ? s.getNamedItem("bm-name").value : "";var n = s.getNamedItem("data-anim-prerender") ? s.getNamedItem("data-anim-prerender").value : s.getNamedItem("data-bm-prerender") ? s.getNamedItem("data-bm-prerender").value : s.getNamedItem("bm-prerender") ? s.getNamedItem("bm-prerender").value : "";"false" === n && (r.prerender = !1), this.setParams(r);
  }, AnimationItem.prototype.includeLayers = function (t) {
    t.op > this.animationData.op && (this.animationData.op = t.op, this.totalFrames = Math.floor(t.op - this.animationData.ip), this.animationData.tf = this.totalFrames);var e,
        r,
        s = this.animationData.layers,
        i = s.length,
        a = t.layers,
        n = a.length;for (r = 0; n > r; r += 1) {
      for (e = 0; i > e;) {
        if (s[e].id == a[r].id) {
          s[e] = a[r];break;
        }e += 1;
      }
    }if ((t.chars || t.fonts) && (this.renderer.globalData.fontManager.addChars(t.chars), this.renderer.globalData.fontManager.addFonts(t.fonts, this.renderer.globalData.defs)), t.assets) for (i = t.assets.length, e = 0; i > e; e += 1) {
      this.animationData.assets.push(t.assets[e]);
    }this.animationData.__complete = !1, dataManager.completeData(this.animationData, this.renderer.globalData.fontManager), this.renderer.includeLayers(t.layers), expressionsPlugin && expressionsPlugin.initExpressions(this), this.renderer.renderFrame(null), this.loadNextSegment();
  }, AnimationItem.prototype.loadNextSegment = function () {
    var t = this.animationData.segments;if (!t || 0 === t.length || !this.autoloadSegments) return this.trigger("data_ready"), void (this.timeCompleted = this.animationData.tf);var e = t.shift();this.timeCompleted = e.time * this.frameRate;var r = new XMLHttpRequest(),
        s = this,
        i = this.path + this.fileName + "_" + this.segmentPos + ".json";this.segmentPos += 1, r.open("GET", i, !0), r.send(), r.onreadystatechange = function () {
      if (4 == r.readyState) if (200 == r.status) s.includeLayers(JSON.parse(r.responseText));else try {
        var t = JSON.parse(r.responseText);s.includeLayers(t);
      } catch (e) {}
    };
  }, AnimationItem.prototype.loadSegments = function () {
    var t = this.animationData.segments;t || (this.timeCompleted = this.animationData.tf), this.loadNextSegment();
  }, AnimationItem.prototype.configAnimation = function (t) {
    this.renderer && this.renderer.destroyed || (this.animationData = t, this.totalFrames = Math.floor(this.animationData.op - this.animationData.ip), this.animationData.tf = this.totalFrames, this.renderer.configAnimation(t), t.assets || (t.assets = []), t.comps && (t.assets = t.assets.concat(t.comps), t.comps = null), this.renderer.searchExtraCompositions(t.assets), this.layers = this.animationData.layers, this.assets = this.animationData.assets, this.frameRate = this.animationData.fr, this.firstFrame = Math.round(this.animationData.ip), this.frameMult = this.animationData.fr / 1e3, this.trigger("config_ready"), this.imagePreloader = new ImagePreloader(), this.imagePreloader.setAssetsPath(this.assetsPath), this.imagePreloader.setPath(this.path), this.imagePreloader.loadAssets(t.assets), this.loadSegments(), this.updaFrameModifier(), this.renderer.globalData.fontManager ? this.waitForFontsLoaded() : (dataManager.completeData(this.animationData, this.renderer.globalData.fontManager), this.checkLoaded()));
  }, AnimationItem.prototype.waitForFontsLoaded = function () {
    function t() {
      this.renderer.globalData.fontManager.loaded ? (dataManager.completeData(this.animationData, this.renderer.globalData.fontManager), this.checkLoaded()) : setTimeout(t.bind(this), 20);
    }return function () {
      t.bind(this)();
    };
  }(), AnimationItem.prototype.addPendingElement = function () {
    this.pendingElements += 1;
  }, AnimationItem.prototype.elementLoaded = function () {
    this.pendingElements--, this.checkLoaded();
  }, AnimationItem.prototype.checkLoaded = function () {
    0 === this.pendingElements && (expressionsPlugin && expressionsPlugin.initExpressions(this), this.renderer.initItems(), setTimeout(function () {
      this.trigger("DOMLoaded");
    }.bind(this), 0), this.isLoaded = !0, this.gotoFrame(), this.autoplay && this.play());
  }, AnimationItem.prototype.resize = function () {
    this.renderer.updateContainerSize();
  }, AnimationItem.prototype.setSubframe = function (t) {
    this.subframeEnabled = t ? !0 : !1;
  }, AnimationItem.prototype.gotoFrame = function () {
    this.currentFrame = this.subframeEnabled ? this.currentRawFrame : Math.floor(this.currentRawFrame), this.timeCompleted !== this.totalFrames && this.currentFrame > this.timeCompleted && (this.currentFrame = this.timeCompleted), this.trigger("enterFrame"), this.renderFrame();
  }, AnimationItem.prototype.renderFrame = function () {
    this.isLoaded !== !1 && this.renderer.renderFrame(this.currentFrame + this.firstFrame);
  }, AnimationItem.prototype.play = function (t) {
    t && this.name != t || this.isPaused === !0 && (this.isPaused = !1, this._idle && (this._idle = !1, this.trigger("_active")));
  }, AnimationItem.prototype.pause = function (t) {
    t && this.name != t || this.isPaused === !1 && (this.isPaused = !0, this.pendingSegment || (this._idle = !0, this.trigger("_idle")));
  }, AnimationItem.prototype.togglePause = function (t) {
    t && this.name != t || (this.isPaused === !0 ? this.play() : this.pause());
  }, AnimationItem.prototype.stop = function (t) {
    t && this.name != t || (this.pause(), this.currentFrame = this.currentRawFrame = 0, this.playCount = 0, this.gotoFrame());
  }, AnimationItem.prototype.goToAndStop = function (t, e, r) {
    r && this.name != r || (this.setCurrentRawFrameValue(e ? t : t * this.frameModifier), this.pause());
  }, AnimationItem.prototype.goToAndPlay = function (t, e, r) {
    this.goToAndStop(t, e, r), this.play();
  }, AnimationItem.prototype.advanceTime = function (t) {
    return this.pendingSegment ? (this.pendingSegment = !1, this.adjustSegment(this.segments.shift()), void (this.isPaused && this.play())) : void (this.isPaused !== !0 && this.isLoaded !== !1 && this.setCurrentRawFrameValue(this.currentRawFrame + t * this.frameModifier));
  }, AnimationItem.prototype.updateAnimation = function (t) {
    this.setCurrentRawFrameValue(this.totalFrames * t);
  }, AnimationItem.prototype.moveFrame = function (t, e) {
    e && this.name != e || this.setCurrentRawFrameValue(this.currentRawFrame + t);
  }, AnimationItem.prototype.adjustSegment = function (t) {
    this.playCount = 0, t[1] < t[0] ? (this.frameModifier > 0 && (this.playSpeed < 0 ? this.setSpeed(-this.playSpeed) : this.setDirection(-1)), this.totalFrames = t[0] - t[1], this.firstFrame = t[1], this.setCurrentRawFrameValue(this.totalFrames - .01)) : t[1] > t[0] && (this.frameModifier < 0 && (this.playSpeed < 0 ? this.setSpeed(-this.playSpeed) : this.setDirection(1)), this.totalFrames = t[1] - t[0], this.firstFrame = t[0], this.setCurrentRawFrameValue(0)), this.trigger("segmentStart");
  }, AnimationItem.prototype.setSegment = function (t, e) {
    var r = -1;this.isPaused && (this.currentRawFrame + this.firstFrame < t ? r = t : this.currentRawFrame + this.firstFrame > e && (r = e - t - .01)), this.firstFrame = t, this.totalFrames = e - t, -1 !== r && this.goToAndStop(r, !0);
  }, AnimationItem.prototype.playSegments = function (t, e) {
    if ("object" == _typeof(t[0])) {
      var r,
          s = t.length;for (r = 0; s > r; r += 1) {
        this.segments.push(t[r]);
      }
    } else this.segments.push(t);e && this.adjustSegment(this.segments.shift()), this.isPaused && this.play();
  }, AnimationItem.prototype.resetSegments = function (t) {
    this.segments.length = 0, this.segments.push([this.animationData.ip * this.frameRate, Math.floor(this.animationData.op - this.animationData.ip + this.animationData.ip * this.frameRate)]), t && this.adjustSegment(this.segments.shift());
  }, AnimationItem.prototype.checkSegments = function () {
    this.segments.length && (this.pendingSegment = !0);
  }, AnimationItem.prototype.remove = function (t) {
    t && this.name != t || this.renderer.destroy();
  }, AnimationItem.prototype.destroy = function (t) {
    t && this.name != t || this.renderer && this.renderer.destroyed || (this.renderer.destroy(), this.trigger("destroy"), this._cbs = null, this.onEnterFrame = this.onLoopComplete = this.onComplete = this.onSegmentStart = this.onDestroy = null);
  }, AnimationItem.prototype.setCurrentRawFrameValue = function (t) {
    if (this.currentRawFrame = t, this.currentRawFrame >= this.totalFrames) {
      if (this.checkSegments(), this.loop === !1) return this.currentRawFrame = this.totalFrames - .01, this.gotoFrame(), this.pause(), void this.trigger("complete");if (this.trigger("loopComplete"), this.playCount += 1, this.loop !== !0 && this.playCount == this.loop || this.pendingSegment) return this.currentRawFrame = this.totalFrames - .01, this.gotoFrame(), this.pause(), void this.trigger("complete");this.currentRawFrame = this.currentRawFrame % this.totalFrames;
    } else if (this.currentRawFrame < 0) return this.checkSegments(), this.playCount -= 1, this.playCount < 0 && (this.playCount = 0), this.loop === !1 || this.pendingSegment ? (this.currentRawFrame = 0, this.gotoFrame(), this.pause(), void this.trigger("complete")) : (this.trigger("loopComplete"), this.currentRawFrame = (this.totalFrames + this.currentRawFrame) % this.totalFrames, void this.gotoFrame());this.gotoFrame();
  }, AnimationItem.prototype.setSpeed = function (t) {
    this.playSpeed = t, this.updaFrameModifier();
  }, AnimationItem.prototype.setDirection = function (t) {
    this.playDirection = 0 > t ? -1 : 1, this.updaFrameModifier();
  }, AnimationItem.prototype.updaFrameModifier = function () {
    this.frameModifier = this.frameMult * this.playSpeed * this.playDirection;
  }, AnimationItem.prototype.getPath = function () {
    return this.path;
  }, AnimationItem.prototype.getAssetsPath = function (t) {
    var e = "";if (this.assetsPath) {
      var r = t.p;-1 !== r.indexOf("images/") && (r = r.split("/")[1]), e = this.assetsPath + r;
    } else e = this.path, e += t.u ? t.u : "", e += t.p;return e;
  }, AnimationItem.prototype.getAssetData = function (t) {
    for (var e = 0, r = this.assets.length; r > e;) {
      if (t == this.assets[e].id) return this.assets[e];e += 1;
    }
  }, AnimationItem.prototype.hide = function () {
    this.renderer.hide();
  }, AnimationItem.prototype.show = function () {
    this.renderer.show();
  }, AnimationItem.prototype.getAssets = function () {
    return this.assets;
  }, AnimationItem.prototype.trigger = function (t) {
    if (this._cbs && this._cbs[t]) switch (t) {case "enterFrame":
        this.triggerEvent(t, new BMEnterFrameEvent(t, this.currentFrame, this.totalFrames, this.frameMult));break;case "loopComplete":
        this.triggerEvent(t, new BMCompleteLoopEvent(t, this.loop, this.playCount, this.frameMult));break;case "complete":
        this.triggerEvent(t, new BMCompleteEvent(t, this.frameMult));break;case "segmentStart":
        this.triggerEvent(t, new BMSegmentStartEvent(t, this.firstFrame, this.totalFrames));break;case "destroy":
        this.triggerEvent(t, new BMDestroyEvent(t, this));break;default:
        this.triggerEvent(t);}"enterFrame" === t && this.onEnterFrame && this.onEnterFrame.call(this, new BMEnterFrameEvent(t, this.currentFrame, this.totalFrames, this.frameMult)), "loopComplete" === t && this.onLoopComplete && this.onLoopComplete.call(this, new BMCompleteLoopEvent(t, this.loop, this.playCount, this.frameMult)), "complete" === t && this.onComplete && this.onComplete.call(this, new BMCompleteEvent(t, this.frameMult)), "segmentStart" === t && this.onSegmentStart && this.onSegmentStart.call(this, new BMSegmentStartEvent(t, this.firstFrame, this.totalFrames)), "destroy" === t && this.onDestroy && this.onDestroy.call(this, new BMDestroyEvent(t, this));
  }, AnimationItem.prototype.addEventListener = _addEventListener, AnimationItem.prototype.removeEventListener = _removeEventListener, AnimationItem.prototype.triggerEvent = _triggerEvent, extendPrototype(BaseRenderer, CanvasRenderer), CanvasRenderer.prototype.createBase = function (t) {
    return new CVBaseElement(t, this, this.globalData);
  }, CanvasRenderer.prototype.createShape = function (t) {
    return new CVShapeElement(t, this, this.globalData);
  }, CanvasRenderer.prototype.createText = function (t) {
    return new CVTextElement(t, this, this.globalData);
  }, CanvasRenderer.prototype.createImage = function (t) {
    return new CVImageElement(t, this, this.globalData);
  }, CanvasRenderer.prototype.createComp = function (t) {
    return new CVCompElement(t, this, this.globalData);
  }, CanvasRenderer.prototype.createSolid = function (t) {
    return new CVSolidElement(t, this, this.globalData);
  }, CanvasRenderer.prototype.ctxTransform = function (t) {
    if (1 !== t[0] || 0 !== t[1] || 0 !== t[4] || 1 !== t[5] || 0 !== t[12] || 0 !== t[13]) {
      if (!this.renderConfig.clearCanvas) return void this.canvasContext.transform(t[0], t[1], t[4], t[5], t[12], t[13]);this.transformMat.cloneFromProps(t), this.transformMat.transform(this.contextData.cTr.props[0], this.contextData.cTr.props[1], this.contextData.cTr.props[2], this.contextData.cTr.props[3], this.contextData.cTr.props[4], this.contextData.cTr.props[5], this.contextData.cTr.props[6], this.contextData.cTr.props[7], this.contextData.cTr.props[8], this.contextData.cTr.props[9], this.contextData.cTr.props[10], this.contextData.cTr.props[11], this.contextData.cTr.props[12], this.contextData.cTr.props[13], this.contextData.cTr.props[14], this.contextData.cTr.props[15]), this.contextData.cTr.cloneFromProps(this.transformMat.props);var e = this.contextData.cTr.props;this.canvasContext.setTransform(e[0], e[1], e[4], e[5], e[12], e[13]);
    }
  }, CanvasRenderer.prototype.ctxOpacity = function (t) {
    if (1 !== t) {
      if (!this.renderConfig.clearCanvas) return void (this.canvasContext.globalAlpha *= 0 > t ? 0 : t);this.contextData.cO *= 0 > t ? 0 : t, this.canvasContext.globalAlpha = this.contextData.cO;
    }
  }, CanvasRenderer.prototype.reset = function () {
    return this.renderConfig.clearCanvas ? (this.contextData.cArrPos = 0, this.contextData.cTr.reset(), void (this.contextData.cO = 1)) : void this.canvasContext.restore();
  }, CanvasRenderer.prototype.save = function (t) {
    if (!this.renderConfig.clearCanvas) return void this.canvasContext.save();t && this.canvasContext.save();var e = this.contextData.cTr.props;(null === this.contextData.saved[this.contextData.cArrPos] || void 0 === this.contextData.saved[this.contextData.cArrPos]) && (this.contextData.saved[this.contextData.cArrPos] = new Array(16));var r,
        s = this.contextData.saved[this.contextData.cArrPos];for (r = 0; 16 > r; r += 1) {
      s[r] = e[r];
    }this.contextData.savedOp[this.contextData.cArrPos] = this.contextData.cO, this.contextData.cArrPos += 1;
  }, CanvasRenderer.prototype.restore = function (t) {
    if (!this.renderConfig.clearCanvas) return void this.canvasContext.restore();t && this.canvasContext.restore(), this.contextData.cArrPos -= 1;var e,
        r = this.contextData.saved[this.contextData.cArrPos],
        s = this.contextData.cTr.props;for (e = 0; 16 > e; e += 1) {
      s[e] = r[e];
    }this.canvasContext.setTransform(r[0], r[1], r[4], r[5], r[12], r[13]), r = this.contextData.savedOp[this.contextData.cArrPos], this.contextData.cO = r, this.canvasContext.globalAlpha = r;
  }, CanvasRenderer.prototype.configAnimation = function (t) {
    this.animationItem.wrapper ? (this.animationItem.container = document.createElement("canvas"), this.animationItem.container.style.width = "100%", this.animationItem.container.style.height = "100%", this.animationItem.container.style.transformOrigin = this.animationItem.container.style.mozTransformOrigin = this.animationItem.container.style.webkitTransformOrigin = this.animationItem.container.style["-webkit-transform"] = "0px 0px 0px", this.animationItem.wrapper.appendChild(this.animationItem.container), this.canvasContext = this.animationItem.container.getContext("2d")) : this.canvasContext = this.renderConfig.context, this.data = t, this.globalData.canvasContext = this.canvasContext, this.globalData.renderer = this, this.globalData.isDashed = !1, this.globalData.totalFrames = Math.floor(t.tf), this.globalData.compWidth = t.w, this.globalData.compHeight = t.h, this.globalData.frameRate = t.fr, this.globalData.frameId = 0, this.globalData.compSize = { w: t.w, h: t.h }, this.globalData.progressiveLoad = this.renderConfig.progressiveLoad, this.layers = t.layers, this.transformCanvas = {}, this.transformCanvas.w = t.w, this.transformCanvas.h = t.h, this.globalData.fontManager = new FontManager(), this.globalData.fontManager.addChars(t.chars), this.globalData.fontManager.addFonts(t.fonts, document.body), this.globalData.getAssetData = this.animationItem.getAssetData.bind(this.animationItem), this.globalData.getAssetsPath = this.animationItem.getAssetsPath.bind(this.animationItem), this.globalData.elementLoaded = this.animationItem.elementLoaded.bind(this.animationItem), this.globalData.addPendingElement = this.animationItem.addPendingElement.bind(this.animationItem), this.globalData.transformCanvas = this.transformCanvas, this.elements = Array.apply(null, { length: t.layers.length }), this.updateContainerSize();
  }, CanvasRenderer.prototype.updateContainerSize = function () {
    var t, e;this.animationItem.wrapper && this.animationItem.container ? (t = this.animationItem.wrapper.offsetWidth, e = this.animationItem.wrapper.offsetHeight, this.animationItem.container.setAttribute("width", t * this.renderConfig.dpr), this.animationItem.container.setAttribute("height", e * this.renderConfig.dpr)) : (t = this.canvasContext.canvas.width * this.renderConfig.dpr, e = this.canvasContext.canvas.height * this.renderConfig.dpr);var r, s;if (-1 !== this.renderConfig.preserveAspectRatio.indexOf("meet") || -1 !== this.renderConfig.preserveAspectRatio.indexOf("slice")) {
      var i = this.renderConfig.preserveAspectRatio.split(" "),
          a = i[1] || "meet",
          n = i[0] || "xMidYMid",
          o = n.substr(0, 4),
          h = n.substr(4);r = t / e, s = this.transformCanvas.w / this.transformCanvas.h, s > r && "meet" === a || r > s && "slice" === a ? (this.transformCanvas.sx = t / (this.transformCanvas.w / this.renderConfig.dpr), this.transformCanvas.sy = t / (this.transformCanvas.w / this.renderConfig.dpr)) : (this.transformCanvas.sx = e / (this.transformCanvas.h / this.renderConfig.dpr), this.transformCanvas.sy = e / (this.transformCanvas.h / this.renderConfig.dpr)), this.transformCanvas.tx = "xMid" === o && (r > s && "meet" === a || s > r && "slice" === a) ? (t - this.transformCanvas.w * (e / this.transformCanvas.h)) / 2 * this.renderConfig.dpr : "xMax" === o && (r > s && "meet" === a || s > r && "slice" === a) ? (t - this.transformCanvas.w * (e / this.transformCanvas.h)) * this.renderConfig.dpr : 0, this.transformCanvas.ty = "YMid" === h && (s > r && "meet" === a || r > s && "slice" === a) ? (e - this.transformCanvas.h * (t / this.transformCanvas.w)) / 2 * this.renderConfig.dpr : "YMax" === h && (s > r && "meet" === a || r > s && "slice" === a) ? (e - this.transformCanvas.h * (t / this.transformCanvas.w)) * this.renderConfig.dpr : 0;
    } else "none" == this.renderConfig.preserveAspectRatio ? (this.transformCanvas.sx = t / (this.transformCanvas.w / this.renderConfig.dpr), this.transformCanvas.sy = e / (this.transformCanvas.h / this.renderConfig.dpr), this.transformCanvas.tx = 0, this.transformCanvas.ty = 0) : (this.transformCanvas.sx = this.renderConfig.dpr, this.transformCanvas.sy = this.renderConfig.dpr, this.transformCanvas.tx = 0, this.transformCanvas.ty = 0);this.transformCanvas.props = [this.transformCanvas.sx, 0, 0, 0, 0, this.transformCanvas.sy, 0, 0, 0, 0, 1, 0, this.transformCanvas.tx, this.transformCanvas.ty, 0, 1];var l,
        p = this.elements.length;for (l = 0; p > l; l += 1) {
      this.elements[l] && 0 === this.elements[l].data.ty && this.elements[l].resize(this.globalData.transformCanvas);
    }
  }, CanvasRenderer.prototype.destroy = function () {
    this.renderConfig.clearCanvas && (this.animationItem.wrapper.innerHTML = "");var t,
        e = this.layers ? this.layers.length : 0;for (t = e - 1; t >= 0; t -= 1) {
      this.elements[t].destroy();
    }this.elements.length = 0, this.globalData.canvasContext = null, this.animationItem.container = null, this.destroyed = !0;
  }, CanvasRenderer.prototype.renderFrame = function (t) {
    if (!(this.renderedFrame == t && this.renderConfig.clearCanvas === !0 || this.destroyed || null === t)) {
      this.renderedFrame = t, this.globalData.frameNum = t - this.animationItem.firstFrame, this.globalData.frameId += 1, this.globalData.projectInterface.currentFrame = t, this.renderConfig.clearCanvas === !0 ? (this.reset(), this.canvasContext.save(), this.canvasContext.clearRect(this.transformCanvas.tx, this.transformCanvas.ty, this.transformCanvas.w * this.transformCanvas.sx, this.transformCanvas.h * this.transformCanvas.sy)) : this.save(), this.ctxTransform(this.transformCanvas.props), this.canvasContext.beginPath(), this.canvasContext.rect(0, 0, this.transformCanvas.w, this.transformCanvas.h), this.canvasContext.closePath(), this.canvasContext.clip();var e,
          r = this.layers.length;for (this.completeLayers || this.checkLayers(t), e = 0; r > e; e++) {
        (this.completeLayers || this.elements[e]) && this.elements[e].prepareFrame(t - this.layers[e].st);
      }for (e = r - 1; e >= 0; e -= 1) {
        (this.completeLayers || this.elements[e]) && this.elements[e].renderFrame();
      }this.renderConfig.clearCanvas !== !0 ? this.restore() : this.canvasContext.restore();
    }
  }, CanvasRenderer.prototype.buildItem = function (t) {
    var e = this.elements;if (!e[t] && 99 != this.layers[t].ty) {
      var r = this.createItem(this.layers[t], this, this.globalData);e[t] = r, r.initExpressions(), 0 === this.layers[t].ty && r.resize(this.globalData.transformCanvas);
    }
  }, CanvasRenderer.prototype.checkPendingElements = function () {
    for (; this.pendingElements.length;) {
      var t = this.pendingElements.pop();t.checkParenting();
    }
  }, CanvasRenderer.prototype.hide = function () {
    this.animationItem.container.style.display = "none";
  }, CanvasRenderer.prototype.show = function () {
    this.animationItem.container.style.display = "block";
  }, CanvasRenderer.prototype.searchExtraCompositions = function (t) {
    {
      var e,
          r = t.length;document.createElementNS(svgNS, "g");
    }for (e = 0; r > e; e += 1) {
      if (t[e].xt) {
        var s = this.createComp(t[e], this.globalData.comp, this.globalData);s.initExpressions(), this.globalData.projectInterface.registerComposition(s);
      }
    }
  }, extendPrototype(BaseRenderer, HybridRenderer), HybridRenderer.prototype.buildItem = SVGRenderer.prototype.buildItem, HybridRenderer.prototype.checkPendingElements = function () {
    for (; this.pendingElements.length;) {
      var t = this.pendingElements.pop();t.checkParenting();
    }
  }, HybridRenderer.prototype.appendElementInPos = function (t, e) {
    var r = t.getBaseElement();if (r) {
      var s = this.layers[e];if (s.ddd && this.supports3d) this.addTo3dContainer(r, e);else {
        for (var i, a = 0; e > a;) {
          this.elements[a] && this.elements[a] !== !0 && this.elements[a].getBaseElement && (i = this.elements[a].getBaseElement()), a += 1;
        }i ? s.ddd && this.supports3d || this.layerElement.insertBefore(r, i) : s.ddd && this.supports3d || this.layerElement.appendChild(r);
      }
    }
  }, HybridRenderer.prototype.createBase = function (t) {
    return new SVGBaseElement(t, this.layerElement, this.globalData, this);
  }, HybridRenderer.prototype.createShape = function (t) {
    return this.supports3d ? new HShapeElement(t, this.layerElement, this.globalData, this) : new IShapeElement(t, this.layerElement, this.globalData, this);
  }, HybridRenderer.prototype.createText = function (t) {
    return this.supports3d ? new HTextElement(t, this.layerElement, this.globalData, this) : new SVGTextElement(t, this.layerElement, this.globalData, this);
  }, HybridRenderer.prototype.createCamera = function (t) {
    return this.camera = new HCameraElement(t, this.layerElement, this.globalData, this), this.camera;
  }, HybridRenderer.prototype.createImage = function (t) {
    return this.supports3d ? new HImageElement(t, this.layerElement, this.globalData, this) : new IImageElement(t, this.layerElement, this.globalData, this);
  }, HybridRenderer.prototype.createComp = function (t) {
    return this.supports3d ? new HCompElement(t, this.layerElement, this.globalData, this) : new ICompElement(t, this.layerElement, this.globalData, this);
  }, HybridRenderer.prototype.createSolid = function (t) {
    return this.supports3d ? new HSolidElement(t, this.layerElement, this.globalData, this) : new ISolidElement(t, this.layerElement, this.globalData, this);
  }, HybridRenderer.prototype.getThreeDContainer = function (t) {
    var e = document.createElement("div");styleDiv(e), e.style.width = this.globalData.compSize.w + "px", e.style.height = this.globalData.compSize.h + "px", e.style.transformOrigin = e.style.mozTransformOrigin = e.style.webkitTransformOrigin = "50% 50%";var r = document.createElement("div");styleDiv(r), r.style.transform = r.style.webkitTransform = "matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)", e.appendChild(r), this.resizerElem.appendChild(e);var s = { container: r, perspectiveElem: e, startPos: t, endPos: t };return this.threeDElements.push(s), s;
  }, HybridRenderer.prototype.build3dContainers = function () {
    var t,
        e,
        r = this.layers.length;for (t = 0; r > t; t += 1) {
      this.layers[t].ddd ? (e || (e = this.getThreeDContainer(t)), e.endPos = Math.max(e.endPos, t)) : e = null;
    }
  }, HybridRenderer.prototype.addTo3dContainer = function (t, e) {
    for (var r = 0, s = this.threeDElements.length; s > r;) {
      if (e <= this.threeDElements[r].endPos) {
        for (var i, a = this.threeDElements[r].startPos; e > a;) {
          this.elements[a] && this.elements[a].getBaseElement && (i = this.elements[a].getBaseElement()), a += 1;
        }i ? this.threeDElements[r].container.insertBefore(t, i) : this.threeDElements[r].container.appendChild(t);break;
      }r += 1;
    }
  }, HybridRenderer.prototype.configAnimation = function (t) {
    var e = document.createElement("div"),
        r = this.animationItem.wrapper;e.style.width = t.w + "px", e.style.height = t.h + "px", this.resizerElem = e, styleDiv(e), e.style.transformStyle = e.style.webkitTransformStyle = e.style.mozTransformStyle = "flat", r.appendChild(e), e.style.overflow = "hidden";var s = document.createElementNS(svgNS, "svg");s.setAttribute("width", "1"), s.setAttribute("height", "1"), styleDiv(s), this.resizerElem.appendChild(s);var i = document.createElementNS(svgNS, "defs");s.appendChild(i), this.globalData.defs = i, this.data = t, this.globalData.getAssetData = this.animationItem.getAssetData.bind(this.animationItem), this.globalData.getAssetsPath = this.animationItem.getAssetsPath.bind(this.animationItem), this.globalData.elementLoaded = this.animationItem.elementLoaded.bind(this.animationItem), this.globalData.frameId = 0, this.globalData.compSize = { w: t.w, h: t.h }, this.globalData.frameRate = t.fr, this.layers = t.layers, this.globalData.fontManager = new FontManager(), this.globalData.fontManager.addChars(t.chars), this.globalData.fontManager.addFonts(t.fonts, s), this.layerElement = this.resizerElem, this.build3dContainers(), this.updateContainerSize();
  }, HybridRenderer.prototype.destroy = function () {
    this.animationItem.wrapper.innerHTML = "", this.animationItem.container = null, this.globalData.defs = null;var t,
        e = this.layers ? this.layers.length : 0;for (t = 0; e > t; t++) {
      this.elements[t].destroy();
    }this.elements.length = 0, this.destroyed = !0, this.animationItem = null;
  }, HybridRenderer.prototype.updateContainerSize = function () {
    var t,
        e,
        r,
        s,
        i = this.animationItem.wrapper.offsetWidth,
        a = this.animationItem.wrapper.offsetHeight,
        n = i / a,
        o = this.globalData.compSize.w / this.globalData.compSize.h;o > n ? (t = i / this.globalData.compSize.w, e = i / this.globalData.compSize.w, r = 0, s = (a - this.globalData.compSize.h * (i / this.globalData.compSize.w)) / 2) : (t = a / this.globalData.compSize.h, e = a / this.globalData.compSize.h, r = (i - this.globalData.compSize.w * (a / this.globalData.compSize.h)) / 2, s = 0), this.resizerElem.style.transform = this.resizerElem.style.webkitTransform = "matrix3d(" + t + ",0,0,0,0," + e + ",0,0,0,0,1,0," + r + "," + s + ",0,1)";
  }, HybridRenderer.prototype.renderFrame = SVGRenderer.prototype.renderFrame, HybridRenderer.prototype.hide = function () {
    this.resizerElem.style.display = "none";
  }, HybridRenderer.prototype.show = function () {
    this.resizerElem.style.display = "block";
  }, HybridRenderer.prototype.initItems = function () {
    if (this.buildAllItems(), this.camera) this.camera.setup();else {
      var t,
          e = this.globalData.compSize.w,
          r = this.globalData.compSize.h,
          s = this.threeDElements.length;for (t = 0; s > t; t += 1) {
        this.threeDElements[t].perspectiveElem.style.perspective = this.threeDElements[t].perspectiveElem.style.webkitPerspective = Math.sqrt(Math.pow(e, 2) + Math.pow(r, 2)) + "px";
      }
    }
  }, HybridRenderer.prototype.searchExtraCompositions = function (t) {
    var e,
        r = t.length,
        s = document.createElement("div");for (e = 0; r > e; e += 1) {
      if (t[e].xt) {
        var i = this.createComp(t[e], s, this.globalData.comp, null);i.initExpressions(), this.globalData.projectInterface.registerComposition(i);
      }
    }
  }, createElement(BaseElement, CVBaseElement), CVBaseElement.prototype.createElements = function () {
    this.checkParenting();
  }, CVBaseElement.prototype.checkBlendMode = function (t) {
    if (t.blendMode !== this.data.bm) {
      t.blendMode = this.data.bm;var e = "";switch (this.data.bm) {case 0:
          e = "normal";break;case 1:
          e = "multiply";break;case 2:
          e = "screen";break;case 3:
          e = "overlay";break;case 4:
          e = "darken";break;case 5:
          e = "lighten";break;case 6:
          e = "color-dodge";break;case 7:
          e = "color-burn";break;case 8:
          e = "hard-light";break;case 9:
          e = "soft-light";break;case 10:
          e = "difference";break;case 11:
          e = "exclusion";break;case 12:
          e = "hue";break;case 13:
          e = "saturation";break;case 14:
          e = "color";break;case 15:
          e = "luminosity";}t.canvasContext.globalCompositeOperation = e;
    }
  }, CVBaseElement.prototype.renderFrame = function (t) {
    if (3 === this.data.ty) return !1;if (this.checkBlendMode(0 === this.data.ty ? this.parentGlobalData : this.globalData), !this.isVisible) return this.isVisible;this.finalTransform.opMdf = this.finalTransform.op.mdf, this.finalTransform.matMdf = this.finalTransform.mProp.mdf, this.finalTransform.opacity = this.finalTransform.op.v;var e,
        r = this.finalTransform.mat;if (this.hierarchy) {
      var s,
          i = this.hierarchy.length;for (e = this.finalTransform.mProp.v.props, r.cloneFromProps(e), s = 0; i > s; s += 1) {
        this.finalTransform.matMdf = this.hierarchy[s].finalTransform.mProp.mdf ? !0 : this.finalTransform.matMdf, e = this.hierarchy[s].finalTransform.mProp.v.props, r.transform(e[0], e[1], e[2], e[3], e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15]);
      }
    } else t ? (e = this.finalTransform.mProp.v.props, r.cloneFromProps(e)) : r.cloneFromProps(this.finalTransform.mProp.v.props);return t && (e = t.mat.props, r.transform(e[0], e[1], e[2], e[3], e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15]), this.finalTransform.opacity *= t.opacity, this.finalTransform.opMdf = t.opMdf ? !0 : this.finalTransform.opMdf, this.finalTransform.matMdf = t.matMdf ? !0 : this.finalTransform.matMdf), this.data.hasMask && (this.globalData.renderer.save(!0), this.maskManager.renderFrame(0 === this.data.ty ? null : r)), this.data.hd && (this.isVisible = !1), this.isVisible;
  }, CVBaseElement.prototype.addMasks = function (t) {
    this.maskManager = new CVMaskElement(t, this, this.globalData);
  }, CVBaseElement.prototype.destroy = function () {
    this.canvasContext = null, this.data = null, this.globalData = null, this.maskManager && this.maskManager.destroy();
  }, CVBaseElement.prototype.mHelper = new Matrix(), createElement(CVBaseElement, CVCompElement), CVCompElement.prototype.ctxTransform = CanvasRenderer.prototype.ctxTransform, CVCompElement.prototype.ctxOpacity = CanvasRenderer.prototype.ctxOpacity, CVCompElement.prototype.save = CanvasRenderer.prototype.save, CVCompElement.prototype.restore = CanvasRenderer.prototype.restore, CVCompElement.prototype.reset = function () {
    this.contextData.cArrPos = 0, this.contextData.cTr.reset(), this.contextData.cO = 1;
  }, CVCompElement.prototype.resize = function (t) {
    var e = Math.max(t.sx, t.sy);this.canvas.width = this.data.w * e, this.canvas.height = this.data.h * e, this.transformCanvas = { sc: e, w: this.data.w * e, h: this.data.h * e, props: [e, 0, 0, 0, 0, e, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1] };var r,
        s = this.elements.length;for (r = 0; s > r; r += 1) {
      this.elements[r] && 0 === this.elements[r].data.ty && this.elements[r].resize(t);
    }
  }, CVCompElement.prototype.prepareFrame = function (t) {
    if (this.globalData.frameId = this.parentGlobalData.frameId, this.globalData.mdf = !1, this._parent.prepareFrame.call(this, t), this.isVisible !== !1 || this.data.xt) {
      var e = t;this.tm && (e = this.tm.v, e === this.data.op && (e = this.data.op - 1)), this.renderedFrame = e / this.data.sr;var r,
          s = this.elements.length;for (this.completeLayers || this.checkLayers(t), r = 0; s > r; r += 1) {
        (this.completeLayers || this.elements[r]) && (this.elements[r].prepareFrame(e / this.data.sr - this.layers[r].st), 0 === this.elements[r].data.ty && this.elements[r].globalData.mdf && (this.globalData.mdf = !0));
      }this.globalData.mdf && !this.data.xt && (this.canvasContext.clearRect(0, 0, this.data.w, this.data.h), this.ctxTransform(this.transformCanvas.props));
    }
  }, CVCompElement.prototype.renderFrame = function (t) {
    if (this._parent.renderFrame.call(this, t) !== !1) {
      if (this.globalData.mdf) {
        var e,
            r = this.layers.length;for (e = r - 1; e >= 0; e -= 1) {
          (this.completeLayers || this.elements[e]) && this.elements[e].renderFrame();
        }
      }this.data.hasMask && this.globalData.renderer.restore(!0), this.firstFrame && (this.firstFrame = !1), this.parentGlobalData.renderer.save(), this.parentGlobalData.renderer.ctxTransform(this.finalTransform.mat.props), this.parentGlobalData.renderer.ctxOpacity(this.finalTransform.opacity), this.parentGlobalData.renderer.canvasContext.drawImage(this.canvas, 0, 0, this.data.w, this.data.h), this.parentGlobalData.renderer.restore(), this.globalData.mdf && this.reset();
    }
  }, CVCompElement.prototype.setElements = function (t) {
    this.elements = t;
  }, CVCompElement.prototype.getElements = function () {
    return this.elements;
  }, CVCompElement.prototype.destroy = function () {
    var t,
        e = this.layers.length;for (t = e - 1; t >= 0; t -= 1) {
      this.elements[t].destroy();
    }this.layers = null, this.elements = null, this._parent.destroy.call(this._parent);
  }, CVCompElement.prototype.checkLayers = CanvasRenderer.prototype.checkLayers, CVCompElement.prototype.buildItem = CanvasRenderer.prototype.buildItem, CVCompElement.prototype.checkPendingElements = CanvasRenderer.prototype.checkPendingElements, CVCompElement.prototype.addPendingElement = CanvasRenderer.prototype.addPendingElement, CVCompElement.prototype.buildAllItems = CanvasRenderer.prototype.buildAllItems, CVCompElement.prototype.createItem = CanvasRenderer.prototype.createItem, CVCompElement.prototype.createImage = CanvasRenderer.prototype.createImage, CVCompElement.prototype.createComp = CanvasRenderer.prototype.createComp, CVCompElement.prototype.createSolid = CanvasRenderer.prototype.createSolid, CVCompElement.prototype.createShape = CanvasRenderer.prototype.createShape, CVCompElement.prototype.createText = CanvasRenderer.prototype.createText, CVCompElement.prototype.createBase = CanvasRenderer.prototype.createBase, CVCompElement.prototype.buildElementParenting = CanvasRenderer.prototype.buildElementParenting, createElement(CVBaseElement, CVImageElement), CVImageElement.prototype.createElements = function () {
    var t = function () {
      if (this.globalData.elementLoaded(), this.assetData.w !== this.img.width || this.assetData.h !== this.img.height) {
        var t = document.createElement("canvas");t.width = this.assetData.w, t.height = this.assetData.h;var e,
            r,
            s = t.getContext("2d"),
            i = this.img.width,
            a = this.img.height,
            n = i / a,
            o = this.assetData.w / this.assetData.h;n > o ? (r = a, e = r * o) : (e = i, r = e / o), s.drawImage(this.img, (i - e) / 2, (a - r) / 2, e, r, 0, 0, this.assetData.w, this.assetData.h), this.img = t;
      }
    }.bind(this),
        e = function () {
      this.failed = !0, this.globalData.elementLoaded();
    }.bind(this);this.img = new Image(), this.img.addEventListener("load", t, !1), this.img.addEventListener("error", e, !1);var r = this.globalData.getAssetsPath(this.assetData);this.img.src = r, this._parent.createElements.call(this);
  }, CVImageElement.prototype.renderFrame = function (t) {
    if (!this.failed && this._parent.renderFrame.call(this, t) !== !1) {
      var e = this.canvasContext;this.globalData.renderer.save();var r = this.finalTransform.mat.props;this.globalData.renderer.ctxTransform(r), this.globalData.renderer.ctxOpacity(this.finalTransform.opacity), e.drawImage(this.img, 0, 0), this.globalData.renderer.restore(this.data.hasMask), this.firstFrame && (this.firstFrame = !1);
    }
  }, CVImageElement.prototype.destroy = function () {
    this.img = null, this._parent.destroy.call(this._parent);
  }, CVMaskElement.prototype.getMaskProperty = function (t) {
    return this.viewData[t];
  }, CVMaskElement.prototype.prepareFrame = function (t) {
    var e,
        r = this.dynamicProperties.length;for (e = 0; r > e; e += 1) {
      this.dynamicProperties[e].getValue(t), this.dynamicProperties[e].mdf && (this.element.globalData.mdf = !0);
    }
  }, CVMaskElement.prototype.renderFrame = function (t) {
    var e,
        r,
        s,
        i,
        a,
        n = this.element.canvasContext,
        o = this.data.masksProperties.length,
        h = !1;for (e = 0; o > e; e++) {
      if ("n" !== this.masksProperties[e].mode) {
        h === !1 && (n.beginPath(), h = !0), this.masksProperties[e].inv && (n.moveTo(0, 0), n.lineTo(this.element.globalData.compWidth, 0), n.lineTo(this.element.globalData.compWidth, this.element.globalData.compHeight), n.lineTo(0, this.element.globalData.compHeight), n.lineTo(0, 0)), a = this.viewData[e].v, r = t ? t.applyToPointArray(a.v[0][0], a.v[0][1], 0) : a.v[0], n.moveTo(r[0], r[1]);var l,
            p = a._length;for (l = 1; p > l; l++) {
          r = t ? t.applyToPointArray(a.o[l - 1][0], a.o[l - 1][1], 0) : a.o[l - 1], s = t ? t.applyToPointArray(a.i[l][0], a.i[l][1], 0) : a.i[l], i = t ? t.applyToPointArray(a.v[l][0], a.v[l][1], 0) : a.v[l], n.bezierCurveTo(r[0], r[1], s[0], s[1], i[0], i[1]);
        }r = t ? t.applyToPointArray(a.o[l - 1][0], a.o[l - 1][1], 0) : a.o[l - 1], s = t ? t.applyToPointArray(a.i[0][0], a.i[0][1], 0) : a.i[0], i = t ? t.applyToPointArray(a.v[0][0], a.v[0][1], 0) : a.v[0], n.bezierCurveTo(r[0], r[1], s[0], s[1], i[0], i[1]);
      }
    }h && n.clip();
  }, CVMaskElement.prototype.getMask = function (t) {
    for (var e = 0, r = this.masksProperties.length; r > e;) {
      if (this.masksProperties[e].nm === t) return { maskPath: this.viewData[e].pv };e += 1;
    }
  }, CVMaskElement.prototype.destroy = function () {
    this.element = null;
  }, createElement(CVBaseElement, CVShapeElement), CVShapeElement.prototype.lcEnum = { 1: "butt", 2: "round", 3: "butt" }, CVShapeElement.prototype.ljEnum = { 1: "miter", 2: "round", 3: "butt" }, CVShapeElement.prototype.transformHelper = { opacity: 1, mat: new Matrix(), matMdf: !1, opMdf: !1 }, CVShapeElement.prototype.dashResetter = [], CVShapeElement.prototype.createElements = function () {
    this._parent.createElements.call(this), this.searchShapes(this.shapesData, this.viewData, this.dynamicProperties);
  }, CVShapeElement.prototype.searchShapes = function (t, e, r) {
    var s,
        i,
        a,
        n,
        o = t.length - 1,
        h = [],
        l = [];for (s = o; s >= 0; s -= 1) {
      if ("fl" == t[s].ty || "st" == t[s].ty) {
        if (n = { type: t[s].ty, elements: [] }, e[s] = {}, ("fl" == t[s].ty || "st" == t[s].ty) && (e[s].c = PropertyFactory.getProp(this, t[s].c, 1, 255, r), e[s].c.k || (n.co = "rgb(" + bm_floor(e[s].c.v[0]) + "," + bm_floor(e[s].c.v[1]) + "," + bm_floor(e[s].c.v[2]) + ")")), e[s].o = PropertyFactory.getProp(this, t[s].o, 0, .01, r), "st" == t[s].ty) {
          if (n.lc = this.lcEnum[t[s].lc] || "round", n.lj = this.ljEnum[t[s].lj] || "round", 1 == t[s].lj && (n.ml = t[s].ml), e[s].w = PropertyFactory.getProp(this, t[s].w, 0, null, r), e[s].w.k || (n.wi = e[s].w.v), t[s].d) {
            var p = PropertyFactory.getDashProp(this, t[s].d, "canvas", r);e[s].d = p, e[s].d.k || (n.da = e[s].d.dasharray, n["do"] = e[s].d.dashoffset);
          }
        } else n.r = 2 === t[s].r ? "evenodd" : "nonzero";this.stylesList.push(n), e[s].style = n, h.push(e[s].style);
      } else if ("gr" == t[s].ty) e[s] = { it: [] }, this.searchShapes(t[s].it, e[s].it, r);else if ("tr" == t[s].ty) e[s] = { transform: { mat: new Matrix(), opacity: 1, matMdf: !1, opMdf: !1, op: PropertyFactory.getProp(this, t[s].o, 0, .01, r), mProps: PropertyFactory.getProp(this, t[s], 2, null, r) }, elements: [] };else if ("sh" == t[s].ty || "rc" == t[s].ty || "el" == t[s].ty || "sr" == t[s].ty) {
        e[s] = { nodes: [], trNodes: [], tr: [0, 0, 0, 0, 0, 0] };var m = 4;"rc" == t[s].ty ? m = 5 : "el" == t[s].ty ? m = 6 : "sr" == t[s].ty && (m = 7), e[s].sh = ShapePropertyFactory.getShapeProp(this, t[s], m, r), this.shapes.push(e[s].sh), this.addShapeToModifiers(e[s]), a = this.stylesList.length;var f = !1,
            c = !1;for (i = 0; a > i; i += 1) {
          this.stylesList[i].closed || (this.stylesList[i].elements.push(e[s]), "st" === this.stylesList[i].type ? f = !0 : c = !0);
        }e[s].st = f, e[s].fl = c;
      } else if ("tm" == t[s].ty || "rd" == t[s].ty || "rp" == t[s].ty) {
        var d = ShapeModifiers.getModifier(t[s].ty);d.init(this, t[s], r), this.shapeModifiers.push(d), l.push(d), e[s] = d;
      }
    }for (o = h.length, s = 0; o > s; s += 1) {
      h[s].closed = !0;
    }for (o = l.length, s = 0; o > s; s += 1) {
      l[s].closed = !0;
    }
  }, CVShapeElement.prototype.addShapeToModifiers = IShapeElement.prototype.addShapeToModifiers, CVShapeElement.prototype.renderModifiers = IShapeElement.prototype.renderModifiers, CVShapeElement.prototype.renderFrame = function (t) {
    this._parent.renderFrame.call(this, t) !== !1 && (this.transformHelper.mat.reset(), this.transformHelper.opacity = this.finalTransform.opacity, this.transformHelper.matMdf = !1, this.transformHelper.opMdf = this.finalTransform.opMdf, this.renderModifiers(), this.renderShape(this.transformHelper, null, null, !0), this.data.hasMask && this.globalData.renderer.restore(!0));
  }, CVShapeElement.prototype.renderShape = function (t, e, r, s) {
    var i, a;if (!e) for (e = this.shapesData, a = this.stylesList.length, i = 0; a > i; i += 1) {
      this.stylesList[i].d = "", this.stylesList[i].mdf = !1;
    }r || (r = this.viewData), a = e.length - 1;var n, o;for (n = t, i = a; i >= 0; i -= 1) {
      if ("tr" == e[i].ty) {
        n = r[i].transform;var h = r[i].transform.mProps.v.props;if (n.matMdf = n.mProps.mdf, n.opMdf = n.op.mdf, o = n.mat, o.cloneFromProps(h), t) {
          var l = t.mat.props;n.opacity = t.opacity, n.opacity *= r[i].transform.op.v, n.matMdf = t.matMdf ? !0 : n.matMdf, n.opMdf = t.opMdf ? !0 : n.opMdf, o.transform(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11], l[12], l[13], l[14], l[15]);
        } else n.opacity = n.op.o;
      } else "sh" == e[i].ty || "el" == e[i].ty || "rc" == e[i].ty || "sr" == e[i].ty ? this.renderPath(e[i], r[i], n) : "fl" == e[i].ty ? this.renderFill(e[i], r[i], n) : "st" == e[i].ty ? this.renderStroke(e[i], r[i], n) : "gr" == e[i].ty ? this.renderShape(n, e[i].it, r[i].it) : "tm" == e[i].ty;
    }if (s) {
      a = this.stylesList.length;var p,
          m,
          f,
          c,
          d,
          u,
          y,
          g = this.globalData.renderer,
          v = this.globalData.canvasContext;for (g.save(), g.ctxTransform(this.finalTransform.mat.props), i = 0; a > i; i += 1) {
        if (y = this.stylesList[i].type, "st" !== y || 0 !== this.stylesList[i].wi) {
          for (g.save(), d = this.stylesList[i].elements, "st" === y ? (v.strokeStyle = this.stylesList[i].co, v.lineWidth = this.stylesList[i].wi, v.lineCap = this.stylesList[i].lc, v.lineJoin = this.stylesList[i].lj, v.miterLimit = this.stylesList[i].ml || 0) : v.fillStyle = this.stylesList[i].co, g.ctxOpacity(this.stylesList[i].coOp), "st" !== y && v.beginPath(), m = d.length, p = 0; m > p; p += 1) {
            for ("st" === y && (v.beginPath(), this.stylesList[i].da ? (v.setLineDash(this.stylesList[i].da), v.lineDashOffset = this.stylesList[i]["do"], this.globalData.isDashed = !0) : this.globalData.isDashed && (v.setLineDash(this.dashResetter), this.globalData.isDashed = !1)), u = d[p].trNodes, c = u.length, f = 0; c > f; f += 1) {
              "m" == u[f].t ? v.moveTo(u[f].p[0], u[f].p[1]) : "c" == u[f].t ? v.bezierCurveTo(u[f].p1[0], u[f].p1[1], u[f].p2[0], u[f].p2[1], u[f].p3[0], u[f].p3[1]) : v.closePath();
            }"st" === y && v.stroke();
          }"st" !== y && v.fill(this.stylesList[i].r), g.restore();
        }
      }g.restore(), this.firstFrame && (this.firstFrame = !1);
    }
  }, CVShapeElement.prototype.renderPath = function (t, e, r) {
    var s,
        i,
        a,
        n,
        o = r.matMdf || e.sh.mdf || this.firstFrame;if (o) {
      var h = e.sh.paths;n = h._length;var l = e.trNodes;for (l.length = 0, a = 0; n > a; a += 1) {
        var p = h.shapes[a];if (p && p.v) {
          for (s = p._length, i = 1; s > i; i += 1) {
            1 == i && l.push({ t: "m", p: r.mat.applyToPointArray(p.v[0][0], p.v[0][1], 0) }), l.push({ t: "c", p1: r.mat.applyToPointArray(p.o[i - 1][0], p.o[i - 1][1], 0), p2: r.mat.applyToPointArray(p.i[i][0], p.i[i][1], 0), p3: r.mat.applyToPointArray(p.v[i][0], p.v[i][1], 0) });
          }1 == s && l.push({ t: "m", p: r.mat.applyToPointArray(p.v[0][0], p.v[0][1], 0) }), p.c && s && (l.push({ t: "c", p1: r.mat.applyToPointArray(p.o[i - 1][0], p.o[i - 1][1], 0), p2: r.mat.applyToPointArray(p.i[0][0], p.i[0][1], 0), p3: r.mat.applyToPointArray(p.v[0][0], p.v[0][1], 0) }), l.push({ t: "z" })), e.lStr = l;
        }
      }if (e.st) for (i = 0; 16 > i; i += 1) {
        e.tr[i] = r.mat.props[i];
      }e.trNodes = l;
    }
  }, CVShapeElement.prototype.renderFill = function (t, e, r) {
    var s = e.style;(e.c.mdf || this.firstFrame) && (s.co = "rgb(" + bm_floor(e.c.v[0]) + "," + bm_floor(e.c.v[1]) + "," + bm_floor(e.c.v[2]) + ")"), (e.o.mdf || r.opMdf || this.firstFrame) && (s.coOp = e.o.v * r.opacity);
  }, CVShapeElement.prototype.renderStroke = function (t, e, r) {
    var s = e.style,
        i = e.d;i && (i.mdf || this.firstFrame) && (s.da = i.dasharray, s["do"] = i.dashoffset), (e.c.mdf || this.firstFrame) && (s.co = "rgb(" + bm_floor(e.c.v[0]) + "," + bm_floor(e.c.v[1]) + "," + bm_floor(e.c.v[2]) + ")"), (e.o.mdf || r.opMdf || this.firstFrame) && (s.coOp = e.o.v * r.opacity), (e.w.mdf || this.firstFrame) && (s.wi = e.w.v);
  }, CVShapeElement.prototype.destroy = function () {
    this.shapesData = null, this.globalData = null, this.canvasContext = null, this.stylesList.length = 0, this.viewData.length = 0, this._parent.destroy.call(this._parent);
  }, createElement(CVBaseElement, CVSolidElement), CVSolidElement.prototype.renderFrame = function (t) {
    if (this._parent.renderFrame.call(this, t) !== !1) {
      var e = this.canvasContext;this.globalData.renderer.save(), this.globalData.renderer.ctxTransform(this.finalTransform.mat.props), this.globalData.renderer.ctxOpacity(this.finalTransform.opacity), e.fillStyle = this.data.sc, e.fillRect(0, 0, this.data.sw, this.data.sh), this.globalData.renderer.restore(this.data.hasMask), this.firstFrame && (this.firstFrame = !1);
    }
  }, createElement(CVBaseElement, CVTextElement), CVTextElement.prototype.init = ITextElement.prototype.init, CVTextElement.prototype.getMeasures = ITextElement.prototype.getMeasures, CVTextElement.prototype.getMult = ITextElement.prototype.getMult, CVTextElement.prototype.prepareFrame = ITextElement.prototype.prepareFrame, CVTextElement.prototype.tHelper = document.createElement("canvas").getContext("2d"), CVTextElement.prototype.createElements = function () {
    this._parent.createElements.call(this);
  }, CVTextElement.prototype.buildNewText = function () {
    var t = this.currentTextDocumentData;this.renderedLetters = Array.apply(null, { length: this.currentTextDocumentData.l ? this.currentTextDocumentData.l.length : 0 });var e = !1;t.fc ? (e = !0, this.values.fill = "rgb(" + Math.round(255 * t.fc[0]) + "," + Math.round(255 * t.fc[1]) + "," + Math.round(255 * t.fc[2]) + ")") : this.values.fill = "rgba(0,0,0,0)", this.fill = e;var r = !1;t.sc && (r = !0, this.values.stroke = "rgb(" + Math.round(255 * t.sc[0]) + "," + Math.round(255 * t.sc[1]) + "," + Math.round(255 * t.sc[2]) + ")", this.values.sWidth = t.sw);var s,
        i,
        a = this.globalData.fontManager.getFontByName(t.f),
        n = t.l,
        o = this.mHelper;this.stroke = r, this.values.fValue = t.s + "px " + this.globalData.fontManager.getFontByName(t.f).fFamily, i = t.t.length, this.tHelper.font = this.values.fValue;var h,
        l,
        p,
        m,
        f,
        c,
        d,
        u,
        y,
        g,
        v = this.data.singleShape;if (v) var b = 0,
        E = 0,
        P = t.lineWidths,
        x = t.boxWidth,
        S = !0;var C = 0;for (s = 0; i > s; s += 1) {
      h = this.globalData.fontManager.getCharData(t.t.charAt(s), a.fStyle, this.globalData.fontManager.getFontByName(t.f).fFamily);var l;if (l = h ? h.data : null, o.reset(), v && n[s].n && (b = 0, E += t.yOffset, E += S ? 1 : 0, S = !1), l && l.shapes) {
        if (f = l.shapes[0].it, d = f.length, o.scale(t.s / 100, t.s / 100), v) {
          switch (t.ps && o.translate(t.ps[0], t.ps[1] + t.ascent, 0), t.j) {case 1:
              o.translate(t.justifyOffset + (x - P[n[s].line]), 0, 0);break;case 2:
              o.translate(t.justifyOffset + (x - P[n[s].line]) / 2, 0, 0);}o.translate(b, E, 0);
        }for (y = new Array(d), c = 0; d > c; c += 1) {
          for (m = f[c].ks.k.i.length, u = f[c].ks.k, g = [], p = 1; m > p; p += 1) {
            1 == p && g.push(o.applyToX(u.v[0][0], u.v[0][1], 0), o.applyToY(u.v[0][0], u.v[0][1], 0)), g.push(o.applyToX(u.o[p - 1][0], u.o[p - 1][1], 0), o.applyToY(u.o[p - 1][0], u.o[p - 1][1], 0), o.applyToX(u.i[p][0], u.i[p][1], 0), o.applyToY(u.i[p][0], u.i[p][1], 0), o.applyToX(u.v[p][0], u.v[p][1], 0), o.applyToY(u.v[p][0], u.v[p][1], 0));
          }g.push(o.applyToX(u.o[p - 1][0], u.o[p - 1][1], 0), o.applyToY(u.o[p - 1][0], u.o[p - 1][1], 0), o.applyToX(u.i[0][0], u.i[0][1], 0), o.applyToY(u.i[0][0], u.i[0][1], 0), o.applyToX(u.v[0][0], u.v[0][1], 0), o.applyToY(u.v[0][0], u.v[0][1], 0)), y[c] = g;
        }
      } else y = [];v && (b += n[s].l), this.textSpans[C] ? this.textSpans[C].elem = y : this.textSpans[C] = { elem: y }, C += 1;
    }
  }, CVTextElement.prototype.renderFrame = function (t) {
    if (this._parent.renderFrame.call(this, t) !== !1) {
      var e = this.canvasContext,
          r = this.finalTransform.mat.props;this.globalData.renderer.save(), this.globalData.renderer.ctxTransform(r), this.globalData.renderer.ctxOpacity(this.finalTransform.opacity), e.font = this.values.fValue, e.lineCap = "butt", e.lineJoin = "miter", e.miterLimit = 4, this.data.singleShape || this.getMeasures();var s,
          i,
          a,
          n,
          o,
          h,
          l = this.renderedLetters,
          p = this.currentTextDocumentData.l;i = p.length;var m,
          f,
          c,
          d = null,
          u = null,
          y = null;for (s = 0; i > s; s += 1) {
        if (!p[s].n) {
          if (m = l[s], m && (this.globalData.renderer.save(), this.globalData.renderer.ctxTransform(m.props), this.globalData.renderer.ctxOpacity(m.o)), this.fill) {
            for (m && m.fc ? d !== m.fc && (d = m.fc, e.fillStyle = m.fc) : d !== this.values.fill && (d = this.values.fill, e.fillStyle = this.values.fill), f = this.textSpans[s].elem, n = f.length, this.globalData.canvasContext.beginPath(), a = 0; n > a; a += 1) {
              for (c = f[a], h = c.length, this.globalData.canvasContext.moveTo(c[0], c[1]), o = 2; h > o; o += 6) {
                this.globalData.canvasContext.bezierCurveTo(c[o], c[o + 1], c[o + 2], c[o + 3], c[o + 4], c[o + 5]);
              }
            }this.globalData.canvasContext.closePath(), this.globalData.canvasContext.fill();
          }if (this.stroke) {
            for (m && m.sw ? y !== m.sw && (y = m.sw, e.lineWidth = m.sw) : y !== this.values.sWidth && (y = this.values.sWidth, e.lineWidth = this.values.sWidth), m && m.sc ? u !== m.sc && (u = m.sc, e.strokeStyle = m.sc) : u !== this.values.stroke && (u = this.values.stroke, e.strokeStyle = this.values.stroke), f = this.textSpans[s].elem, n = f.length, this.globalData.canvasContext.beginPath(), a = 0; n > a; a += 1) {
              for (c = f[a], h = c.length, this.globalData.canvasContext.moveTo(c[0], c[1]), o = 2; h > o; o += 6) {
                this.globalData.canvasContext.bezierCurveTo(c[o], c[o + 1], c[o + 2], c[o + 3], c[o + 4], c[o + 5]);
              }
            }this.globalData.canvasContext.closePath(), this.globalData.canvasContext.stroke();
          }m && this.globalData.renderer.restore();
        }
      }this.globalData.renderer.restore(this.data.hasMask), this.firstFrame && (this.firstFrame = !1);
    }
  }, createElement(BaseElement, HBaseElement), HBaseElement.prototype.checkBlendMode = function () {}, HBaseElement.prototype.setBlendMode = BaseElement.prototype.setBlendMode, HBaseElement.prototype.getBaseElement = function () {
    return this.baseElement;
  }, HBaseElement.prototype.createElements = function () {
    this.data.hasMask ? (this.layerElement = document.createElementNS(svgNS, "svg"), styleDiv(this.layerElement), this.baseElement = this.layerElement, this.maskedElement = this.layerElement) : this.layerElement = this.parentContainer, this.transformedElement = this.layerElement, !this.data.ln || 4 !== this.data.ty && 0 !== this.data.ty || (this.layerElement === this.parentContainer && (this.layerElement = document.createElementNS(svgNS, "g"), this.baseElement = this.layerElement), this.layerElement.setAttribute("id", this.data.ln)), this.setBlendMode(), this.layerElement !== this.parentContainer && (this.placeholder = null), this.checkParenting();
  }, HBaseElement.prototype.renderFrame = function (t) {
    if (3 === this.data.ty) return !1;if (this.currentFrameNum === this.lastNum || !this.isVisible) return this.isVisible;this.lastNum = this.currentFrameNum, this.finalTransform.opMdf = this.finalTransform.op.mdf, this.finalTransform.matMdf = this.finalTransform.mProp.mdf, this.finalTransform.opacity = this.finalTransform.op.v, this.firstFrame && (this.finalTransform.opMdf = !0, this.finalTransform.matMdf = !0);var e,
        r = this.finalTransform.mat;if (this.hierarchy) {
      var s,
          i = this.hierarchy.length;for (e = this.finalTransform.mProp.v.props, r.cloneFromProps(e), s = 0; i > s; s += 1) {
        this.finalTransform.matMdf = this.hierarchy[s].finalTransform.mProp.mdf ? !0 : this.finalTransform.matMdf, e = this.hierarchy[s].finalTransform.mProp.v.props, r.transform(e[0], e[1], e[2], e[3], e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15]);
      }
    } else this.isVisible && this.finalTransform.matMdf && (t ? (e = this.finalTransform.mProp.v.props, r.cloneFromProps(e)) : r.cloneFromProps(this.finalTransform.mProp.v.props));return this.data.hasMask && this.maskManager.renderFrame(r), t && (e = t.mat.props, r.cloneFromProps(e), this.finalTransform.opacity *= t.opacity, this.finalTransform.opMdf = t.opMdf ? !0 : this.finalTransform.opMdf, this.finalTransform.matMdf = t.matMdf ? !0 : this.finalTransform.matMdf), this.finalTransform.matMdf && (this.transformedElement.style.transform = this.transformedElement.style.webkitTransform = r.toCSS(), this.finalMat = r), this.finalTransform.opMdf && (this.transformedElement.style.opacity = this.finalTransform.opacity), this.isVisible;
  }, HBaseElement.prototype.destroy = function () {
    this.layerElement = null, this.transformedElement = null, this.parentContainer = null, this.matteElement && (this.matteElement = null), this.maskManager && (this.maskManager.destroy(), this.maskManager = null);
  }, HBaseElement.prototype.getDomElement = function () {
    return this.layerElement;
  }, HBaseElement.prototype.addMasks = function (t) {
    this.maskManager = new MaskElement(t, this, this.globalData);
  }, HBaseElement.prototype.hide = function () {}, HBaseElement.prototype.setMatte = function () {}, HBaseElement.prototype.buildElementParenting = HybridRenderer.prototype.buildElementParenting, createElement(HBaseElement, HSolidElement), HSolidElement.prototype.createElements = function () {
    var t = document.createElement("div");styleDiv(t);var e = document.createElementNS(svgNS, "svg");styleDiv(e), e.setAttribute("width", this.data.sw), e.setAttribute("height", this.data.sh), t.appendChild(e), this.layerElement = t, this.transformedElement = t, this.baseElement = t, this.innerElem = t, this.data.ln && this.innerElem.setAttribute("id", this.data.ln), 0 !== this.data.bm && this.setBlendMode();var r = document.createElementNS(svgNS, "rect");r.setAttribute("width", this.data.sw), r.setAttribute("height", this.data.sh), r.setAttribute("fill", this.data.sc), e.appendChild(r), this.data.hasMask && (this.maskedElement = r), this.checkParenting();
  }, HSolidElement.prototype.hide = IImageElement.prototype.hide, HSolidElement.prototype.renderFrame = IImageElement.prototype.renderFrame, HSolidElement.prototype.destroy = IImageElement.prototype.destroy, createElement(HBaseElement, HCompElement), HCompElement.prototype.createElements = function () {
    var t = document.createElement("div");if (styleDiv(t), this.data.ln && t.setAttribute("id", this.data.ln), t.style.clip = "rect(0px, " + this.data.w + "px, " + this.data.h + "px, 0px)", this.data.hasMask) {
      var e = document.createElementNS(svgNS, "svg");styleDiv(e), e.setAttribute("width", this.data.w), e.setAttribute("height", this.data.h);var r = document.createElementNS(svgNS, "g");e.appendChild(r), t.appendChild(e), this.maskedElement = r, this.baseElement = t, this.layerElement = r, this.transformedElement = t;
    } else this.layerElement = t, this.baseElement = this.layerElement, this.transformedElement = t;this.checkParenting();
  }, HCompElement.prototype.hide = ICompElement.prototype.hide, HCompElement.prototype.prepareFrame = ICompElement.prototype.prepareFrame, HCompElement.prototype.setElements = ICompElement.prototype.setElements, HCompElement.prototype.getElements = ICompElement.prototype.getElements, HCompElement.prototype.destroy = ICompElement.prototype.destroy, HCompElement.prototype.renderFrame = function (t) {
    var e,
        r = this._parent.renderFrame.call(this, t),
        s = this.layers.length;if (r === !1) return void this.hide();for (this.hidden = !1, e = 0; s > e; e += 1) {
      (this.completeLayers || this.elements[e]) && this.elements[e].renderFrame();
    }this.firstFrame && (this.firstFrame = !1);
  }, HCompElement.prototype.checkLayers = BaseRenderer.prototype.checkLayers, HCompElement.prototype.buildItem = HybridRenderer.prototype.buildItem, HCompElement.prototype.checkPendingElements = HybridRenderer.prototype.checkPendingElements, HCompElement.prototype.addPendingElement = HybridRenderer.prototype.addPendingElement, HCompElement.prototype.buildAllItems = BaseRenderer.prototype.buildAllItems, HCompElement.prototype.createItem = HybridRenderer.prototype.createItem, HCompElement.prototype.buildElementParenting = HybridRenderer.prototype.buildElementParenting, HCompElement.prototype.createImage = HybridRenderer.prototype.createImage, HCompElement.prototype.createComp = HybridRenderer.prototype.createComp, HCompElement.prototype.createSolid = HybridRenderer.prototype.createSolid, HCompElement.prototype.createShape = HybridRenderer.prototype.createShape, HCompElement.prototype.createText = HybridRenderer.prototype.createText, HCompElement.prototype.createBase = HybridRenderer.prototype.createBase, HCompElement.prototype.appendElementInPos = HybridRenderer.prototype.appendElementInPos, createElement(HBaseElement, HShapeElement);var parent = HShapeElement.prototype._parent;extendPrototype(IShapeElement, HShapeElement), HShapeElement.prototype._parent = parent, HShapeElement.prototype.createElements = function () {
    var t = document.createElement("div");styleDiv(t);var e = document.createElementNS(svgNS, "svg");styleDiv(e);var r = this.comp.data ? this.comp.data : this.globalData.compSize;if (e.setAttribute("width", r.w), e.setAttribute("height", r.h), this.data.hasMask) {
      var s = document.createElementNS(svgNS, "g");t.appendChild(e), e.appendChild(s), this.maskedElement = s, this.layerElement = s, this.shapesContainer = s;
    } else t.appendChild(e), this.layerElement = e, this.shapesContainer = document.createElementNS(svgNS, "g"), this.layerElement.appendChild(this.shapesContainer);this.data.hd || (this.baseElement = t), this.innerElem = t, this.data.ln && this.innerElem.setAttribute("id", this.data.ln), this.searchShapes(this.shapesData, this.viewData, this.layerElement, this.dynamicProperties, 0), this.buildExpressionInterface(), this.layerElement = t, this.transformedElement = t, this.shapeCont = e, 0 !== this.data.bm && this.setBlendMode(), this.checkParenting();
  }, HShapeElement.prototype.renderFrame = function (t) {
    var e = this._parent.renderFrame.call(this, t);if (e === !1) return void this.hide();if (this.hidden && (this.layerElement.style.display = "block", this.hidden = !1), this.renderModifiers(), this.addedTransforms.mdf = this.finalTransform.matMdf, this.addedTransforms.mats.length = 1, this.addedTransforms.mats[0] = this.finalTransform.mat, this.renderShape(null, null, !0, null), this.isVisible && (this.elemMdf || this.firstFrame)) {
      var r = this.shapeCont.getBBox(),
          s = !1;this.currentBBox.w !== r.width && (this.currentBBox.w = r.width, this.shapeCont.setAttribute("width", r.width), s = !0), this.currentBBox.h !== r.height && (this.currentBBox.h = r.height, this.shapeCont.setAttribute("height", r.height), s = !0), (s || this.currentBBox.x !== r.x || this.currentBBox.y !== r.y) && (this.currentBBox.w = r.width, this.currentBBox.h = r.height, this.currentBBox.x = r.x, this.currentBBox.y = r.y, this.shapeCont.setAttribute("viewBox", this.currentBBox.x + " " + this.currentBBox.y + " " + this.currentBBox.w + " " + this.currentBBox.h), this.shapeCont.style.transform = this.shapeCont.style.webkitTransform = "translate(" + this.currentBBox.x + "px," + this.currentBBox.y + "px)");
    }
  }, createElement(HBaseElement, HTextElement), HTextElement.prototype.init = ITextElement.prototype.init, HTextElement.prototype.getMeasures = ITextElement.prototype.getMeasures, HTextElement.prototype.createPathShape = ITextElement.prototype.createPathShape, HTextElement.prototype.prepareFrame = ITextElement.prototype.prepareFrame, HTextElement.prototype.createElements = function () {
    this.isMasked = this.checkMasks();var t = document.createElement("div");if (styleDiv(t), this.layerElement = t, this.transformedElement = t, this.isMasked) {
      this.renderType = "svg";var e = document.createElementNS(svgNS, "svg");styleDiv(e), this.cont = e, this.compW = this.comp.data.w, this.compH = this.comp.data.h, e.setAttribute("width", this.compW), e.setAttribute("height", this.compH);var r = document.createElementNS(svgNS, "g");e.appendChild(r), t.appendChild(e), this.maskedElement = r, this.innerElem = r;
    } else this.renderType = "html", this.innerElem = t;this.baseElement = t, this.checkParenting();
  }, HTextElement.prototype.buildNewText = function () {
    var t = this.currentTextDocumentData;this.renderedLetters = Array.apply(null, { length: this.currentTextDocumentData.l ? this.currentTextDocumentData.l.length : 0 }), this.innerElem.style.color = this.innerElem.style.fill = t.fc ? "rgb(" + Math.round(255 * t.fc[0]) + "," + Math.round(255 * t.fc[1]) + "," + Math.round(255 * t.fc[2]) + ")" : "rgba(0,0,0,0)", t.sc && (this.innerElem.style.stroke = "rgb(" + Math.round(255 * t.sc[0]) + "," + Math.round(255 * t.sc[1]) + "," + Math.round(255 * t.sc[2]) + ")", this.innerElem.style.strokeWidth = t.sw + "px");var e = this.globalData.fontManager.getFontByName(t.f);if (!this.globalData.fontManager.chars) if (this.innerElem.style.fontSize = t.s + "px", this.innerElem.style.lineHeight = t.s + "px", e.fClass) this.innerElem.className = e.fClass;else {
      this.innerElem.style.fontFamily = e.fFamily;var r = t.fWeight,
          s = t.fStyle;this.innerElem.style.fontStyle = s, this.innerElem.style.fontWeight = r;
    }var i,
        a,
        n = t.l;a = n.length;var o,
        h,
        l,
        p,
        m = this.mHelper,
        f = "",
        c = 0;for (i = 0; a > i; i += 1) {
      if (this.globalData.fontManager.chars ? (this.textPaths[c] ? o = this.textPaths[c] : (o = document.createElementNS(svgNS, "path"), o.setAttribute("stroke-linecap", "butt"), o.setAttribute("stroke-linejoin", "round"), o.setAttribute("stroke-miterlimit", "4")), this.isMasked || (this.textSpans[c] ? (h = this.textSpans[c], l = h.children[0]) : (h = document.createElement("div"), l = document.createElementNS(svgNS, "svg"), l.appendChild(o), styleDiv(h)))) : this.isMasked ? o = this.textPaths[c] ? this.textPaths[c] : document.createElementNS(svgNS, "text") : this.textSpans[c] ? (h = this.textSpans[c], o = this.textPaths[c]) : (h = document.createElement("span"), styleDiv(h), o = document.createElement("span"), styleDiv(o), h.appendChild(o)), this.globalData.fontManager.chars) {
        var d,
            u = this.globalData.fontManager.getCharData(t.t.charAt(i), e.fStyle, this.globalData.fontManager.getFontByName(t.f).fFamily);if (d = u ? u.data : null, m.reset(), d && d.shapes && (p = d.shapes[0].it, m.scale(t.s / 100, t.s / 100), f = this.createPathShape(m, p), o.setAttribute("d", f)), this.isMasked) this.innerElem.appendChild(o);else if (this.innerElem.appendChild(h), d && d.shapes) {
          document.body.appendChild(l);var y = l.getBBox();l.setAttribute("width", y.width), l.setAttribute("height", y.height), l.setAttribute("viewBox", y.x + " " + y.y + " " + y.width + " " + y.height), l.style.transform = l.style.webkitTransform = "translate(" + y.x + "px," + y.y + "px)", n[i].yOffset = y.y, h.appendChild(l);
        } else l.setAttribute("width", 1), l.setAttribute("height", 1);
      } else o.textContent = n[i].val, o.setAttributeNS("http://www.w3.org/XML/1998/namespace", "xml:space", "preserve"), this.isMasked ? this.innerElem.appendChild(o) : (this.innerElem.appendChild(h), o.style.transform = o.style.webkitTransform = "translate3d(0," + -t.s / 1.2 + "px,0)");this.textSpans[c] = this.isMasked ? o : h, this.textSpans[c].style.display = "block", this.textPaths[c] = o, c += 1;
    }for (; c < this.textSpans.length;) {
      this.textSpans[c].style.display = "none", c += 1;
    }
  }, HTextElement.prototype.hide = SVGTextElement.prototype.hide, HTextElement.prototype.renderFrame = function (t) {
    var e = this._parent.renderFrame.call(this, t);if (e === !1) return void this.hide();if (this.hidden && (this.hidden = !1, this.innerElem.style.display = "block", this.layerElement.style.display = "block"), this.data.singleShape) {
      if (!this.firstFrame && !this.lettersChangedFlag) return;this.isMasked && this.finalTransform.matMdf && (this.cont.setAttribute("viewBox", -this.finalTransform.mProp.p.v[0] + " " + -this.finalTransform.mProp.p.v[1] + " " + this.compW + " " + this.compH), this.cont.style.transform = this.cont.style.webkitTransform = "translate(" + -this.finalTransform.mProp.p.v[0] + "px," + -this.finalTransform.mProp.p.v[1] + "px)");
    }if (this.getMeasures(), this.lettersChangedFlag) {
      var r,
          s,
          i = this.renderedLetters,
          a = this.currentTextDocumentData.l;s = a.length;var n;for (r = 0; s > r; r += 1) {
        a[r].n || (n = i[r], this.isMasked ? this.textSpans[r].setAttribute("transform", n.m) : this.textSpans[r].style.transform = this.textSpans[r].style.webkitTransform = n.m, this.textSpans[r].style.opacity = n.o, n.sw && this.textPaths[r].setAttribute("stroke-width", n.sw), n.sc && this.textPaths[r].setAttribute("stroke", n.sc), n.fc && (this.textPaths[r].setAttribute("fill", n.fc), this.textPaths[r].style.color = n.fc));
      }if (this.isVisible && (this.elemMdf || this.firstFrame) && this.innerElem.getBBox) {
        var o = this.innerElem.getBBox();this.currentBBox.w !== o.width && (this.currentBBox.w = o.width, this.cont.setAttribute("width", o.width)), this.currentBBox.h !== o.height && (this.currentBBox.h = o.height, this.cont.setAttribute("height", o.height)), (this.currentBBox.w !== o.width || this.currentBBox.h !== o.height || this.currentBBox.x !== o.x || this.currentBBox.y !== o.y) && (this.currentBBox.w = o.width, this.currentBBox.h = o.height, this.currentBBox.x = o.x, this.currentBBox.y = o.y, this.cont.setAttribute("viewBox", this.currentBBox.x + " " + this.currentBBox.y + " " + this.currentBBox.w + " " + this.currentBBox.h), this.cont.style.transform = this.cont.style.webkitTransform = "translate(" + this.currentBBox.x + "px," + this.currentBBox.y + "px)");
      }this.firstFrame && (this.firstFrame = !1);
    }
  }, HTextElement.prototype.destroy = SVGTextElement.prototype.destroy, createElement(HBaseElement, HImageElement), HImageElement.prototype.createElements = function () {
    var t = this.globalData.getAssetsPath(this.assetData),
        e = new Image();if (this.data.hasMask) {
      var r = document.createElement("div");styleDiv(r);var s = document.createElementNS(svgNS, "svg");styleDiv(s), s.setAttribute("width", this.assetData.w), s.setAttribute("height", this.assetData.h), r.appendChild(s), this.imageElem = document.createElementNS(svgNS, "image"), this.imageElem.setAttribute("width", this.assetData.w + "px"), this.imageElem.setAttribute("height", this.assetData.h + "px"), this.imageElem.setAttributeNS("http://www.w3.org/1999/xlink", "href", t), s.appendChild(this.imageElem), this.layerElement = r, this.transformedElement = r, this.baseElement = r, this.innerElem = r, this.maskedElement = this.imageElem;
    } else styleDiv(e), this.layerElement = e, this.baseElement = e, this.innerElem = e, this.transformedElement = e;e.src = t, this.data.ln && this.innerElem.setAttribute("id", this.data.ln), this.checkParenting();
  }, HImageElement.prototype.hide = HSolidElement.prototype.hide, HImageElement.prototype.renderFrame = HSolidElement.prototype.renderFrame, HImageElement.prototype.destroy = HSolidElement.prototype.destroy, createElement(HBaseElement, HCameraElement), HCameraElement.prototype.setup = function () {
    var t,
        e,
        r = this.comp.threeDElements.length;for (t = 0; r > t; t += 1) {
      e = this.comp.threeDElements[t], e.perspectiveElem.style.perspective = e.perspectiveElem.style.webkitPerspective = this.pe.v + "px", e.container.style.transformOrigin = e.container.style.mozTransformOrigin = e.container.style.webkitTransformOrigin = "0px 0px 0px", e.perspectiveElem.style.transform = e.perspectiveElem.style.webkitTransform = "matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)";
    }
  }, HCameraElement.prototype.createElements = function () {}, HCameraElement.prototype.hide = function () {}, HCameraElement.prototype.renderFrame = function () {
    var t,
        e,
        r = this.firstFrame;if (this.hierarchy) for (e = this.hierarchy.length, t = 0; e > t; t += 1) {
      r = this.hierarchy[t].finalTransform.mProp.mdf ? !0 : r;
    }if (r || this.p && this.p.mdf || this.px && (this.px.mdf || this.py.mdf || this.pz.mdf) || this.rx.mdf || this.ry.mdf || this.rz.mdf || this.or.mdf || this.a && this.a.mdf) {
      if (this.mat.reset(), this.p ? this.mat.translate(-this.p.v[0], -this.p.v[1], this.p.v[2]) : this.mat.translate(-this.px.v, -this.py.v, this.pz.v), this.a) {
        var s = [this.p.v[0] - this.a.v[0], this.p.v[1] - this.a.v[1], this.p.v[2] - this.a.v[2]],
            i = Math.sqrt(Math.pow(s[0], 2) + Math.pow(s[1], 2) + Math.pow(s[2], 2)),
            a = [s[0] / i, s[1] / i, s[2] / i],
            n = Math.sqrt(a[2] * a[2] + a[0] * a[0]),
            o = Math.atan2(a[1], n),
            h = Math.atan2(a[0], -a[2]);this.mat.rotateY(h).rotateX(-o);
      }if (this.mat.rotateX(-this.rx.v).rotateY(-this.ry.v).rotateZ(this.rz.v), this.mat.rotateX(-this.or.v[0]).rotateY(-this.or.v[1]).rotateZ(this.or.v[2]), this.mat.translate(this.globalData.compSize.w / 2, this.globalData.compSize.h / 2, 0), this.mat.translate(0, 0, this.pe.v), this.hierarchy) {
        var l;for (e = this.hierarchy.length, t = 0; e > t; t += 1) {
          l = this.hierarchy[t].finalTransform.mProp.iv.props, this.mat.transform(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11], -l[12], -l[13], l[14], l[15]);
        }
      }e = this.comp.threeDElements.length;var p;for (t = 0; e > t; t += 1) {
        p = this.comp.threeDElements[t], p.container.style.transform = p.container.style.webkitTransform = this.mat.toCSS();
      }
    }this.firstFrame = !1;
  }, HCameraElement.prototype.destroy = function () {};var Expressions = function () {
    function t(t) {
      t.renderer.compInterface = CompExpressionInterface(t.renderer), t.renderer.globalData.projectInterface.registerComposition(t.renderer);
    }var e = {};return e.initExpressions = t, e;
  }();expressionsPlugin = Expressions, function () {
    function t() {
      return this.pv;
    }function e(t, e) {
      t *= this.elem.globalData.frameRate;var r,
          s,
          i = 0,
          a = this.keyframes.length - 1,
          n = 1,
          o = !0;e = void 0 === e ? this.offsetTime : 0;for (var h = "object" == _typeof(this.pv) ? [this.pv.length] : 0; o;) {
        if (r = this.keyframes[i], s = this.keyframes[i + 1], i == a - 1 && t >= s.t - e) {
          r.h && (r = s);break;
        }if (s.t - e > t) break;a - 1 > i ? i += n : o = !1;
      }var l,
          p,
          m,
          f,
          c,
          d = 0;if (r.to) {
        r.bezierData || bez.buildBezierData(r);var u = r.bezierData;if (t >= s.t - e || t < r.t - e) {
          var y = t >= s.t - e ? u.points.length - 1 : 0;for (p = u.points[y].point.length, l = 0; p > l; l += 1) {
            h[l] = u.points[y].point[l];
          }
        } else {
          r.__fnct ? c = r.__fnct : (c = BezierFactory.getBezierEasing(r.o.x, r.o.y, r.i.x, r.i.y, r.n).get, r.__fnct = c), m = c((t - (r.t - e)) / (s.t - e - (r.t - e)));var g,
              v = u.segmentLength * m,
              b = 0;for (n = 1, o = !0, f = u.points.length; o;) {
            if (b += u.points[d].partialLength * n, 0 === v || 0 === m || d == u.points.length - 1) {
              for (p = u.points[d].point.length, l = 0; p > l; l += 1) {
                h[l] = u.points[d].point[l];
              }break;
            }if (v >= b && v < b + u.points[d + 1].partialLength) {
              for (g = (v - b) / u.points[d + 1].partialLength, p = u.points[d].point.length, l = 0; p > l; l += 1) {
                h[l] = u.points[d].point[l] + (u.points[d + 1].point[l] - u.points[d].point[l]) * g;
              }break;
            }f - 1 > d && 1 == n || d > 0 && -1 == n ? d += n : o = !1;
          }
        }
      } else {
        var E,
            P,
            x,
            S,
            C,
            k = !1;for (a = r.s.length, i = 0; a > i; i += 1) {
          if (1 !== r.h && (r.o.x instanceof Array ? (k = !0, r.__fnct || (r.__fnct = []), r.__fnct[i] || (E = r.o.x[i] || r.o.x[0], P = r.o.y[i] || r.o.y[0], x = r.i.x[i] || r.i.x[0], S = r.i.y[i] || r.i.y[0])) : (k = !1, r.__fnct || (E = r.o.x, P = r.o.y, x = r.i.x, S = r.i.y)), k ? r.__fnct[i] ? c = r.__fnct[i] : (c = BezierFactory.getBezierEasing(E, P, x, S).get, r.__fnct[i] = c) : r.__fnct ? c = r.__fnct : (c = BezierFactory.getBezierEasing(E, P, x, S).get, r.__fnct = c), m = t >= s.t - e ? 1 : t < r.t - e ? 0 : c((t - (r.t - e)) / (s.t - e - (r.t - e)))), this.sh && 1 !== r.h) {
            var M = r.s[i],
                A = r.e[i];-180 > M - A ? M += 360 : M - A > 180 && (M -= 360), C = M + (A - M) * m;
          } else C = 1 === r.h ? r.s[i] : r.s[i] + (r.e[i] - r.s[i]) * m;1 === a ? h = C : h[i] = C;
        }
      }return h;
    }function r(t) {
      if (void 0 !== this.vel) return this.vel;var e,
          r = -.01,
          s = this.getValueAtTime(t, 0),
          i = this.getValueAtTime(t + r, 0);if (s.length) {
        e = Array.apply(null, { length: s.length });var a;for (a = 0; a < s.length; a += 1) {
          e[a] = this.elem.globalData.frameRate * ((i[a] - s[a]) / r);
        }
      } else e = (i - s) / r;return e;
    }function s(t) {
      this.propertyGroup = t;
    }function i(t, e, r) {
      e.x && (r.k = !0, r.x = !0, r.getValue && (r.getPreValue = r.getValue), r.getValue = ExpressionManager.initiateExpression.bind(r)(t, e, r));
    }var a = function () {
      function a(t, e) {
        return this.textIndex = t + 1, this.textTotal = e, this.getValue(), this.v;
      }return function (n, o) {
        this.pv = 1, this.comp = n.comp, this.elem = n, this.mult = .01, this.type = "textSelector", this.textTotal = o.totalChars, this.selectorValue = 100, this.lastValue = [1, 1, 1], i.bind(this)(n, o, this), this.getMult = a, this.getVelocityAtTime = r, this.getValueAtTime = this.kf ? e.bind(this) : t.bind(this), this.setGroupProperty = s;
      };
    }(),
        n = PropertyFactory.getProp;PropertyFactory.getProp = function (a, o, h, l, p) {
      var m = n(a, o, h, l, p);m.getVelocityAtTime = r, m.getValueAtTime = m.kf ? e.bind(m) : t.bind(m), m.setGroupProperty = s;var f = m.k;return void 0 !== o.ix && Object.defineProperty(m, "propertyIndex", { get: function get() {
          return o.ix;
        } }), i(a, o, m), !f && m.x && p.push(m), m;
    };var o = ShapePropertyFactory.getShapeProp;ShapePropertyFactory.getShapeProp = function (r, a, n, h, l) {
      var p = o(r, a, n, h, l);p.setGroupProperty = s, p.getValueAtTime = p.kf ? e : t;var m = p.k;return void 0 !== a.ix && Object.defineProperty(p, "propertyIndex", { get: function get() {
          return a.ix;
        } }), 3 === n ? i(r, a.pt, p) : 4 === n && i(r, a.ks, p), !m && p.x && h.push(p), p;
    };var h = PropertyFactory.getTextSelectorProp;PropertyFactory.getTextSelectorProp = function (t, e, r) {
      return 1 === e.t ? new a(t, e, r) : h(t, e, r);
    };
  }();var ExpressionManager = function () {
    function duplicatePropertyValue(t, e) {
      if (e = e || 1, "number" == typeof t || t instanceof Number) return t * e;if (t.i) return JSON.parse(JSON.stringify(t));var r,
          s = Array.apply(null, { length: t.length }),
          i = t.length;for (r = 0; i > r; r += 1) {
        s[r] = t[r] * e;
      }return s;
    }function shapesEqual(t, e) {
      if (t._length !== e._length || t.c !== e.c) return !1;var r,
          s = t._length;for (r = 0; s > r; r += 1) {
        if (t.v[r][0] !== e.v[r][0] || t.v[r][1] !== e.v[r][1] || t.o[r][0] !== e.o[r][0] || t.o[r][1] !== e.o[r][1] || t.i[r][0] !== e.i[r][0] || t.i[r][1] !== e.i[r][1]) return !1;
      }return !0;
    }function $bm_neg(t) {
      var e = typeof t === "undefined" ? "undefined" : _typeof(t);if ("number" === e || "boolean" === e || t instanceof Number) return -t;if (t.constructor === Array) {
        var r,
            s = t.length,
            i = [];for (r = 0; s > r; r += 1) {
          i[r] = -t[r];
        }return i;
      }
    }function sum(t, e) {
      var r = typeof t === "undefined" ? "undefined" : _typeof(t),
          s = typeof e === "undefined" ? "undefined" : _typeof(e);if ("string" === r || "string" === s) return t + e;if (("number" === r || "boolean" === r || "string" === r || t instanceof Number) && ("number" === s || "boolean" === s || "string" === s || e instanceof Number)) return t + e;if (t.constructor === Array && ("number" === s || "boolean" === s || "string" === s || e instanceof Number)) return t[0] = t[0] + e, t;if (("number" === r || "boolean" === r || "string" === r || t instanceof Number) && e.constructor === Array) return e[0] = t + e[0], e;if (t.constructor === Array && e.constructor === Array) {
        for (var i = 0, a = t.length, n = e.length, o = []; a > i || n > i;) {
          o[i] = "number" == typeof t[i] && "number" == typeof e[i] ? t[i] + e[i] : void 0 == e[i] ? t[i] : t[i] || e[i], i += 1;
        }return o;
      }return 0;
    }function sub(t, e) {
      var r = typeof t === "undefined" ? "undefined" : _typeof(t),
          s = typeof e === "undefined" ? "undefined" : _typeof(e);if (("number" === r || "boolean" === r || "string" === r || t instanceof Number) && ("number" === s || "boolean" === s || "string" === s || e instanceof Number)) return t - e;if (t.constructor === Array && ("number" === s || "boolean" === s || "string" === s || e instanceof Number)) return t[0] = t[0] - e, t;if (("number" === r || "boolean" === r || "string" === r || t instanceof Number) && e.constructor === Array) return e[0] = t - e[0], e;if (t.constructor === Array && e.constructor === Array) {
        for (var i = 0, a = t.length, n = e.length, o = []; a > i || n > i;) {
          o[i] = "number" == typeof t[i] && "number" == typeof e[i] ? t[i] - e[i] : void 0 == e[i] ? t[i] : t[i] || e[i], i += 1;
        }return o;
      }return 0;
    }function mul(t, e) {
      var r,
          s = typeof t === "undefined" ? "undefined" : _typeof(t),
          i = typeof e === "undefined" ? "undefined" : _typeof(e);if (("number" === s || "boolean" === s || "string" === s || t instanceof Number) && ("number" === i || "boolean" === i || "string" === i || e instanceof Number)) return t * e;var a, n;if (t.constructor === Array && ("number" === i || "boolean" === i || "string" === i || e instanceof Number)) {
        for (n = t.length, r = Array.apply(null, { length: n }), a = 0; n > a; a += 1) {
          r[a] = t[a] * e;
        }return r;
      }if (("number" === s || "boolean" === s || "string" === s || t instanceof Number) && e.constructor === Array) {
        for (n = e.length, r = Array.apply(null, { length: n }), a = 0; n > a; a += 1) {
          r[a] = t * e[a];
        }return r;
      }return 0;
    }function div(t, e) {
      var r,
          s = typeof t === "undefined" ? "undefined" : _typeof(t),
          i = typeof e === "undefined" ? "undefined" : _typeof(e);if (("number" === s || "boolean" === s || "string" === s || t instanceof Number) && ("number" === i || "boolean" === i || "string" === i || e instanceof Number)) return t / e;var a, n;if (t.constructor === Array && ("number" === i || "boolean" === i || "string" === i || e instanceof Number)) {
        for (n = t.length, r = Array.apply(null, { length: n }), a = 0; n > a; a += 1) {
          r[a] = t[a] / e;
        }return r;
      }if (("number" === s || "boolean" === s || "string" === s || t instanceof Number) && e.constructor === Array) {
        for (n = e.length, r = Array.apply(null, { length: n }), a = 0; n > a; a += 1) {
          r[a] = t / e[a];
        }return r;
      }return 0;
    }function clamp(t, e, r) {
      if (e > r) {
        var s = r;r = e, e = s;
      }return Math.min(Math.max(t, e), r);
    }function radiansToDegrees(t) {
      return t / degToRads;
    }function degreesToRadians(t) {
      return t * degToRads;
    }function length(t, e) {
      if ("number" == typeof t) return e = e || 0, Math.abs(t - e);e || (e = helperLengthArray);var r,
          s = Math.min(t.length, e.length),
          i = 0;for (r = 0; s > r; r += 1) {
        i += Math.pow(e[r] - t[r], 2);
      }return Math.sqrt(i);
    }function normalize(t) {
      return div(t, length(t));
    }function rgbToHsl(t) {
      var e,
          r,
          s = t[0],
          i = t[1],
          a = t[2],
          n = Math.max(s, i, a),
          o = Math.min(s, i, a),
          h = (n + o) / 2;if (n == o) e = r = 0;else {
        var l = n - o;switch (r = h > .5 ? l / (2 - n - o) : l / (n + o), n) {case s:
            e = (i - a) / l + (a > i ? 6 : 0);break;case i:
            e = (a - s) / l + 2;break;case a:
            e = (s - i) / l + 4;}e /= 6;
      }return [e, r, h, t[3]];
    }function hslToRgb(t) {
      function e(t, e, r) {
        return 0 > r && (r += 1), r > 1 && (r -= 1), 1 / 6 > r ? t + 6 * (e - t) * r : .5 > r ? e : 2 / 3 > r ? t + (e - t) * (2 / 3 - r) * 6 : t;
      }var r,
          s,
          i,
          a = t[0],
          n = t[1],
          o = t[2];if (0 == n) r = s = i = o;else {
        var h = .5 > o ? o * (1 + n) : o + n - o * n,
            l = 2 * o - h;r = e(l, h, a + 1 / 3), s = e(l, h, a), i = e(l, h, a - 1 / 3);
      }return [r, s, i, t[3]];
    }function linear(t, e, r, s, i) {
      if (void 0 === s || void 0 === i) return linear(t, 0, 1, e, r);if (e >= t) return s;if (t >= r) return i;var a = r === e ? 0 : (t - e) / (r - e);if (!s.length) return s + (i - s) * a;var n,
          o = s.length,
          h = Array.apply(null, { length: o });for (n = 0; o > n; n += 1) {
        h[n] = s[n] + (i[n] - s[n]) * a;
      }return h;
    }function random(t, e) {
      if (void 0 === e && (void 0 === t ? (t = 0, e = 1) : (e = t, t = void 0)), e.length) {
        var r,
            s = e.length;t || (t = Array.apply(null, { length: s }));var i = Array.apply(null, { length: s }),
            a = BMMath.random();for (r = 0; s > r; r += 1) {
          i[r] = t[r] + a * (e[r] - t[r]);
        }return i;
      }void 0 === t && (t = 0);var n = BMMath.random();return t + n * (e - t);
    }function initiateExpression(elem, data, property) {
      function lookAt(t, e) {
        var r = [e[0] - t[0], e[1] - t[1], e[2] - t[2]],
            s = Math.atan2(r[0], Math.sqrt(r[1] * r[1] + r[2] * r[2])) / degToRads,
            i = -Math.atan2(r[1], r[2]) / degToRads;return [i, s, 0];
      }function easeOut(t, e, r) {
        return -(r - e) * t * (t - 2) + e;
      }function nearestKey(t) {
        var e,
            r,
            s,
            i = data.k.length;if (data.k.length && "number" != typeof data.k[0]) {
          for (r = -1, t *= elem.comp.globalData.frameRate, e = 0; i - 1 > e; e += 1) {
            if (t === data.k[e].t) {
              r = e + 1, s = data.k[e].t;break;
            }if (t > data.k[e].t && t < data.k[e + 1].t) {
              t - data.k[e].t > data.k[e + 1].t - t ? (r = e + 2, s = data.k[e + 1].t) : (r = e + 1, s = data.k[e].t);break;
            }
          }-1 === r && (r = e + 1, s = data.k[e].t);
        } else r = 0, s = 0;var a = {};return a.index = r, a.time = s / elem.comp.globalData.frameRate, a;
      }function key(t) {
        if (!data.k.length || "number" == typeof data.k[0]) return { time: 0 };t -= 1;var e,
            r = { time: data.k[t].t / elem.comp.globalData.frameRate };e = t !== data.k.length - 1 || data.k[t].h ? data.k[t].s : data.k[t - 1].e;var s,
            i = e.length;for (s = 0; i > s; s += 1) {
          r[s] = e[s];
        }return r;
      }function framesToTime(t, e) {
        return e || (e = elem.comp.globalData.frameRate), t / e;
      }function timeToFrames(t, e) {
        return t || (t = time), e || (e = elem.comp.globalData.frameRate), t * e;
      }function toWorld(t) {
        if (toworldMatrix.reset(), elem.finalTransform.mProp.applyToMatrix(toworldMatrix), elem.hierarchy && elem.hierarchy.length) {
          var e,
              r = elem.hierarchy.length;for (e = 0; r > e; e += 1) {
            elem.hierarchy[e].finalTransform.mProp.applyToMatrix(toworldMatrix);
          }return toworldMatrix.applyToPointArray(t[0], t[1], t[2] || 0);
        }return toworldMatrix.applyToPointArray(t[0], t[1], t[2] || 0);
      }function fromWorld(t) {
        fromworldMatrix.reset();var e = [];if (e.push(t), elem.finalTransform.mProp.applyToMatrix(fromworldMatrix), elem.hierarchy && elem.hierarchy.length) {
          var r,
              s = elem.hierarchy.length;for (r = 0; s > r; r += 1) {
            elem.hierarchy[r].finalTransform.mProp.applyToMatrix(fromworldMatrix);
          }return fromworldMatrix.inversePoints(e)[0];
        }return fromworldMatrix.inversePoints(e)[0];
      }function seedRandom(t) {
        BMMath.seedrandom(randSeed + t);
      }function execute() {
        if (_needsRandom && seedRandom(randSeed), this.frameExpressionId !== elem.globalData.frameId || "textSelector" === this.type) {
          if (this.lock) return this.v = duplicatePropertyValue(this.pv, this.mult), !0;"textSelector" === this.type && (textIndex = this.textIndex, textTotal = this.textTotal, selectorValue = this.selectorValue), thisLayer || (thisLayer = elem.layerInterface, thisComp = elem.comp.compInterface), transform || (transform = elem.layerInterface("ADBE Transform Group")), 4 !== elemType || content || (content = thisLayer("ADBE Root Vectors Group")), effect || (effect = thisLayer(4)), hasParent = !(!elem.hierarchy || !elem.hierarchy.length), hasParent && !parent && (parent = elem.hierarchy[elem.hierarchy.length - 1].layerInterface), this.lock = !0, this.getPreValue && this.getPreValue(), value = this.pv, time = this.comp.renderedFrame / this.comp.globalData.frameRate, needsVelocity && (velocity = velocityAtTime(time)), bindedFn(), this.frameExpressionId = elem.globalData.frameId;var t, e;if (this.mult) if ("number" == typeof this.v || this.v instanceof Number || "string" == typeof this.v) this.v *= this.mult;else if (1 === this.v.length) this.v = this.v[0] * this.mult;else for (e = this.v.length, value === this.v && (this.v = 2 === e ? [value[0], value[1]] : [value[0], value[1], value[2]]), t = 0; e > t; t += 1) {
            this.v[t] *= this.mult;
          }if (1 === this.v.length && (this.v = this.v[0]), "number" == typeof this.v || this.v instanceof Number || "string" == typeof this.v) this.lastValue !== this.v && (this.lastValue = this.v, this.mdf = !0);else if (this.v._length) shapesEqual(this.v, this.localShapeCollection.shapes[0]) || (this.mdf = !0, this.localShapeCollection.releaseShapes(), this.localShapeCollection.addShape(shape_pool.clone(this.v)));else for (e = this.v.length, t = 0; e > t; t += 1) {
            this.v[t] !== this.lastValue[t] && (this.lastValue[t] = this.v[t], this.mdf = !0);
          }this.lock = !1;
        }
      }var val = data.x,
          needsVelocity = -1 !== val.indexOf("velocity"),
          _needsRandom = -1 !== val.indexOf("random"),
          elemType = elem.data.ty,
          transform,
          content,
          effect,
          thisComp = elem.comp,
          thisProperty = property;elem.comp.frameDuration = 1 / elem.comp.globalData.frameRate;var inPoint = elem.data.ip / elem.comp.globalData.frameRate,
          outPoint = elem.data.op / elem.comp.globalData.frameRate,
          thisLayer,
          thisComp,
          fn = new Function(),
          fn = eval("[function(){" + val + ";this.v = $bm_rt;}]")[0],
          bindedFn = fn.bind(this),
          numKeys = property.kf ? data.k.length : 0,
          wiggle = function (t, e) {
        var r,
            s,
            i = this.pv.length ? this.pv.length : 1,
            a = Array.apply(null, { len: i });for (s = 0; i > s; s += 1) {
          a[s] = 0;
        }t = 5;var n = Math.floor(time * t);for (r = 0, s = 0; n > r;) {
          for (s = 0; i > s; s += 1) {
            a[s] += -e + 2 * e * BMMath.random();
          }r += 1;
        }var o = time * t,
            h = o - Math.floor(o),
            l = Array.apply({ length: i });for (s = 0; i > s; s += 1) {
          l[s] = this.pv[s] + a[s] + (-e + 2 * e * BMMath.random()) * h;
        }return l;
      }.bind(this),
          loopIn = function (t, e, r) {
        if (!this.k) return this.pv;var s = time * elem.comp.globalData.frameRate,
            i = this.keyframes,
            a = i[0].t;if (s >= a) return this.pv;var n, o;r ? (n = e ? Math.abs(elem.comp.globalData.frameRate * e) : Math.max(0, this.elem.data.op - a), o = a + n) : ((!e || e > i.length - 1) && (e = i.length - 1), o = i[e].t, n = o - a);var h, l, p;if ("pingpong" === t) {
          var m = Math.floor((a - s) / n);if (m % 2 === 0) return this.getValueAtTime(((a - s) % n + a) / this.comp.globalData.frameRate, 0);
        } else {
          if ("offset" === t) {
            var f = this.getValueAtTime(a / this.comp.globalData.frameRate, 0),
                c = this.getValueAtTime(o / this.comp.globalData.frameRate, 0),
                d = this.getValueAtTime((n - (a - s) % n + a) / this.comp.globalData.frameRate, 0),
                u = Math.floor((a - s) / n) + 1;if (this.pv.length) {
              for (p = new Array(f.length), l = p.length, h = 0; l > h; h += 1) {
                p[h] = d[h] - (c[h] - f[h]) * u;
              }return p;
            }return d - (c - f) * u;
          }if ("continue" === t) {
            var y = this.getValueAtTime(a / this.comp.globalData.frameRate, 0),
                g = this.getValueAtTime((a + .001) / this.comp.globalData.frameRate, 0);if (this.pv.length) {
              for (p = new Array(y.length), l = p.length, h = 0; l > h; h += 1) {
                p[h] = y[h] + (y[h] - g[h]) * (a - s) / .001;
              }return p;
            }return y + (y - g) * (a - s) / .001;
          }
        }return this.getValueAtTime((n - (a - s) % n + a) / this.comp.globalData.frameRate, 0);
      }.bind(this),
          loopInDuration = function (t, e) {
        return loopIn(t, e, !0);
      }.bind(this),
          loopOut = function (t, e, r) {
        if (!this.k || !this.keyframes) return this.pv;var s = time * elem.comp.globalData.frameRate,
            i = this.keyframes,
            a = i[i.length - 1].t;if (a >= s) return this.pv;var n, o;r ? (n = e ? Math.abs(a - elem.comp.globalData.frameRate * e) : Math.max(0, a - this.elem.data.ip), o = a - n) : ((!e || e > i.length - 1) && (e = i.length - 1), o = i[i.length - 1 - e].t, n = a - o);var h, l, p;if ("pingpong" === t) {
          var m = Math.floor((s - o) / n);if (m % 2 !== 0) return this.getValueAtTime((n - (s - o) % n + o) / this.comp.globalData.frameRate, 0);
        } else {
          if ("offset" === t) {
            var f = this.getValueAtTime(o / this.comp.globalData.frameRate, 0),
                c = this.getValueAtTime(a / this.comp.globalData.frameRate, 0),
                d = this.getValueAtTime(((s - o) % n + o) / this.comp.globalData.frameRate, 0),
                u = Math.floor((s - o) / n);if (this.pv.length) {
              for (p = new Array(f.length), l = p.length, h = 0; l > h; h += 1) {
                p[h] = (c[h] - f[h]) * u + d[h];
              }return p;
            }return (c - f) * u + d;
          }if ("continue" === t) {
            var y = this.getValueAtTime(a / this.comp.globalData.frameRate, 0),
                g = this.getValueAtTime((a - .001) / this.comp.globalData.frameRate, 0);if (this.pv.length) {
              for (p = new Array(y.length), l = p.length, h = 0; l > h; h += 1) {
                p[h] = y[h] + (y[h] - g[h]) * ((s - a) / this.comp.globalData.frameRate) / 5e-4;
              }return p;
            }return y + (y - g) * ((s - a) / .001);
          }
        }return this.getValueAtTime(((s - o) % n + o) / this.comp.globalData.frameRate, 0);
      }.bind(this),
          loop_out = loopOut,
          loopOutDuration = function (t, e) {
        return loopOut(t, e, !0);
      }.bind(this),
          valueAtTime = function (t) {
        return this.getValueAtTime(t, 0);
      }.bind(this),
          velocityAtTime = function (t) {
        return this.getVelocityAtTime(t);
      }.bind(this),
          comp = elem.comp.globalData.projectInterface.bind(elem.comp.globalData.projectInterface),
          toworldMatrix = new Matrix(),
          fromworldMatrix = new Matrix(),
          time,
          velocity,
          value,
          textIndex,
          textTotal,
          selectorValue,
          index = elem.data.ind,
          hasParent = !(!elem.hierarchy || !elem.hierarchy.length),
          parent,
          randSeed = Math.floor(1e6 * Math.random());return execute;
    }var ob = {},
        Math = BMMath,
        add = sum,
        radians_to_degrees = radiansToDegrees,
        degrees_to_radians = radiansToDegrees,
        helperLengthArray = [0, 0, 0, 0, 0, 0];return ob.initiateExpression = initiateExpression, ob;
  }(),
      ShapeExpressionInterface = function () {
    function t(t, e, r) {
      return d(t, e, r);
    }function e(t, e, r) {
      return y(t, e, r);
    }function r(t, e, r) {
      return g(t, e, r);
    }function s(t, e, r) {
      return v(t, e, r);
    }function i(t, e, r) {
      return b(t, e, r);
    }function a(t, e, r) {
      return E(t, e, r);
    }function n(t, e, r) {
      return P(t, e, r);
    }function o(t, e, r) {
      return x(t, e, r);
    }function h(t, e, r) {
      return S(t, e, r);
    }function l(t, e, r) {
      return C(t, e, r);
    }function p(t, e, r) {
      return k(t, e, r);
    }function m(t, e, r) {
      return M(t, e, r);
    }function f(t, e, r) {
      var s,
          i = [],
          a = t ? t.length : 0;for (s = 0; a > s; s += 1) {
        "gr" == t[s].ty ? i.push(ShapeExpressionInterface.createGroupInterface(t[s], e[s], r)) : "fl" == t[s].ty ? i.push(ShapeExpressionInterface.createFillInterface(t[s], e[s], r)) : "st" == t[s].ty ? i.push(ShapeExpressionInterface.createStrokeInterface(t[s], e[s], r)) : "tm" == t[s].ty ? i.push(ShapeExpressionInterface.createTrimInterface(t[s], e[s], r)) : "tr" == t[s].ty || ("el" == t[s].ty ? i.push(ShapeExpressionInterface.createEllipseInterface(t[s], e[s], r)) : "sr" == t[s].ty ? i.push(ShapeExpressionInterface.createStarInterface(t[s], e[s], r)) : "sh" == t[s].ty ? i.push(ShapeExpressionInterface.createPathInterface(t[s], e[s], r)) : "rc" == t[s].ty ? i.push(ShapeExpressionInterface.createRectInterface(t[s], e[s], r)) : "rd" == t[s].ty ? i.push(ShapeExpressionInterface.createRoundedInterface(t[s], e[s], r)) : "rp" == t[s].ty && i.push(ShapeExpressionInterface.createRepatearInterface(t[s], e[s], r)));
      }return i;
    }var c = { createShapeInterface: t, createGroupInterface: e, createTrimInterface: i, createStrokeInterface: s, createTransformInterface: a, createEllipseInterface: n, createStarInterface: o, createRectInterface: h, createRoundedInterface: l, createRepatearInterface: p, createPathInterface: m, createFillInterface: r },
        d = function () {
      return function (t, e, r) {
        function s(t) {
          if ("number" == typeof t) return i[t - 1];for (var e = 0, r = i.length; r > e;) {
            if (i[e]._name === t) return i[e];e += 1;
          }
        }var i;return s.propertyGroup = r, i = f(t, e, s), s;
      };
    }(),
        u = function () {
      return function (t, e, r) {
        var s,
            i = function i(t) {
          for (var e = 0, r = s.length; r > e;) {
            if (s[e]._name === t || s[e].mn === t || s[e].propertyIndex === t || s[e].ix === t || s[e].ind === t) return s[e];e += 1;
          }return "number" == typeof t ? s[t - 1] : void 0;
        };return i.propertyGroup = function (t) {
          return 1 === t ? i : r(t - 1);
        }, s = f(t.it, e.it, i.propertyGroup), i.numProperties = s.length, i.propertyIndex = t.cix, i;
      };
    }(),
        y = function () {
      return function (t, e, r) {
        var s = function s(t) {
          switch (t) {case "ADBE Vectors Group":case "Contents":case 2:
              return s.content;case "ADBE Vector Transform Group":case 3:default:
              return s.transform;}
        };s.propertyGroup = function (t) {
          return 1 === t ? s : r(t - 1);
        };var i = u(t, e, s.propertyGroup),
            a = ShapeExpressionInterface.createTransformInterface(t.it[t.it.length - 1], e.it[e.it.length - 1], s.propertyGroup);return s.content = i, s.transform = a, Object.defineProperty(s, "_name", { get: function get() {
            return t.nm;
          } }), s.numProperties = t.np, s.propertyIndex = t.ix, s.nm = t.nm, s.mn = t.mn, s;
      };
    }(),
        g = function () {
      return function (t, e, r) {
        function s(t) {
          return "Color" === t || "color" === t ? s.color : "Opacity" === t || "opacity" === t ? s.opacity : void 0;
        }return Object.defineProperty(s, "color", { get: function get() {
            return ExpressionValue(e.c, 1 / e.c.mult, "color");
          } }), Object.defineProperty(s, "opacity", { get: function get() {
            return ExpressionValue(e.o, 100);
          } }), Object.defineProperty(s, "_name", { value: t.nm }), Object.defineProperty(s, "mn", { value: t.mn }), e.c.setGroupProperty(r), e.o.setGroupProperty(r), s;
      };
    }(),
        v = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 === t ? c : r(t - 1);
        }function i(t) {
          return 1 === t ? l : s(t - 1);
        }function a(r) {
          Object.defineProperty(l, t.d[r].nm, { get: function get() {
              return ExpressionValue(e.d.dataProps[r].p);
            } });
        }function n(t) {
          return "Color" === t || "color" === t ? n.color : "Opacity" === t || "opacity" === t ? n.opacity : "Stroke Width" === t || "stroke width" === t ? n.strokeWidth : void 0;
        }var o,
            h = t.d ? t.d.length : 0,
            l = {};for (o = 0; h > o; o += 1) {
          a(o), e.d.dataProps[o].p.setGroupProperty(i);
        }return Object.defineProperty(n, "color", { get: function get() {
            return ExpressionValue(e.c, 1 / e.c.mult, "color");
          } }), Object.defineProperty(n, "opacity", { get: function get() {
            return ExpressionValue(e.o, 100);
          } }), Object.defineProperty(n, "strokeWidth", { get: function get() {
            return ExpressionValue(e.w);
          } }), Object.defineProperty(n, "dash", { get: function get() {
            return l;
          } }), Object.defineProperty(n, "_name", { value: t.nm }), Object.defineProperty(n, "mn", { value: t.mn }), e.c.setGroupProperty(s), e.o.setGroupProperty(s), e.w.setGroupProperty(s), n;
      };
    }(),
        b = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 == t ? i : r(--t);
        }function i(e) {
          return e === t.e.ix || "End" === e || "end" === e ? i.end : e === t.s.ix ? i.start : e === t.o.ix ? i.offset : void 0;
        }return i.propertyIndex = t.ix, e.s.setGroupProperty(s), e.e.setGroupProperty(s), e.o.setGroupProperty(s), i.propertyIndex = t.ix, Object.defineProperty(i, "start", { get: function get() {
            return ExpressionValue(e.s, 1 / e.s.mult);
          } }), Object.defineProperty(i, "end", { get: function get() {
            return ExpressionValue(e.e, 1 / e.e.mult);
          } }), Object.defineProperty(i, "offset", { get: function get() {
            return ExpressionValue(e.o);
          } }), Object.defineProperty(i, "_name", { get: function get() {
            return t.nm;
          } }), i.mn = t.mn, i;
      };
    }(),
        E = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 == t ? i : r(--t);
        }function i(e) {
          return t.a.ix === e ? i.anchorPoint : t.o.ix === e ? i.opacity : t.p.ix === e ? i.position : t.r.ix === e ? i.rotation : t.s.ix === e ? i.scale : t.sk && t.sk.ix === e ? i.skew : t.sa && t.sa.ix === e ? i.skewAxis : "Opacity" === e ? i.opacity : "Position" === e ? i.position : "Anchor Point" === e ? i.anchorPoint : "Scale" === e ? i.scale : "Rotation" === e || "ADBE Vector Rotation" === e ? i.rotation : "Skew" === e ? i.skew : "Skew Axis" === e ? i.skewAxis : void 0;
        }e.transform.mProps.o.setGroupProperty(s), e.transform.mProps.p.setGroupProperty(s), e.transform.mProps.a.setGroupProperty(s), e.transform.mProps.s.setGroupProperty(s), e.transform.mProps.r.setGroupProperty(s), e.transform.mProps.sk && (e.transform.mProps.sk.setGroupProperty(s), e.transform.mProps.sa.setGroupProperty(s)), e.transform.op.setGroupProperty(s), Object.defineProperty(i, "opacity", { get: function get() {
            return ExpressionValue(e.transform.mProps.o, 1 / e.transform.mProps.o.mult);
          } }), Object.defineProperty(i, "position", { get: function get() {
            return ExpressionValue(e.transform.mProps.p);
          } }), Object.defineProperty(i, "anchorPoint", { get: function get() {
            return ExpressionValue(e.transform.mProps.a);
          } });return Object.defineProperty(i, "scale", { get: function get() {
            return ExpressionValue(e.transform.mProps.s, 1 / e.transform.mProps.s.mult);
          } }), Object.defineProperty(i, "rotation", { get: function get() {
            return ExpressionValue(e.transform.mProps.r, 1 / e.transform.mProps.r.mult);
          } }), Object.defineProperty(i, "skew", { get: function get() {
            return ExpressionValue(e.transform.mProps.sk);
          } }), Object.defineProperty(i, "skewAxis", { get: function get() {
            return ExpressionValue(e.transform.mProps.sa);
          } }), Object.defineProperty(i, "_name", { get: function get() {
            return t.nm;
          } }), i.ty = "tr", i.mn = t.mn, i;
      };
    }(),
        P = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 == t ? i : r(--t);
        }function i(e) {
          return t.p.ix === e ? i.position : t.s.ix === e ? i.size : void 0;
        }i.propertyIndex = t.ix;var a = "tm" === e.sh.ty ? e.sh.prop : e.sh;return a.s.setGroupProperty(s), a.p.setGroupProperty(s), Object.defineProperty(i, "size", { get: function get() {
            return ExpressionValue(a.s);
          } }), Object.defineProperty(i, "position", { get: function get() {
            return ExpressionValue(a.p);
          } }), Object.defineProperty(i, "_name", { get: function get() {
            return t.nm;
          } }), i.mn = t.mn, i;
      };
    }(),
        x = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 == t ? i : r(--t);
        }function i(e) {
          return t.p.ix === e ? i.position : t.r.ix === e ? i.rotation : t.pt.ix === e ? i.points : t.or.ix === e || "ADBE Vector Star Outer Radius" === e ? i.outerRadius : t.os.ix === e ? i.outerRoundness : !t.ir || t.ir.ix !== e && "ADBE Vector Star Inner Radius" !== e ? t.is && t.is.ix === e ? i.innerRoundness : void 0 : i.innerRadius;
        }var a = "tm" === e.sh.ty ? e.sh.prop : e.sh;return i.propertyIndex = t.ix, a.or.setGroupProperty(s), a.os.setGroupProperty(s), a.pt.setGroupProperty(s), a.p.setGroupProperty(s), a.r.setGroupProperty(s), t.ir && (a.ir.setGroupProperty(s), a.is.setGroupProperty(s)), Object.defineProperty(i, "position", { get: function get() {
            return ExpressionValue(a.p);
          } }), Object.defineProperty(i, "rotation", { get: function get() {
            return ExpressionValue(a.r, 1 / a.r.mult);
          } }), Object.defineProperty(i, "points", { get: function get() {
            return ExpressionValue(a.pt);
          } }), Object.defineProperty(i, "outerRadius", { get: function get() {
            return ExpressionValue(a.or);
          } }), Object.defineProperty(i, "outerRoundness", { get: function get() {
            return ExpressionValue(a.os);
          } }), Object.defineProperty(i, "innerRadius", { get: function get() {
            return a.ir ? ExpressionValue(a.ir) : 0;
          } }), Object.defineProperty(i, "innerRoundness", { get: function get() {
            return a.is ? ExpressionValue(a.is, 1 / a.is.mult) : 0;
          } }), Object.defineProperty(i, "_name", { get: function get() {
            return t.nm;
          } }), i.mn = t.mn, i;
      };
    }(),
        S = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 == t ? i : r(--t);
        }function i(e) {
          return t.p.ix === e ? i.position : t.r.ix === e ? i.rotation : t.pt.ix === e ? i.points : t.or.ix === e || "ADBE Vector Star Outer Radius" === e ? i.outerRadius : t.os.ix === e ? i.outerRoundness : !t.ir || t.ir.ix !== e && "ADBE Vector Star Inner Radius" !== e ? t.is && t.is.ix === e ? i.innerRoundness : void 0 : i.innerRadius;
        }var a = "tm" === e.sh.ty ? e.sh.prop : e.sh;return i.propertyIndex = t.ix, a.p.setGroupProperty(s), a.s.setGroupProperty(s), a.r.setGroupProperty(s), Object.defineProperty(i, "position", { get: function get() {
            return ExpressionValue(a.p);
          } }), Object.defineProperty(i, "roundness", { get: function get() {
            return ExpressionValue(a.r);
          } }), Object.defineProperty(i, "size", { get: function get() {
            return ExpressionValue(a.s);
          } }), Object.defineProperty(i, "_name", { get: function get() {
            return t.nm;
          } }), i.mn = t.mn, i;
      };
    }(),
        C = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 == t ? i : r(--t);
        }function i(e) {
          return t.r.ix === e || "Round Corners 1" === e ? i.radius : void 0;
        }var a = e;return i.propertyIndex = t.ix, a.rd.setGroupProperty(s), Object.defineProperty(i, "radius", { get: function get() {
            return ExpressionValue(a.rd);
          } }), Object.defineProperty(i, "_name", { get: function get() {
            return t.nm;
          } }), i.mn = t.mn, i;
      };
    }(),
        k = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 == t ? i : r(--t);
        }function i(e) {
          return t.c.ix === e || "Copies" === e ? i.copies : t.o.ix === e || "Offset" === e ? i.offset : void 0;
        }var a = e;return i.propertyIndex = t.ix, a.c.setGroupProperty(s), a.o.setGroupProperty(s), Object.defineProperty(i, "copies", { get: function get() {
            return ExpressionValue(a.c);
          } }), Object.defineProperty(i, "offset", { get: function get() {
            return ExpressionValue(a.o);
          } }), Object.defineProperty(i, "_name", { get: function get() {
            return t.nm;
          } }), i.mn = t.mn, i;
      };
    }(),
        M = function () {
      return function (t, e, r) {
        function s(t) {
          return 1 == t ? i : r(--t);
        }function i(t) {
          return "Shape" === t || "shape" === t || "Path" === t || "path" === t ? i.path : void 0;
        }var a = "tm" === e.sh.ty ? e.sh.prop : e.sh;return a.setGroupProperty(s), Object.defineProperty(i, "path", { get: function get() {
            return a.k && a.getValue(), a.v;
          } }), Object.defineProperty(i, "shape", { get: function get() {
            return a.k && a.getValue(), a.v;
          } }), Object.defineProperty(i, "_name", { value: t.nm }), Object.defineProperty(i, "ix", { value: t.ix }), Object.defineProperty(i, "mn", { value: t.mn }), i;
      };
    }();return c;
  }(),
      TextExpressionInterface = function () {
    return function (t) {
      function e() {}return Object.defineProperty(e, "sourceText", { get: function get() {
          return t.currentTextDocumentData.t ? t.currentTextDocumentData.t : "";
        } }), e;
    };
  }(),
      LayerExpressionInterface = function () {
    function t(t) {
      var e = new Matrix();if (e.reset(), this._elem.finalTransform.mProp.applyToMatrix(e), this._elem.hierarchy && this._elem.hierarchy.length) {
        var r,
            s = this._elem.hierarchy.length;for (r = 0; s > r; r += 1) {
          this._elem.hierarchy[r].finalTransform.mProp.applyToMatrix(e);
        }return e.applyToPointArray(t[0], t[1], t[2] || 0);
      }return e.applyToPointArray(t[0], t[1], t[2] || 0);
    }return function (e) {
      function r(t) {
        i.mask = t.getMask.bind(t);
      }function s(t) {
        i.effect = t;
      }function i(t) {
        switch (t) {case "ADBE Root Vectors Group":case "Contents":case 2:
            return i.shapeInterface;case 1:case "Transform":case "transform":case "ADBE Transform Group":
            return a;case 4:case "ADBE Effect Parade":
            return i.effect;}
      }var a = TransformExpressionInterface(e.transform);return i.toWorld = t, i.toComp = t, i._elem = e, Object.defineProperty(i, "hasParent", { get: function get() {
          return !!e.hierarchy;
        } }), Object.defineProperty(i, "parent", { get: function get() {
          return e.hierarchy[0].layerInterface;
        } }), Object.defineProperty(i, "rotation", { get: function get() {
          return a.rotation;
        } }), Object.defineProperty(i, "scale", { get: function get() {
          return a.scale;
        } }), Object.defineProperty(i, "position", { get: function get() {
          return a.position;
        } }), Object.defineProperty(i, "anchorPoint", { get: function get() {
          return a.anchorPoint;
        } }), Object.defineProperty(i, "transform", { get: function get() {
          return a;
        } }), Object.defineProperty(i, "width", { get: function get() {
          return 0 === e.data.ty ? e.data.w : 100;
        } }), Object.defineProperty(i, "height", { get: function get() {
          return 0 === e.data.ty ? e.data.h : 100;
        } }), Object.defineProperty(i, "source", { get: function get() {
          return e.data.refId;
        } }), Object.defineProperty(i, "_name", { value: e.data.nm }), Object.defineProperty(i, "content", { get: function get() {
          return i.shapeInterface;
        } }), Object.defineProperty(i, "active", { get: function get() {
          return e.isVisible;
        } }), Object.defineProperty(i, "text", { get: function get() {
          return i.textInterface;
        } }), i.registerMaskInterface = r, i.registerEffectsInterface = s, i;
    };
  }(),
      CompExpressionInterface = function () {
    return function (t) {
      function e(e) {
        for (var r = 0, s = t.layers.length; s > r;) {
          if (t.layers[r].nm === e || t.layers[r].ind === e) return t.elements[r].layerInterface;r += 1;
        }return { active: !1 };
      }return Object.defineProperty(e, "_name", { value: t.data.nm }), e.layer = e, e.pixelAspect = 1, e.height = t.globalData.compSize.h, e.width = t.globalData.compSize.w, e.pixelAspect = 1, e.frameDuration = 1 / t.globalData.frameRate, e;
    };
  }(),
      TransformExpressionInterface = function () {
    return function (t) {
      function e(r) {
        switch (r) {case "scale":case "Scale":case "ADBE Scale":
            return e.scale;case "rotation":case "Rotation":case "ADBE Rotation":case "ADBE Rotate Z":
            return e.rotation;case "position":case "Position":case "ADBE Position":
            return t.position;case "anchorPoint":case "AnchorPoint":case "Anchor Point":case "ADBE AnchorPoint":
            return e.anchorPoint;case "opacity":case "Opacity":
            return e.opacity;}
      }return Object.defineProperty(e, "rotation", { get: function get() {
          return t.rotation;
        } }), Object.defineProperty(e, "scale", { get: function get() {
          return t.scale;
        } }), Object.defineProperty(e, "position", { get: function get() {
          return t.position;
        } }), Object.defineProperty(e, "xPosition", { get: function get() {
          return t.xPosition;
        } }), Object.defineProperty(e, "yPosition", { get: function get() {
          return t.yPosition;
        } }), Object.defineProperty(e, "anchorPoint", { get: function get() {
          return t.anchorPoint;
        } }), Object.defineProperty(e, "opacity", { get: function get() {
          return t.opacity;
        } }), Object.defineProperty(e, "skew", { get: function get() {
          return t.skew;
        } }), Object.defineProperty(e, "skewAxis", { get: function get() {
          return t.skewAxis;
        } }), e;
    };
  }(),
      ProjectInterface = function () {
    function t(t) {
      this.compositions.push(t);
    }return function () {
      function e(t) {
        for (var e = 0, r = this.compositions.length; r > e;) {
          if (this.compositions[e].data && this.compositions[e].data.nm === t) return this.compositions[e].prepareFrame(this.currentFrame), this.compositions[e].compInterface;e += 1;
        }
      }return e.compositions = [], e.currentFrame = 0, e.registerComposition = t, e;
    };
  }(),
      EffectsExpressionInterface = function () {
    function t(t, r) {
      if (t.effects) {
        var s,
            i = [],
            a = t.data.ef,
            n = t.effects.effectElements.length;for (s = 0; n > s; s += 1) {
          i.push(e(a[s], t.effects.effectElements[s], r, t));
        }return function (e) {
          for (var r = t.data.ef, s = 0, a = r.length; a > s;) {
            if (e === r[s].nm || e === r[s].mn || e === r[s].ix) return i[s];s += 1;
          }
        };
      }
    }function e(t, s, i, a) {
      var n,
          o = [],
          h = t.ef.length;for (n = 0; h > n; n += 1) {
        o.push(5 === t.ef[n].ty ? e(t.ef[n], s.effectElements[n], i, a) : r(s.effectElements[n], t.ef[n].ty, a));
      }var l = function l(e) {
        for (var r = t.ef, s = 0, i = r.length; i > s;) {
          if (e === r[s].nm || e === r[s].mn || e === r[s].ix) return 5 === r[s].ty ? o[s] : o[s]();s += 1;
        }return o[0]();
      };return "ADBE Color Control" === t.mn && Object.defineProperty(l, "color", { get: function get() {
          return o[0]();
        } }), l.active = 0 !== t.en, l;
    }function r(t, e, r) {
      return function () {
        return 10 === e ? r.comp.compInterface(t.p.v) : ExpressionValue(t.p);
      };
    }var s = { createEffectsInterface: t };return s;
  }(),
      ExpressionValue = function () {
    return function (t, e, r) {
      var s;t.k && t.getValue();var i, a, n;if (r) {
        if ("color" === r) {
          for (a = 4, s = Array.apply(null, { length: a }), n = Array.apply(null, { length: a }), i = 0; a > i; i += 1) {
            s[i] = n[i] = e && 3 > i ? t.v[i] * e : 1;
          }s.value = n;
        }
      } else if ("number" == typeof t.v || t.v instanceof Number) s = new Number(e ? t.v * e : t.v), s.value = e ? t.v * e : t.v;else {
        for (a = t.v.length, s = Array.apply(null, { length: a }), n = Array.apply(null, { length: a }), i = 0; a > i; i += 1) {
          s[i] = n[i] = e ? t.v[i] * e : t.v[i];
        }s.value = n;
      }return s.numKeys = t.keyframes ? t.keyframes.length : 0, s.key = function (e) {
        return s.numKeys ? t.keyframes[e - 1].t : 0;
      }, s.valueAtTime = t.getValueAtTime, s.propertyGroup = t.propertyGroup, s;
    };
  }();GroupEffect.prototype.getValue = function () {
    this.mdf = !1;var t,
        e = this.dynamicProperties.length;for (t = 0; e > t; t += 1) {
      this.dynamicProperties[t].getValue(), this.mdf = this.dynamicProperties[t].mdf ? !0 : this.mdf;
    }
  }, GroupEffect.prototype.init = function (t, e, r) {
    this.data = t, this.mdf = !1, this.effectElements = [];var s,
        i,
        a = this.data.ef.length,
        n = this.data.ef;for (s = 0; a > s; s += 1) {
      switch (n[s].ty) {case 0:
          i = new SliderEffect(n[s], e, r), this.effectElements.push(i);break;case 1:
          i = new AngleEffect(n[s], e, r), this.effectElements.push(i);break;case 2:
          i = new ColorEffect(n[s], e, r), this.effectElements.push(i);break;case 3:
          i = new PointEffect(n[s], e, r), this.effectElements.push(i);break;case 4:case 7:
          i = new CheckboxEffect(n[s], e, r), this.effectElements.push(i);break;case 10:
          i = new LayerIndexEffect(n[s], e, r), this.effectElements.push(i);break;case 11:
          i = new MaskIndexEffect(n[s], e, r), this.effectElements.push(i);break;case 5:
          i = new EffectsManager(n[s], e, r), this.effectElements.push(i);break;case 6:
          i = new NoValueEffect(n[s], e, r), this.effectElements.push(i);}
    }
  };var bodymovinjs = {};bodymovinjs.play = play, bodymovinjs.pause = pause, bodymovinjs.togglePause = togglePause, bodymovinjs.setSpeed = setSpeed, bodymovinjs.setDirection = setDirection, bodymovinjs.stop = stop, bodymovinjs.moveFrame = moveFrame, bodymovinjs.searchAnimations = searchAnimations, bodymovinjs.registerAnimation = registerAnimation, bodymovinjs.loadAnimation = loadAnimation, bodymovinjs.setSubframeRendering = setSubframeRendering, bodymovinjs.resize = resize, bodymovinjs.start = start, bodymovinjs.goToAndStop = goToAndStop, bodymovinjs.destroy = destroy, bodymovinjs.setQuality = setQuality, bodymovinjs.installPlugin = installPlugin, bodymovinjs.__getFactory = getFactory, bodymovinjs.version = "4.6.3";var standalone = "__[STANDALONE]__",
      animationData = "__[ANIMATIONDATA]__",
      renderer = "";if (standalone) {
    var scripts = document.getElementsByTagName("script"),
        index = scripts.length - 1,
        myScript = scripts[index],
        queryString = myScript.src.replace(/^[^\?]+\??/, "");renderer = getQueryVariable("renderer");
  }var readyStateCheckInterval = setInterval(checkReady, 100);return bodymovinjs;
});

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * Bootstrap-select v1.12.2 (http://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2017 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */
!function (a, b) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(0)], __WEBPACK_AMD_DEFINE_RESULT__ = function (a) {
    return b(a);
  }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = b(require("jquery")) : b(a.jQuery);
}(this, function (a) {
  !function (a) {
    "use strict";
    function b(b) {
      var c = [{ re: /[\xC0-\xC6]/g, ch: "A" }, { re: /[\xE0-\xE6]/g, ch: "a" }, { re: /[\xC8-\xCB]/g, ch: "E" }, { re: /[\xE8-\xEB]/g, ch: "e" }, { re: /[\xCC-\xCF]/g, ch: "I" }, { re: /[\xEC-\xEF]/g, ch: "i" }, { re: /[\xD2-\xD6]/g, ch: "O" }, { re: /[\xF2-\xF6]/g, ch: "o" }, { re: /[\xD9-\xDC]/g, ch: "U" }, { re: /[\xF9-\xFC]/g, ch: "u" }, { re: /[\xC7-\xE7]/g, ch: "c" }, { re: /[\xD1]/g, ch: "N" }, { re: /[\xF1]/g, ch: "n" }];return a.each(c, function () {
        b = b ? b.replace(this.re, this.ch) : "";
      }), b;
    }function c(b) {
      var c = arguments,
          d = b;[].shift.apply(c);var e,
          f = this.each(function () {
        var b = a(this);if (b.is("select")) {
          var f = b.data("selectpicker"),
              g = "object" == (typeof d === "undefined" ? "undefined" : _typeof(d)) && d;if (f) {
            if (g) for (var h in g) {
              g.hasOwnProperty(h) && (f.options[h] = g[h]);
            }
          } else {
            var i = a.extend({}, k.DEFAULTS, a.fn.selectpicker.defaults || {}, b.data(), g);i.template = a.extend({}, k.DEFAULTS.template, a.fn.selectpicker.defaults ? a.fn.selectpicker.defaults.template : {}, b.data().template, g.template), b.data("selectpicker", f = new k(this, i));
          }"string" == typeof d && (e = f[d] instanceof Function ? f[d].apply(f, c) : f.options[d]);
        }
      });return "undefined" != typeof e ? e : f;
    }String.prototype.includes || !function () {
      var a = {}.toString,
          b = function () {
        try {
          var a = {},
              b = Object.defineProperty,
              c = b(a, a, a) && b;
        } catch (a) {}return c;
      }(),
          c = "".indexOf,
          d = function d(b) {
        if (null == this) throw new TypeError();var d = String(this);if (b && "[object RegExp]" == a.call(b)) throw new TypeError();var e = d.length,
            f = String(b),
            g = f.length,
            h = arguments.length > 1 ? arguments[1] : void 0,
            i = h ? Number(h) : 0;i != i && (i = 0);var j = Math.min(Math.max(i, 0), e);return !(g + j > e) && c.call(d, f, i) != -1;
      };b ? b(String.prototype, "includes", { value: d, configurable: !0, writable: !0 }) : String.prototype.includes = d;
    }(), String.prototype.startsWith || !function () {
      var a = function () {
        try {
          var a = {},
              b = Object.defineProperty,
              c = b(a, a, a) && b;
        } catch (a) {}return c;
      }(),
          b = {}.toString,
          c = function c(a) {
        if (null == this) throw new TypeError();var c = String(this);if (a && "[object RegExp]" == b.call(a)) throw new TypeError();var d = c.length,
            e = String(a),
            f = e.length,
            g = arguments.length > 1 ? arguments[1] : void 0,
            h = g ? Number(g) : 0;h != h && (h = 0);var i = Math.min(Math.max(h, 0), d);if (f + i > d) return !1;for (var j = -1; ++j < f;) {
          if (c.charCodeAt(i + j) != e.charCodeAt(j)) return !1;
        }return !0;
      };a ? a(String.prototype, "startsWith", { value: c, configurable: !0, writable: !0 }) : String.prototype.startsWith = c;
    }(), Object.keys || (Object.keys = function (a, b, c) {
      c = [];for (b in a) {
        c.hasOwnProperty.call(a, b) && c.push(b);
      }return c;
    });var d = { useDefault: !1, _set: a.valHooks.select.set };a.valHooks.select.set = function (b, c) {
      return c && !d.useDefault && a(b).data("selected", !0), d._set.apply(this, arguments);
    };var e = null;a.fn.triggerNative = function (a) {
      var b,
          c = this[0];c.dispatchEvent ? ("function" == typeof Event ? b = new Event(a, { bubbles: !0 }) : (b = document.createEvent("Event"), b.initEvent(a, !0, !1)), c.dispatchEvent(b)) : c.fireEvent ? (b = document.createEventObject(), b.eventType = a, c.fireEvent("on" + a, b)) : this.trigger(a);
    }, a.expr.pseudos.icontains = function (b, c, d) {
      var e = a(b),
          f = (e.data("tokens") || e.text()).toString().toUpperCase();return f.includes(d[3].toUpperCase());
    }, a.expr.pseudos.ibegins = function (b, c, d) {
      var e = a(b),
          f = (e.data("tokens") || e.text()).toString().toUpperCase();return f.startsWith(d[3].toUpperCase());
    }, a.expr.pseudos.aicontains = function (b, c, d) {
      var e = a(b),
          f = (e.data("tokens") || e.data("normalizedText") || e.text()).toString().toUpperCase();return f.includes(d[3].toUpperCase());
    }, a.expr.pseudos.aibegins = function (b, c, d) {
      var e = a(b),
          f = (e.data("tokens") || e.data("normalizedText") || e.text()).toString().toUpperCase();return f.startsWith(d[3].toUpperCase());
    };var f = { "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#x27;", "`": "&#x60;" },
        g = { "&amp;": "&", "&lt;": "<", "&gt;": ">", "&quot;": '"', "&#x27;": "'", "&#x60;": "`" },
        h = function h(a) {
      var b = function b(_b) {
        return a[_b];
      },
          c = "(?:" + Object.keys(a).join("|") + ")",
          d = RegExp(c),
          e = RegExp(c, "g");return function (a) {
        return a = null == a ? "" : "" + a, d.test(a) ? a.replace(e, b) : a;
      };
    },
        i = h(f),
        j = h(g),
        k = function k(b, c) {
      d.useDefault || (a.valHooks.select.set = d._set, d.useDefault = !0), this.$element = a(b), this.$newElement = null, this.$button = null, this.$menu = null, this.$lis = null, this.options = c, null === this.options.title && (this.options.title = this.$element.attr("title"));var e = this.options.windowPadding;"number" == typeof e && (this.options.windowPadding = [e, e, e, e]), this.val = k.prototype.val, this.render = k.prototype.render, this.refresh = k.prototype.refresh, this.setStyle = k.prototype.setStyle, this.selectAll = k.prototype.selectAll, this.deselectAll = k.prototype.deselectAll, this.destroy = k.prototype.destroy, this.remove = k.prototype.remove, this.show = k.prototype.show, this.hide = k.prototype.hide, this.init();
    };k.VERSION = "1.12.2", k.DEFAULTS = { noneSelectedText: "Nothing selected", noneResultsText: "No results matched {0}", countSelectedText: function countSelectedText(a, b) {
        return 1 == a ? "{0} item selected" : "{0} items selected";
      }, maxOptionsText: function maxOptionsText(a, b) {
        return [1 == a ? "Limit reached ({n} item max)" : "Limit reached ({n} items max)", 1 == b ? "Group limit reached ({n} item max)" : "Group limit reached ({n} items max)"];
      }, selectAllText: "Select All", deselectAllText: "Deselect All", doneButton: !1, doneButtonText: "Close", multipleSeparator: ", ", styleBase: "btn", style: "btn-default", size: "auto", title: null, selectedTextFormat: "values", width: !1, container: !1, hideDisabled: !1, showSubtext: !1, showIcon: !0, showContent: !0, dropupAuto: !0, header: !1, liveSearch: !1, liveSearchPlaceholder: null, liveSearchNormalize: !1, liveSearchStyle: "contains", actionsBox: !1, iconBase: "glyphicon", tickIcon: "glyphicon-ok", showTick: !1, template: { caret: '<span class="caret"></span>' }, maxOptions: !1, mobile: !1, selectOnTab: !1, dropdownAlignRight: !1, windowPadding: 0 }, k.prototype = { constructor: k, init: function init() {
        var b = this,
            c = this.$element.attr("id");this.$element.addClass("bs-select-hidden"), this.liObj = {}, this.multiple = this.$element.prop("multiple"), this.autofocus = this.$element.prop("autofocus"), this.$newElement = this.createView(), this.$element.after(this.$newElement).appendTo(this.$newElement), this.$button = this.$newElement.children("button"), this.$menu = this.$newElement.children(".dropdown-menu"), this.$menuInner = this.$menu.children(".inner"), this.$searchbox = this.$menu.find("input"), this.$element.removeClass("bs-select-hidden"), this.options.dropdownAlignRight === !0 && this.$menu.addClass("dropdown-menu-right"), "undefined" != typeof c && (this.$button.attr("data-id", c), a('label[for="' + c + '"]').click(function (a) {
          a.preventDefault(), b.$button.focus();
        })), this.checkDisabled(), this.clickListener(), this.options.liveSearch && this.liveSearchListener(), this.render(), this.setStyle(), this.setWidth(), this.options.container && this.selectPosition(), this.$menu.data("this", this), this.$newElement.data("this", this), this.options.mobile && this.mobile(), this.$newElement.on({ "hide.bs.dropdown": function hideBsDropdown(a) {
            b.$menuInner.attr("aria-expanded", !1), b.$element.trigger("hide.bs.select", a);
          }, "hidden.bs.dropdown": function hiddenBsDropdown(a) {
            b.$element.trigger("hidden.bs.select", a);
          }, "show.bs.dropdown": function showBsDropdown(a) {
            b.$menuInner.attr("aria-expanded", !0), b.$element.trigger("show.bs.select", a);
          }, "shown.bs.dropdown": function shownBsDropdown(a) {
            b.$element.trigger("shown.bs.select", a);
          } }), b.$element[0].hasAttribute("required") && this.$element.on("invalid", function () {
          b.$button.addClass("bs-invalid").focus(), b.$element.on({ "focus.bs.select": function focusBsSelect() {
              b.$button.focus(), b.$element.off("focus.bs.select");
            }, "shown.bs.select": function shownBsSelect() {
              b.$element.val(b.$element.val()).off("shown.bs.select");
            }, "rendered.bs.select": function renderedBsSelect() {
              this.validity.valid && b.$button.removeClass("bs-invalid"), b.$element.off("rendered.bs.select");
            } });
        }), setTimeout(function () {
          b.$element.trigger("loaded.bs.select");
        });
      }, createDropdown: function createDropdown() {
        var b = this.multiple || this.options.showTick ? " show-tick" : "",
            c = this.$element.parent().hasClass("input-group") ? " input-group-btn" : "",
            d = this.autofocus ? " autofocus" : "",
            e = this.options.header ? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + "</div>" : "",
            f = this.options.liveSearch ? '<div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off"' + (null === this.options.liveSearchPlaceholder ? "" : ' placeholder="' + i(this.options.liveSearchPlaceholder) + '"') + ' role="textbox" aria-label="Search"></div>' : "",
            g = this.multiple && this.options.actionsBox ? '<div class="bs-actionsbox"><div class="btn-group btn-group-sm btn-block"><button type="button" class="actions-btn bs-select-all btn btn-default">' + this.options.selectAllText + '</button><button type="button" class="actions-btn bs-deselect-all btn btn-default">' + this.options.deselectAllText + "</button></div></div>" : "",
            h = this.multiple && this.options.doneButton ? '<div class="bs-donebutton"><div class="btn-group btn-block"><button type="button" class="btn btn-sm btn-default">' + this.options.doneButtonText + "</button></div></div>" : "",
            j = '<div class="btn-group bootstrap-select' + b + c + '"><button type="button" class="' + this.options.styleBase + ' dropdown-toggle" data-toggle="dropdown"' + d + ' role="button"><span class="filter-option pull-left"></span>&nbsp;<span class="bs-caret">' + this.options.template.caret + '</span></button><div class="dropdown-menu open" role="combobox">' + e + f + g + '<ul class="dropdown-menu inner" role="listbox" aria-expanded="false"></ul>' + h + "</div></div>";return a(j);
      }, createView: function createView() {
        var a = this.createDropdown(),
            b = this.createLi();return a.find("ul")[0].innerHTML = b, a;
      }, reloadLi: function reloadLi() {
        var a = this.createLi();this.$menuInner[0].innerHTML = a;
      }, createLi: function createLi() {
        var c = this,
            d = [],
            e = 0,
            f = document.createElement("option"),
            g = -1,
            h = function h(a, b, c, d) {
          return "<li" + ("undefined" != typeof c & "" !== c ? ' class="' + c + '"' : "") + ("undefined" != typeof b & null !== b ? ' data-original-index="' + b + '"' : "") + ("undefined" != typeof d & null !== d ? 'data-optgroup="' + d + '"' : "") + ">" + a + "</li>";
        },
            j = function j(d, e, f, g) {
          return '<a tabindex="0"' + ("undefined" != typeof e ? ' class="' + e + '"' : "") + (f ? ' style="' + f + '"' : "") + (c.options.liveSearchNormalize ? ' data-normalized-text="' + b(i(a(d).html())) + '"' : "") + ("undefined" != typeof g || null !== g ? ' data-tokens="' + g + '"' : "") + ' role="option">' + d + '<span class="' + c.options.iconBase + " " + c.options.tickIcon + ' check-mark"></span></a>';
        };if (this.options.title && !this.multiple && (g--, !this.$element.find(".bs-title-option").length)) {
          var k = this.$element[0];f.className = "bs-title-option", f.innerHTML = this.options.title, f.value = "", k.insertBefore(f, k.firstChild);var l = a(k.options[k.selectedIndex]);void 0 === l.attr("selected") && void 0 === this.$element.data("selected") && (f.selected = !0);
        }return this.$element.find("option").each(function (b) {
          var f = a(this);if (g++, !f.hasClass("bs-title-option")) {
            var k = this.className || "",
                l = this.style.cssText,
                m = f.data("content") ? f.data("content") : f.html(),
                n = f.data("tokens") ? f.data("tokens") : null,
                o = "undefined" != typeof f.data("subtext") ? '<small class="text-muted">' + f.data("subtext") + "</small>" : "",
                p = "undefined" != typeof f.data("icon") ? '<span class="' + c.options.iconBase + " " + f.data("icon") + '"></span> ' : "",
                q = f.parent(),
                r = "OPTGROUP" === q[0].tagName,
                s = r && q[0].disabled,
                t = this.disabled || s;if ("" !== p && t && (p = "<span>" + p + "</span>"), c.options.hideDisabled && (t && !r || s)) return void g--;if (f.data("content") || (m = p + '<span class="text">' + m + o + "</span>"), r && f.data("divider") !== !0) {
              if (c.options.hideDisabled && t) {
                if (void 0 === q.data("allOptionsDisabled")) {
                  var u = q.children();q.data("allOptionsDisabled", u.filter(":disabled").length === u.length);
                }if (q.data("allOptionsDisabled")) return void g--;
              }var v = " " + q[0].className || "";if (0 === f.index()) {
                e += 1;var w = q[0].label,
                    x = "undefined" != typeof q.data("subtext") ? '<small class="text-muted">' + q.data("subtext") + "</small>" : "",
                    y = q.data("icon") ? '<span class="' + c.options.iconBase + " " + q.data("icon") + '"></span> ' : "";w = y + '<span class="text">' + i(w) + x + "</span>", 0 !== b && d.length > 0 && (g++, d.push(h("", null, "divider", e + "div"))), g++, d.push(h(w, null, "dropdown-header" + v, e));
              }if (c.options.hideDisabled && t) return void g--;d.push(h(j(m, "opt " + k + v, l, n), b, "", e));
            } else if (f.data("divider") === !0) d.push(h("", b, "divider"));else if (f.data("hidden") === !0) d.push(h(j(m, k, l, n), b, "hidden is-hidden"));else {
              var z = this.previousElementSibling && "OPTGROUP" === this.previousElementSibling.tagName;if (!z && c.options.hideDisabled) for (var A = a(this).prevAll(), B = 0; B < A.length; B++) {
                if ("OPTGROUP" === A[B].tagName) {
                  for (var C = 0, D = 0; D < B; D++) {
                    var E = A[D];(E.disabled || a(E).data("hidden") === !0) && C++;
                  }C === B && (z = !0);break;
                }
              }z && (g++, d.push(h("", null, "divider", e + "div"))), d.push(h(j(m, k, l, n), b));
            }c.liObj[b] = g;
          }
        }), this.multiple || 0 !== this.$element.find("option:selected").length || this.options.title || this.$element.find("option").eq(0).prop("selected", !0).attr("selected", "selected"), d.join("");
      }, findLis: function findLis() {
        return null == this.$lis && (this.$lis = this.$menu.find("li")), this.$lis;
      }, render: function render(b) {
        var c,
            d = this;b !== !1 && this.$element.find("option").each(function (a) {
          var b = d.findLis().eq(d.liObj[a]);d.setDisabled(a, this.disabled || "OPTGROUP" === this.parentNode.tagName && this.parentNode.disabled, b), d.setSelected(a, this.selected, b);
        }), this.togglePlaceholder(), this.tabIndex();var e = this.$element.find("option").map(function () {
          if (this.selected) {
            if (d.options.hideDisabled && (this.disabled || "OPTGROUP" === this.parentNode.tagName && this.parentNode.disabled)) return;var b,
                c = a(this),
                e = c.data("icon") && d.options.showIcon ? '<i class="' + d.options.iconBase + " " + c.data("icon") + '"></i> ' : "";return b = d.options.showSubtext && c.data("subtext") && !d.multiple ? ' <small class="text-muted">' + c.data("subtext") + "</small>" : "", "undefined" != typeof c.attr("title") ? c.attr("title") : c.data("content") && d.options.showContent ? c.data("content").toString() : e + c.html() + b;
          }
        }).toArray(),
            f = this.multiple ? e.join(this.options.multipleSeparator) : e[0];if (this.multiple && this.options.selectedTextFormat.indexOf("count") > -1) {
          var g = this.options.selectedTextFormat.split(">");if (g.length > 1 && e.length > g[1] || 1 == g.length && e.length >= 2) {
            c = this.options.hideDisabled ? ", [disabled]" : "";var h = this.$element.find("option").not('[data-divider="true"], [data-hidden="true"]' + c).length,
                i = "function" == typeof this.options.countSelectedText ? this.options.countSelectedText(e.length, h) : this.options.countSelectedText;f = i.replace("{0}", e.length.toString()).replace("{1}", h.toString());
          }
        }void 0 == this.options.title && (this.options.title = this.$element.attr("title")), "static" == this.options.selectedTextFormat && (f = this.options.title), f || (f = "undefined" != typeof this.options.title ? this.options.title : this.options.noneSelectedText), this.$button.attr("title", j(a.trim(f.replace(/<[^>]*>?/g, "")))), this.$button.children(".filter-option").html(f), this.$element.trigger("rendered.bs.select");
      }, setStyle: function setStyle(a, b) {
        this.$element.attr("class") && this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi, ""));var c = a ? a : this.options.style;"add" == b ? this.$button.addClass(c) : "remove" == b ? this.$button.removeClass(c) : (this.$button.removeClass(this.options.style), this.$button.addClass(c));
      }, liHeight: function liHeight(b) {
        if (b || this.options.size !== !1 && !this.sizeInfo) {
          var c = document.createElement("div"),
              d = document.createElement("div"),
              e = document.createElement("ul"),
              f = document.createElement("li"),
              g = document.createElement("li"),
              h = document.createElement("a"),
              i = document.createElement("span"),
              j = this.options.header && this.$menu.find(".popover-title").length > 0 ? this.$menu.find(".popover-title")[0].cloneNode(!0) : null,
              k = this.options.liveSearch ? document.createElement("div") : null,
              l = this.options.actionsBox && this.multiple && this.$menu.find(".bs-actionsbox").length > 0 ? this.$menu.find(".bs-actionsbox")[0].cloneNode(!0) : null,
              m = this.options.doneButton && this.multiple && this.$menu.find(".bs-donebutton").length > 0 ? this.$menu.find(".bs-donebutton")[0].cloneNode(!0) : null;if (i.className = "text", c.className = this.$menu[0].parentNode.className + " open", d.className = "dropdown-menu open", e.className = "dropdown-menu inner", f.className = "divider", i.appendChild(document.createTextNode("Inner text")), h.appendChild(i), g.appendChild(h), e.appendChild(g), e.appendChild(f), j && d.appendChild(j), k) {
            var n = document.createElement("input");k.className = "bs-searchbox", n.className = "form-control", k.appendChild(n), d.appendChild(k);
          }l && d.appendChild(l), d.appendChild(e), m && d.appendChild(m), c.appendChild(d), document.body.appendChild(c);var o = h.offsetHeight,
              p = j ? j.offsetHeight : 0,
              q = k ? k.offsetHeight : 0,
              r = l ? l.offsetHeight : 0,
              s = m ? m.offsetHeight : 0,
              t = a(f).outerHeight(!0),
              u = "function" == typeof getComputedStyle && getComputedStyle(d),
              v = u ? null : a(d),
              w = { vert: parseInt(u ? u.paddingTop : v.css("paddingTop")) + parseInt(u ? u.paddingBottom : v.css("paddingBottom")) + parseInt(u ? u.borderTopWidth : v.css("borderTopWidth")) + parseInt(u ? u.borderBottomWidth : v.css("borderBottomWidth")), horiz: parseInt(u ? u.paddingLeft : v.css("paddingLeft")) + parseInt(u ? u.paddingRight : v.css("paddingRight")) + parseInt(u ? u.borderLeftWidth : v.css("borderLeftWidth")) + parseInt(u ? u.borderRightWidth : v.css("borderRightWidth")) },
              x = { vert: w.vert + parseInt(u ? u.marginTop : v.css("marginTop")) + parseInt(u ? u.marginBottom : v.css("marginBottom")) + 2, horiz: w.horiz + parseInt(u ? u.marginLeft : v.css("marginLeft")) + parseInt(u ? u.marginRight : v.css("marginRight")) + 2 };document.body.removeChild(c), this.sizeInfo = { liHeight: o, headerHeight: p, searchHeight: q, actionsHeight: r, doneButtonHeight: s, dividerHeight: t, menuPadding: w, menuExtras: x };
        }
      }, setSize: function setSize() {
        if (this.findLis(), this.liHeight(), this.options.header && this.$menu.css("padding-top", 0), this.options.size !== !1) {
          var b,
              c,
              d,
              e,
              f,
              g,
              h,
              i,
              j = this,
              k = this.$menu,
              l = this.$menuInner,
              m = a(window),
              n = this.$newElement[0].offsetHeight,
              o = this.$newElement[0].offsetWidth,
              p = this.sizeInfo.liHeight,
              q = this.sizeInfo.headerHeight,
              r = this.sizeInfo.searchHeight,
              s = this.sizeInfo.actionsHeight,
              t = this.sizeInfo.doneButtonHeight,
              u = this.sizeInfo.dividerHeight,
              v = this.sizeInfo.menuPadding,
              w = this.sizeInfo.menuExtras,
              x = this.options.hideDisabled ? ".disabled" : "",
              y = function y() {
            var b,
                c = j.$newElement.offset(),
                d = a(j.options.container);j.options.container && !d.is("body") ? (b = d.offset(), b.top += parseInt(d.css("borderTopWidth")), b.left += parseInt(d.css("borderLeftWidth"))) : b = { top: 0, left: 0 };var e = j.options.windowPadding;f = c.top - b.top - m.scrollTop(), g = m.height() - f - n - b.top - e[2], h = c.left - b.left - m.scrollLeft(), i = m.width() - h - o - b.left - e[1], f -= e[0], h -= e[3];
          };if (y(), "auto" === this.options.size) {
            var z = function z() {
              var m,
                  n = function n(b, c) {
                return function (d) {
                  return c ? d.classList ? d.classList.contains(b) : a(d).hasClass(b) : !(d.classList ? d.classList.contains(b) : a(d).hasClass(b));
                };
              },
                  u = j.$menuInner[0].getElementsByTagName("li"),
                  x = Array.prototype.filter ? Array.prototype.filter.call(u, n("hidden", !1)) : j.$lis.not(".hidden"),
                  z = Array.prototype.filter ? Array.prototype.filter.call(x, n("dropdown-header", !0)) : x.filter(".dropdown-header");y(), b = g - w.vert, c = i - w.horiz, j.options.container ? (k.data("height") || k.data("height", k.height()), d = k.data("height"), k.data("width") || k.data("width", k.width()), e = k.data("width")) : (d = k.height(), e = k.width()), j.options.dropupAuto && j.$newElement.toggleClass("dropup", f > g && b - w.vert < d), j.$newElement.hasClass("dropup") && (b = f - w.vert), "auto" === j.options.dropdownAlignRight && k.toggleClass("dropdown-menu-right", h > i && c - w.horiz < e - o), m = x.length + z.length > 3 ? 3 * p + w.vert - 2 : 0, k.css({ "max-height": b + "px", overflow: "hidden", "min-height": m + q + r + s + t + "px" }), l.css({ "max-height": b - q - r - s - t - v.vert + "px", "overflow-y": "auto", "min-height": Math.max(m - v.vert, 0) + "px" });
            };z(), this.$searchbox.off("input.getSize propertychange.getSize").on("input.getSize propertychange.getSize", z), m.off("resize.getSize scroll.getSize").on("resize.getSize scroll.getSize", z);
          } else if (this.options.size && "auto" != this.options.size && this.$lis.not(x).length > this.options.size) {
            var A = this.$lis.not(".divider").not(x).children().slice(0, this.options.size).last().parent().index(),
                B = this.$lis.slice(0, A + 1).filter(".divider").length;b = p * this.options.size + B * u + v.vert, j.options.container ? (k.data("height") || k.data("height", k.height()), d = k.data("height")) : d = k.height(), j.options.dropupAuto && this.$newElement.toggleClass("dropup", f > g && b - w.vert < d), k.css({ "max-height": b + q + r + s + t + "px", overflow: "hidden", "min-height": "" }), l.css({ "max-height": b - v.vert + "px", "overflow-y": "auto", "min-height": "" });
          }
        }
      }, setWidth: function setWidth() {
        if ("auto" === this.options.width) {
          this.$menu.css("min-width", "0");var a = this.$menu.parent().clone().appendTo("body"),
              b = this.options.container ? this.$newElement.clone().appendTo("body") : a,
              c = a.children(".dropdown-menu").outerWidth(),
              d = b.css("width", "auto").children("button").outerWidth();a.remove(), b.remove(), this.$newElement.css("width", Math.max(c, d) + "px");
        } else "fit" === this.options.width ? (this.$menu.css("min-width", ""), this.$newElement.css("width", "").addClass("fit-width")) : this.options.width ? (this.$menu.css("min-width", ""), this.$newElement.css("width", this.options.width)) : (this.$menu.css("min-width", ""), this.$newElement.css("width", ""));this.$newElement.hasClass("fit-width") && "fit" !== this.options.width && this.$newElement.removeClass("fit-width");
      }, selectPosition: function selectPosition() {
        this.$bsContainer = a('<div class="bs-container" />');var b,
            c,
            d,
            e = this,
            f = a(this.options.container),
            g = function g(a) {
          e.$bsContainer.addClass(a.attr("class").replace(/form-control|fit-width/gi, "")).toggleClass("dropup", a.hasClass("dropup")), b = a.offset(), f.is("body") ? c = { top: 0, left: 0 } : (c = f.offset(), c.top += parseInt(f.css("borderTopWidth")) - f.scrollTop(), c.left += parseInt(f.css("borderLeftWidth")) - f.scrollLeft()), d = a.hasClass("dropup") ? 0 : a[0].offsetHeight, e.$bsContainer.css({ top: b.top - c.top + d, left: b.left - c.left, width: a[0].offsetWidth });
        };this.$button.on("click", function () {
          var b = a(this);e.isDisabled() || (g(e.$newElement), e.$bsContainer.appendTo(e.options.container).toggleClass("open", !b.hasClass("open")).append(e.$menu));
        }), a(window).on("resize scroll", function () {
          g(e.$newElement);
        }), this.$element.on("hide.bs.select", function () {
          e.$menu.data("height", e.$menu.height()), e.$bsContainer.detach();
        });
      }, setSelected: function setSelected(a, b, c) {
        c || (this.togglePlaceholder(), c = this.findLis().eq(this.liObj[a])), c.toggleClass("selected", b).find("a").attr("aria-selected", b);
      }, setDisabled: function setDisabled(a, b, c) {
        c || (c = this.findLis().eq(this.liObj[a])), b ? c.addClass("disabled").children("a").attr("href", "#").attr("tabindex", -1).attr("aria-disabled", !0) : c.removeClass("disabled").children("a").removeAttr("href").attr("tabindex", 0).attr("aria-disabled", !1);
      }, isDisabled: function isDisabled() {
        return this.$element[0].disabled;
      }, checkDisabled: function checkDisabled() {
        var a = this;this.isDisabled() ? (this.$newElement.addClass("disabled"), this.$button.addClass("disabled").attr("tabindex", -1).attr("aria-disabled", !0)) : (this.$button.hasClass("disabled") && (this.$newElement.removeClass("disabled"), this.$button.removeClass("disabled").attr("aria-disabled", !1)), this.$button.attr("tabindex") != -1 || this.$element.data("tabindex") || this.$button.removeAttr("tabindex")), this.$button.click(function () {
          return !a.isDisabled();
        });
      }, togglePlaceholder: function togglePlaceholder() {
        var a = this.$element.val();this.$button.toggleClass("bs-placeholder", null === a || "" === a || a.constructor === Array && 0 === a.length);
      }, tabIndex: function tabIndex() {
        this.$element.data("tabindex") !== this.$element.attr("tabindex") && this.$element.attr("tabindex") !== -98 && "-98" !== this.$element.attr("tabindex") && (this.$element.data("tabindex", this.$element.attr("tabindex")), this.$button.attr("tabindex", this.$element.data("tabindex"))), this.$element.attr("tabindex", -98);
      }, clickListener: function clickListener() {
        var b = this,
            c = a(document);c.data("spaceSelect", !1), this.$button.on("keyup", function (a) {
          /(32)/.test(a.keyCode.toString(10)) && c.data("spaceSelect") && (a.preventDefault(), c.data("spaceSelect", !1));
        }), this.$button.on("click", function () {
          b.setSize();
        }), this.$element.on("shown.bs.select", function () {
          if (b.options.liveSearch || b.multiple) {
            if (!b.multiple) {
              var a = b.liObj[b.$element[0].selectedIndex];if ("number" != typeof a || b.options.size === !1) return;var c = b.$lis.eq(a)[0].offsetTop - b.$menuInner[0].offsetTop;c = c - b.$menuInner[0].offsetHeight / 2 + b.sizeInfo.liHeight / 2, b.$menuInner[0].scrollTop = c;
            }
          } else b.$menuInner.find(".selected a").focus();
        }), this.$menuInner.on("click", "li a", function (c) {
          var d = a(this),
              f = d.parent().data("originalIndex"),
              g = b.$element.val(),
              h = b.$element.prop("selectedIndex"),
              i = !0;if (b.multiple && 1 !== b.options.maxOptions && c.stopPropagation(), c.preventDefault(), !b.isDisabled() && !d.parent().hasClass("disabled")) {
            var j = b.$element.find("option"),
                k = j.eq(f),
                l = k.prop("selected"),
                m = k.parent("optgroup"),
                n = b.options.maxOptions,
                o = m.data("maxOptions") || !1;if (b.multiple) {
              if (k.prop("selected", !l), b.setSelected(f, !l), d.blur(), n !== !1 || o !== !1) {
                var p = n < j.filter(":selected").length,
                    q = o < m.find("option:selected").length;if (n && p || o && q) if (n && 1 == n) j.prop("selected", !1), k.prop("selected", !0), b.$menuInner.find(".selected").removeClass("selected"), b.setSelected(f, !0);else if (o && 1 == o) {
                  m.find("option:selected").prop("selected", !1), k.prop("selected", !0);var r = d.parent().data("optgroup");b.$menuInner.find('[data-optgroup="' + r + '"]').removeClass("selected"), b.setSelected(f, !0);
                } else {
                  var s = "string" == typeof b.options.maxOptionsText ? [b.options.maxOptionsText, b.options.maxOptionsText] : b.options.maxOptionsText,
                      t = "function" == typeof s ? s(n, o) : s,
                      u = t[0].replace("{n}", n),
                      v = t[1].replace("{n}", o),
                      w = a('<div class="notify"></div>');t[2] && (u = u.replace("{var}", t[2][n > 1 ? 0 : 1]), v = v.replace("{var}", t[2][o > 1 ? 0 : 1])), k.prop("selected", !1), b.$menu.append(w), n && p && (w.append(a("<div>" + u + "</div>")), i = !1, b.$element.trigger("maxReached.bs.select")), o && q && (w.append(a("<div>" + v + "</div>")), i = !1, b.$element.trigger("maxReachedGrp.bs.select")), setTimeout(function () {
                    b.setSelected(f, !1);
                  }, 10), w.delay(750).fadeOut(300, function () {
                    a(this).remove();
                  });
                }
              }
            } else j.prop("selected", !1), k.prop("selected", !0), b.$menuInner.find(".selected").removeClass("selected").find("a").attr("aria-selected", !1), b.setSelected(f, !0);!b.multiple || b.multiple && 1 === b.options.maxOptions ? b.$button.focus() : b.options.liveSearch && b.$searchbox.focus(), i && (g != b.$element.val() && b.multiple || h != b.$element.prop("selectedIndex") && !b.multiple) && (e = [f, k.prop("selected"), l], b.$element.triggerNative("change"));
          }
        }), this.$menu.on("click", "li.disabled a, .popover-title, .popover-title :not(.close)", function (c) {
          c.currentTarget == this && (c.preventDefault(), c.stopPropagation(), b.options.liveSearch && !a(c.target).hasClass("close") ? b.$searchbox.focus() : b.$button.focus());
        }), this.$menuInner.on("click", ".divider, .dropdown-header", function (a) {
          a.preventDefault(), a.stopPropagation(), b.options.liveSearch ? b.$searchbox.focus() : b.$button.focus();
        }), this.$menu.on("click", ".popover-title .close", function () {
          b.$button.click();
        }), this.$searchbox.on("click", function (a) {
          a.stopPropagation();
        }), this.$menu.on("click", ".actions-btn", function (c) {
          b.options.liveSearch ? b.$searchbox.focus() : b.$button.focus(), c.preventDefault(), c.stopPropagation(), a(this).hasClass("bs-select-all") ? b.selectAll() : b.deselectAll();
        }), this.$element.change(function () {
          b.render(!1), b.$element.trigger("changed.bs.select", e), e = null;
        });
      }, liveSearchListener: function liveSearchListener() {
        var c = this,
            d = a('<li class="no-results"></li>');this.$button.on("click.dropdown.data-api", function () {
          c.$menuInner.find(".active").removeClass("active"), c.$searchbox.val() && (c.$searchbox.val(""), c.$lis.not(".is-hidden").removeClass("hidden"), d.parent().length && d.remove()), c.multiple || c.$menuInner.find(".selected").addClass("active"), setTimeout(function () {
            c.$searchbox.focus();
          }, 10);
        }), this.$searchbox.on("click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api", function (a) {
          a.stopPropagation();
        }), this.$searchbox.on("input propertychange", function () {
          if (c.$lis.not(".is-hidden").removeClass("hidden"), c.$lis.filter(".active").removeClass("active"), d.remove(), c.$searchbox.val()) {
            var e,
                f = c.$lis.not(".is-hidden, .divider, .dropdown-header");if (e = c.options.liveSearchNormalize ? f.find("a").not(":a" + c._searchStyle() + '("' + b(c.$searchbox.val()) + '")') : f.find("a").not(":" + c._searchStyle() + '("' + c.$searchbox.val() + '")'), e.length === f.length) d.html(c.options.noneResultsText.replace("{0}", '"' + i(c.$searchbox.val()) + '"')), c.$menuInner.append(d), c.$lis.addClass("hidden");else {
              e.parent().addClass("hidden");var g,
                  h = c.$lis.not(".hidden");h.each(function (b) {
                var c = a(this);c.hasClass("divider") ? void 0 === g ? c.addClass("hidden") : (g && g.addClass("hidden"), g = c) : c.hasClass("dropdown-header") && h.eq(b + 1).data("optgroup") !== c.data("optgroup") ? c.addClass("hidden") : g = null;
              }), g && g.addClass("hidden"), f.not(".hidden").first().addClass("active");
            }
          }
        });
      }, _searchStyle: function _searchStyle() {
        var a = { begins: "ibegins", startsWith: "ibegins" };return a[this.options.liveSearchStyle] || "icontains";
      }, val: function val(a) {
        return "undefined" != typeof a ? (this.$element.val(a), this.render(), this.$element) : this.$element.val();
      }, changeAll: function changeAll(b) {
        if (this.multiple) {
          "undefined" == typeof b && (b = !0), this.findLis();var c = this.$element.find("option"),
              d = this.$lis.not(".divider, .dropdown-header, .disabled, .hidden"),
              e = d.length,
              f = [];if (b) {
            if (d.filter(".selected").length === d.length) return;
          } else if (0 === d.filter(".selected").length) return;d.toggleClass("selected", b);for (var g = 0; g < e; g++) {
            var h = d[g].getAttribute("data-original-index");f[f.length] = c.eq(h)[0];
          }a(f).prop("selected", b), this.render(!1), this.togglePlaceholder(), this.$element.triggerNative("change");
        }
      }, selectAll: function selectAll() {
        return this.changeAll(!0);
      }, deselectAll: function deselectAll() {
        return this.changeAll(!1);
      }, toggle: function toggle(a) {
        a = a || window.event, a && a.stopPropagation(), this.$button.trigger("click");
      }, keydown: function keydown(c) {
        var d,
            e,
            f,
            g,
            h,
            i,
            j,
            k,
            l,
            m = a(this),
            n = m.is("input") ? m.parent().parent() : m.parent(),
            o = n.data("this"),
            p = ":not(.disabled, .hidden, .dropdown-header, .divider)",
            q = { 32: " ", 48: "0", 49: "1", 50: "2", 51: "3", 52: "4", 53: "5", 54: "6", 55: "7", 56: "8", 57: "9", 59: ";", 65: "a", 66: "b", 67: "c", 68: "d", 69: "e", 70: "f", 71: "g", 72: "h", 73: "i", 74: "j", 75: "k", 76: "l", 77: "m", 78: "n", 79: "o", 80: "p", 81: "q", 82: "r", 83: "s", 84: "t", 85: "u", 86: "v", 87: "w", 88: "x", 89: "y", 90: "z", 96: "0", 97: "1", 98: "2", 99: "3", 100: "4", 101: "5", 102: "6", 103: "7", 104: "8", 105: "9" };if (o.options.liveSearch && (n = m.parent().parent()), o.options.container && (n = o.$menu), d = a('[role="listbox"] li', n), l = o.$newElement.hasClass("open"), !l && (c.keyCode >= 48 && c.keyCode <= 57 || c.keyCode >= 96 && c.keyCode <= 105 || c.keyCode >= 65 && c.keyCode <= 90)) return o.options.container ? o.$button.trigger("click") : (o.setSize(), o.$menu.parent().addClass("open"), l = !0), void o.$searchbox.focus();if (o.options.liveSearch && (/(^9$|27)/.test(c.keyCode.toString(10)) && l && (c.preventDefault(), c.stopPropagation(), o.$menuInner.click(), o.$button.focus()), d = a('[role="listbox"] li' + p, n), m.val() || /(38|40)/.test(c.keyCode.toString(10)) || 0 === d.filter(".active").length && (d = o.$menuInner.find("li"), d = o.options.liveSearchNormalize ? d.filter(":a" + o._searchStyle() + "(" + b(q[c.keyCode]) + ")") : d.filter(":" + o._searchStyle() + "(" + q[c.keyCode] + ")"))), d.length) {
          if (/(38|40)/.test(c.keyCode.toString(10))) e = d.index(d.find("a").filter(":focus").parent()), g = d.filter(p).first().index(), h = d.filter(p).last().index(), f = d.eq(e).nextAll(p).eq(0).index(), i = d.eq(e).prevAll(p).eq(0).index(), j = d.eq(f).prevAll(p).eq(0).index(), o.options.liveSearch && (d.each(function (b) {
            a(this).hasClass("disabled") || a(this).data("index", b);
          }), e = d.index(d.filter(".active")), g = d.first().data("index"), h = d.last().data("index"), f = d.eq(e).nextAll().eq(0).data("index"), i = d.eq(e).prevAll().eq(0).data("index"), j = d.eq(f).prevAll().eq(0).data("index")), k = m.data("prevIndex"), 38 == c.keyCode ? (o.options.liveSearch && e--, e != j && e > i && (e = i), e < g && (e = g), e == k && (e = h)) : 40 == c.keyCode && (o.options.liveSearch && e++, e == -1 && (e = 0), e != j && e < f && (e = f), e > h && (e = h), e == k && (e = g)), m.data("prevIndex", e), o.options.liveSearch ? (c.preventDefault(), m.hasClass("dropdown-toggle") || (d.removeClass("active").eq(e).addClass("active").children("a").focus(), m.focus())) : d.eq(e).children("a").focus();else if (!m.is("input")) {
            var r,
                s,
                t = [];d.each(function () {
              a(this).hasClass("disabled") || a.trim(a(this).children("a").text().toLowerCase()).substring(0, 1) == q[c.keyCode] && t.push(a(this).index());
            }), r = a(document).data("keycount"), r++, a(document).data("keycount", r), s = a.trim(a(":focus").text().toLowerCase()).substring(0, 1), s != q[c.keyCode] ? (r = 1, a(document).data("keycount", r)) : r >= t.length && (a(document).data("keycount", 0), r > t.length && (r = 1)), d.eq(t[r - 1]).children("a").focus();
          }if ((/(13|32)/.test(c.keyCode.toString(10)) || /(^9$)/.test(c.keyCode.toString(10)) && o.options.selectOnTab) && l) {
            if (/(32)/.test(c.keyCode.toString(10)) || c.preventDefault(), o.options.liveSearch) /(32)/.test(c.keyCode.toString(10)) || (o.$menuInner.find(".active a").click(), m.focus());else {
              var u = a(":focus");u.click(), u.focus(), c.preventDefault(), a(document).data("spaceSelect", !0);
            }a(document).data("keycount", 0);
          }(/(^9$|27)/.test(c.keyCode.toString(10)) && l && (o.multiple || o.options.liveSearch) || /(27)/.test(c.keyCode.toString(10)) && !l) && (o.$menu.parent().removeClass("open"), o.options.container && o.$newElement.removeClass("open"), o.$button.focus());
        }
      }, mobile: function mobile() {
        this.$element.addClass("mobile-device");
      }, refresh: function refresh() {
        this.$lis = null, this.liObj = {}, this.reloadLi(), this.render(), this.checkDisabled(), this.liHeight(!0), this.setStyle(), this.setWidth(), this.$lis && this.$searchbox.trigger("propertychange"), this.$element.trigger("refreshed.bs.select");
      }, hide: function hide() {
        this.$newElement.hide();
      }, show: function show() {
        this.$newElement.show();
      }, remove: function remove() {
        this.$newElement.remove(), this.$element.remove();
      }, destroy: function destroy() {
        this.$newElement.before(this.$element).remove(), this.$bsContainer ? this.$bsContainer.remove() : this.$menu.remove(), this.$element.off(".bs.select").removeData("selectpicker").removeClass("bs-select-hidden selectpicker");
      } };var l = a.fn.selectpicker;a.fn.selectpicker = c, a.fn.selectpicker.Constructor = k, a.fn.selectpicker.noConflict = function () {
      return a.fn.selectpicker = l, this;
    }, a(document).data("keycount", 0).on("keydown.bs.select", '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="listbox"], .bs-searchbox input', k.prototype.keydown).on("focusin.modal", '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="listbox"], .bs-searchbox input', function (a) {
      a.stopPropagation();
    }), a(window).on("load.bs.select.data-api", function () {
      a(".selectpicker").each(function () {
        var b = a(this);c.call(b, b.data());
      });
    });
  }(a);
});
//# sourceMappingURL=bootstrap-select.js.map

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */
if ("undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery");+function (a) {
  "use strict";
  var b = a.fn.jquery.split(" ")[0].split(".");if (b[0] < 2 && b[1] < 9 || 1 == b[0] && 9 == b[1] && b[2] < 1 || b[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4");
}(jQuery), +function (a) {
  "use strict";
  function b() {
    var a = document.createElement("bootstrap"),
        b = { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd otransitionend", transition: "transitionend" };for (var c in b) {
      if (void 0 !== a.style[c]) return { end: b[c] };
    }return !1;
  }a.fn.emulateTransitionEnd = function (b) {
    var c = !1,
        d = this;a(this).one("bsTransitionEnd", function () {
      c = !0;
    });var e = function e() {
      c || a(d).trigger(a.support.transition.end);
    };return setTimeout(e, b), this;
  }, a(function () {
    a.support.transition = b(), a.support.transition && (a.event.special.bsTransitionEnd = { bindType: a.support.transition.end, delegateType: a.support.transition.end, handle: function handle(b) {
        if (a(b.target).is(this)) return b.handleObj.handler.apply(this, arguments);
      } });
  });
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    return this.each(function () {
      var c = a(this),
          e = c.data("bs.alert");e || c.data("bs.alert", e = new d(this)), "string" == typeof b && e[b].call(c);
    });
  }var c = '[data-dismiss="alert"]',
      d = function d(b) {
    a(b).on("click", c, this.close);
  };d.VERSION = "3.3.7", d.TRANSITION_DURATION = 150, d.prototype.close = function (b) {
    function c() {
      g.detach().trigger("closed.bs.alert").remove();
    }var e = a(this),
        f = e.attr("data-target");f || (f = e.attr("href"), f = f && f.replace(/.*(?=#[^\s]*$)/, ""));var g = a("#" === f ? [] : f);b && b.preventDefault(), g.length || (g = e.closest(".alert")), g.trigger(b = a.Event("close.bs.alert")), b.isDefaultPrevented() || (g.removeClass("in"), a.support.transition && g.hasClass("fade") ? g.one("bsTransitionEnd", c).emulateTransitionEnd(d.TRANSITION_DURATION) : c());
  };var e = a.fn.alert;a.fn.alert = b, a.fn.alert.Constructor = d, a.fn.alert.noConflict = function () {
    return a.fn.alert = e, this;
  }, a(document).on("click.bs.alert.data-api", c, d.prototype.close);
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    return this.each(function () {
      var d = a(this),
          e = d.data("bs.button"),
          f = "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b;e || d.data("bs.button", e = new c(this, f)), "toggle" == b ? e.toggle() : b && e.setState(b);
    });
  }var c = function c(b, d) {
    this.$element = a(b), this.options = a.extend({}, c.DEFAULTS, d), this.isLoading = !1;
  };c.VERSION = "3.3.7", c.DEFAULTS = { loadingText: "loading..." }, c.prototype.setState = function (b) {
    var c = "disabled",
        d = this.$element,
        e = d.is("input") ? "val" : "html",
        f = d.data();b += "Text", null == f.resetText && d.data("resetText", d[e]()), setTimeout(a.proxy(function () {
      d[e](null == f[b] ? this.options[b] : f[b]), "loadingText" == b ? (this.isLoading = !0, d.addClass(c).attr(c, c).prop(c, !0)) : this.isLoading && (this.isLoading = !1, d.removeClass(c).removeAttr(c).prop(c, !1));
    }, this), 0);
  }, c.prototype.toggle = function () {
    var a = !0,
        b = this.$element.closest('[data-toggle="buttons"]');if (b.length) {
      var c = this.$element.find("input");"radio" == c.prop("type") ? (c.prop("checked") && (a = !1), b.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == c.prop("type") && (c.prop("checked") !== this.$element.hasClass("active") && (a = !1), this.$element.toggleClass("active")), c.prop("checked", this.$element.hasClass("active")), a && c.trigger("change");
    } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active");
  };var d = a.fn.button;a.fn.button = b, a.fn.button.Constructor = c, a.fn.button.noConflict = function () {
    return a.fn.button = d, this;
  }, a(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function (c) {
    var d = a(c.target).closest(".btn");b.call(d, "toggle"), a(c.target).is('input[type="radio"], input[type="checkbox"]') || (c.preventDefault(), d.is("input,button") ? d.trigger("focus") : d.find("input:visible,button:visible").first().trigger("focus"));
  }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function (b) {
    a(b.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(b.type));
  });
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    return this.each(function () {
      var d = a(this),
          e = d.data("bs.carousel"),
          f = a.extend({}, c.DEFAULTS, d.data(), "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b),
          g = "string" == typeof b ? b : f.slide;e || d.data("bs.carousel", e = new c(this, f)), "number" == typeof b ? e.to(b) : g ? e[g]() : f.interval && e.pause().cycle();
    });
  }var c = function c(b, _c) {
    this.$element = a(b), this.$indicators = this.$element.find(".carousel-indicators"), this.options = _c, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", a.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", a.proxy(this.pause, this)).on("mouseleave.bs.carousel", a.proxy(this.cycle, this));
  };c.VERSION = "3.3.7", c.TRANSITION_DURATION = 600, c.DEFAULTS = { interval: 5e3, pause: "hover", wrap: !0, keyboard: !0 }, c.prototype.keydown = function (a) {
    if (!/input|textarea/i.test(a.target.tagName)) {
      switch (a.which) {case 37:
          this.prev();break;case 39:
          this.next();break;default:
          return;}a.preventDefault();
    }
  }, c.prototype.cycle = function (b) {
    return b || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(a.proxy(this.next, this), this.options.interval)), this;
  }, c.prototype.getItemIndex = function (a) {
    return this.$items = a.parent().children(".item"), this.$items.index(a || this.$active);
  }, c.prototype.getItemForDirection = function (a, b) {
    var c = this.getItemIndex(b),
        d = "prev" == a && 0 === c || "next" == a && c == this.$items.length - 1;if (d && !this.options.wrap) return b;var e = "prev" == a ? -1 : 1,
        f = (c + e) % this.$items.length;return this.$items.eq(f);
  }, c.prototype.to = function (a) {
    var b = this,
        c = this.getItemIndex(this.$active = this.$element.find(".item.active"));if (!(a > this.$items.length - 1 || a < 0)) return this.sliding ? this.$element.one("slid.bs.carousel", function () {
      b.to(a);
    }) : c == a ? this.pause().cycle() : this.slide(a > c ? "next" : "prev", this.$items.eq(a));
  }, c.prototype.pause = function (b) {
    return b || (this.paused = !0), this.$element.find(".next, .prev").length && a.support.transition && (this.$element.trigger(a.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this;
  }, c.prototype.next = function () {
    if (!this.sliding) return this.slide("next");
  }, c.prototype.prev = function () {
    if (!this.sliding) return this.slide("prev");
  }, c.prototype.slide = function (b, d) {
    var e = this.$element.find(".item.active"),
        f = d || this.getItemForDirection(b, e),
        g = this.interval,
        h = "next" == b ? "left" : "right",
        i = this;if (f.hasClass("active")) return this.sliding = !1;var j = f[0],
        k = a.Event("slide.bs.carousel", { relatedTarget: j, direction: h });if (this.$element.trigger(k), !k.isDefaultPrevented()) {
      if (this.sliding = !0, g && this.pause(), this.$indicators.length) {
        this.$indicators.find(".active").removeClass("active");var l = a(this.$indicators.children()[this.getItemIndex(f)]);l && l.addClass("active");
      }var m = a.Event("slid.bs.carousel", { relatedTarget: j, direction: h });return a.support.transition && this.$element.hasClass("slide") ? (f.addClass(b), f[0].offsetWidth, e.addClass(h), f.addClass(h), e.one("bsTransitionEnd", function () {
        f.removeClass([b, h].join(" ")).addClass("active"), e.removeClass(["active", h].join(" ")), i.sliding = !1, setTimeout(function () {
          i.$element.trigger(m);
        }, 0);
      }).emulateTransitionEnd(c.TRANSITION_DURATION)) : (e.removeClass("active"), f.addClass("active"), this.sliding = !1, this.$element.trigger(m)), g && this.cycle(), this;
    }
  };var d = a.fn.carousel;a.fn.carousel = b, a.fn.carousel.Constructor = c, a.fn.carousel.noConflict = function () {
    return a.fn.carousel = d, this;
  };var e = function e(c) {
    var d,
        e = a(this),
        f = a(e.attr("data-target") || (d = e.attr("href")) && d.replace(/.*(?=#[^\s]+$)/, ""));if (f.hasClass("carousel")) {
      var g = a.extend({}, f.data(), e.data()),
          h = e.attr("data-slide-to");h && (g.interval = !1), b.call(f, g), h && f.data("bs.carousel").to(h), c.preventDefault();
    }
  };a(document).on("click.bs.carousel.data-api", "[data-slide]", e).on("click.bs.carousel.data-api", "[data-slide-to]", e), a(window).on("load", function () {
    a('[data-ride="carousel"]').each(function () {
      var c = a(this);b.call(c, c.data());
    });
  });
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    var c,
        d = b.attr("data-target") || (c = b.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, "");return a(d);
  }function c(b) {
    return this.each(function () {
      var c = a(this),
          e = c.data("bs.collapse"),
          f = a.extend({}, d.DEFAULTS, c.data(), "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b);!e && f.toggle && /show|hide/.test(b) && (f.toggle = !1), e || c.data("bs.collapse", e = new d(this, f)), "string" == typeof b && e[b]();
    });
  }var d = function d(b, c) {
    this.$element = a(b), this.options = a.extend({}, d.DEFAULTS, c), this.$trigger = a('[data-toggle="collapse"][href="#' + b.id + '"],[data-toggle="collapse"][data-target="#' + b.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle();
  };d.VERSION = "3.3.7", d.TRANSITION_DURATION = 350, d.DEFAULTS = { toggle: !0 }, d.prototype.dimension = function () {
    var a = this.$element.hasClass("width");return a ? "width" : "height";
  }, d.prototype.show = function () {
    if (!this.transitioning && !this.$element.hasClass("in")) {
      var b,
          e = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");if (!(e && e.length && (b = e.data("bs.collapse"), b && b.transitioning))) {
        var f = a.Event("show.bs.collapse");if (this.$element.trigger(f), !f.isDefaultPrevented()) {
          e && e.length && (c.call(e, "hide"), b || e.data("bs.collapse", null));var g = this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;var h = function h() {
            this.$element.removeClass("collapsing").addClass("collapse in")[g](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse");
          };if (!a.support.transition) return h.call(this);var i = a.camelCase(["scroll", g].join("-"));this.$element.one("bsTransitionEnd", a.proxy(h, this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i]);
        }
      }
    }
  }, d.prototype.hide = function () {
    if (!this.transitioning && this.$element.hasClass("in")) {
      var b = a.Event("hide.bs.collapse");if (this.$element.trigger(b), !b.isDefaultPrevented()) {
        var c = this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;var e = function e() {
          this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse");
        };return a.support.transition ? void this.$element[c](0).one("bsTransitionEnd", a.proxy(e, this)).emulateTransitionEnd(d.TRANSITION_DURATION) : e.call(this);
      }
    }
  }, d.prototype.toggle = function () {
    this[this.$element.hasClass("in") ? "hide" : "show"]();
  }, d.prototype.getParent = function () {
    return a(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(a.proxy(function (c, d) {
      var e = a(d);this.addAriaAndCollapsedClass(b(e), e);
    }, this)).end();
  }, d.prototype.addAriaAndCollapsedClass = function (a, b) {
    var c = a.hasClass("in");a.attr("aria-expanded", c), b.toggleClass("collapsed", !c).attr("aria-expanded", c);
  };var e = a.fn.collapse;a.fn.collapse = c, a.fn.collapse.Constructor = d, a.fn.collapse.noConflict = function () {
    return a.fn.collapse = e, this;
  }, a(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function (d) {
    var e = a(this);e.attr("data-target") || d.preventDefault();var f = b(e),
        g = f.data("bs.collapse"),
        h = g ? "toggle" : e.data();c.call(f, h);
  });
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    var c = b.attr("data-target");c || (c = b.attr("href"), c = c && /#[A-Za-z]/.test(c) && c.replace(/.*(?=#[^\s]*$)/, ""));var d = c && a(c);return d && d.length ? d : b.parent();
  }function c(c) {
    c && 3 === c.which || (a(e).remove(), a(f).each(function () {
      var d = a(this),
          e = b(d),
          f = { relatedTarget: this };e.hasClass("open") && (c && "click" == c.type && /input|textarea/i.test(c.target.tagName) && a.contains(e[0], c.target) || (e.trigger(c = a.Event("hide.bs.dropdown", f)), c.isDefaultPrevented() || (d.attr("aria-expanded", "false"), e.removeClass("open").trigger(a.Event("hidden.bs.dropdown", f)))));
    }));
  }function d(b) {
    return this.each(function () {
      var c = a(this),
          d = c.data("bs.dropdown");d || c.data("bs.dropdown", d = new g(this)), "string" == typeof b && d[b].call(c);
    });
  }var e = ".dropdown-backdrop",
      f = '[data-toggle="dropdown"]',
      g = function g(b) {
    a(b).on("click.bs.dropdown", this.toggle);
  };g.VERSION = "3.3.7", g.prototype.toggle = function (d) {
    var e = a(this);if (!e.is(".disabled, :disabled")) {
      var f = b(e),
          g = f.hasClass("open");if (c(), !g) {
        "ontouchstart" in document.documentElement && !f.closest(".navbar-nav").length && a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click", c);var h = { relatedTarget: this };if (f.trigger(d = a.Event("show.bs.dropdown", h)), d.isDefaultPrevented()) return;e.trigger("focus").attr("aria-expanded", "true"), f.toggleClass("open").trigger(a.Event("shown.bs.dropdown", h));
      }return !1;
    }
  }, g.prototype.keydown = function (c) {
    if (/(38|40|27|32)/.test(c.which) && !/input|textarea/i.test(c.target.tagName)) {
      var d = a(this);if (c.preventDefault(), c.stopPropagation(), !d.is(".disabled, :disabled")) {
        var e = b(d),
            g = e.hasClass("open");if (!g && 27 != c.which || g && 27 == c.which) return 27 == c.which && e.find(f).trigger("focus"), d.trigger("click");var h = " li:not(.disabled):visible a",
            i = e.find(".dropdown-menu" + h);if (i.length) {
          var j = i.index(c.target);38 == c.which && j > 0 && j--, 40 == c.which && j < i.length - 1 && j++, ~j || (j = 0), i.eq(j).trigger("focus");
        }
      }
    }
  };var h = a.fn.dropdown;a.fn.dropdown = d, a.fn.dropdown.Constructor = g, a.fn.dropdown.noConflict = function () {
    return a.fn.dropdown = h, this;
  }, a(document).on("click.bs.dropdown.data-api", c).on("click.bs.dropdown.data-api", ".dropdown form", function (a) {
    a.stopPropagation();
  }).on("click.bs.dropdown.data-api", f, g.prototype.toggle).on("keydown.bs.dropdown.data-api", f, g.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", g.prototype.keydown);
}(jQuery), +function (a) {
  "use strict";
  function b(b, d) {
    return this.each(function () {
      var e = a(this),
          f = e.data("bs.modal"),
          g = a.extend({}, c.DEFAULTS, e.data(), "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b);f || e.data("bs.modal", f = new c(this, g)), "string" == typeof b ? f[b](d) : g.show && f.show(d);
    });
  }var c = function c(b, _c2) {
    this.options = _c2, this.$body = a(document.body), this.$element = a(b), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, a.proxy(function () {
      this.$element.trigger("loaded.bs.modal");
    }, this));
  };c.VERSION = "3.3.7", c.TRANSITION_DURATION = 300, c.BACKDROP_TRANSITION_DURATION = 150, c.DEFAULTS = { backdrop: !0, keyboard: !0, show: !0 }, c.prototype.toggle = function (a) {
    return this.isShown ? this.hide() : this.show(a);
  }, c.prototype.show = function (b) {
    var d = this,
        e = a.Event("show.bs.modal", { relatedTarget: b });this.$element.trigger(e), this.isShown || e.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', a.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function () {
      d.$element.one("mouseup.dismiss.bs.modal", function (b) {
        a(b.target).is(d.$element) && (d.ignoreBackdropClick = !0);
      });
    }), this.backdrop(function () {
      var e = a.support.transition && d.$element.hasClass("fade");d.$element.parent().length || d.$element.appendTo(d.$body), d.$element.show().scrollTop(0), d.adjustDialog(), e && d.$element[0].offsetWidth, d.$element.addClass("in"), d.enforceFocus();var f = a.Event("shown.bs.modal", { relatedTarget: b });e ? d.$dialog.one("bsTransitionEnd", function () {
        d.$element.trigger("focus").trigger(f);
      }).emulateTransitionEnd(c.TRANSITION_DURATION) : d.$element.trigger("focus").trigger(f);
    }));
  }, c.prototype.hide = function (b) {
    b && b.preventDefault(), b = a.Event("hide.bs.modal"), this.$element.trigger(b), this.isShown && !b.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), a(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), a.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", a.proxy(this.hideModal, this)).emulateTransitionEnd(c.TRANSITION_DURATION) : this.hideModal());
  }, c.prototype.enforceFocus = function () {
    a(document).off("focusin.bs.modal").on("focusin.bs.modal", a.proxy(function (a) {
      document === a.target || this.$element[0] === a.target || this.$element.has(a.target).length || this.$element.trigger("focus");
    }, this));
  }, c.prototype.escape = function () {
    this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", a.proxy(function (a) {
      27 == a.which && this.hide();
    }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal");
  }, c.prototype.resize = function () {
    this.isShown ? a(window).on("resize.bs.modal", a.proxy(this.handleUpdate, this)) : a(window).off("resize.bs.modal");
  }, c.prototype.hideModal = function () {
    var a = this;this.$element.hide(), this.backdrop(function () {
      a.$body.removeClass("modal-open"), a.resetAdjustments(), a.resetScrollbar(), a.$element.trigger("hidden.bs.modal");
    });
  }, c.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove(), this.$backdrop = null;
  }, c.prototype.backdrop = function (b) {
    var d = this,
        e = this.$element.hasClass("fade") ? "fade" : "";if (this.isShown && this.options.backdrop) {
      var f = a.support.transition && e;if (this.$backdrop = a(document.createElement("div")).addClass("modal-backdrop " + e).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", a.proxy(function (a) {
        return this.ignoreBackdropClick ? void (this.ignoreBackdropClick = !1) : void (a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()));
      }, this)), f && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !b) return;f ? this.$backdrop.one("bsTransitionEnd", b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : b();
    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass("in");var g = function g() {
        d.removeBackdrop(), b && b();
      };a.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : g();
    } else b && b();
  }, c.prototype.handleUpdate = function () {
    this.adjustDialog();
  }, c.prototype.adjustDialog = function () {
    var a = this.$element[0].scrollHeight > document.documentElement.clientHeight;this.$element.css({ paddingLeft: !this.bodyIsOverflowing && a ? this.scrollbarWidth : "", paddingRight: this.bodyIsOverflowing && !a ? this.scrollbarWidth : "" });
  }, c.prototype.resetAdjustments = function () {
    this.$element.css({ paddingLeft: "", paddingRight: "" });
  }, c.prototype.checkScrollbar = function () {
    var a = window.innerWidth;if (!a) {
      var b = document.documentElement.getBoundingClientRect();a = b.right - Math.abs(b.left);
    }this.bodyIsOverflowing = document.body.clientWidth < a, this.scrollbarWidth = this.measureScrollbar();
  }, c.prototype.setScrollbar = function () {
    var a = parseInt(this.$body.css("padding-right") || 0, 10);this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", a + this.scrollbarWidth);
  }, c.prototype.resetScrollbar = function () {
    this.$body.css("padding-right", this.originalBodyPad);
  }, c.prototype.measureScrollbar = function () {
    var a = document.createElement("div");a.className = "modal-scrollbar-measure", this.$body.append(a);var b = a.offsetWidth - a.clientWidth;return this.$body[0].removeChild(a), b;
  };var d = a.fn.modal;a.fn.modal = b, a.fn.modal.Constructor = c, a.fn.modal.noConflict = function () {
    return a.fn.modal = d, this;
  }, a(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (c) {
    var d = a(this),
        e = d.attr("href"),
        f = a(d.attr("data-target") || e && e.replace(/.*(?=#[^\s]+$)/, "")),
        g = f.data("bs.modal") ? "toggle" : a.extend({ remote: !/#/.test(e) && e }, f.data(), d.data());d.is("a") && c.preventDefault(), f.one("show.bs.modal", function (a) {
      a.isDefaultPrevented() || f.one("hidden.bs.modal", function () {
        d.is(":visible") && d.trigger("focus");
      });
    }), b.call(f, g, this);
  });
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    return this.each(function () {
      var d = a(this),
          e = d.data("bs.tooltip"),
          f = "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b;!e && /destroy|hide/.test(b) || (e || d.data("bs.tooltip", e = new c(this, f)), "string" == typeof b && e[b]());
    });
  }var c = function c(a, b) {
    this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", a, b);
  };c.VERSION = "3.3.7", c.TRANSITION_DURATION = 150, c.DEFAULTS = { animation: !0, placement: "top", selector: !1, template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>', trigger: "hover focus", title: "", delay: 0, html: !1, container: !1, viewport: { selector: "body", padding: 0 } }, c.prototype.init = function (b, c, d) {
    if (this.enabled = !0, this.type = b, this.$element = a(c), this.options = this.getOptions(d), this.$viewport = this.options.viewport && a(a.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = { click: !1, hover: !1, focus: !1 }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");for (var e = this.options.trigger.split(" "), f = e.length; f--;) {
      var g = e[f];if ("click" == g) this.$element.on("click." + this.type, this.options.selector, a.proxy(this.toggle, this));else if ("manual" != g) {
        var h = "hover" == g ? "mouseenter" : "focusin",
            i = "hover" == g ? "mouseleave" : "focusout";this.$element.on(h + "." + this.type, this.options.selector, a.proxy(this.enter, this)), this.$element.on(i + "." + this.type, this.options.selector, a.proxy(this.leave, this));
      }
    }this.options.selector ? this._options = a.extend({}, this.options, { trigger: "manual", selector: "" }) : this.fixTitle();
  }, c.prototype.getDefaults = function () {
    return c.DEFAULTS;
  }, c.prototype.getOptions = function (b) {
    return b = a.extend({}, this.getDefaults(), this.$element.data(), b), b.delay && "number" == typeof b.delay && (b.delay = { show: b.delay, hide: b.delay }), b;
  }, c.prototype.getDelegateOptions = function () {
    var b = {},
        c = this.getDefaults();return this._options && a.each(this._options, function (a, d) {
      c[a] != d && (b[a] = d);
    }), b;
  }, c.prototype.enter = function (b) {
    var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type);return c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusin" == b.type ? "focus" : "hover"] = !0), c.tip().hasClass("in") || "in" == c.hoverState ? void (c.hoverState = "in") : (clearTimeout(c.timeout), c.hoverState = "in", c.options.delay && c.options.delay.show ? void (c.timeout = setTimeout(function () {
      "in" == c.hoverState && c.show();
    }, c.options.delay.show)) : c.show());
  }, c.prototype.isInStateTrue = function () {
    for (var a in this.inState) {
      if (this.inState[a]) return !0;
    }return !1;
  }, c.prototype.leave = function (b) {
    var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type);if (c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusout" == b.type ? "focus" : "hover"] = !1), !c.isInStateTrue()) return clearTimeout(c.timeout), c.hoverState = "out", c.options.delay && c.options.delay.hide ? void (c.timeout = setTimeout(function () {
      "out" == c.hoverState && c.hide();
    }, c.options.delay.hide)) : c.hide();
  }, c.prototype.show = function () {
    var b = a.Event("show.bs." + this.type);if (this.hasContent() && this.enabled) {
      this.$element.trigger(b);var d = a.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);if (b.isDefaultPrevented() || !d) return;var e = this,
          f = this.tip(),
          g = this.getUID(this.type);this.setContent(), f.attr("id", g), this.$element.attr("aria-describedby", g), this.options.animation && f.addClass("fade");var h = "function" == typeof this.options.placement ? this.options.placement.call(this, f[0], this.$element[0]) : this.options.placement,
          i = /\s?auto?\s?/i,
          j = i.test(h);j && (h = h.replace(i, "") || "top"), f.detach().css({ top: 0, left: 0, display: "block" }).addClass(h).data("bs." + this.type, this), this.options.container ? f.appendTo(this.options.container) : f.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);var k = this.getPosition(),
          l = f[0].offsetWidth,
          m = f[0].offsetHeight;if (j) {
        var n = h,
            o = this.getPosition(this.$viewport);h = "bottom" == h && k.bottom + m > o.bottom ? "top" : "top" == h && k.top - m < o.top ? "bottom" : "right" == h && k.right + l > o.width ? "left" : "left" == h && k.left - l < o.left ? "right" : h, f.removeClass(n).addClass(h);
      }var p = this.getCalculatedOffset(h, k, l, m);this.applyPlacement(p, h);var q = function q() {
        var a = e.hoverState;e.$element.trigger("shown.bs." + e.type), e.hoverState = null, "out" == a && e.leave(e);
      };a.support.transition && this.$tip.hasClass("fade") ? f.one("bsTransitionEnd", q).emulateTransitionEnd(c.TRANSITION_DURATION) : q();
    }
  }, c.prototype.applyPlacement = function (b, c) {
    var d = this.tip(),
        e = d[0].offsetWidth,
        f = d[0].offsetHeight,
        g = parseInt(d.css("margin-top"), 10),
        h = parseInt(d.css("margin-left"), 10);isNaN(g) && (g = 0), isNaN(h) && (h = 0), b.top += g, b.left += h, a.offset.setOffset(d[0], a.extend({ using: function using(a) {
        d.css({ top: Math.round(a.top), left: Math.round(a.left) });
      } }, b), 0), d.addClass("in");var i = d[0].offsetWidth,
        j = d[0].offsetHeight;"top" == c && j != f && (b.top = b.top + f - j);var k = this.getViewportAdjustedDelta(c, b, i, j);k.left ? b.left += k.left : b.top += k.top;var l = /top|bottom/.test(c),
        m = l ? 2 * k.left - e + i : 2 * k.top - f + j,
        n = l ? "offsetWidth" : "offsetHeight";d.offset(b), this.replaceArrow(m, d[0][n], l);
  }, c.prototype.replaceArrow = function (a, b, c) {
    this.arrow().css(c ? "left" : "top", 50 * (1 - a / b) + "%").css(c ? "top" : "left", "");
  }, c.prototype.setContent = function () {
    var a = this.tip(),
        b = this.getTitle();a.find(".tooltip-inner")[this.options.html ? "html" : "text"](b), a.removeClass("fade in top bottom left right");
  }, c.prototype.hide = function (b) {
    function d() {
      "in" != e.hoverState && f.detach(), e.$element && e.$element.removeAttr("aria-describedby").trigger("hidden.bs." + e.type), b && b();
    }var e = this,
        f = a(this.$tip),
        g = a.Event("hide.bs." + this.type);if (this.$element.trigger(g), !g.isDefaultPrevented()) return f.removeClass("in"), a.support.transition && f.hasClass("fade") ? f.one("bsTransitionEnd", d).emulateTransitionEnd(c.TRANSITION_DURATION) : d(), this.hoverState = null, this;
  }, c.prototype.fixTitle = function () {
    var a = this.$element;(a.attr("title") || "string" != typeof a.attr("data-original-title")) && a.attr("data-original-title", a.attr("title") || "").attr("title", "");
  }, c.prototype.hasContent = function () {
    return this.getTitle();
  }, c.prototype.getPosition = function (b) {
    b = b || this.$element;var c = b[0],
        d = "BODY" == c.tagName,
        e = c.getBoundingClientRect();null == e.width && (e = a.extend({}, e, { width: e.right - e.left, height: e.bottom - e.top }));var f = window.SVGElement && c instanceof window.SVGElement,
        g = d ? { top: 0, left: 0 } : f ? null : b.offset(),
        h = { scroll: d ? document.documentElement.scrollTop || document.body.scrollTop : b.scrollTop() },
        i = d ? { width: a(window).width(), height: a(window).height() } : null;return a.extend({}, e, h, i, g);
  }, c.prototype.getCalculatedOffset = function (a, b, c, d) {
    return "bottom" == a ? { top: b.top + b.height, left: b.left + b.width / 2 - c / 2 } : "top" == a ? { top: b.top - d, left: b.left + b.width / 2 - c / 2 } : "left" == a ? { top: b.top + b.height / 2 - d / 2, left: b.left - c } : { top: b.top + b.height / 2 - d / 2, left: b.left + b.width };
  }, c.prototype.getViewportAdjustedDelta = function (a, b, c, d) {
    var e = { top: 0, left: 0 };if (!this.$viewport) return e;var f = this.options.viewport && this.options.viewport.padding || 0,
        g = this.getPosition(this.$viewport);if (/right|left/.test(a)) {
      var h = b.top - f - g.scroll,
          i = b.top + f - g.scroll + d;h < g.top ? e.top = g.top - h : i > g.top + g.height && (e.top = g.top + g.height - i);
    } else {
      var j = b.left - f,
          k = b.left + f + c;j < g.left ? e.left = g.left - j : k > g.right && (e.left = g.left + g.width - k);
    }return e;
  }, c.prototype.getTitle = function () {
    var a,
        b = this.$element,
        c = this.options;return a = b.attr("data-original-title") || ("function" == typeof c.title ? c.title.call(b[0]) : c.title);
  }, c.prototype.getUID = function (a) {
    do {
      a += ~~(1e6 * Math.random());
    } while (document.getElementById(a));return a;
  }, c.prototype.tip = function () {
    if (!this.$tip && (this.$tip = a(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");return this.$tip;
  }, c.prototype.arrow = function () {
    return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow");
  }, c.prototype.enable = function () {
    this.enabled = !0;
  }, c.prototype.disable = function () {
    this.enabled = !1;
  }, c.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled;
  }, c.prototype.toggle = function (b) {
    var c = this;b && (c = a(b.currentTarget).data("bs." + this.type), c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c))), b ? (c.inState.click = !c.inState.click, c.isInStateTrue() ? c.enter(c) : c.leave(c)) : c.tip().hasClass("in") ? c.leave(c) : c.enter(c);
  }, c.prototype.destroy = function () {
    var a = this;clearTimeout(this.timeout), this.hide(function () {
      a.$element.off("." + a.type).removeData("bs." + a.type), a.$tip && a.$tip.detach(), a.$tip = null, a.$arrow = null, a.$viewport = null, a.$element = null;
    });
  };var d = a.fn.tooltip;a.fn.tooltip = b, a.fn.tooltip.Constructor = c, a.fn.tooltip.noConflict = function () {
    return a.fn.tooltip = d, this;
  };
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    return this.each(function () {
      var d = a(this),
          e = d.data("bs.popover"),
          f = "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b;!e && /destroy|hide/.test(b) || (e || d.data("bs.popover", e = new c(this, f)), "string" == typeof b && e[b]());
    });
  }var c = function c(a, b) {
    this.init("popover", a, b);
  };if (!a.fn.tooltip) throw new Error("Popover requires tooltip.js");c.VERSION = "3.3.7", c.DEFAULTS = a.extend({}, a.fn.tooltip.Constructor.DEFAULTS, { placement: "right", trigger: "click", content: "", template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>' }), c.prototype = a.extend({}, a.fn.tooltip.Constructor.prototype), c.prototype.constructor = c, c.prototype.getDefaults = function () {
    return c.DEFAULTS;
  }, c.prototype.setContent = function () {
    var a = this.tip(),
        b = this.getTitle(),
        c = this.getContent();a.find(".popover-title")[this.options.html ? "html" : "text"](b), a.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof c ? "html" : "append" : "text"](c), a.removeClass("fade top bottom left right in"), a.find(".popover-title").html() || a.find(".popover-title").hide();
  }, c.prototype.hasContent = function () {
    return this.getTitle() || this.getContent();
  }, c.prototype.getContent = function () {
    var a = this.$element,
        b = this.options;return a.attr("data-content") || ("function" == typeof b.content ? b.content.call(a[0]) : b.content);
  }, c.prototype.arrow = function () {
    return this.$arrow = this.$arrow || this.tip().find(".arrow");
  };var d = a.fn.popover;a.fn.popover = b, a.fn.popover.Constructor = c, a.fn.popover.noConflict = function () {
    return a.fn.popover = d, this;
  };
}(jQuery), +function (a) {
  "use strict";
  function b(c, d) {
    this.$body = a(document.body), this.$scrollElement = a(a(c).is(document.body) ? window : c), this.options = a.extend({}, b.DEFAULTS, d), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", a.proxy(this.process, this)), this.refresh(), this.process();
  }function c(c) {
    return this.each(function () {
      var d = a(this),
          e = d.data("bs.scrollspy"),
          f = "object" == (typeof c === "undefined" ? "undefined" : _typeof(c)) && c;e || d.data("bs.scrollspy", e = new b(this, f)), "string" == typeof c && e[c]();
    });
  }b.VERSION = "3.3.7", b.DEFAULTS = { offset: 10 }, b.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight);
  }, b.prototype.refresh = function () {
    var b = this,
        c = "offset",
        d = 0;this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), a.isWindow(this.$scrollElement[0]) || (c = "position", d = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function () {
      var b = a(this),
          e = b.data("target") || b.attr("href"),
          f = /^#./.test(e) && a(e);return f && f.length && f.is(":visible") && [[f[c]().top + d, e]] || null;
    }).sort(function (a, b) {
      return a[0] - b[0];
    }).each(function () {
      b.offsets.push(this[0]), b.targets.push(this[1]);
    });
  }, b.prototype.process = function () {
    var a,
        b = this.$scrollElement.scrollTop() + this.options.offset,
        c = this.getScrollHeight(),
        d = this.options.offset + c - this.$scrollElement.height(),
        e = this.offsets,
        f = this.targets,
        g = this.activeTarget;if (this.scrollHeight != c && this.refresh(), b >= d) return g != (a = f[f.length - 1]) && this.activate(a);if (g && b < e[0]) return this.activeTarget = null, this.clear();for (a = e.length; a--;) {
      g != f[a] && b >= e[a] && (void 0 === e[a + 1] || b < e[a + 1]) && this.activate(f[a]);
    }
  }, b.prototype.activate = function (b) {
    this.activeTarget = b, this.clear();var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]',
        d = a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length && (d = d.closest("li.dropdown").addClass("active")), d.trigger("activate.bs.scrollspy");
  }, b.prototype.clear = function () {
    a(this.selector).parentsUntil(this.options.target, ".active").removeClass("active");
  };var d = a.fn.scrollspy;a.fn.scrollspy = c, a.fn.scrollspy.Constructor = b, a.fn.scrollspy.noConflict = function () {
    return a.fn.scrollspy = d, this;
  }, a(window).on("load.bs.scrollspy.data-api", function () {
    a('[data-spy="scroll"]').each(function () {
      var b = a(this);c.call(b, b.data());
    });
  });
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    return this.each(function () {
      var d = a(this),
          e = d.data("bs.tab");e || d.data("bs.tab", e = new c(this)), "string" == typeof b && e[b]();
    });
  }var c = function c(b) {
    this.element = a(b);
  };c.VERSION = "3.3.7", c.TRANSITION_DURATION = 150, c.prototype.show = function () {
    var b = this.element,
        c = b.closest("ul:not(.dropdown-menu)"),
        d = b.data("target");if (d || (d = b.attr("href"), d = d && d.replace(/.*(?=#[^\s]*$)/, "")), !b.parent("li").hasClass("active")) {
      var e = c.find(".active:last a"),
          f = a.Event("hide.bs.tab", { relatedTarget: b[0] }),
          g = a.Event("show.bs.tab", { relatedTarget: e[0] });if (e.trigger(f), b.trigger(g), !g.isDefaultPrevented() && !f.isDefaultPrevented()) {
        var h = a(d);this.activate(b.closest("li"), c), this.activate(h, h.parent(), function () {
          e.trigger({ type: "hidden.bs.tab", relatedTarget: b[0] }), b.trigger({ type: "shown.bs.tab", relatedTarget: e[0] });
        });
      }
    }
  }, c.prototype.activate = function (b, d, e) {
    function f() {
      g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), h ? (b[0].offsetWidth, b.addClass("in")) : b.removeClass("fade"), b.parent(".dropdown-menu").length && b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), e && e();
    }var g = d.find("> .active"),
        h = e && a.support.transition && (g.length && g.hasClass("fade") || !!d.find("> .fade").length);g.length && h ? g.one("bsTransitionEnd", f).emulateTransitionEnd(c.TRANSITION_DURATION) : f(), g.removeClass("in");
  };var d = a.fn.tab;a.fn.tab = b, a.fn.tab.Constructor = c, a.fn.tab.noConflict = function () {
    return a.fn.tab = d, this;
  };var e = function e(c) {
    c.preventDefault(), b.call(a(this), "show");
  };a(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', e).on("click.bs.tab.data-api", '[data-toggle="pill"]', e);
}(jQuery), +function (a) {
  "use strict";
  function b(b) {
    return this.each(function () {
      var d = a(this),
          e = d.data("bs.affix"),
          f = "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b;e || d.data("bs.affix", e = new c(this, f)), "string" == typeof b && e[b]();
    });
  }var c = function c(b, d) {
    this.options = a.extend({}, c.DEFAULTS, d), this.$target = a(this.options.target).on("scroll.bs.affix.data-api", a.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", a.proxy(this.checkPositionWithEventLoop, this)), this.$element = a(b), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition();
  };c.VERSION = "3.3.7", c.RESET = "affix affix-top affix-bottom", c.DEFAULTS = { offset: 0, target: window }, c.prototype.getState = function (a, b, c, d) {
    var e = this.$target.scrollTop(),
        f = this.$element.offset(),
        g = this.$target.height();if (null != c && "top" == this.affixed) return e < c && "top";if ("bottom" == this.affixed) return null != c ? !(e + this.unpin <= f.top) && "bottom" : !(e + g <= a - d) && "bottom";var h = null == this.affixed,
        i = h ? e : f.top,
        j = h ? g : b;return null != c && e <= c ? "top" : null != d && i + j >= a - d && "bottom";
  }, c.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset;this.$element.removeClass(c.RESET).addClass("affix");var a = this.$target.scrollTop(),
        b = this.$element.offset();return this.pinnedOffset = b.top - a;
  }, c.prototype.checkPositionWithEventLoop = function () {
    setTimeout(a.proxy(this.checkPosition, this), 1);
  }, c.prototype.checkPosition = function () {
    if (this.$element.is(":visible")) {
      var b = this.$element.height(),
          d = this.options.offset,
          e = d.top,
          f = d.bottom,
          g = Math.max(a(document).height(), a(document.body).height());"object" != (typeof d === "undefined" ? "undefined" : _typeof(d)) && (f = e = d), "function" == typeof e && (e = d.top(this.$element)), "function" == typeof f && (f = d.bottom(this.$element));var h = this.getState(g, b, e, f);if (this.affixed != h) {
        null != this.unpin && this.$element.css("top", "");var i = "affix" + (h ? "-" + h : ""),
            j = a.Event(i + ".bs.affix");if (this.$element.trigger(j), j.isDefaultPrevented()) return;this.affixed = h, this.unpin = "bottom" == h ? this.getPinnedOffset() : null, this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix", "affixed") + ".bs.affix");
      }"bottom" == h && this.$element.offset({ top: g - b - f });
    }
  };var d = a.fn.affix;a.fn.affix = b, a.fn.affix.Constructor = c, a.fn.affix.noConflict = function () {
    return a.fn.affix = d, this;
  }, a(window).on("load", function () {
    a('[data-spy="affix"]').each(function () {
      var c = a(this),
          d = c.data();d.offset = d.offset || {}, null != d.offsetBottom && (d.offset.bottom = d.offsetBottom), null != d.offsetTop && (d.offset.top = d.offsetTop), b.call(c, d);
    });
  });
}(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery, module) {(function () {
  var a,
      b,
      c,
      d,
      e,
      f,
      g,
      h,
      i = [].slice,
      j = {}.hasOwnProperty,
      k = function k(a, b) {
    function c() {
      this.constructor = a;
    }for (var d in b) {
      j.call(b, d) && (a[d] = b[d]);
    }return c.prototype = b.prototype, a.prototype = new c(), a.__super__ = b.prototype, a;
  };g = function g() {}, b = function () {
    function a() {}return a.prototype.addEventListener = a.prototype.on, a.prototype.on = function (a, b) {
      return this._callbacks = this._callbacks || {}, this._callbacks[a] || (this._callbacks[a] = []), this._callbacks[a].push(b), this;
    }, a.prototype.emit = function () {
      var a, b, c, d, e, f;if (d = arguments[0], a = 2 <= arguments.length ? i.call(arguments, 1) : [], this._callbacks = this._callbacks || {}, c = this._callbacks[d]) for (e = 0, f = c.length; f > e; e++) {
        b = c[e], b.apply(this, a);
      }return this;
    }, a.prototype.removeListener = a.prototype.off, a.prototype.removeAllListeners = a.prototype.off, a.prototype.removeEventListener = a.prototype.off, a.prototype.off = function (a, b) {
      var c, d, e, f, g;if (!this._callbacks || 0 === arguments.length) return this._callbacks = {}, this;if (d = this._callbacks[a], !d) return this;if (1 === arguments.length) return delete this._callbacks[a], this;for (e = f = 0, g = d.length; g > f; e = ++f) {
        if (c = d[e], c === b) {
          d.splice(e, 1);break;
        }
      }return this;
    }, a;
  }(), a = function (a) {
    function c(a, b) {
      var e, f, g;if (this.element = a, this.version = c.version, this.defaultOptions.previewTemplate = this.defaultOptions.previewTemplate.replace(/\n*/g, ""), this.clickableElements = [], this.listeners = [], this.files = [], "string" == typeof this.element && (this.element = document.querySelector(this.element)), !this.element || null == this.element.nodeType) throw new Error("Invalid dropzone element.");if (this.element.dropzone) throw new Error("Dropzone already attached.");if (c.instances.push(this), this.element.dropzone = this, e = null != (g = c.optionsForElement(this.element)) ? g : {}, this.options = d({}, this.defaultOptions, e, null != b ? b : {}), this.options.forceFallback || !c.isBrowserSupported()) return this.options.fallback.call(this);if (null == this.options.url && (this.options.url = this.element.getAttribute("action")), !this.options.url) throw new Error("No URL provided.");if (this.options.acceptedFiles && this.options.acceptedMimeTypes) throw new Error("You can't provide both 'acceptedFiles' and 'acceptedMimeTypes'. 'acceptedMimeTypes' is deprecated.");this.options.acceptedMimeTypes && (this.options.acceptedFiles = this.options.acceptedMimeTypes, delete this.options.acceptedMimeTypes), this.options.method = this.options.method.toUpperCase(), (f = this.getExistingFallback()) && f.parentNode && f.parentNode.removeChild(f), this.options.previewsContainer !== !1 && (this.previewsContainer = this.options.previewsContainer ? c.getElement(this.options.previewsContainer, "previewsContainer") : this.element), this.options.clickable && (this.clickableElements = this.options.clickable === !0 ? [this.element] : c.getElements(this.options.clickable, "clickable")), this.init();
    }var d, e;return k(c, a), c.prototype.Emitter = b, c.prototype.events = ["drop", "dragstart", "dragend", "dragenter", "dragover", "dragleave", "addedfile", "addedfiles", "removedfile", "thumbnail", "error", "errormultiple", "processing", "processingmultiple", "uploadprogress", "totaluploadprogress", "sending", "sendingmultiple", "success", "successmultiple", "canceled", "canceledmultiple", "complete", "completemultiple", "reset", "maxfilesexceeded", "maxfilesreached", "queuecomplete"], c.prototype.defaultOptions = { url: null, method: "post", withCredentials: !1, parallelUploads: 2, uploadMultiple: !1, maxFilesize: 256, paramName: "file", createImageThumbnails: !0, maxThumbnailFilesize: 10, thumbnailWidth: 120, thumbnailHeight: 120, filesizeBase: 1e3, maxFiles: null, params: {}, clickable: !0, ignoreHiddenFiles: !0, acceptedFiles: null, acceptedMimeTypes: null, autoProcessQueue: !0, autoQueue: !0, addRemoveLinks: !1, previewsContainer: null, hiddenInputContainer: "body", capture: null, renameFilename: null, dictDefaultMessage: "Drop files here to upload", dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.", dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.", dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.", dictInvalidFileType: "You can't upload files of this type.", dictResponseError: "Server responded with {{statusCode}} code.", dictCancelUpload: "Cancel upload", dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?", dictRemoveFile: "Remove file", dictRemoveFileConfirmation: null, dictMaxFilesExceeded: "You can not upload any more files.", accept: function accept(a, b) {
        return b();
      }, init: function init() {
        return g;
      }, forceFallback: !1, fallback: function fallback() {
        var a, b, d, e, f, g;for (this.element.className = "" + this.element.className + " dz-browser-not-supported", g = this.element.getElementsByTagName("div"), e = 0, f = g.length; f > e; e++) {
          a = g[e], /(^| )dz-message($| )/.test(a.className) && (b = a, a.className = "dz-message");
        }return b || (b = c.createElement('<div class="dz-message"><span></span></div>'), this.element.appendChild(b)), d = b.getElementsByTagName("span")[0], d && (null != d.textContent ? d.textContent = this.options.dictFallbackMessage : null != d.innerText && (d.innerText = this.options.dictFallbackMessage)), this.element.appendChild(this.getFallbackForm());
      }, resize: function resize(a) {
        var b, c, d;return b = { srcX: 0, srcY: 0, srcWidth: a.width, srcHeight: a.height }, c = a.width / a.height, b.optWidth = this.options.thumbnailWidth, b.optHeight = this.options.thumbnailHeight, null == b.optWidth && null == b.optHeight ? (b.optWidth = b.srcWidth, b.optHeight = b.srcHeight) : null == b.optWidth ? b.optWidth = c * b.optHeight : null == b.optHeight && (b.optHeight = 1 / c * b.optWidth), d = b.optWidth / b.optHeight, a.height < b.optHeight || a.width < b.optWidth ? (b.trgHeight = b.srcHeight, b.trgWidth = b.srcWidth) : c > d ? (b.srcHeight = a.height, b.srcWidth = b.srcHeight * d) : (b.srcWidth = a.width, b.srcHeight = b.srcWidth / d), b.srcX = (a.width - b.srcWidth) / 2, b.srcY = (a.height - b.srcHeight) / 2, b;
      }, drop: function drop() {
        return this.element.classList.remove("dz-drag-hover");
      }, dragstart: g, dragend: function dragend() {
        return this.element.classList.remove("dz-drag-hover");
      }, dragenter: function dragenter() {
        return this.element.classList.add("dz-drag-hover");
      }, dragover: function dragover() {
        return this.element.classList.add("dz-drag-hover");
      }, dragleave: function dragleave() {
        return this.element.classList.remove("dz-drag-hover");
      }, paste: g, reset: function reset() {
        return this.element.classList.remove("dz-started");
      }, addedfile: function addedfile(a) {
        var b, d, e, f, g, h, i, j, k, l, m, n, o;if (this.element === this.previewsContainer && this.element.classList.add("dz-started"), this.previewsContainer) {
          for (a.previewElement = c.createElement(this.options.previewTemplate.trim()), a.previewTemplate = a.previewElement, this.previewsContainer.appendChild(a.previewElement), l = a.previewElement.querySelectorAll("[data-dz-name]"), f = 0, i = l.length; i > f; f++) {
            b = l[f], b.textContent = this._renameFilename(a.name);
          }for (m = a.previewElement.querySelectorAll("[data-dz-size]"), g = 0, j = m.length; j > g; g++) {
            b = m[g], b.innerHTML = this.filesize(a.size);
          }for (this.options.addRemoveLinks && (a._removeLink = c.createElement('<a class="dz-remove" href="javascript:undefined;" data-dz-remove>' + this.options.dictRemoveFile + "</a>"), a.previewElement.appendChild(a._removeLink)), d = function (b) {
            return function (d) {
              return d.preventDefault(), d.stopPropagation(), a.status === c.UPLOADING ? c.confirm(b.options.dictCancelUploadConfirmation, function () {
                return b.removeFile(a);
              }) : b.options.dictRemoveFileConfirmation ? c.confirm(b.options.dictRemoveFileConfirmation, function () {
                return b.removeFile(a);
              }) : b.removeFile(a);
            };
          }(this), n = a.previewElement.querySelectorAll("[data-dz-remove]"), o = [], h = 0, k = n.length; k > h; h++) {
            e = n[h], o.push(e.addEventListener("click", d));
          }return o;
        }
      }, removedfile: function removedfile(a) {
        var b;return a.previewElement && null != (b = a.previewElement) && b.parentNode.removeChild(a.previewElement), this._updateMaxFilesReachedClass();
      }, thumbnail: function thumbnail(a, b) {
        var c, d, e, f;if (a.previewElement) {
          for (a.previewElement.classList.remove("dz-file-preview"), f = a.previewElement.querySelectorAll("[data-dz-thumbnail]"), d = 0, e = f.length; e > d; d++) {
            c = f[d], c.alt = a.name, c.src = b;
          }return setTimeout(function () {
            return function () {
              return a.previewElement.classList.add("dz-image-preview");
            };
          }(this), 1);
        }
      }, error: function error(a, b) {
        var c, d, e, f, g;if (a.previewElement) {
          for (a.previewElement.classList.add("dz-error"), "String" != typeof b && b.error && (b = b.error), f = a.previewElement.querySelectorAll("[data-dz-errormessage]"), g = [], d = 0, e = f.length; e > d; d++) {
            c = f[d], g.push(c.textContent = b);
          }return g;
        }
      }, errormultiple: g, processing: function processing(a) {
        return a.previewElement && (a.previewElement.classList.add("dz-processing"), a._removeLink) ? a._removeLink.textContent = this.options.dictCancelUpload : void 0;
      }, processingmultiple: g, uploadprogress: function uploadprogress(a, b) {
        var c, d, e, f, g;if (a.previewElement) {
          for (f = a.previewElement.querySelectorAll("[data-dz-uploadprogress]"), g = [], d = 0, e = f.length; e > d; d++) {
            c = f[d], g.push("PROGRESS" === c.nodeName ? c.value = b : c.style.width = "" + b + "%");
          }return g;
        }
      }, totaluploadprogress: g, sending: g, sendingmultiple: g, success: function success(a) {
        return a.previewElement ? a.previewElement.classList.add("dz-success") : void 0;
      }, successmultiple: g, canceled: function canceled(a) {
        return this.emit("error", a, "Upload canceled.");
      }, canceledmultiple: g, complete: function complete(a) {
        return a._removeLink && (a._removeLink.textContent = this.options.dictRemoveFile), a.previewElement ? a.previewElement.classList.add("dz-complete") : void 0;
      }, completemultiple: g, maxfilesexceeded: g, maxfilesreached: g, queuecomplete: g, addedfiles: g, previewTemplate: '<div class="dz-preview dz-file-preview">\n  <div class="dz-image"><img data-dz-thumbnail /></div>\n  <div class="dz-details">\n    <div class="dz-size"><span data-dz-size></span></div>\n    <div class="dz-filename"><span data-dz-name></span></div>\n  </div>\n  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n  <div class="dz-error-message"><span data-dz-errormessage></span></div>\n  <div class="dz-success-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Check</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>\n      </g>\n    </svg>\n  </div>\n  <div class="dz-error-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Error</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">\n          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n</div>' }, d = function d() {
      var a, b, c, d, e, f, g;for (d = arguments[0], c = 2 <= arguments.length ? i.call(arguments, 1) : [], f = 0, g = c.length; g > f; f++) {
        b = c[f];for (a in b) {
          e = b[a], d[a] = e;
        }
      }return d;
    }, c.prototype.getAcceptedFiles = function () {
      var a, b, c, d, e;for (d = this.files, e = [], b = 0, c = d.length; c > b; b++) {
        a = d[b], a.accepted && e.push(a);
      }return e;
    }, c.prototype.getRejectedFiles = function () {
      var a, b, c, d, e;for (d = this.files, e = [], b = 0, c = d.length; c > b; b++) {
        a = d[b], a.accepted || e.push(a);
      }return e;
    }, c.prototype.getFilesWithStatus = function (a) {
      var b, c, d, e, f;for (e = this.files, f = [], c = 0, d = e.length; d > c; c++) {
        b = e[c], b.status === a && f.push(b);
      }return f;
    }, c.prototype.getQueuedFiles = function () {
      return this.getFilesWithStatus(c.QUEUED);
    }, c.prototype.getUploadingFiles = function () {
      return this.getFilesWithStatus(c.UPLOADING);
    }, c.prototype.getAddedFiles = function () {
      return this.getFilesWithStatus(c.ADDED);
    }, c.prototype.getActiveFiles = function () {
      var a, b, d, e, f;for (e = this.files, f = [], b = 0, d = e.length; d > b; b++) {
        a = e[b], (a.status === c.UPLOADING || a.status === c.QUEUED) && f.push(a);
      }return f;
    }, c.prototype.init = function () {
      var a, b, d, e, f, g, h;for ("form" === this.element.tagName && this.element.setAttribute("enctype", "multipart/form-data"), this.element.classList.contains("dropzone") && !this.element.querySelector(".dz-message") && this.element.appendChild(c.createElement('<div class="dz-default dz-message"><span>' + this.options.dictDefaultMessage + "</span></div>")), this.clickableElements.length && (d = function (a) {
        return function () {
          return a.hiddenFileInput && a.hiddenFileInput.parentNode.removeChild(a.hiddenFileInput), a.hiddenFileInput = document.createElement("input"), a.hiddenFileInput.setAttribute("type", "file"), (null == a.options.maxFiles || a.options.maxFiles > 1) && a.hiddenFileInput.setAttribute("multiple", "multiple"), a.hiddenFileInput.className = "dz-hidden-input", null != a.options.acceptedFiles && a.hiddenFileInput.setAttribute("accept", a.options.acceptedFiles), null != a.options.capture && a.hiddenFileInput.setAttribute("capture", a.options.capture), a.hiddenFileInput.style.visibility = "hidden", a.hiddenFileInput.style.position = "absolute", a.hiddenFileInput.style.top = "0", a.hiddenFileInput.style.left = "0", a.hiddenFileInput.style.height = "0", a.hiddenFileInput.style.width = "0", document.querySelector(a.options.hiddenInputContainer).appendChild(a.hiddenFileInput), a.hiddenFileInput.addEventListener("change", function () {
            var b, c, e, f;if (c = a.hiddenFileInput.files, c.length) for (e = 0, f = c.length; f > e; e++) {
              b = c[e], a.addFile(b);
            }return a.emit("addedfiles", c), d();
          });
        };
      }(this))(), this.URL = null != (g = window.URL) ? g : window.webkitURL, h = this.events, e = 0, f = h.length; f > e; e++) {
        a = h[e], this.on(a, this.options[a]);
      }return this.on("uploadprogress", function (a) {
        return function () {
          return a.updateTotalUploadProgress();
        };
      }(this)), this.on("removedfile", function (a) {
        return function () {
          return a.updateTotalUploadProgress();
        };
      }(this)), this.on("canceled", function (a) {
        return function (b) {
          return a.emit("complete", b);
        };
      }(this)), this.on("complete", function (a) {
        return function () {
          return 0 === a.getAddedFiles().length && 0 === a.getUploadingFiles().length && 0 === a.getQueuedFiles().length ? setTimeout(function () {
            return a.emit("queuecomplete");
          }, 0) : void 0;
        };
      }(this)), b = function b(a) {
        return a.stopPropagation(), a.preventDefault ? a.preventDefault() : a.returnValue = !1;
      }, this.listeners = [{ element: this.element, events: { dragstart: function (a) {
            return function (b) {
              return a.emit("dragstart", b);
            };
          }(this), dragenter: function (a) {
            return function (c) {
              return b(c), a.emit("dragenter", c);
            };
          }(this), dragover: function (a) {
            return function (c) {
              var d;try {
                d = c.dataTransfer.effectAllowed;
              } catch (e) {}return c.dataTransfer.dropEffect = "move" === d || "linkMove" === d ? "move" : "copy", b(c), a.emit("dragover", c);
            };
          }(this), dragleave: function (a) {
            return function (b) {
              return a.emit("dragleave", b);
            };
          }(this), drop: function (a) {
            return function (c) {
              return b(c), a.drop(c);
            };
          }(this), dragend: function (a) {
            return function (b) {
              return a.emit("dragend", b);
            };
          }(this) } }], this.clickableElements.forEach(function (a) {
        return function (b) {
          return a.listeners.push({ element: b, events: { click: function click(d) {
                return (b !== a.element || d.target === a.element || c.elementInside(d.target, a.element.querySelector(".dz-message"))) && a.hiddenFileInput.click(), !0;
              } } });
        };
      }(this)), this.enable(), this.options.init.call(this);
    }, c.prototype.destroy = function () {
      var a;return this.disable(), this.removeAllFiles(!0), (null != (a = this.hiddenFileInput) ? a.parentNode : void 0) && (this.hiddenFileInput.parentNode.removeChild(this.hiddenFileInput), this.hiddenFileInput = null), delete this.element.dropzone, c.instances.splice(c.instances.indexOf(this), 1);
    }, c.prototype.updateTotalUploadProgress = function () {
      var a, b, c, d, e, f, g, h;if (d = 0, c = 0, a = this.getActiveFiles(), a.length) {
        for (h = this.getActiveFiles(), f = 0, g = h.length; g > f; f++) {
          b = h[f], d += b.upload.bytesSent, c += b.upload.total;
        }e = 100 * d / c;
      } else e = 100;return this.emit("totaluploadprogress", e, c, d);
    }, c.prototype._getParamName = function (a) {
      return "function" == typeof this.options.paramName ? this.options.paramName(a) : "" + this.options.paramName + (this.options.uploadMultiple ? "[" + a + "]" : "");
    }, c.prototype._renameFilename = function (a) {
      return "function" != typeof this.options.renameFilename ? a : this.options.renameFilename(a);
    }, c.prototype.getFallbackForm = function () {
      var a, b, d, e;return (a = this.getExistingFallback()) ? a : (d = '<div class="dz-fallback">', this.options.dictFallbackText && (d += "<p>" + this.options.dictFallbackText + "</p>"), d += '<input type="file" name="' + this._getParamName(0) + '" ' + (this.options.uploadMultiple ? 'multiple="multiple"' : void 0) + ' /><input type="submit" value="Upload!"></div>', b = c.createElement(d), "FORM" !== this.element.tagName ? (e = c.createElement('<form action="' + this.options.url + '" enctype="multipart/form-data" method="' + this.options.method + '"></form>'), e.appendChild(b)) : (this.element.setAttribute("enctype", "multipart/form-data"), this.element.setAttribute("method", this.options.method)), null != e ? e : b);
    }, c.prototype.getExistingFallback = function () {
      var a, b, c, d, e, f;for (b = function b(a) {
        var b, c, d;for (c = 0, d = a.length; d > c; c++) {
          if (b = a[c], /(^| )fallback($| )/.test(b.className)) return b;
        }
      }, f = ["div", "form"], d = 0, e = f.length; e > d; d++) {
        if (c = f[d], a = b(this.element.getElementsByTagName(c))) return a;
      }
    }, c.prototype.setupEventListeners = function () {
      var a, b, c, d, e, f, g;for (f = this.listeners, g = [], d = 0, e = f.length; e > d; d++) {
        a = f[d], g.push(function () {
          var d, e;d = a.events, e = [];for (b in d) {
            c = d[b], e.push(a.element.addEventListener(b, c, !1));
          }return e;
        }());
      }return g;
    }, c.prototype.removeEventListeners = function () {
      var a, b, c, d, e, f, g;for (f = this.listeners, g = [], d = 0, e = f.length; e > d; d++) {
        a = f[d], g.push(function () {
          var d, e;d = a.events, e = [];for (b in d) {
            c = d[b], e.push(a.element.removeEventListener(b, c, !1));
          }return e;
        }());
      }return g;
    }, c.prototype.disable = function () {
      var a, b, c, d, e;for (this.clickableElements.forEach(function (a) {
        return a.classList.remove("dz-clickable");
      }), this.removeEventListeners(), d = this.files, e = [], b = 0, c = d.length; c > b; b++) {
        a = d[b], e.push(this.cancelUpload(a));
      }return e;
    }, c.prototype.enable = function () {
      return this.clickableElements.forEach(function (a) {
        return a.classList.add("dz-clickable");
      }), this.setupEventListeners();
    }, c.prototype.filesize = function (a) {
      var b, c, d, e, f, g, h, i;if (d = 0, e = "b", a > 0) {
        for (g = ["TB", "GB", "MB", "KB", "b"], c = h = 0, i = g.length; i > h; c = ++h) {
          if (f = g[c], b = Math.pow(this.options.filesizeBase, 4 - c) / 10, a >= b) {
            d = a / Math.pow(this.options.filesizeBase, 4 - c), e = f;break;
          }
        }d = Math.round(10 * d) / 10;
      }return "<strong>" + d + "</strong> " + e;
    }, c.prototype._updateMaxFilesReachedClass = function () {
      return null != this.options.maxFiles && this.getAcceptedFiles().length >= this.options.maxFiles ? (this.getAcceptedFiles().length === this.options.maxFiles && this.emit("maxfilesreached", this.files), this.element.classList.add("dz-max-files-reached")) : this.element.classList.remove("dz-max-files-reached");
    }, c.prototype.drop = function (a) {
      var b, c;a.dataTransfer && (this.emit("drop", a), b = a.dataTransfer.files, this.emit("addedfiles", b), b.length && (c = a.dataTransfer.items, c && c.length && null != c[0].webkitGetAsEntry ? this._addFilesFromItems(c) : this.handleFiles(b)));
    }, c.prototype.paste = function (a) {
      var b, c;if (null != (null != a && null != (c = a.clipboardData) ? c.items : void 0)) return this.emit("paste", a), b = a.clipboardData.items, b.length ? this._addFilesFromItems(b) : void 0;
    }, c.prototype.handleFiles = function (a) {
      var b, c, d, e;for (e = [], c = 0, d = a.length; d > c; c++) {
        b = a[c], e.push(this.addFile(b));
      }return e;
    }, c.prototype._addFilesFromItems = function (a) {
      var b, c, d, e, f;for (f = [], d = 0, e = a.length; e > d; d++) {
        c = a[d], f.push(null != c.webkitGetAsEntry && (b = c.webkitGetAsEntry()) ? b.isFile ? this.addFile(c.getAsFile()) : b.isDirectory ? this._addFilesFromDirectory(b, b.name) : void 0 : null != c.getAsFile ? null == c.kind || "file" === c.kind ? this.addFile(c.getAsFile()) : void 0 : void 0);
      }return f;
    }, c.prototype._addFilesFromDirectory = function (a, b) {
      var c, d, e;return c = a.createReader(), d = function d(a) {
        return "undefined" != typeof console && null !== console && "function" == typeof console.log ? console.log(a) : void 0;
      }, (e = function (a) {
        return function () {
          return c.readEntries(function (c) {
            var d, f, g;if (c.length > 0) {
              for (f = 0, g = c.length; g > f; f++) {
                d = c[f], d.isFile ? d.file(function (c) {
                  return a.options.ignoreHiddenFiles && "." === c.name.substring(0, 1) ? void 0 : (c.fullPath = "" + b + "/" + c.name, a.addFile(c));
                }) : d.isDirectory && a._addFilesFromDirectory(d, "" + b + "/" + d.name);
              }e();
            }return null;
          }, d);
        };
      }(this))();
    }, c.prototype.accept = function (a, b) {
      return a.size > 1024 * this.options.maxFilesize * 1024 ? b(this.options.dictFileTooBig.replace("{{filesize}}", Math.round(a.size / 1024 / 10.24) / 100).replace("{{maxFilesize}}", this.options.maxFilesize)) : c.isValidFile(a, this.options.acceptedFiles) ? null != this.options.maxFiles && this.getAcceptedFiles().length >= this.options.maxFiles ? (b(this.options.dictMaxFilesExceeded.replace("{{maxFiles}}", this.options.maxFiles)), this.emit("maxfilesexceeded", a)) : this.options.accept.call(this, a, b) : b(this.options.dictInvalidFileType);
    }, c.prototype.addFile = function (a) {
      return a.upload = { progress: 0, total: a.size, bytesSent: 0 }, this.files.push(a), a.status = c.ADDED, this.emit("addedfile", a), this._enqueueThumbnail(a), this.accept(a, function (b) {
        return function (c) {
          return c ? (a.accepted = !1, b._errorProcessing([a], c)) : (a.accepted = !0, b.options.autoQueue && b.enqueueFile(a)), b._updateMaxFilesReachedClass();
        };
      }(this));
    }, c.prototype.enqueueFiles = function (a) {
      var b, c, d;for (c = 0, d = a.length; d > c; c++) {
        b = a[c], this.enqueueFile(b);
      }return null;
    }, c.prototype.enqueueFile = function (a) {
      if (a.status !== c.ADDED || a.accepted !== !0) throw new Error("This file can't be queued because it has already been processed or was rejected.");return a.status = c.QUEUED, this.options.autoProcessQueue ? setTimeout(function (a) {
        return function () {
          return a.processQueue();
        };
      }(this), 0) : void 0;
    }, c.prototype._thumbnailQueue = [], c.prototype._processingThumbnail = !1, c.prototype._enqueueThumbnail = function (a) {
      return this.options.createImageThumbnails && a.type.match(/image.*/) && a.size <= 1024 * this.options.maxThumbnailFilesize * 1024 ? (this._thumbnailQueue.push(a), setTimeout(function (a) {
        return function () {
          return a._processThumbnailQueue();
        };
      }(this), 0)) : void 0;
    }, c.prototype._processThumbnailQueue = function () {
      return this._processingThumbnail || 0 === this._thumbnailQueue.length ? void 0 : (this._processingThumbnail = !0, this.createThumbnail(this._thumbnailQueue.shift(), function (a) {
        return function () {
          return a._processingThumbnail = !1, a._processThumbnailQueue();
        };
      }(this)));
    }, c.prototype.removeFile = function (a) {
      return a.status === c.UPLOADING && this.cancelUpload(a), this.files = h(this.files, a), this.emit("removedfile", a), 0 === this.files.length ? this.emit("reset") : void 0;
    }, c.prototype.removeAllFiles = function (a) {
      var b, d, e, f;for (null == a && (a = !1), f = this.files.slice(), d = 0, e = f.length; e > d; d++) {
        b = f[d], (b.status !== c.UPLOADING || a) && this.removeFile(b);
      }return null;
    }, c.prototype.createThumbnail = function (a, b) {
      var c;return c = new FileReader(), c.onload = function (d) {
        return function () {
          return "image/svg+xml" === a.type ? (d.emit("thumbnail", a, c.result), void (null != b && b())) : d.createThumbnailFromUrl(a, c.result, b);
        };
      }(this), c.readAsDataURL(a);
    }, c.prototype.createThumbnailFromUrl = function (a, b, c, d) {
      var e;return e = document.createElement("img"), d && (e.crossOrigin = d), e.onload = function (b) {
        return function () {
          var d, g, h, i, j, k, l, m;return a.width = e.width, a.height = e.height, h = b.options.resize.call(b, a), null == h.trgWidth && (h.trgWidth = h.optWidth), null == h.trgHeight && (h.trgHeight = h.optHeight), d = document.createElement("canvas"), g = d.getContext("2d"), d.width = h.trgWidth, d.height = h.trgHeight, f(g, e, null != (j = h.srcX) ? j : 0, null != (k = h.srcY) ? k : 0, h.srcWidth, h.srcHeight, null != (l = h.trgX) ? l : 0, null != (m = h.trgY) ? m : 0, h.trgWidth, h.trgHeight), i = d.toDataURL("image/png"), b.emit("thumbnail", a, i), null != c ? c() : void 0;
        };
      }(this), null != c && (e.onerror = c), e.src = b;
    }, c.prototype.processQueue = function () {
      var a, b, c, d;if (b = this.options.parallelUploads, c = this.getUploadingFiles().length, a = c, !(c >= b) && (d = this.getQueuedFiles(), d.length > 0)) {
        if (this.options.uploadMultiple) return this.processFiles(d.slice(0, b - c));for (; b > a;) {
          if (!d.length) return;this.processFile(d.shift()), a++;
        }
      }
    }, c.prototype.processFile = function (a) {
      return this.processFiles([a]);
    }, c.prototype.processFiles = function (a) {
      var b, d, e;for (d = 0, e = a.length; e > d; d++) {
        b = a[d], b.processing = !0, b.status = c.UPLOADING, this.emit("processing", b);
      }return this.options.uploadMultiple && this.emit("processingmultiple", a), this.uploadFiles(a);
    }, c.prototype._getFilesWithXhr = function (a) {
      var b, c;return c = function () {
        var c, d, e, f;for (e = this.files, f = [], c = 0, d = e.length; d > c; c++) {
          b = e[c], b.xhr === a && f.push(b);
        }return f;
      }.call(this);
    }, c.prototype.cancelUpload = function (a) {
      var b, d, e, f, g, h, i;if (a.status === c.UPLOADING) {
        for (d = this._getFilesWithXhr(a.xhr), e = 0, g = d.length; g > e; e++) {
          b = d[e], b.status = c.CANCELED;
        }for (a.xhr.abort(), f = 0, h = d.length; h > f; f++) {
          b = d[f], this.emit("canceled", b);
        }this.options.uploadMultiple && this.emit("canceledmultiple", d);
      } else ((i = a.status) === c.ADDED || i === c.QUEUED) && (a.status = c.CANCELED, this.emit("canceled", a), this.options.uploadMultiple && this.emit("canceledmultiple", [a]));return this.options.autoProcessQueue ? this.processQueue() : void 0;
    }, e = function e() {
      var a, b;return b = arguments[0], a = 2 <= arguments.length ? i.call(arguments, 1) : [], "function" == typeof b ? b.apply(this, a) : b;
    }, c.prototype.uploadFile = function (a) {
      return this.uploadFiles([a]);
    }, c.prototype.uploadFiles = function (a) {
      var b, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L;for (w = new XMLHttpRequest(), x = 0, B = a.length; B > x; x++) {
        b = a[x], b.xhr = w;
      }p = e(this.options.method, a), u = e(this.options.url, a), w.open(p, u, !0), w.withCredentials = !!this.options.withCredentials, s = null, g = function (c) {
        return function () {
          var d, e, f;for (f = [], d = 0, e = a.length; e > d; d++) {
            b = a[d], f.push(c._errorProcessing(a, s || c.options.dictResponseError.replace("{{statusCode}}", w.status), w));
          }return f;
        };
      }(this), t = function (c) {
        return function (d) {
          var e, f, g, h, i, j, k, l, m;if (null != d) for (f = 100 * d.loaded / d.total, g = 0, j = a.length; j > g; g++) {
            b = a[g], b.upload = { progress: f, total: d.total, bytesSent: d.loaded };
          } else {
            for (e = !0, f = 100, h = 0, k = a.length; k > h; h++) {
              b = a[h], (100 !== b.upload.progress || b.upload.bytesSent !== b.upload.total) && (e = !1), b.upload.progress = f, b.upload.bytesSent = b.upload.total;
            }if (e) return;
          }for (m = [], i = 0, l = a.length; l > i; i++) {
            b = a[i], m.push(c.emit("uploadprogress", b, f, b.upload.bytesSent));
          }return m;
        };
      }(this), w.onload = function (b) {
        return function (d) {
          var e;if (a[0].status !== c.CANCELED && 4 === w.readyState) {
            if (s = w.responseText, w.getResponseHeader("content-type") && ~w.getResponseHeader("content-type").indexOf("application/json")) try {
              s = JSON.parse(s);
            } catch (f) {
              d = f, s = "Invalid JSON response from server.";
            }return t(), 200 <= (e = w.status) && 300 > e ? b._finished(a, s, d) : g();
          }
        };
      }(this), w.onerror = function () {
        return function () {
          return a[0].status !== c.CANCELED ? g() : void 0;
        };
      }(this), r = null != (G = w.upload) ? G : w, r.onprogress = t, j = { Accept: "application/json", "Cache-Control": "no-cache", "X-Requested-With": "XMLHttpRequest" }, this.options.headers && d(j, this.options.headers);for (h in j) {
        i = j[h], i && w.setRequestHeader(h, i);
      }if (f = new FormData(), this.options.params) {
        H = this.options.params;for (o in H) {
          v = H[o], f.append(o, v);
        }
      }for (y = 0, C = a.length; C > y; y++) {
        b = a[y], this.emit("sending", b, w, f);
      }if (this.options.uploadMultiple && this.emit("sendingmultiple", a, w, f), "FORM" === this.element.tagName) for (I = this.element.querySelectorAll("input, textarea, select, button"), z = 0, D = I.length; D > z; z++) {
        if (l = I[z], m = l.getAttribute("name"), n = l.getAttribute("type"), "SELECT" === l.tagName && l.hasAttribute("multiple")) for (J = l.options, A = 0, E = J.length; E > A; A++) {
          q = J[A], q.selected && f.append(m, q.value);
        } else (!n || "checkbox" !== (K = n.toLowerCase()) && "radio" !== K || l.checked) && f.append(m, l.value);
      }for (k = F = 0, L = a.length - 1; L >= 0 ? L >= F : F >= L; k = L >= 0 ? ++F : --F) {
        f.append(this._getParamName(k), a[k], this._renameFilename(a[k].name));
      }return this.submitRequest(w, f, a);
    }, c.prototype.submitRequest = function (a, b) {
      return a.send(b);
    }, c.prototype._finished = function (a, b, d) {
      var e, f, g;for (f = 0, g = a.length; g > f; f++) {
        e = a[f], e.status = c.SUCCESS, this.emit("success", e, b, d), this.emit("complete", e);
      }return this.options.uploadMultiple && (this.emit("successmultiple", a, b, d), this.emit("completemultiple", a)), this.options.autoProcessQueue ? this.processQueue() : void 0;
    }, c.prototype._errorProcessing = function (a, b, d) {
      var e, f, g;for (f = 0, g = a.length; g > f; f++) {
        e = a[f], e.status = c.ERROR, this.emit("error", e, b, d), this.emit("complete", e);
      }return this.options.uploadMultiple && (this.emit("errormultiple", a, b, d), this.emit("completemultiple", a)), this.options.autoProcessQueue ? this.processQueue() : void 0;
    }, c;
  }(b), a.version = "4.3.0", a.options = {}, a.optionsForElement = function (b) {
    return b.getAttribute("id") ? a.options[c(b.getAttribute("id"))] : void 0;
  }, a.instances = [], a.forElement = function (a) {
    if ("string" == typeof a && (a = document.querySelector(a)), null == (null != a ? a.dropzone : void 0)) throw new Error("No Dropzone found for given element. This is probably because you're trying to access it before Dropzone had the time to initialize. Use the `init` option to setup any additional observers on your Dropzone.");return a.dropzone;
  }, a.autoDiscover = !0, a.discover = function () {
    var b, c, d, e, f, g;for (document.querySelectorAll ? d = document.querySelectorAll(".dropzone") : (d = [], b = function b(a) {
      var b, c, e, f;for (f = [], c = 0, e = a.length; e > c; c++) {
        b = a[c], f.push(/(^| )dropzone($| )/.test(b.className) ? d.push(b) : void 0);
      }return f;
    }, b(document.getElementsByTagName("div")), b(document.getElementsByTagName("form"))), g = [], e = 0, f = d.length; f > e; e++) {
      c = d[e], g.push(a.optionsForElement(c) !== !1 ? new a(c) : void 0);
    }return g;
  }, a.blacklistedBrowsers = [/opera.*Macintosh.*version\/12/i], a.isBrowserSupported = function () {
    var b, c, d, e, f;if (b = !0, window.File && window.FileReader && window.FileList && window.Blob && window.FormData && document.querySelector) {
      if ("classList" in document.createElement("a")) for (f = a.blacklistedBrowsers, d = 0, e = f.length; e > d; d++) {
        c = f[d], c.test(navigator.userAgent) && (b = !1);
      } else b = !1;
    } else b = !1;return b;
  }, h = function h(a, b) {
    var c, d, e, f;for (f = [], d = 0, e = a.length; e > d; d++) {
      c = a[d], c !== b && f.push(c);
    }return f;
  }, c = function c(a) {
    return a.replace(/[\-_](\w)/g, function (a) {
      return a.charAt(1).toUpperCase();
    });
  }, a.createElement = function (a) {
    var b;return b = document.createElement("div"), b.innerHTML = a, b.childNodes[0];
  }, a.elementInside = function (a, b) {
    if (a === b) return !0;for (; a = a.parentNode;) {
      if (a === b) return !0;
    }return !1;
  }, a.getElement = function (a, b) {
    var c;if ("string" == typeof a ? c = document.querySelector(a) : null != a.nodeType && (c = a), null == c) throw new Error("Invalid `" + b + "` option provided. Please provide a CSS selector or a plain HTML element.");return c;
  }, a.getElements = function (a, b) {
    var c, d, e, f, g, h, i, j;if (a instanceof Array) {
      e = [];try {
        for (f = 0, h = a.length; h > f; f++) {
          d = a[f], e.push(this.getElement(d, b));
        }
      } catch (k) {
        c = k, e = null;
      }
    } else if ("string" == typeof a) for (e = [], j = document.querySelectorAll(a), g = 0, i = j.length; i > g; g++) {
      d = j[g], e.push(d);
    } else null != a.nodeType && (e = [a]);if (null == e || !e.length) throw new Error("Invalid `" + b + "` option provided. Please provide a CSS selector, a plain HTML element or a list of those.");return e;
  }, a.confirm = function (a, b, c) {
    return window.confirm(a) ? b() : null != c ? c() : void 0;
  }, a.isValidFile = function (a, b) {
    var c, d, e, f, g;if (!b) return !0;for (b = b.split(","), d = a.type, c = d.replace(/\/.*$/, ""), f = 0, g = b.length; g > f; f++) {
      if (e = b[f], e = e.trim(), "." === e.charAt(0)) {
        if (-1 !== a.name.toLowerCase().indexOf(e.toLowerCase(), a.name.length - e.length)) return !0;
      } else if (/\/\*$/.test(e)) {
        if (c === e.replace(/\/.*$/, "")) return !0;
      } else if (d === e) return !0;
    }return !1;
  }, "undefined" != typeof jQuery && null !== jQuery && (jQuery.fn.dropzone = function (b) {
    return this.each(function () {
      return new a(this, b);
    });
  }), "undefined" != typeof module && null !== module ? module.exports = a : window.Dropzone = a, a.ADDED = "added", a.QUEUED = "queued", a.ACCEPTED = a.QUEUED, a.UPLOADING = "uploading", a.PROCESSING = a.UPLOADING, a.CANCELED = "canceled", a.ERROR = "error", a.SUCCESS = "success", e = function e(a) {
    var b, c, d, e, f, g, h, i, j, k;for (h = a.naturalWidth, g = a.naturalHeight, c = document.createElement("canvas"), c.width = 1, c.height = g, d = c.getContext("2d"), d.drawImage(a, 0, 0), e = d.getImageData(0, 0, 1, g).data, k = 0, f = g, i = g; i > k;) {
      b = e[4 * (i - 1) + 3], 0 === b ? f = i : k = i, i = f + k >> 1;
    }return j = i / g, 0 === j ? 1 : j;
  }, f = function f(a, b, c, d, _f, g, h, i, j, k) {
    var l;return l = e(b), a.drawImage(b, c, d, _f, g, h, i, j, k / l);
  }, d = function d(a, b) {
    var c, d, e, _f2, _g, h, i, j, k;if (e = !1, k = !0, d = a.document, j = d.documentElement, c = d.addEventListener ? "addEventListener" : "attachEvent", i = d.addEventListener ? "removeEventListener" : "detachEvent", h = d.addEventListener ? "" : "on", _f2 = function f(c) {
      return "readystatechange" !== c.type || "complete" === d.readyState ? (("load" === c.type ? a : d)[i](h + c.type, _f2, !1), !e && (e = !0) ? b.call(a, c.type || c) : void 0) : void 0;
    }, _g = function g() {
      var a;try {
        j.doScroll("left");
      } catch (b) {
        return a = b, void setTimeout(_g, 50);
      }return _f2("poll");
    }, "complete" !== d.readyState) {
      if (d.createEventObject && j.doScroll) {
        try {
          k = !a.frameElement;
        } catch (l) {}k && _g();
      }return d[c](h + "DOMContentLoaded", _f2, !1), d[c](h + "readystatechange", _f2, !1), a[c](h + "load", _f2, !1);
    }
  }, a._autoDiscoverFunction = function () {
    return a.autoDiscover ? a.discover() : void 0;
  }, d(window, a._autoDiscoverFunction);
}).call(this);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0), __webpack_require__(2)(module)))

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {/*
 jQuery animateNumber plugin v0.0.14
 (c) 2013, Alexandr Borisov.
 https://github.com/aishek/jquery-animateNumber
*/
(function (d) {
  var r = function r(b) {
    return b.split("").reverse().join("");
  },
      m = { numberStep: function numberStep(b, a) {
      var e = Math.floor(b);d(a.elem).text(e);
    } },
      g = function g(b) {
    var a = b.elem;a.nodeType && a.parentNode && (a = a._animateNumberSetter, a || (a = m.numberStep), a(b.now, b));
  };d.Tween && d.Tween.propHooks ? d.Tween.propHooks.number = { set: g } : d.fx.step.number = g;d.animateNumber = { numberStepFactories: { append: function append(b) {
        return function (a, e) {
          var f = Math.floor(a);d(e.elem).prop("number", a).text(f + b);
        };
      }, separator: function separator(b, a, e) {
        b = b || " ";
        a = a || 3;e = e || "";return function (f, k) {
          var u = 0 > f,
              c = Math.floor((u ? -1 : 1) * f).toString(),
              n = d(k.elem);if (c.length > a) {
            for (var h = c, l = a, m = h.split("").reverse(), c = [], p, s, q, t = 0, g = Math.ceil(h.length / l); t < g; t++) {
              p = "";for (q = 0; q < l; q++) {
                s = t * l + q;if (s === h.length) break;p += m[s];
              }c.push(p);
            }h = c.length - 1;l = r(c[h]);c[h] = r(parseInt(l, 10).toString());c = c.join(b);c = r(c);
          }n.prop("number", f).text((u ? "-" : "") + c + e);
        };
      } } };d.fn.animateNumber = function () {
    for (var b = arguments[0], a = d.extend({}, m, b), e = d(this), f = [a], k = 1, g = arguments.length; k < g; k++) {
      f.push(arguments[k]);
    }if (b.numberStep) {
      var c = this.each(function () {
        this._animateNumberSetter = b.numberStep;
      }),
          n = a.complete;a.complete = function () {
        c.each(function () {
          delete this._animateNumberSetter;
        });n && n.apply(this, arguments);
      };
    }return e.animate.apply(e, f);
  };
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function (global, factory) {
  'use strict';
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(0)], __WEBPACK_AMD_DEFINE_RESULT__ = function ($) {
      return factory($, global, global.document, global.Math);
    }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === "object" && exports) {
    module.exports = factory(require('jquery'), global, global.document, global.Math);
  } else {
    factory(jQuery, global, global.document, global.Math);
  }
})(typeof window !== 'undefined' ? window : this, function ($, window, document, Math, undefined) {
  'use strict';
  var WRAPPER = 'fullpage-wrapper';var WRAPPER_SEL = '.' + WRAPPER;var SCROLLABLE = 'fp-scrollable';var SCROLLABLE_SEL = '.' + SCROLLABLE;var RESPONSIVE = 'fp-responsive';var NO_TRANSITION = 'fp-notransition';var DESTROYED = 'fp-destroyed';var ENABLED = 'fp-enabled';var VIEWING_PREFIX = 'fp-viewing';var ACTIVE = 'active';var ACTIVE_SEL = '.' + ACTIVE;var COMPLETELY = 'fp-completely';var COMPLETELY_SEL = '.' + COMPLETELY;var SECTION_DEFAULT_SEL = '.section';var SECTION = 'fp-section';var SECTION_SEL = '.' + SECTION;var SECTION_ACTIVE_SEL = SECTION_SEL + ACTIVE_SEL;var SECTION_FIRST_SEL = SECTION_SEL + ':first';var SECTION_LAST_SEL = SECTION_SEL + ':last';var TABLE_CELL = 'fp-tableCell';var TABLE_CELL_SEL = '.' + TABLE_CELL;var AUTO_HEIGHT = 'fp-auto-height';var AUTO_HEIGHT_SEL = '.fp-auto-height';var NORMAL_SCROLL = 'fp-normal-scroll';var NORMAL_SCROLL_SEL = '.fp-normal-scroll';var SECTION_NAV = 'fp-nav';var SECTION_NAV_SEL = '#' + SECTION_NAV;var SECTION_NAV_TOOLTIP = 'fp-tooltip';var SECTION_NAV_TOOLTIP_SEL = '.' + SECTION_NAV_TOOLTIP;var SHOW_ACTIVE_TOOLTIP = 'fp-show-active';var SLIDE_DEFAULT_SEL = '.slide';var SLIDE = 'fp-slide';var SLIDE_SEL = '.' + SLIDE;var SLIDE_ACTIVE_SEL = SLIDE_SEL + ACTIVE_SEL;var SLIDES_WRAPPER = 'fp-slides';var SLIDES_WRAPPER_SEL = '.' + SLIDES_WRAPPER;var SLIDES_CONTAINER = 'fp-slidesContainer';var SLIDES_CONTAINER_SEL = '.' + SLIDES_CONTAINER;var TABLE = 'fp-table';var SLIDES_NAV = 'fp-slidesNav';var SLIDES_NAV_SEL = '.' + SLIDES_NAV;var SLIDES_NAV_LINK_SEL = SLIDES_NAV_SEL + ' a';var SLIDES_ARROW = 'fp-controlArrow';var SLIDES_ARROW_SEL = '.' + SLIDES_ARROW;var SLIDES_PREV = 'fp-prev';var SLIDES_PREV_SEL = '.' + SLIDES_PREV;var SLIDES_ARROW_PREV = SLIDES_ARROW + ' ' + SLIDES_PREV;var SLIDES_ARROW_PREV_SEL = SLIDES_ARROW_SEL + SLIDES_PREV_SEL;var SLIDES_NEXT = 'fp-next';var SLIDES_NEXT_SEL = '.' + SLIDES_NEXT;var SLIDES_ARROW_NEXT = SLIDES_ARROW + ' ' + SLIDES_NEXT;var SLIDES_ARROW_NEXT_SEL = SLIDES_ARROW_SEL + SLIDES_NEXT_SEL;var $window = $(window);var $document = $(document);var iscrollOptions = { scrollbars: true, mouseWheel: true, hideScrollbars: false, fadeScrollbars: false, disableMouse: true, interactiveScrollbars: true };$.fn.fullpage = function (options) {
    if ($('html').hasClass(ENABLED)) {
      displayWarnings();return;
    }var $htmlBody = $('html, body');var $body = $('body');var FP = $.fn.fullpage;options = $.extend({ menu: false, anchors: [], lockAnchors: false, navigation: false, navigationPosition: 'right', navigationTooltips: [], showActiveTooltip: false, slidesNavigation: false, slidesNavPosition: 'bottom', scrollBar: false, hybrid: false, css3: true, scrollingSpeed: 700, autoScrolling: true, fitToSection: true, fitToSectionDelay: 1000, easing: 'easeInOutCubic', easingcss3: 'ease', loopBottom: false, loopTop: false, loopHorizontal: true, continuousVertical: false, continuousHorizontal: false, scrollHorizontally: false, interlockedSlides: false, dragAndMove: false, offsetSections: false, resetSliders: false, fadingEffect: false, normalScrollElements: null, scrollOverflow: false, scrollOverflowReset: false, scrollOverflowHandler: iscrollHandler, scrollOverflowOptions: null, touchSensitivity: 5, normalScrollElementTouchThreshold: 5, bigSectionsDestination: null, keyboardScrolling: true, animateAnchor: true, recordHistory: true, controlArrows: true, controlArrowColor: '#fff', verticalCentered: true, sectionsColor: [], paddingTop: 0, paddingBottom: 0, fixedElements: null, responsive: 0, responsiveWidth: 0, responsiveHeight: 0, responsiveSlides: false, parallax: false, parallaxOptions: { type: 'reveal', percentage: 62, property: 'translate' }, sectionSelector: SECTION_DEFAULT_SEL, slideSelector: SLIDE_DEFAULT_SEL, afterLoad: null, onLeave: null, afterRender: null, afterResize: null, afterReBuild: null, afterSlideLoad: null, onSlideLeave: null, afterResponsive: null, lazyLoading: true }, options);var slideMoving = false;var isTouchDevice = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|playbook|silk|BlackBerry|BB10|Windows Phone|Tizen|Bada|webOS|IEMobile|Opera Mini)/);var isTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints > 0 || navigator.maxTouchPoints;var container = $(this);var windowsHeight = $window.height();var isResizing = false;var isWindowFocused = true;var lastScrolledDestiny;var lastScrolledSlide;var canScroll = true;var scrollings = [];var controlPressed;var startingSection;var isScrollAllowed = {};isScrollAllowed.m = { 'up': true, 'down': true, 'left': true, 'right': true };isScrollAllowed.k = $.extend(true, {}, isScrollAllowed.m);var MSPointer = getMSPointer();var events = { touchmove: 'ontouchmove' in window ? 'touchmove' : MSPointer.move, touchstart: 'ontouchstart' in window ? 'touchstart' : MSPointer.down };var resizeId;var afterSectionLoadsId;var afterSlideLoadsId;var scrollId;var scrollId2;var keydownId;var originals = $.extend(true, {}, options);displayWarnings();iscrollOptions.click = isTouch;iscrollOptions = $.extend(iscrollOptions, options.scrollOverflowOptions);$.extend($.easing, { easeInOutCubic: function easeInOutCubic(x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;return c / 2 * ((t -= 2) * t * t + 2) + b;
      } });function setAutoScrolling(value, type) {
      if (!value) {
        silentScroll(0);
      }setVariableState('autoScrolling', value, type);var element = $(SECTION_ACTIVE_SEL);if (options.autoScrolling && !options.scrollBar) {
        $htmlBody.css({ 'overflow': 'hidden', 'height': '100%' });setRecordHistory(originals.recordHistory, 'internal');container.css({ '-ms-touch-action': 'none', 'touch-action': 'none' });if (element.length) {
          silentScroll(element.position().top);
        }
      } else {
        $htmlBody.css({ 'overflow': 'visible', 'height': 'initial' });setRecordHistory(false, 'internal');container.css({ '-ms-touch-action': '', 'touch-action': '' });if (element.length) {
          $htmlBody.scrollTop(element.position().top);
        }
      }
    }function setRecordHistory(value, type) {
      setVariableState('recordHistory', value, type);
    }function setScrollingSpeed(value, type) {
      setVariableState('scrollingSpeed', value, type);
    }function setFitToSection(value, type) {
      setVariableState('fitToSection', value, type);
    }function setLockAnchors(value) {
      options.lockAnchors = value;
    }function setMouseWheelScrolling(value) {
      if (value) {
        addMouseWheelHandler();addMiddleWheelHandler();
      } else {
        removeMouseWheelHandler();removeMiddleWheelHandler();
      }
    }function setAllowScrolling(value, directions) {
      if (typeof directions !== 'undefined') {
        directions = directions.replace(/ /g, '').split(',');$.each(directions, function (index, direction) {
          setIsScrollAllowed(value, direction, 'm');
        });
      } else if (value) {
        setMouseWheelScrolling(true);addTouchHandler();
      } else {
        setMouseWheelScrolling(false);removeTouchHandler();
      }
    }function setKeyboardScrolling(value, directions) {
      if (typeof directions !== 'undefined') {
        directions = directions.replace(/ /g, '').split(',');$.each(directions, function (index, direction) {
          setIsScrollAllowed(value, direction, 'k');
        });
      } else {
        options.keyboardScrolling = value;
      }
    }function moveSectionUp() {
      var prev = $(SECTION_ACTIVE_SEL).prev(SECTION_SEL);if (!prev.length && (options.loopTop || options.continuousVertical)) {
        prev = $(SECTION_SEL).last();
      }if (prev.length) {
        scrollPage(prev, null, true);
      }
    }function moveSectionDown() {
      var next = $(SECTION_ACTIVE_SEL).next(SECTION_SEL);if (!next.length && (options.loopBottom || options.continuousVertical)) {
        next = $(SECTION_SEL).first();
      }if (next.length) {
        scrollPage(next, null, false);
      }
    }function silentMoveTo(sectionAnchor, slideAnchor) {
      setScrollingSpeed(0, 'internal');moveTo(sectionAnchor, slideAnchor);setScrollingSpeed(originals.scrollingSpeed, 'internal');
    }function moveTo(sectionAnchor, slideAnchor) {
      var destiny = getSectionByAnchor(sectionAnchor);if (typeof slideAnchor !== 'undefined') {
        scrollPageAndSlide(sectionAnchor, slideAnchor);
      } else if (destiny.length > 0) {
        scrollPage(destiny);
      }
    }function moveSlideRight(section) {
      moveSlide('right', section);
    }function moveSlideLeft(section) {
      moveSlide('left', section);
    }function reBuild(resizing) {
      if (container.hasClass(DESTROYED)) {
        return;
      }isResizing = true;windowsHeight = $window.height();$(SECTION_SEL).each(function () {
        var slidesWrap = $(this).find(SLIDES_WRAPPER_SEL);var slides = $(this).find(SLIDE_SEL);if (options.verticalCentered) {
          $(this).find(TABLE_CELL_SEL).css('height', getTableHeight($(this)) + 'px');
        }$(this).css('height', windowsHeight + 'px');if (options.scrollOverflow) {
          if (slides.length) {
            slides.each(function () {
              createScrollBar($(this));
            });
          } else {
            createScrollBar($(this));
          }
        }if (slides.length > 1) {
          landscapeScroll(slidesWrap, slidesWrap.find(SLIDE_ACTIVE_SEL));
        }
      });var activeSection = $(SECTION_ACTIVE_SEL);var sectionIndex = activeSection.index(SECTION_SEL);if (sectionIndex) {
        silentMoveTo(sectionIndex + 1);
      }isResizing = false;$.isFunction(options.afterResize) && resizing && options.afterResize.call(container);$.isFunction(options.afterReBuild) && !resizing && options.afterReBuild.call(container);
    }function setResponsive(active) {
      var isResponsive = $body.hasClass(RESPONSIVE);if (active) {
        if (!isResponsive) {
          setAutoScrolling(false, 'internal');setFitToSection(false, 'internal');$(SECTION_NAV_SEL).hide();$body.addClass(RESPONSIVE);$.isFunction(options.afterResponsive) && options.afterResponsive.call(container, active);
        }
      } else if (isResponsive) {
        setAutoScrolling(originals.autoScrolling, 'internal');setFitToSection(originals.autoScrolling, 'internal');$(SECTION_NAV_SEL).show();$body.removeClass(RESPONSIVE);$.isFunction(options.afterResponsive) && options.afterResponsive.call(container, active);
      }
    }if ($(this).length) {
      FP.setAutoScrolling = setAutoScrolling;FP.setRecordHistory = setRecordHistory;FP.setScrollingSpeed = setScrollingSpeed;FP.setFitToSection = setFitToSection;FP.setLockAnchors = setLockAnchors;FP.setMouseWheelScrolling = setMouseWheelScrolling;FP.setAllowScrolling = setAllowScrolling;FP.setKeyboardScrolling = setKeyboardScrolling;FP.moveSectionUp = moveSectionUp;FP.moveSectionDown = moveSectionDown;FP.silentMoveTo = silentMoveTo;FP.moveTo = moveTo;FP.moveSlideRight = moveSlideRight;FP.moveSlideLeft = moveSlideLeft;FP.fitToSection = fitToSection;FP.reBuild = reBuild;FP.setResponsive = setResponsive;FP.destroy = destroy;init();bindEvents();
    }function init() {
      if (options.css3) {
        options.css3 = support3d();
      }options.scrollBar = options.scrollBar || options.hybrid;setOptionsFromDOM();prepareDom();setAllowScrolling(true);setAutoScrolling(options.autoScrolling, 'internal');responsive();setBodyClass();if (document.readyState === 'complete') {
        scrollToAnchor();
      }$window.on('load', scrollToAnchor);
    }function bindEvents() {
      $window.on('scroll', scrollHandler).on('hashchange', hashChangeHandler).blur(blurHandler).resize(resizeHandler);$document.keydown(keydownHandler).keyup(keyUpHandler).on('click touchstart', SECTION_NAV_SEL + ' a', sectionBulletHandler).on('click touchstart', SLIDES_NAV_LINK_SEL, slideBulletHandler).on('click', SECTION_NAV_TOOLTIP_SEL, tooltipTextHandler);$(SECTION_SEL).on('click touchstart', SLIDES_ARROW_SEL, slideArrowHandler);if (options.normalScrollElements) {
        $document.on('mouseenter', options.normalScrollElements, function () {
          setMouseWheelScrolling(false);
        });$document.on('mouseleave', options.normalScrollElements, function () {
          setMouseWheelScrolling(true);
        });
      }
    }function setOptionsFromDOM() {
      var sections = container.find(options.sectionSelector);if (!options.anchors.length) {
        options.anchors = sections.filter('[data-anchor]').map(function () {
          return $(this).data('anchor').toString();
        }).get();
      }if (!options.navigationTooltips.length) {
        options.navigationTooltips = sections.filter('[data-tooltip]').map(function () {
          return $(this).data('tooltip').toString();
        }).get();
      }
    }function prepareDom() {
      container.css({ 'height': '100%', 'position': 'relative' });container.addClass(WRAPPER);$('html').addClass(ENABLED);windowsHeight = $window.height();container.removeClass(DESTROYED);addInternalSelectors();$(SECTION_SEL).each(function (index) {
        var section = $(this);var slides = section.find(SLIDE_SEL);var numSlides = slides.length;styleSection(section, index);styleMenu(section, index);if (numSlides > 0) {
          styleSlides(section, slides, numSlides);
        } else {
          if (options.verticalCentered) {
            addTableClass(section);
          }
        }
      });if (options.fixedElements && options.css3) {
        $(options.fixedElements).appendTo($body);
      }if (options.navigation) {
        addVerticalNavigation();
      }enableYoutubeAPI();if (options.scrollOverflow) {
        if (document.readyState === 'complete') {
          createScrollBarHandler();
        }$window.on('load', createScrollBarHandler);
      } else {
        afterRenderActions();
      }
    }function styleSlides(section, slides, numSlides) {
      var sliderWidth = numSlides * 100;var slideWidth = 100 / numSlides;slides.wrapAll('<div class="' + SLIDES_CONTAINER + '" />');slides.parent().wrap('<div class="' + SLIDES_WRAPPER + '" />');section.find(SLIDES_CONTAINER_SEL).css('width', sliderWidth + '%');if (numSlides > 1) {
        if (options.controlArrows) {
          createSlideArrows(section);
        }if (options.slidesNavigation) {
          addSlidesNavigation(section, numSlides);
        }
      }slides.each(function (index) {
        $(this).css('width', slideWidth + '%');if (options.verticalCentered) {
          addTableClass($(this));
        }
      });var startingSlide = section.find(SLIDE_ACTIVE_SEL);if (startingSlide.length && ($(SECTION_ACTIVE_SEL).index(SECTION_SEL) !== 0 || $(SECTION_ACTIVE_SEL).index(SECTION_SEL) === 0 && startingSlide.index() !== 0)) {
        silentLandscapeScroll(startingSlide, 'internal');
      } else {
        slides.eq(0).addClass(ACTIVE);
      }
    }function styleSection(section, index) {
      if (!index && $(SECTION_ACTIVE_SEL).length === 0) {
        section.addClass(ACTIVE);
      }startingSection = $(SECTION_ACTIVE_SEL);section.css('height', windowsHeight + 'px');if (options.paddingTop) {
        section.css('padding-top', options.paddingTop);
      }if (options.paddingBottom) {
        section.css('padding-bottom', options.paddingBottom);
      }if (typeof options.sectionsColor[index] !== 'undefined') {
        section.css('background-color', options.sectionsColor[index]);
      }if (typeof options.anchors[index] !== 'undefined') {
        section.attr('data-anchor', options.anchors[index]);
      }
    }function styleMenu(section, index) {
      if (typeof options.anchors[index] !== 'undefined') {
        if (section.hasClass(ACTIVE)) {
          activateMenuAndNav(options.anchors[index], index);
        }
      }if (options.menu && options.css3 && $(options.menu).closest(WRAPPER_SEL).length) {
        $(options.menu).appendTo($body);
      }
    }function addInternalSelectors() {
      container.find(options.sectionSelector).addClass(SECTION);container.find(options.slideSelector).addClass(SLIDE);
    }function createSlideArrows(section) {
      section.find(SLIDES_WRAPPER_SEL).after('<div class="' + SLIDES_ARROW_PREV + '"></div><div class="' + SLIDES_ARROW_NEXT + '"></div>');if (options.controlArrowColor != '#fff') {
        section.find(SLIDES_ARROW_NEXT_SEL).css('border-color', 'transparent transparent transparent ' + options.controlArrowColor);section.find(SLIDES_ARROW_PREV_SEL).css('border-color', 'transparent ' + options.controlArrowColor + ' transparent transparent');
      }if (!options.loopHorizontal) {
        section.find(SLIDES_ARROW_PREV_SEL).hide();
      }
    }function addVerticalNavigation() {
      $body.append('<div id="' + SECTION_NAV + '"><ul></ul></div>');var nav = $(SECTION_NAV_SEL);nav.addClass(function () {
        return options.showActiveTooltip ? SHOW_ACTIVE_TOOLTIP + ' ' + options.navigationPosition : options.navigationPosition;
      });for (var i = 0; i < $(SECTION_SEL).length; i++) {
        var link = '';if (options.anchors.length) {
          link = options.anchors[i];
        }var tooltip = options.navigationTooltips[i];var li = '<li><a href="#' + link + '" aria-label="' + tooltip + '"><span></span></a>';var tooltip = options.navigationTooltips[i];if (typeof tooltip !== 'undefined' && tooltip !== '') {
          li += '<div class="' + SECTION_NAV_TOOLTIP + ' ' + options.navigationPosition + '">' + tooltip + '</div>';
        }li += '</li>';nav.find('ul').append(li);
      }$(SECTION_NAV_SEL).css('margin-top', '-' + $(SECTION_NAV_SEL).height() / 2 + 'px');$(SECTION_NAV_SEL).find('li').eq($(SECTION_ACTIVE_SEL).index(SECTION_SEL)).find('a').addClass(ACTIVE);$(SECTION_NAV_SEL).find('li').eq($(SECTION_ACTIVE_SEL).index(SECTION_SEL)).find('.fp-tooltip').addClass(ACTIVE);
    }function createScrollBarHandler() {
      $(SECTION_SEL).each(function () {
        var slides = $(this).find(SLIDE_SEL);if (slides.length) {
          slides.each(function () {
            createScrollBar($(this));
          });
        } else {
          createScrollBar($(this));
        }
      });afterRenderActions();
    }function enableYoutubeAPI() {
      container.find('iframe[src*="youtube.com/embed/"]').each(function () {
        addURLParam($(this), 'enablejsapi=1');
      });
    }function addURLParam(element, newParam) {
      var originalSrc = element.attr('src');element.attr('src', originalSrc + getUrlParamSign(originalSrc) + newParam);
    }function getUrlParamSign(url) {
      return !/\?/.test(url) ? '?' : '&';
    }function afterRenderActions() {
      var section = $(SECTION_ACTIVE_SEL);section.addClass(COMPLETELY);if (options.scrollOverflowHandler.afterRender) {
        options.scrollOverflowHandler.afterRender(section);
      }lazyLoad(section);playMedia(section);options.scrollOverflowHandler.afterLoad();if (isDestinyTheStartingSection()) {
        $.isFunction(options.afterLoad) && options.afterLoad.call(section, section.data('anchor'), section.index(SECTION_SEL) + 1);
      }$.isFunction(options.afterRender) && options.afterRender.call(container);
    }function isDestinyTheStartingSection() {
      var anchors = window.location.hash.replace('#', '').split('/');var destinationSection = getSectionByAnchor(decodeURIComponent(anchors[0]));return !destinationSection.length || destinationSection.length && destinationSection.index() === startingSection.index();
    }var isScrolling = false;var lastScroll = 0;function scrollHandler() {
      var currentSection;if (!options.autoScrolling || options.scrollBar) {
        var currentScroll = $window.scrollTop();var scrollDirection = getScrollDirection(currentScroll);var visibleSectionIndex = 0;var screen_mid = currentScroll + $window.height() / 2.0;var isAtBottom = $body.height() - $window.height() === currentScroll;var sections = document.querySelectorAll(SECTION_SEL);if (isAtBottom) {
          visibleSectionIndex = sections.length - 1;
        } else if (!currentScroll) {
          visibleSectionIndex = 0;
        } else {
          for (var i = 0; i < sections.length; ++i) {
            var section = sections[i];if (section.offsetTop <= screen_mid) {
              visibleSectionIndex = i;
            }
          }
        }if (isCompletelyInViewPort(scrollDirection)) {
          if (!$(SECTION_ACTIVE_SEL).hasClass(COMPLETELY)) {
            $(SECTION_ACTIVE_SEL).addClass(COMPLETELY).siblings().removeClass(COMPLETELY);
          }
        }currentSection = $(sections).eq(visibleSectionIndex);if (!currentSection.hasClass(ACTIVE)) {
          isScrolling = true;var leavingSection = $(SECTION_ACTIVE_SEL);var leavingSectionIndex = leavingSection.index(SECTION_SEL) + 1;var yMovement = getYmovement(currentSection);var anchorLink = currentSection.data('anchor');var sectionIndex = currentSection.index(SECTION_SEL) + 1;var activeSlide = currentSection.find(SLIDE_ACTIVE_SEL);var slideIndex;var slideAnchorLink;if (activeSlide.length) {
            slideAnchorLink = activeSlide.data('anchor');slideIndex = activeSlide.index();
          }if (canScroll) {
            currentSection.addClass(ACTIVE).siblings().removeClass(ACTIVE);$.isFunction(options.onLeave) && options.onLeave.call(leavingSection, leavingSectionIndex, sectionIndex, yMovement);$.isFunction(options.afterLoad) && options.afterLoad.call(currentSection, anchorLink, sectionIndex);stopMedia(leavingSection);lazyLoad(currentSection);playMedia(currentSection);activateMenuAndNav(anchorLink, sectionIndex - 1);if (options.anchors.length) {
              lastScrolledDestiny = anchorLink;
            }setState(slideIndex, slideAnchorLink, anchorLink, sectionIndex);
          }clearTimeout(scrollId);scrollId = setTimeout(function () {
            isScrolling = false;
          }, 100);
        }if (options.fitToSection) {
          clearTimeout(scrollId2);scrollId2 = setTimeout(function () {
            if (options.fitToSection) {
              fitToSection();
            }
          }, options.fitToSectionDelay);
        }
      }
    }function fitToSection() {
      if (canScroll) {
        isResizing = true;scrollPage($(SECTION_ACTIVE_SEL));isResizing = false;
      }
    }function isCompletelyInViewPort(movement) {
      var top = $(SECTION_ACTIVE_SEL).position().top;var bottom = top + $window.height();if (movement == 'up') {
        return bottom >= $window.scrollTop() + $window.height();
      }return top <= $window.scrollTop();
    }function getScrollDirection(currentScroll) {
      var direction = currentScroll > lastScroll ? 'down' : 'up';lastScroll = currentScroll;previousDestTop = currentScroll;return direction;
    }function scrolling(type, scrollable) {
      if (!isScrollAllowed.m[type]) {
        return;
      }var check = type === 'down' ? 'bottom' : 'top';var scrollSection = type === 'down' ? moveSectionDown : moveSectionUp;if (scrollable.length > 0) {
        if (options.scrollOverflowHandler.isScrolled(check, scrollable)) {
          scrollSection();
        } else {
          return true;
        }
      } else {
        scrollSection();
      }
    }function preventBouncing(event) {
      var e = event.originalEvent;if (!checkParentForNormalScrollElement(event.target) && options.autoScrolling && isReallyTouch(e)) {
        event.preventDefault();
      }
    }var touchStartY = 0;var touchStartX = 0;var touchEndY = 0;var touchEndX = 0;function touchMoveHandler(event) {
      var e = event.originalEvent;var activeSection = $(e.target).closest(SECTION_SEL);if (!checkParentForNormalScrollElement(event.target) && isReallyTouch(e)) {
        if (options.autoScrolling) {
          event.preventDefault();
        }var scrollable = options.scrollOverflowHandler.scrollable(activeSection);var touchEvents = getEventsPage(e);touchEndY = touchEvents.y;touchEndX = touchEvents.x;if (activeSection.find(SLIDES_WRAPPER_SEL).length && Math.abs(touchStartX - touchEndX) > Math.abs(touchStartY - touchEndY)) {
          if (!slideMoving && Math.abs(touchStartX - touchEndX) > $window.outerWidth() / 100 * options.touchSensitivity) {
            if (touchStartX > touchEndX) {
              if (isScrollAllowed.m.right) {
                moveSlideRight(activeSection);
              }
            } else {
              if (isScrollAllowed.m.left) {
                moveSlideLeft(activeSection);
              }
            }
          }
        } else if (options.autoScrolling && canScroll) {
          if (Math.abs(touchStartY - touchEndY) > $window.height() / 100 * options.touchSensitivity) {
            if (touchStartY > touchEndY) {
              scrolling('down', scrollable);
            } else if (touchEndY > touchStartY) {
              scrolling('up', scrollable);
            }
          }
        }
      }
    }function checkParentForNormalScrollElement(el, hop) {
      hop = hop || 0;var parent = $(el).parent();if (hop < options.normalScrollElementTouchThreshold && parent.is(options.normalScrollElements)) {
        return true;
      } else if (hop == options.normalScrollElementTouchThreshold) {
        return false;
      } else {
        return checkParentForNormalScrollElement(parent, ++hop);
      }
    }function isReallyTouch(e) {
      return typeof e.pointerType === 'undefined' || e.pointerType != 'mouse';
    }function touchStartHandler(event) {
      var e = event.originalEvent;if (options.fitToSection) {
        $htmlBody.stop();
      }if (isReallyTouch(e)) {
        var touchEvents = getEventsPage(e);touchStartY = touchEvents.y;touchStartX = touchEvents.x;
      }
    }function getAverage(elements, number) {
      var sum = 0;var lastElements = elements.slice(Math.max(elements.length - number, 1));for (var i = 0; i < lastElements.length; i++) {
        sum = sum + lastElements[i];
      }return Math.ceil(sum / number);
    }var prevTime = new Date().getTime();function MouseWheelHandler(e) {
      var curTime = new Date().getTime();var isNormalScroll = $(COMPLETELY_SEL).hasClass(NORMAL_SCROLL);if (options.autoScrolling && !controlPressed && !isNormalScroll) {
        e = e || window.event;var value = e.wheelDelta || -e.deltaY || -e.detail;var delta = Math.max(-1, Math.min(1, value));var horizontalDetection = typeof e.wheelDeltaX !== 'undefined' || typeof e.deltaX !== 'undefined';var isScrollingVertically = Math.abs(e.wheelDeltaX) < Math.abs(e.wheelDelta) || Math.abs(e.deltaX) < Math.abs(e.deltaY) || !horizontalDetection;if (scrollings.length > 149) {
          scrollings.shift();
        }scrollings.push(Math.abs(value));if (options.scrollBar) {
          e.preventDefault ? e.preventDefault() : e.returnValue = false;
        }var activeSection = $(SECTION_ACTIVE_SEL);var scrollable = options.scrollOverflowHandler.scrollable(activeSection);var timeDiff = curTime - prevTime;prevTime = curTime;if (timeDiff > 200) {
          scrollings = [];
        }if (canScroll) {
          var averageEnd = getAverage(scrollings, 10);var averageMiddle = getAverage(scrollings, 70);var isAccelerating = averageEnd >= averageMiddle;if (isAccelerating && isScrollingVertically) {
            if (delta < 0) {
              scrolling('down', scrollable);
            } else {
              scrolling('up', scrollable);
            }
          }
        }return false;
      }if (options.fitToSection) {
        $htmlBody.stop();
      }
    }function moveSlide(direction, section) {
      var activeSection = typeof section === 'undefined' ? $(SECTION_ACTIVE_SEL) : section;var slides = activeSection.find(SLIDES_WRAPPER_SEL);var numSlides = slides.find(SLIDE_SEL).length;if (!slides.length || slideMoving || numSlides < 2) {
        return;
      }var currentSlide = slides.find(SLIDE_ACTIVE_SEL);var destiny = null;if (direction === 'left') {
        destiny = currentSlide.prev(SLIDE_SEL);
      } else {
        destiny = currentSlide.next(SLIDE_SEL);
      }if (!destiny.length) {
        if (!options.loopHorizontal) return;if (direction === 'left') {
          destiny = currentSlide.siblings(':last');
        } else {
          destiny = currentSlide.siblings(':first');
        }
      }slideMoving = true;landscapeScroll(slides, destiny, direction);
    }function keepSlidesPosition() {
      $(SLIDE_ACTIVE_SEL).each(function () {
        silentLandscapeScroll($(this), 'internal');
      });
    }var previousDestTop = 0;function getDestinationPosition(element) {
      var elemPosition = element.position();var position = elemPosition.top;var isScrollingDown = elemPosition.top > previousDestTop;var sectionBottom = position - windowsHeight + element.outerHeight();var bigSectionsDestination = options.bigSectionsDestination;if (element.outerHeight() > windowsHeight) {
        if (!isScrollingDown && !bigSectionsDestination || bigSectionsDestination === 'bottom') {
          position = sectionBottom;
        }
      } else if (isScrollingDown || isResizing && element.is(':last-child')) {
        position = sectionBottom;
      }previousDestTop = position;return position;
    }function scrollPage(element, callback, isMovementUp) {
      if (typeof element === 'undefined') {
        return;
      }var dtop = getDestinationPosition(element);var slideAnchorLink;var slideIndex;var v = { element: element, callback: callback, isMovementUp: isMovementUp, dtop: dtop, yMovement: getYmovement(element), anchorLink: element.data('anchor'), sectionIndex: element.index(SECTION_SEL), activeSlide: element.find(SLIDE_ACTIVE_SEL), activeSection: $(SECTION_ACTIVE_SEL), leavingSection: $(SECTION_ACTIVE_SEL).index(SECTION_SEL) + 1, localIsResizing: isResizing };if (v.activeSection.is(element) && !isResizing || options.scrollBar && $window.scrollTop() === v.dtop && !element.hasClass(AUTO_HEIGHT)) {
        return;
      }if (v.activeSlide.length) {
        slideAnchorLink = v.activeSlide.data('anchor');slideIndex = v.activeSlide.index();
      }if (options.autoScrolling && options.continuousVertical && typeof v.isMovementUp !== "undefined" && (!v.isMovementUp && v.yMovement == 'up' || v.isMovementUp && v.yMovement == 'down')) {
        v = createInfiniteSections(v);
      }if ($.isFunction(options.onLeave) && !v.localIsResizing) {
        if (options.onLeave.call(v.activeSection, v.leavingSection, v.sectionIndex + 1, v.yMovement) === false) {
          return;
        }
      }if (!v.localIsResizing) {
        stopMedia(v.activeSection);
      }options.scrollOverflowHandler.beforeLeave();element.addClass(ACTIVE).siblings().removeClass(ACTIVE);lazyLoad(element);options.scrollOverflowHandler.onLeave();canScroll = false;setState(slideIndex, slideAnchorLink, v.anchorLink, v.sectionIndex);performMovement(v);lastScrolledDestiny = v.anchorLink;activateMenuAndNav(v.anchorLink, v.sectionIndex);
    }function performMovement(v) {
      if (options.css3 && options.autoScrolling && !options.scrollBar) {
        var translate3d = 'translate3d(0px, -' + Math.round(v.dtop) + 'px, 0px)';transformContainer(translate3d, true);if (options.scrollingSpeed) {
          clearTimeout(afterSectionLoadsId);afterSectionLoadsId = setTimeout(function () {
            afterSectionLoads(v);
          }, options.scrollingSpeed);
        } else {
          afterSectionLoads(v);
        }
      } else {
        var scrollSettings = getScrollSettings(v);$(scrollSettings.element).animate(scrollSettings.options, options.scrollingSpeed, options.easing).promise().done(function () {
          if (options.scrollBar) {
            setTimeout(function () {
              afterSectionLoads(v);
            }, 30);
          } else {
            afterSectionLoads(v);
          }
        });
      }
    }function getScrollSettings(v) {
      var scroll = {};if (options.autoScrolling && !options.scrollBar) {
        scroll.options = { 'top': -v.dtop };scroll.element = WRAPPER_SEL;
      } else {
        scroll.options = { 'scrollTop': v.dtop };scroll.element = 'html, body';
      }return scroll;
    }function createInfiniteSections(v) {
      if (!v.isMovementUp) {
        $(SECTION_ACTIVE_SEL).after(v.activeSection.prevAll(SECTION_SEL).get().reverse());
      } else {
        $(SECTION_ACTIVE_SEL).before(v.activeSection.nextAll(SECTION_SEL));
      }silentScroll($(SECTION_ACTIVE_SEL).position().top);keepSlidesPosition();v.wrapAroundElements = v.activeSection;v.dtop = v.element.position().top;v.yMovement = getYmovement(v.element);return v;
    }function continuousVerticalFixSectionOrder(v) {
      if (!v.wrapAroundElements || !v.wrapAroundElements.length) {
        return;
      }if (v.isMovementUp) {
        $(SECTION_FIRST_SEL).before(v.wrapAroundElements);
      } else {
        $(SECTION_LAST_SEL).after(v.wrapAroundElements);
      }silentScroll($(SECTION_ACTIVE_SEL).position().top);keepSlidesPosition();
    }function afterSectionLoads(v) {
      continuousVerticalFixSectionOrder(v);$.isFunction(options.afterLoad) && !v.localIsResizing && options.afterLoad.call(v.element, v.anchorLink, v.sectionIndex + 1);options.scrollOverflowHandler.afterLoad();if (!v.localIsResizing) {
        playMedia(v.element);
      }v.element.addClass(COMPLETELY).siblings().removeClass(COMPLETELY);canScroll = true;$.isFunction(v.callback) && v.callback.call(this);
    }function setSrc(element, attribute) {
      element.attr(attribute, element.data(attribute)).removeAttr('data-' + attribute);
    }function lazyLoad(destiny) {
      if (!options.lazyLoading) {
        return;
      }var panel = getSlideOrSection(destiny);var element;panel.find('img[data-src], img[data-srcset], source[data-src], audio[data-src], iframe[data-src]').each(function () {
        element = $(this);$.each(['src', 'srcset'], function (index, type) {
          var attribute = element.attr('data-' + type);if (typeof attribute !== 'undefined' && attribute) {
            setSrc(element, type);
          }
        });if (element.is('source')) {
          element.closest('video').get(0).load();
        }
      });
    }function playMedia(destiny) {
      var panel = getSlideOrSection(destiny);panel.find('video, audio').each(function () {
        var element = $(this).get(0);if (element.hasAttribute('data-autoplay') && typeof element.play === 'function') {
          element.play();
        }
      });panel.find('iframe[src*="youtube.com/embed/"]').each(function () {
        var element = $(this).get(0);if (element.hasAttribute('data-autoplay')) {
          playYoutube(element);
        }element.onload = function () {
          if (element.hasAttribute('data-autoplay')) {
            playYoutube(element);
          }
        };
      });
    }function playYoutube(element) {
      element.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
    }function stopMedia(destiny) {
      var panel = getSlideOrSection(destiny);panel.find('video, audio').each(function () {
        var element = $(this).get(0);if (!element.hasAttribute('data-keepplaying') && typeof element.pause === 'function') {
          element.pause();
        }
      });panel.find('iframe[src*="youtube.com/embed/"]').each(function () {
        var element = $(this).get(0);if (/youtube\.com\/embed\//.test($(this).attr('src')) && !element.hasAttribute('data-keepplaying')) {
          $(this).get(0).contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        }
      });
    }function getSlideOrSection(destiny) {
      var slide = destiny.find(SLIDE_ACTIVE_SEL);if (slide.length) {
        destiny = $(slide);
      }return destiny;
    }function scrollToAnchor() {
      var value = window.location.hash.replace('#', '').split('/');var sectionAnchor = decodeURIComponent(value[0]);var slideAnchor = decodeURIComponent(value[1]);if (sectionAnchor) {
        if (options.animateAnchor) {
          scrollPageAndSlide(sectionAnchor, slideAnchor);
        } else {
          silentMoveTo(sectionAnchor, slideAnchor);
        }
      }
    }function hashChangeHandler() {
      if (!isScrolling && !options.lockAnchors) {
        var value = window.location.hash.replace('#', '').split('/');var sectionAnchor = decodeURIComponent(value[0]);var slideAnchor = decodeURIComponent(value[1]);var isFirstSlideMove = typeof lastScrolledDestiny === 'undefined';var isFirstScrollMove = typeof lastScrolledDestiny === 'undefined' && typeof slideAnchor === 'undefined' && !slideMoving;if (sectionAnchor.length) {
          if (sectionAnchor && sectionAnchor !== lastScrolledDestiny && !isFirstSlideMove || isFirstScrollMove || !slideMoving && lastScrolledSlide != slideAnchor) {
            scrollPageAndSlide(sectionAnchor, slideAnchor);
          }
        }
      }
    }function keydownHandler(e) {
      clearTimeout(keydownId);var activeElement = $(':focus');if (!activeElement.is('textarea') && !activeElement.is('input') && !activeElement.is('select') && activeElement.attr('contentEditable') !== "true" && activeElement.attr('contentEditable') !== '' && options.keyboardScrolling && options.autoScrolling) {
        var keyCode = e.which;var keyControls = [40, 38, 32, 33, 34];if ($.inArray(keyCode, keyControls) > -1) {
          e.preventDefault();
        }controlPressed = e.ctrlKey;keydownId = setTimeout(function () {
          onkeydown(e);
        }, 150);
      }
    }function tooltipTextHandler() {
      $(this).prev().trigger('click');
    }function keyUpHandler(e) {
      if (isWindowFocused) {
        controlPressed = e.ctrlKey;
      }
    }function mouseDownHandler(e) {
      if (e.which == 2) {
        oldPageY = e.pageY;container.on('mousemove', mouseMoveHandler);
      }
    }function mouseUpHandler(e) {
      if (e.which == 2) {
        container.off('mousemove');
      }
    }function slideArrowHandler() {
      var section = $(this).closest(SECTION_SEL);if ($(this).hasClass(SLIDES_PREV)) {
        if (isScrollAllowed.m.left) {
          moveSlideLeft(section);
        }
      } else {
        if (isScrollAllowed.m.right) {
          moveSlideRight(section);
        }
      }
    }function blurHandler() {
      isWindowFocused = false;controlPressed = false;
    }function sectionBulletHandler(e) {
      e.preventDefault();var index = $(this).parent().index();scrollPage($(SECTION_SEL).eq(index));
    }function slideBulletHandler(e) {
      e.preventDefault();var slides = $(this).closest(SECTION_SEL).find(SLIDES_WRAPPER_SEL);var destiny = slides.find(SLIDE_SEL).eq($(this).closest('li').index());landscapeScroll(slides, destiny);
    }function onkeydown(e) {
      var shiftPressed = e.shiftKey;if (!canScroll && [37, 39].indexOf(e.which) < 0) {
        return;
      }switch (e.which) {case 38:case 33:
          if (isScrollAllowed.k.up) {
            moveSectionUp();
          }break;case 32:
          if (shiftPressed && isScrollAllowed.k.up) {
            moveSectionUp();break;
          }case 40:case 34:
          if (isScrollAllowed.k.down) {
            moveSectionDown();
          }break;case 36:
          if (isScrollAllowed.k.up) {
            moveTo(1);
          }break;case 35:
          if (isScrollAllowed.k.down) {
            moveTo($(SECTION_SEL).length);
          }break;case 37:
          if (isScrollAllowed.k.left) {
            moveSlideLeft();
          }break;case 39:
          if (isScrollAllowed.k.right) {
            moveSlideRight();
          }break;default:
          return;}
    }var oldPageY = 0;function mouseMoveHandler(e) {
      if (canScroll) {
        if (e.pageY < oldPageY && isScrollAllowed.m.up) {
          moveSectionUp();
        } else if (e.pageY > oldPageY && isScrollAllowed.m.down) {
          moveSectionDown();
        }
      }oldPageY = e.pageY;
    }function landscapeScroll(slides, destiny, direction) {
      var section = slides.closest(SECTION_SEL);var v = { slides: slides, destiny: destiny, direction: direction, destinyPos: destiny.position(), slideIndex: destiny.index(), section: section, sectionIndex: section.index(SECTION_SEL), anchorLink: section.data('anchor'), slidesNav: section.find(SLIDES_NAV_SEL), slideAnchor: getAnchor(destiny), prevSlide: section.find(SLIDE_ACTIVE_SEL), prevSlideIndex: section.find(SLIDE_ACTIVE_SEL).index(), localIsResizing: isResizing };v.xMovement = getXmovement(v.prevSlideIndex, v.slideIndex);if (!v.localIsResizing) {
        canScroll = false;
      }if (options.onSlideLeave) {
        if (!v.localIsResizing && v.xMovement !== 'none') {
          if ($.isFunction(options.onSlideLeave)) {
            if (options.onSlideLeave.call(v.prevSlide, v.anchorLink, v.sectionIndex + 1, v.prevSlideIndex, v.xMovement, v.slideIndex) === false) {
              slideMoving = false;return;
            }
          }
        }
      }destiny.addClass(ACTIVE).siblings().removeClass(ACTIVE);if (!v.localIsResizing) {
        stopMedia(v.prevSlide);lazyLoad(destiny);
      }if (!options.loopHorizontal && options.controlArrows) {
        section.find(SLIDES_ARROW_PREV_SEL).toggle(v.slideIndex !== 0);section.find(SLIDES_ARROW_NEXT_SEL).toggle(!destiny.is(':last-child'));
      }if (section.hasClass(ACTIVE) && !v.localIsResizing) {
        setState(v.slideIndex, v.slideAnchor, v.anchorLink, v.sectionIndex);
      }performHorizontalMove(slides, v, true);
    }function afterSlideLoads(v) {
      activeSlidesNavigation(v.slidesNav, v.slideIndex);if (!v.localIsResizing) {
        $.isFunction(options.afterSlideLoad) && options.afterSlideLoad.call(v.destiny, v.anchorLink, v.sectionIndex + 1, v.slideAnchor, v.slideIndex);canScroll = true;playMedia(v.destiny);
      }slideMoving = false;
    }function performHorizontalMove(slides, v, fireCallback) {
      var destinyPos = v.destinyPos;if (options.css3) {
        var translate3d = 'translate3d(-' + Math.round(destinyPos.left) + 'px, 0px, 0px)';addAnimation(slides.find(SLIDES_CONTAINER_SEL)).css(getTransforms(translate3d));afterSlideLoadsId = setTimeout(function () {
          fireCallback && afterSlideLoads(v);
        }, options.scrollingSpeed, options.easing);
      } else {
        slides.animate({ scrollLeft: Math.round(destinyPos.left) }, options.scrollingSpeed, options.easing, function () {
          fireCallback && afterSlideLoads(v);
        });
      }
    }function activeSlidesNavigation(slidesNav, slideIndex) {
      slidesNav.find(ACTIVE_SEL).removeClass(ACTIVE);slidesNav.find('li').eq(slideIndex).find('a').addClass(ACTIVE);
    }var previousHeight = windowsHeight;function resizeHandler() {
      responsive();if (isTouchDevice) {
        var activeElement = $(document.activeElement);if (!activeElement.is('textarea') && !activeElement.is('input') && !activeElement.is('select')) {
          var currentHeight = $window.height();if (Math.abs(currentHeight - previousHeight) > 20 * Math.max(previousHeight, currentHeight) / 100) {
            reBuild(true);previousHeight = currentHeight;
          }
        }
      } else {
        clearTimeout(resizeId);resizeId = setTimeout(function () {
          reBuild(true);
        }, 350);
      }
    }function responsive() {
      var widthLimit = options.responsive || options.responsiveWidth;var heightLimit = options.responsiveHeight;var isBreakingPointWidth = widthLimit && $window.outerWidth() < widthLimit;var isBreakingPointHeight = heightLimit && $window.height() < heightLimit;if (widthLimit && heightLimit) {
        setResponsive(isBreakingPointWidth || isBreakingPointHeight);
      } else if (widthLimit) {
        setResponsive(isBreakingPointWidth);
      } else if (heightLimit) {
        setResponsive(isBreakingPointHeight);
      }
    }function addAnimation(element) {
      var transition = 'all ' + options.scrollingSpeed + 'ms ' + options.easingcss3;element.removeClass(NO_TRANSITION);return element.css({ '-webkit-transition': transition, 'transition': transition });
    }function removeAnimation(element) {
      return element.addClass(NO_TRANSITION);
    }function activateNavDots(name, sectionIndex) {
      if (options.navigation) {
        $(SECTION_NAV_SEL).find(ACTIVE_SEL).removeClass(ACTIVE);if (name) {
          $(SECTION_NAV_SEL).find('a[href="#' + name + '"]').addClass(ACTIVE);$(SECTION_NAV_SEL).find('li').eq(sectionIndex).find('.fp-tooltip').addClass(ACTIVE);
        } else {
          $(SECTION_NAV_SEL).find('li').eq(sectionIndex).find('a').addClass(ACTIVE);
        }
      }
    }function activateMenuElement(name) {
      if (options.menu) {
        $(options.menu).find(ACTIVE_SEL).removeClass(ACTIVE);$(options.menu).find('[data-menuanchor="' + name + '"]').addClass(ACTIVE);
      }
    }function activateMenuAndNav(anchor, index) {
      activateMenuElement(anchor);activateNavDots(anchor, index);
    }function getYmovement(destiny) {
      var fromIndex = $(SECTION_ACTIVE_SEL).index(SECTION_SEL);var toIndex = destiny.index(SECTION_SEL);if (fromIndex == toIndex) {
        return 'none';
      }if (fromIndex > toIndex) {
        return 'up';
      }return 'down';
    }function getXmovement(fromIndex, toIndex) {
      if (fromIndex == toIndex) {
        return 'none';
      }if (fromIndex > toIndex) {
        return 'left';
      }return 'right';
    }function createScrollBar(element) {
      if (element.hasClass('fp-noscroll')) return;element.css('overflow', 'hidden');var scrollOverflowHandler = options.scrollOverflowHandler;var wrap = scrollOverflowHandler.wrapContent();var section = element.closest(SECTION_SEL);var scrollable = scrollOverflowHandler.scrollable(element);var contentHeight;if (scrollable.length) {
        contentHeight = scrollOverflowHandler.scrollHeight(element);
      } else {
        contentHeight = element.get(0).scrollHeight;if (options.verticalCentered) {
          contentHeight = element.find(TABLE_CELL_SEL).get(0).scrollHeight;
        }
      }var scrollHeight = windowsHeight - parseInt(section.css('padding-bottom')) - parseInt(section.css('padding-top'));if (contentHeight > scrollHeight) {
        if (scrollable.length) {
          scrollOverflowHandler.update(element, scrollHeight);
        } else {
          if (options.verticalCentered) {
            element.find(TABLE_CELL_SEL).wrapInner(wrap);
          } else {
            element.wrapInner(wrap);
          }scrollOverflowHandler.create(element, scrollHeight);
        }
      } else {
        scrollOverflowHandler.remove(element);
      }element.css('overflow', '');
    }function addTableClass(element) {
      if (!element.hasClass(TABLE)) {
        element.addClass(TABLE).wrapInner('<div class="' + TABLE_CELL + '" style="height:' + getTableHeight(element) + 'px;" />');
      }
    }function getTableHeight(element) {
      var sectionHeight = windowsHeight;if (options.paddingTop || options.paddingBottom) {
        var section = element;if (!section.hasClass(SECTION)) {
          section = element.closest(SECTION_SEL);
        }var paddings = parseInt(section.css('padding-top')) + parseInt(section.css('padding-bottom'));sectionHeight = windowsHeight - paddings;
      }return sectionHeight;
    }function transformContainer(translate3d, animated) {
      if (animated) {
        addAnimation(container);
      } else {
        removeAnimation(container);
      }container.css(getTransforms(translate3d));setTimeout(function () {
        container.removeClass(NO_TRANSITION);
      }, 10);
    }function getSectionByAnchor(sectionAnchor) {
      if (!sectionAnchor) return [];var section = container.find(SECTION_SEL + '[data-anchor="' + sectionAnchor + '"]');if (!section.length) {
        section = $(SECTION_SEL).eq(sectionAnchor - 1);
      }return section;
    }function getSlideByAnchor(slideAnchor, section) {
      var slides = section.find(SLIDES_WRAPPER_SEL);var slide = slides.find(SLIDE_SEL + '[data-anchor="' + slideAnchor + '"]');if (!slide.length) {
        slide = slides.find(SLIDE_SEL).eq(slideAnchor);
      }return slide;
    }function scrollPageAndSlide(destiny, slide) {
      var section = getSectionByAnchor(destiny);if (!section.length) return;if (typeof slide === 'undefined') {
        slide = 0;
      }if (destiny !== lastScrolledDestiny && !section.hasClass(ACTIVE)) {
        scrollPage(section, function () {
          scrollSlider(section, slide);
        });
      } else {
        scrollSlider(section, slide);
      }
    }function scrollSlider(section, slideAnchor) {
      if (typeof slideAnchor !== 'undefined') {
        var slides = section.find(SLIDES_WRAPPER_SEL);var destiny = getSlideByAnchor(slideAnchor, section);if (destiny.length) {
          landscapeScroll(slides, destiny);
        }
      }
    }function addSlidesNavigation(section, numSlides) {
      section.append('<div class="' + SLIDES_NAV + '"><ul></ul></div>');var nav = section.find(SLIDES_NAV_SEL);nav.addClass(options.slidesNavPosition);for (var i = 0; i < numSlides; i++) {
        nav.find('ul').append('<li><a href="#"><span></span></a></li>');
      }nav.css('margin-left', '-' + nav.width() / 2 + 'px');nav.find('li').first().find('a').addClass(ACTIVE);
    }function setState(slideIndex, slideAnchor, anchorLink, sectionIndex) {
      var sectionHash = '';if (options.anchors.length && !options.lockAnchors) {
        if (slideIndex) {
          if (typeof anchorLink !== 'undefined') {
            sectionHash = anchorLink;
          }if (typeof slideAnchor === 'undefined') {
            slideAnchor = slideIndex;
          }lastScrolledSlide = slideAnchor;setUrlHash(sectionHash + '/' + slideAnchor);
        } else if (typeof slideIndex !== 'undefined') {
          lastScrolledSlide = slideAnchor;setUrlHash(anchorLink);
        } else {
          setUrlHash(anchorLink);
        }
      }setBodyClass();
    }function setUrlHash(url) {
      if (options.recordHistory) {
        location.hash = url;
      } else {
        if (isTouchDevice || isTouch) {
          window.history.replaceState(undefined, undefined, '#' + url);
        } else {
          var baseUrl = window.location.href.split('#')[0];window.location.replace(baseUrl + '#' + url);
        }
      }
    }function getAnchor(element) {
      var anchor = element.data('anchor');var index = element.index();if (typeof anchor === 'undefined') {
        anchor = index;
      }return anchor;
    }function setBodyClass() {
      var section = $(SECTION_ACTIVE_SEL);var slide = section.find(SLIDE_ACTIVE_SEL);var sectionAnchor = getAnchor(section);var slideAnchor = getAnchor(slide);var text = String(sectionAnchor);if (slide.length) {
        text = text + '-' + slideAnchor;
      }text = text.replace('/', '-').replace('#', '');var classRe = new RegExp('\\b\\s?' + VIEWING_PREFIX + '-[^\\s]+\\b', "g");$body[0].className = $body[0].className.replace(classRe, '');$body.addClass(VIEWING_PREFIX + '-' + text);
    }function support3d() {
      var el = document.createElement('p'),
          has3d,
          transforms = { 'webkitTransform': '-webkit-transform', 'OTransform': '-o-transform', 'msTransform': '-ms-transform', 'MozTransform': '-moz-transform', 'transform': 'transform' };document.body.insertBefore(el, null);for (var t in transforms) {
        if (el.style[t] !== undefined) {
          el.style[t] = 'translate3d(1px,1px,1px)';has3d = window.getComputedStyle(el).getPropertyValue(transforms[t]);
        }
      }document.body.removeChild(el);return has3d !== undefined && has3d.length > 0 && has3d !== 'none';
    }function removeMouseWheelHandler() {
      if (document.addEventListener) {
        document.removeEventListener('mousewheel', MouseWheelHandler, false);document.removeEventListener('wheel', MouseWheelHandler, false);document.removeEventListener('MozMousePixelScroll', MouseWheelHandler, false);
      } else {
        document.detachEvent('onmousewheel', MouseWheelHandler);
      }
    }function addMouseWheelHandler() {
      var prefix = '';var _addEventListener;if (window.addEventListener) {
        _addEventListener = "addEventListener";
      } else {
        _addEventListener = "attachEvent";prefix = 'on';
      }var support = 'onwheel' in document.createElement('div') ? 'wheel' : document.onmousewheel !== undefined ? 'mousewheel' : 'DOMMouseScroll';if (support == 'DOMMouseScroll') {
        document[_addEventListener](prefix + 'MozMousePixelScroll', MouseWheelHandler, false);
      } else {
        document[_addEventListener](prefix + support, MouseWheelHandler, false);
      }
    }function addMiddleWheelHandler() {
      container.on('mousedown', mouseDownHandler).on('mouseup', mouseUpHandler);
    }function removeMiddleWheelHandler() {
      container.off('mousedown', mouseDownHandler).off('mouseup', mouseUpHandler);
    }function addTouchHandler() {
      if (isTouchDevice || isTouch) {
        if (options.autoScrolling) {
          $body.off(events.touchmove).on(events.touchmove, preventBouncing);
        }$(WRAPPER_SEL).off(events.touchstart).on(events.touchstart, touchStartHandler).off(events.touchmove).on(events.touchmove, touchMoveHandler);
      }
    }function removeTouchHandler() {
      if (isTouchDevice || isTouch) {
        $(WRAPPER_SEL).off(events.touchstart).off(events.touchmove);
      }
    }function getMSPointer() {
      var pointer;if (window.PointerEvent) {
        pointer = { down: 'pointerdown', move: 'pointermove' };
      } else {
        pointer = { down: 'MSPointerDown', move: 'MSPointerMove' };
      }return pointer;
    }function getEventsPage(e) {
      var events = [];events.y = typeof e.pageY !== 'undefined' && (e.pageY || e.pageX) ? e.pageY : e.touches[0].pageY;events.x = typeof e.pageX !== 'undefined' && (e.pageY || e.pageX) ? e.pageX : e.touches[0].pageX;if (isTouch && isReallyTouch(e) && options.scrollBar) {
        events.y = e.touches[0].pageY;events.x = e.touches[0].pageX;
      }return events;
    }function silentLandscapeScroll(activeSlide, noCallbacks) {
      setScrollingSpeed(0, 'internal');if (typeof noCallbacks !== 'undefined') {
        isResizing = true;
      }landscapeScroll(activeSlide.closest(SLIDES_WRAPPER_SEL), activeSlide);if (typeof noCallbacks !== 'undefined') {
        isResizing = false;
      }setScrollingSpeed(originals.scrollingSpeed, 'internal');
    }function silentScroll(top) {
      var roundedTop = Math.round(top);if (options.css3 && options.autoScrolling && !options.scrollBar) {
        var translate3d = 'translate3d(0px, -' + roundedTop + 'px, 0px)';transformContainer(translate3d, false);
      } else if (options.autoScrolling && !options.scrollBar) {
        container.css('top', -roundedTop);
      } else {
        $htmlBody.scrollTop(roundedTop);
      }
    }function getTransforms(translate3d) {
      return { '-webkit-transform': translate3d, '-moz-transform': translate3d, '-ms-transform': translate3d, 'transform': translate3d };
    }function setIsScrollAllowed(value, direction, type) {
      switch (direction) {case 'up':
          isScrollAllowed[type].up = value;break;case 'down':
          isScrollAllowed[type].down = value;break;case 'left':
          isScrollAllowed[type].left = value;break;case 'right':
          isScrollAllowed[type].right = value;break;case 'all':
          if (type == 'm') {
            setAllowScrolling(value);
          } else {
            setKeyboardScrolling(value);
          }}
    }function destroy(all) {
      setAutoScrolling(false, 'internal');setAllowScrolling(false);setKeyboardScrolling(false);container.addClass(DESTROYED);clearTimeout(afterSlideLoadsId);clearTimeout(afterSectionLoadsId);clearTimeout(resizeId);clearTimeout(scrollId);clearTimeout(scrollId2);$window.off('scroll', scrollHandler).off('hashchange', hashChangeHandler).off('resize', resizeHandler);$document.off('click touchstart', SECTION_NAV_SEL + ' a').off('mouseenter', SECTION_NAV_SEL + ' li').off('mouseleave', SECTION_NAV_SEL + ' li').off('click touchstart', SLIDES_NAV_LINK_SEL).off('mouseover', options.normalScrollElements).off('mouseout', options.normalScrollElements);$(SECTION_SEL).off('click touchstart', SLIDES_ARROW_SEL);clearTimeout(afterSlideLoadsId);clearTimeout(afterSectionLoadsId);if (all) {
        destroyStructure();
      }
    }function destroyStructure() {
      silentScroll(0);container.find('img[data-src], source[data-src], audio[data-src], iframe[data-src]').each(function () {
        setSrc($(this), 'src');
      });container.find('img[data-srcset]').each(function () {
        setSrc($(this), 'srcset');
      });$(SECTION_NAV_SEL + ', ' + SLIDES_NAV_SEL + ', ' + SLIDES_ARROW_SEL).remove();$(SECTION_SEL).css({ 'height': '', 'background-color': '', 'padding': '' });$(SLIDE_SEL).css({ 'width': '' });container.css({ 'height': '', 'position': '', '-ms-touch-action': '', 'touch-action': '' });$htmlBody.css({ 'overflow': '', 'height': '' });$('html').removeClass(ENABLED);$body.removeClass(RESPONSIVE);$.each($body.get(0).className.split(/\s+/), function (index, className) {
        if (className.indexOf(VIEWING_PREFIX) === 0) {
          $body.removeClass(className);
        }
      });$(SECTION_SEL + ', ' + SLIDE_SEL).each(function () {
        options.scrollOverflowHandler.remove($(this));$(this).removeClass(TABLE + ' ' + ACTIVE);
      });removeAnimation(container);container.find(TABLE_CELL_SEL + ', ' + SLIDES_CONTAINER_SEL + ', ' + SLIDES_WRAPPER_SEL).each(function () {
        $(this).replaceWith(this.childNodes);
      });container.css({ '-webkit-transition': 'none', 'transition': 'none' });$htmlBody.scrollTop(0);var usedSelectors = [SECTION, SLIDE, SLIDES_CONTAINER];$.each(usedSelectors, function (index, value) {
        $('.' + value).removeClass(value);
      });
    }function setVariableState(variable, value, type) {
      options[variable] = value;if (type !== 'internal') {
        originals[variable] = value;
      }
    }function displayWarnings() {
      var extensions = ['fadingEffect', 'continuousHorizontal', 'scrollHorizontally', 'interlockedSlides', 'resetSliders', 'responsiveSlides', 'offsetSections', 'dragAndMove', 'scrollOverflowReset', 'parallax'];if ($('html').hasClass(ENABLED)) {
        showError('error', 'Fullpage.js can only be initialized once and you are doing it multiple times!');return;
      }if (options.continuousVertical && (options.loopTop || options.loopBottom)) {
        options.continuousVertical = false;showError('warn', 'Option `loopTop/loopBottom` is mutually exclusive with `continuousVertical`; `continuousVertical` disabled');
      }if (options.scrollBar && options.scrollOverflow) {
        showError('warn', 'Option `scrollBar` is mutually exclusive with `scrollOverflow`. Sections with scrollOverflow might not work well in Firefox');
      }if (options.continuousVertical && (options.scrollBar || !options.autoScrolling)) {
        options.continuousVertical = false;showError('warn', 'Scroll bars (`scrollBar:true` or `autoScrolling:false`) are mutually exclusive with `continuousVertical`; `continuousVertical` disabled');
      }$.each(extensions, function (index, extension) {
        if (options[extension]) {
          showError('warn', 'fullpage.js extensions require jquery.fullpage.extensions.min.js file instead of the usual jquery.fullpage.js. Requested: ' + extension);
        }
      });$.each(options.anchors, function (index, name) {
        var nameAttr = $document.find('[name]').filter(function () {
          return $(this).attr('name') && $(this).attr('name').toLowerCase() == name.toLowerCase();
        });var idAttr = $document.find('[id]').filter(function () {
          return $(this).attr('id') && $(this).attr('id').toLowerCase() == name.toLowerCase();
        });if (idAttr.length || nameAttr.length) {
          showError('error', 'data-anchor tags can not have the same value as any `id` element on the site (or `name` element for IE).');idAttr.length && showError('error', '"' + name + '" is is being used by another element `id` property');nameAttr.length && showError('error', '"' + name + '" is is being used by another element `name` property');
        }
      });
    }function showError(type, text) {
      console && console[type] && console[type]('fullPage: ' + text);
    }
  };if (typeof IScroll !== 'undefined') {
    IScroll.prototype.wheelOn = function () {
      this.wrapper.addEventListener('wheel', this);this.wrapper.addEventListener('mousewheel', this);this.wrapper.addEventListener('DOMMouseScroll', this);
    };IScroll.prototype.wheelOff = function () {
      this.wrapper.removeEventListener('wheel', this);this.wrapper.removeEventListener('mousewheel', this);this.wrapper.removeEventListener('DOMMouseScroll', this);
    };
  }var iscrollHandler = { refreshId: null, iScrollInstances: [], toggleWheel: function toggleWheel(value) {
      var scrollable = $(SECTION_ACTIVE_SEL).find(SCROLLABLE_SEL);scrollable.each(function () {
        var iScrollInstance = $(this).data('iscrollInstance');if (typeof iScrollInstance !== 'undefined' && iScrollInstance) {
          if (value) {
            iScrollInstance.wheelOn();
          } else {
            iScrollInstance.wheelOff();
          }
        }
      });
    }, onLeave: function onLeave() {
      iscrollHandler.toggleWheel(false);
    }, beforeLeave: function beforeLeave() {
      iscrollHandler.onLeave();
    }, afterLoad: function afterLoad() {
      iscrollHandler.toggleWheel(true);
    }, create: function create(element, scrollHeight) {
      var scrollable = element.find(SCROLLABLE_SEL);scrollable.height(scrollHeight);scrollable.each(function () {
        var $this = $(this);var iScrollInstance = $this.data('iscrollInstance');if (iScrollInstance) {
          $.each(iscrollHandler.iScrollInstances, function () {
            $(this).destroy();
          });
        }iScrollInstance = new IScroll($this.get(0), iscrollOptions);iscrollHandler.iScrollInstances.push(iScrollInstance);iScrollInstance.wheelOff();$this.data('iscrollInstance', iScrollInstance);
      });
    }, isScrolled: function isScrolled(type, scrollable) {
      var scroller = scrollable.data('iscrollInstance');if (!scroller) {
        return true;
      }if (type === 'top') {
        return scroller.y >= 0 && !scrollable.scrollTop();
      } else if (type === 'bottom') {
        return 0 - scroller.y + scrollable.scrollTop() + 1 + scrollable.innerHeight() >= scrollable[0].scrollHeight;
      }
    }, scrollable: function scrollable(activeSection) {
      if (activeSection.find(SLIDES_WRAPPER_SEL).length) {
        return activeSection.find(SLIDE_ACTIVE_SEL).find(SCROLLABLE_SEL);
      }return activeSection.find(SCROLLABLE_SEL);
    }, scrollHeight: function scrollHeight(element) {
      return element.find(SCROLLABLE_SEL).children().first().get(0).scrollHeight;
    }, remove: function remove(element) {
      var scrollable = element.find(SCROLLABLE_SEL);if (scrollable.length) {
        var iScrollInstance = scrollable.data('iscrollInstance');iScrollInstance.destroy();scrollable.data('iscrollInstance', null);
      }element.find(SCROLLABLE_SEL).children().first().children().first().unwrap().unwrap();
    }, update: function update(element, scrollHeight) {
      clearTimeout(iscrollHandler.refreshId);iscrollHandler.refreshId = setTimeout(function () {
        $.each(iscrollHandler.iScrollInstances, function () {
          $(this).get(0).refresh();
        });
      }, 150);element.find(SCROLLABLE_SEL).css('height', scrollHeight + 'px').parent().css('height', scrollHeight + 'px');
    }, wrapContent: function wrapContent() {
      return '<div class="' + SCROLLABLE + '"><div class="fp-scroller"></div></div>';
    } };
});

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {$(document).ready(function () {
    Dropzone.autoDiscover = false;
    // $('#fullpage').fullpage({
    // 	sectionsColor: ['#111', '#111', '#FFF', '#FFF', '#FFF', '#FFF', '#13171a'],
    // 	slidesNavigation: true,
    // 	navigation: true,
    // 	keyboardScrolling: true,
    //        scrollingSpeed: 700,
    //        touchSensitivity: 15,
    // 	navigationPosition: 'right',
    // 	navigationTooltips: ['About Us', 'How & Why', 'Solution', 'Solution', 'Solution', 'Solution', 'Case Study', 'Client', 'Contact Us'],
    // 	normalScrollElements: '.jquery-modal, .case-wrap, .budget-body, .form-group',
    // 	afterLoad: function(anchorLink, index) {
    // 		//GA Tracking
    // 		gtag('send', 'pageview', { 'page': anchorLink });
    // 	}
    //    });

    $('.case-study-slides').slick({
        centerMode: true,
        appendArrows: $('.case-study-arrows'),
        prevArrow: '<button type="button" class="slick-prev">&larr;<span>More Cases</span></button>',
        nextArrow: '<button type="button" class="slick-next"><span>More Cases</span>&rarr;</button>',
        slidesToShow: 3,
        variableWidth: true
    });

    $('.test-testimonial').slick({
        centerMode: true,
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    });

    var dropZone = new Dropzone('.dropzone', {
        url: '/upload',
        maxFilesize: 10,
        maxFiles: 1,
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        acceptedFiles: 'application/msword, application/pdf',
        hiddenInputContainer: '.contact-form',
        dictDefaultMessage: '<i class="fa fa-upload" style="font-size: 32px;"></i><br />Got docs? Drag &amp; drop \'em here.',
        dictFallbackMessage: 'Got docs? Click to upload.',
        dictFallbackText: null,
        dictInvalidFileType: 'Only supports DOC, and PDF.',
        dictFileTooBig: 'Maximum file size of 10MB allowed.'
    });

    dropZone.on('success', function (file, response) {
        $('.uploaded').val(response);
        this.disable();
    });

    dropZone.on('error', function (file, errorMessage) {
        this.disable();
    });

    dropZone.on('removedfile', function () {
        $('.uploaded').val('');
        this.enable();
    });

    $('.modal-btn').click(function (event) {
        if ($(this).hasClass('case-study-btn')) {
            var ajaxLink = $(this).attr('data-ajax');

            $.get(ajaxLink, function (modalHtml) {
                var result = $(modalHtml).appendTo('body').modal({
                    clickClose: false,
                    closeText: '&bigotimes;',
                    escapeClose: false
                });

                var url = ajaxLink;
                var array = url.split('/');
                var lastSegment = array[array.length - 1];
                var lastSecondSegment = array[array.length - 2];

                //GA Tracking
                gtag('send', 'pageview', { 'page': lastSecondSegment + '/' + lastSegment });

                $('body').one($.modal.AFTER_CLOSE, function () {
                    location.hash = 'case-study';
                });
            });
        } else if ($(this).hasClass('contact-form-btn')) {
            event.preventDefault();

            $('body').one($.modal.OPEN, function () {
                var modalDropZone = new Dropzone('.contact-modal .dropzone', {
                    url: '/upload',
                    maxFilesize: 10,
                    maxFiles: 1,
                    addRemoveLinks: true,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    acceptedFiles: 'application/msword, application/pdf',
                    hiddenInputContainer: '.contact-form',
                    dictDefaultMessage: '<i class="fa fa-upload" style="font-size: 32px;"></i><br />Got docs? Drag &amp; drop \'em here.',
                    dictFallbackMessage: 'Got docs? Click to upload.',
                    dictFallbackText: null,
                    dictInvalidFileType: 'Only supports DOC, and PDF.',
                    dictFileTooBig: 'Maximum file size of 10MB allowed.'
                });

                modalDropZone.on('success', function (file, response) {
                    $('.contact-modal .uploaded').val(response);
                    this.disable();
                });

                modalDropZone.on('error', function (file, errorMessage) {
                    this.disable();
                });

                modalDropZone.on('removedfile', function () {
                    $('.contact-modal .uploaded').val('');
                    this.enable();
                });
            });

            $(this).modal({
                clickClose: false,
                closeText: '&bigotimes;',
                escapeClose: false,
                modalClass: 'modal contact-modal'
            });
        }
    });

    $('body').on($.modal.CLOSE, function (event, modal) {
        // Re-enable fullPage.js scrolling
        // $.fn.fullpage.setAllowScrolling(true);
        // $.fn.fullpage.setKeyboardScrolling(true);

        $('.ind, .obj').val('default');
        $('.ind, .obj').selectpicker('refresh');

        $('.lc, .bd').val('');
    });

    $('body').on('click', '.case-study-modal .modal-nav a', function (event) {
        var ajaxLink = $(this).attr('data-ajax');

        $('.modal').scrollTop(0);

        $('.case-study-modal').empty();

        // Change the content of the case study modal
        $.get(ajaxLink, function (modalHtml) {
            $('.case-study-modal').html($(modalHtml).html() + '<a href="#close-modal" rel="modal:close" class="close-modal ">&bigotimes;</a>');

            var url = ajaxLink;
            var array = url.split('/');
            var lastSegment = array[array.length - 1];
            var lastSecondSegment = array[array.length - 2];

            //GA Tracking
            gtag('send', 'pageview', { 'page': lastSecondSegment + '/' + lastSegment });
        });
    });

    $('body').on('input', '.numeric', function (event) {
        // Only allow numbers for this input field
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    $('body').on('click', '.submitContact', function () {
        // Check if the form is from modal or body
        if ($(this).parents('.jquery-modal').length) {
            var val = new FormData($('.postContact')[1]);
        } else {
            var val = new FormData($('.postContact')[0]);
        }

        var url = "contact-us";
        var sEmail = $('input[name="email"]').val();

        $.ajax({
            method: 'POST',
            url: url,
            data: val,
            processData: false,
            contentType: false,
            cache: false,
            success: function success(r) {
                if (r.status == '1') {
                    // $('#thanks').modal('show');
                    $(".postContact")[0].reset();
                    dropZone.removeAllFiles();
                    //GA Tracking
                    gtag('send', 'event', 'Form', 'Submission');
                    window.location = "thanks";
                }
            },
            error: function error(er) {
                // Show validation error results
                if (er.responseJSON.name) {
                    $('.name').empty();
                    var err = '<div class="text-danger">Name is required</div>';
                    $('.name').append(err);
                    $('.name').show(err);
                } else {
                    $('.name').hide();
                }

                if (er.responseJSON.contact) {
                    $('.contact').empty();
                    var err = '<div class="text-danger">Contact is required</div>';
                    $('.contact').append(err);
                    $('.contact').show(err);
                } else {
                    $('.contact').hide();
                }

                if (er.responseJSON.company) {
                    $('.company').empty();
                    var err = '<div class="text-danger">Company is required</div>';
                    $('.company').append(err);
                    $('.company').show(err);
                } else {
                    $('.company').hide();
                }

                if (er.responseJSON.message) {
                    $('.message').empty();
                    var err = '<div class="text-danger">Message is required</div>';
                    $('.message').append(err);
                    $('.message').show(err);
                } else {
                    $('.message').hide();
                }

                function validateEmail(sEmail) {
                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    if (filter.test(sEmail)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                if (er.responseJSON.email) {
                    $('.email').empty();
                    var err = '<div class="text-danger">Email is required</div>';
                    $('.email').append(err);
                    $('.email').show(err);
                } else if (!validateEmail(sEmail)) {
                    $('.email').empty();
                    var err = '<div class="text-danger">This must be a valid email address</div>';
                    $('.email').append(err);
                    $('.email').show(err);
                } else {
                    $('.email').hide();
                }
            }
        });
    });

    $('body').on('keyup', "input[data-type='number']", function (event) {
        // Skip for arrow keys
        if (event.which >= 37 && event.which <= 40) {
            event.preventDefault();
        }

        // Add comma to every third places
        var $this = $(this);
        var num = $this.val().replace(/,/gi, '');
        var num2 = num.split(/(?=(?:\d{3})+$)/).join(',');

        $this.val(num2);
    });

    $('.cta-btn').on('click', function () {
        event.preventDefault();

        if ($(window).width() > 991) {
            $('html, body').animate({ scrollTop: $('.section[data-anchor="contact-us"]').offset().top }, 500);
        } else {
            $('#modal-contact-btn').click();
        }
    });
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module, jQuery) {var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
    A simple jQuery modal (http://github.com/kylefox/jquery-modal)
    Version 0.8.0
*/
!function (o) {
    "object" == ( false ? "undefined" : _typeof(module)) && "object" == _typeof(module.exports) ? o(__webpack_require__(0), window, document) : o(jQuery, window, document);
}(function (o, t, e, i) {
    var s = [],
        l = function l() {
        return s.length ? s[s.length - 1] : null;
    },
        n = function n() {
        var o,
            t = !1;for (o = s.length - 1; o >= 0; o--) {
            s[o].$blocker && (s[o].$blocker.toggleClass("current", !t).toggleClass("behind", t), t = !0);
        }
    };o.modal = function (t, e) {
        var i, n;if (this.$body = o("body"), this.options = o.extend({}, o.modal.defaults, e), this.options.doFade = !isNaN(parseInt(this.options.fadeDuration, 10)), this.$blocker = null, this.options.closeExisting) for (; o.modal.isActive();) {
            o.modal.close();
        }if (s.push(this), t.is("a")) {
            if (n = t.attr("href"), /^#/.test(n)) {
                if (this.$elm = o(n), 1 !== this.$elm.length) return null;this.$body.append(this.$elm), this.open();
            } else this.$elm = o("<div>"), this.$body.append(this.$elm), i = function i(o, t) {
                t.elm.remove();
            }, this.showSpinner(), t.trigger(o.modal.AJAX_SEND), o.get(n).done(function (e) {
                if (o.modal.isActive()) {
                    t.trigger(o.modal.AJAX_SUCCESS);var s = l();s.$elm.empty().append(e).on(o.modal.CLOSE, i), s.hideSpinner(), s.open(), t.trigger(o.modal.AJAX_COMPLETE);
                }
            }).fail(function () {
                t.trigger(o.modal.AJAX_FAIL);var e = l();e.hideSpinner(), s.pop(), t.trigger(o.modal.AJAX_COMPLETE);
            });
        } else this.$elm = t, this.$body.append(this.$elm), this.open();
    }, o.modal.prototype = { constructor: o.modal, open: function open() {
            var t = this;this.block(), this.options.doFade ? setTimeout(function () {
                t.show();
            }, this.options.fadeDuration * this.options.fadeDelay) : this.show(), o(e).off("keydown.modal").on("keydown.modal", function (o) {
                var t = l();27 == o.which && t.options.escapeClose && t.close();
            }), this.options.clickClose && this.$blocker.click(function (t) {
                t.target == this && o.modal.close();
            });
        }, close: function close() {
            s.pop(), this.unblock(), this.hide(), o.modal.isActive() || o(e).off("keydown.modal");
        }, block: function block() {
            this.$elm.trigger(o.modal.BEFORE_BLOCK, [this._ctx()]), this.$body.css("overflow", "hidden"), this.$blocker = o('<div class="jquery-modal blocker current"></div>').appendTo(this.$body), n(), this.options.doFade && this.$blocker.css("opacity", 0).animate({ opacity: 1 }, this.options.fadeDuration), this.$elm.trigger(o.modal.BLOCK, [this._ctx()]);
        }, unblock: function unblock(t) {
            !t && this.options.doFade ? this.$blocker.fadeOut(this.options.fadeDuration, this.unblock.bind(this, !0)) : (this.$blocker.children().appendTo(this.$body), this.$blocker.remove(), this.$blocker = null, n(), o.modal.isActive() || this.$body.css("overflow", ""));
        }, show: function show() {
            this.$elm.trigger(o.modal.BEFORE_OPEN, [this._ctx()]), this.options.showClose && (this.closeButton = o('<a href="#close-modal" rel="modal:close" class="close-modal ' + this.options.closeClass + '">' + this.options.closeText + "</a>"), this.$elm.append(this.closeButton)), this.$elm.addClass(this.options.modalClass).appendTo(this.$blocker), this.options.doFade ? this.$elm.css("opacity", 0).show().animate({ opacity: 1 }, this.options.fadeDuration) : this.$elm.show(), this.$elm.trigger(o.modal.OPEN, [this._ctx()]);
        }, hide: function hide() {
            this.$elm.trigger(o.modal.BEFORE_CLOSE, [this._ctx()]), this.closeButton && this.closeButton.remove();var t = this;this.options.doFade ? this.$elm.fadeOut(this.options.fadeDuration, function () {
                t.$elm.trigger(o.modal.AFTER_CLOSE, [t._ctx()]);
            }) : this.$elm.hide(0, function () {
                t.$elm.trigger(o.modal.AFTER_CLOSE, [t._ctx()]);
            }), this.$elm.trigger(o.modal.CLOSE, [this._ctx()]);
        }, showSpinner: function showSpinner() {
            this.options.showSpinner && (this.spinner = this.spinner || o('<div class="' + this.options.modalClass + '-spinner"></div>').append(this.options.spinnerHtml), this.$body.append(this.spinner), this.spinner.show());
        }, hideSpinner: function hideSpinner() {
            this.spinner && this.spinner.remove();
        }, _ctx: function _ctx() {
            return { elm: this.$elm, $blocker: this.$blocker, options: this.options };
        } }, o.modal.close = function (t) {
        if (o.modal.isActive()) {
            t && t.preventDefault();var e = l();return e.close(), e.$elm;
        }
    }, o.modal.isActive = function () {
        return s.length > 0;
    }, o.modal.getCurrent = l, o.modal.defaults = { closeExisting: !0, escapeClose: !0, clickClose: !0, closeText: "Close", closeClass: "", modalClass: "modal", spinnerHtml: null, showSpinner: !0, showClose: !0, fadeDuration: null, fadeDelay: 1 }, o.modal.BEFORE_BLOCK = "modal:before-block", o.modal.BLOCK = "modal:block", o.modal.BEFORE_OPEN = "modal:before-open", o.modal.OPEN = "modal:open", o.modal.BEFORE_CLOSE = "modal:before-close", o.modal.CLOSE = "modal:close", o.modal.AFTER_CLOSE = "modal:after-close", o.modal.AJAX_SEND = "modal:ajax:send", o.modal.AJAX_SUCCESS = "modal:ajax:success", o.modal.AJAX_FAIL = "modal:ajax:fail", o.modal.AJAX_COMPLETE = "modal:ajax:complete", o.fn.modal = function (t) {
        return 1 === this.length && new o.modal(this, t), this;
    }, o(e).on("click.modal", 'a[rel~="modal:close"]', o.modal.close), o(e).on("click.modal", 'a[rel~="modal:open"]', function (t) {
        t.preventDefault(), o(this).modal();
    });
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)(module), __webpack_require__(0)))

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {$(function () {
    if (animation_path) {
        var animation = bodymovin.loadAnimation({
            container: document.getElementById("wd"),
            renderer: "svg",
            loop: !0,
            autoplay: !0,
            path: animation_path + "/wd/data.json"
        });
        animation = bodymovin.loadAnimation({
            container: document.getElementById("da"),
            renderer: "svg",
            loop: !0,
            autoplay: !0,
            path: animation_path + "/da/data.json"
        }), animation = bodymovin.loadAnimation({
            container: document.getElementById("da2"),
            renderer: "svg",
            loop: !0,
            autoplay: !0,
            path: animation_path + "/da/data.json"
        }), animation = bodymovin.loadAnimation({
            container: document.getElementById("cc"),
            renderer: "svg",
            loop: !0,
            autoplay: !0,
            path: animation_path + "/cc/data.json"
        }), animation = bodymovin.loadAnimation({
            container: document.getElementById("sma"),
            renderer: "svg",
            loop: !0,
            autoplay: !0,
            path: animation_path + "/sma/data.json"
        }), animation = bodymovin.loadAnimation({
            container: document.getElementById("sma2"),
            renderer: "svg",
            loop: !0,
            autoplay: !0,
            path: animation_path + "/sma/data.json"
        });
    }
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! iScroll v5.2.0 ~ (c) 2008-2016 Matteo Spinelli ~ http://cubiq.org/license */
(function (g, q, f) {
  function p(a, b) {
    this.wrapper = "string" == typeof a ? q.querySelector(a) : a;this.scroller = this.wrapper.children[0];this.scrollerStyle = this.scroller.style;this.options = { resizeScrollbars: !0, mouseWheelSpeed: 20, snapThreshold: .334, disablePointer: !d.hasPointer, disableTouch: d.hasPointer || !d.hasTouch, disableMouse: d.hasPointer || d.hasTouch, startX: 0, startY: 0, scrollY: !0, directionLockThreshold: 5, momentum: !0, bounce: !0, bounceTime: 600, bounceEasing: "", preventDefault: !0, preventDefaultException: { tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT)$/ },
      HWCompositing: !0, useTransition: !0, useTransform: !0, bindToWrapper: "undefined" === typeof g.onmousedown };for (var c in b) {
      this.options[c] = b[c];
    }this.translateZ = this.options.HWCompositing && d.hasPerspective ? " translateZ(0)" : "";this.options.useTransition = d.hasTransition && this.options.useTransition;this.options.useTransform = d.hasTransform && this.options.useTransform;this.options.eventPassthrough = !0 === this.options.eventPassthrough ? "vertical" : this.options.eventPassthrough;this.options.preventDefault = !this.options.eventPassthrough && this.options.preventDefault;this.options.scrollY = "vertical" == this.options.eventPassthrough ? !1 : this.options.scrollY;this.options.scrollX = "horizontal" == this.options.eventPassthrough ? !1 : this.options.scrollX;this.options.freeScroll = this.options.freeScroll && !this.options.eventPassthrough;this.options.directionLockThreshold = this.options.eventPassthrough ? 0 : this.options.directionLockThreshold;this.options.bounceEasing = "string" == typeof this.options.bounceEasing ? d.ease[this.options.bounceEasing] || d.ease.circular : this.options.bounceEasing;this.options.resizePolling = void 0 === this.options.resizePolling ? 60 : this.options.resizePolling;!0 === this.options.tap && (this.options.tap = "tap");this.options.useTransition || this.options.useTransform || /relative|absolute/i.test(this.scrollerStyle.position) || (this.scrollerStyle.position = "relative");"scale" == this.options.shrinkScrollbars && (this.options.useTransition = !1);this.options.invertWheelDirection = this.options.invertWheelDirection ? -1 : 1;this.directionY = this.directionX = this.y = this.x = 0;this._events = {};this._init();this.refresh();this.scrollTo(this.options.startX, this.options.startY);this.enable();
  }function u(a, b, c) {
    var e = q.createElement("div"),
        d = q.createElement("div");!0 === c && (e.style.cssText = "position:absolute;z-index:9999", d.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;background:rgba(0,0,0,0.5);border:1px solid rgba(255,255,255,0.9);border-radius:3px");d.className = "iScrollIndicator";"h" == a ? (!0 === c && (e.style.cssText += ";height:7px;left:2px;right:2px;bottom:0", d.style.height = "100%"), e.className = "iScrollHorizontalScrollbar") : (!0 === c && (e.style.cssText += ";width:7px;bottom:2px;top:2px;right:1px", d.style.width = "100%"), e.className = "iScrollVerticalScrollbar");e.style.cssText += ";overflow:hidden";b || (e.style.pointerEvents = "none");e.appendChild(d);return e;
  }function v(a, b) {
    this.wrapper = "string" == typeof b.el ? q.querySelector(b.el) : b.el;this.wrapperStyle = this.wrapper.style;this.indicator = this.wrapper.children[0];this.indicatorStyle = this.indicator.style;this.scroller = a;this.options = { listenX: !0, listenY: !0, interactive: !1, resize: !0, defaultScrollbars: !1, shrink: !1, fade: !1, speedRatioX: 0, speedRatioY: 0 };for (var c in b) {
      this.options[c] = b[c];
    }this.sizeRatioY = this.sizeRatioX = 1;this.maxPosY = this.maxPosX = 0;this.options.interactive && (this.options.disableTouch || (d.addEvent(this.indicator, "touchstart", this), d.addEvent(g, "touchend", this)), this.options.disablePointer || (d.addEvent(this.indicator, d.prefixPointerEvent("pointerdown"), this), d.addEvent(g, d.prefixPointerEvent("pointerup"), this)), this.options.disableMouse || (d.addEvent(this.indicator, "mousedown", this), d.addEvent(g, "mouseup", this)));if (this.options.fade) {
      this.wrapperStyle[d.style.transform] = this.scroller.translateZ;var e = d.style.transitionDuration;if (e) {
        this.wrapperStyle[e] = d.isBadAndroid ? "0.0001ms" : "0ms";var f = this;d.isBadAndroid && t(function () {
          "0.0001ms" === f.wrapperStyle[e] && (f.wrapperStyle[e] = "0s");
        });this.wrapperStyle.opacity = "0";
      }
    }
  }var t = g.requestAnimationFrame || g.webkitRequestAnimationFrame || g.mozRequestAnimationFrame || g.oRequestAnimationFrame || g.msRequestAnimationFrame || function (a) {
    g.setTimeout(a, 1E3 / 60);
  },
      d = function () {
    function a(a) {
      return !1 === e ? !1 : "" === e ? a : e + a.charAt(0).toUpperCase() + a.substr(1);
    }var b = {},
        c = q.createElement("div").style,
        e = function () {
      for (var a = ["t", "webkitT", "MozT", "msT", "OT"], b, e = 0, d = a.length; e < d; e++) {
        if (b = a[e] + "ransform", b in c) return a[e].substr(0, a[e].length - 1);
      }return !1;
    }();b.getTime = Date.now || function () {
      return new Date().getTime();
    };b.extend = function (a, b) {
      for (var c in b) {
        a[c] = b[c];
      }
    };b.addEvent = function (a, b, c, e) {
      a.addEventListener(b, c, !!e);
    };b.removeEvent = function (a, b, c, e) {
      a.removeEventListener(b, c, !!e);
    };b.prefixPointerEvent = function (a) {
      return g.MSPointerEvent ? "MSPointer" + a.charAt(7).toUpperCase() + a.substr(8) : a;
    };b.momentum = function (a, b, c, e, d, k) {
      b = a - b;c = f.abs(b) / c;var g;k = void 0 === k ? 6E-4 : k;g = a + c * c / (2 * k) * (0 > b ? -1 : 1);k = c / k;g < e ? (g = d ? e - d / 2.5 * (c / 8) : e, b = f.abs(g - a), k = b / c) : 0 < g && (g = d ? d / 2.5 * (c / 8) : 0, b = f.abs(a) + g, k = b / c);return { destination: f.round(g), duration: k };
    };var d = a("transform");b.extend(b, { hasTransform: !1 !== d, hasPerspective: a("perspective") in c, hasTouch: "ontouchstart" in g, hasPointer: !(!g.PointerEvent && !g.MSPointerEvent), hasTransition: a("transition") in c });b.isBadAndroid = function () {
      var a = g.navigator.appVersion;return (/Android/.test(a) && !/Chrome\/\d/.test(a) ? (a = a.match(/Safari\/(\d+.\d)/)) && "object" === (typeof a === "undefined" ? "undefined" : _typeof(a)) && 2 <= a.length ? 535.19 > parseFloat(a[1]) : !0 : !1
      );
    }();b.extend(b.style = {}, { transform: d, transitionTimingFunction: a("transitionTimingFunction"), transitionDuration: a("transitionDuration"),
      transitionDelay: a("transitionDelay"), transformOrigin: a("transformOrigin") });b.hasClass = function (a, b) {
      return new RegExp("(^|\\s)" + b + "(\\s|$)").test(a.className);
    };b.addClass = function (a, c) {
      if (!b.hasClass(a, c)) {
        var e = a.className.split(" ");e.push(c);a.className = e.join(" ");
      }
    };b.removeClass = function (a, c) {
      b.hasClass(a, c) && (a.className = a.className.replace(new RegExp("(^|\\s)" + c + "(\\s|$)", "g"), " "));
    };b.offset = function (a) {
      for (var b = -a.offsetLeft, c = -a.offsetTop; a = a.offsetParent;) {
        b -= a.offsetLeft, c -= a.offsetTop;
      }return { left: b, top: c };
    };b.preventDefaultException = function (a, b) {
      for (var c in b) {
        if (b[c].test(a[c])) return !0;
      }return !1;
    };b.extend(b.eventType = {}, { touchstart: 1, touchmove: 1, touchend: 1, mousedown: 2, mousemove: 2, mouseup: 2, pointerdown: 3, pointermove: 3, pointerup: 3, MSPointerDown: 3, MSPointerMove: 3, MSPointerUp: 3 });b.extend(b.ease = {}, { quadratic: { style: "cubic-bezier(0.25, 0.46, 0.45, 0.94)", fn: function fn(a) {
          return a * (2 - a);
        } }, circular: { style: "cubic-bezier(0.1, 0.57, 0.1, 1)", fn: function fn(a) {
          return f.sqrt(1 - --a * a);
        } }, back: { style: "cubic-bezier(0.175, 0.885, 0.32, 1.275)",
        fn: function fn(a) {
          return --a * a * (5 * a + 4) + 1;
        } }, bounce: { style: "", fn: function fn(a) {
          return (a /= 1) < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : a < 2.5 / 2.75 ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375;
        } }, elastic: { style: "", fn: function fn(a) {
          return 0 === a ? 0 : 1 == a ? 1 : .4 * f.pow(2, -10 * a) * f.sin(2 * (a - .055) * f.PI / .22) + 1;
        } } });b.tap = function (a, b) {
      var c = q.createEvent("Event");c.initEvent(b, !0, !0);c.pageX = a.pageX;c.pageY = a.pageY;a.target.dispatchEvent(c);
    };b.click = function (a) {
      var b = a.target,
          c;/(SELECT|INPUT|TEXTAREA)/i.test(b.tagName) || (c = q.createEvent(g.MouseEvent ? "MouseEvents" : "Event"), c.initEvent("click", !0, !0), c.view = a.view || g, c.detail = 1, c.screenX = b.screenX || 0, c.screenY = b.screenY || 0, c.clientX = b.clientX || 0, c.clientY = b.clientY || 0, c.ctrlKey = !!a.ctrlKey, c.altKey = !!a.altKey, c.shiftKey = !!a.shiftKey, c.metaKey = !!a.metaKey, c.button = 0, c.relatedTarget = null, c._constructed = !0, b.dispatchEvent(c));
    };return b;
  }();p.prototype = { version: "5.2.0", _init: function _init() {
      this._initEvents();(this.options.scrollbars || this.options.indicators) && this._initIndicators();
      this.options.mouseWheel && this._initWheel();this.options.snap && this._initSnap();this.options.keyBindings && this._initKeys();
    }, destroy: function destroy() {
      this._initEvents(!0);clearTimeout(this.resizeTimeout);this.resizeTimeout = null;this._execEvent("destroy");
    }, _transitionEnd: function _transitionEnd(a) {
      a.target == this.scroller && this.isInTransition && (this._transitionTime(), this.resetPosition(this.options.bounceTime) || (this.isInTransition = !1, this._execEvent("scrollEnd")));
    }, _start: function _start(a) {
      if (!(1 != d.eventType[a.type] && 0 !== (a.which ? a.button : 2 > a.button ? 0 : 4 == a.button ? 1 : 2) || !this.enabled || this.initiated && d.eventType[a.type] !== this.initiated)) {
        !this.options.preventDefault || d.isBadAndroid || d.preventDefaultException(a.target, this.options.preventDefaultException) || a.preventDefault();var b = a.touches ? a.touches[0] : a;this.initiated = d.eventType[a.type];this.moved = !1;this.directionLocked = this.directionY = this.directionX = this.distY = this.distX = 0;this.startTime = d.getTime();this.options.useTransition && this.isInTransition ? (this._transitionTime(), this.isInTransition = !1, a = this.getComputedPosition(), this._translate(f.round(a.x), f.round(a.y)), this._execEvent("scrollEnd")) : !this.options.useTransition && this.isAnimating && (this.isAnimating = !1, this._execEvent("scrollEnd"));this.startX = this.x;this.startY = this.y;this.absStartX = this.x;this.absStartY = this.y;this.pointX = b.pageX;this.pointY = b.pageY;this._execEvent("beforeScrollStart");
      }
    }, _move: function _move(a) {
      if (this.enabled && d.eventType[a.type] === this.initiated) {
        this.options.preventDefault && a.preventDefault();
        var b = a.touches ? a.touches[0] : a,
            c = b.pageX - this.pointX,
            e = b.pageY - this.pointY,
            k = d.getTime(),
            h;this.pointX = b.pageX;this.pointY = b.pageY;this.distX += c;this.distY += e;b = f.abs(this.distX);h = f.abs(this.distY);if (!(300 < k - this.endTime && 10 > b && 10 > h)) {
          this.directionLocked || this.options.freeScroll || (this.directionLocked = b > h + this.options.directionLockThreshold ? "h" : h >= b + this.options.directionLockThreshold ? "v" : "n");if ("h" == this.directionLocked) {
            if ("vertical" == this.options.eventPassthrough) a.preventDefault();else if ("horizontal" == this.options.eventPassthrough) {
              this.initiated = !1;return;
            }e = 0;
          } else if ("v" == this.directionLocked) {
            if ("horizontal" == this.options.eventPassthrough) a.preventDefault();else if ("vertical" == this.options.eventPassthrough) {
              this.initiated = !1;return;
            }c = 0;
          }c = this.hasHorizontalScroll ? c : 0;e = this.hasVerticalScroll ? e : 0;a = this.x + c;b = this.y + e;if (0 < a || a < this.maxScrollX) a = this.options.bounce ? this.x + c / 3 : 0 < a ? 0 : this.maxScrollX;if (0 < b || b < this.maxScrollY) b = this.options.bounce ? this.y + e / 3 : 0 < b ? 0 : this.maxScrollY;this.directionX = 0 < c ? -1 : 0 > c ? 1 : 0;this.directionY = 0 < e ? -1 : 0 > e ? 1 : 0;this.moved || this._execEvent("scrollStart");this.moved = !0;this._translate(a, b);300 < k - this.startTime && (this.startTime = k, this.startX = this.x, this.startY = this.y);
        }
      }
    }, _end: function _end(a) {
      if (this.enabled && d.eventType[a.type] === this.initiated) {
        this.options.preventDefault && !d.preventDefaultException(a.target, this.options.preventDefaultException) && a.preventDefault();var b, c;c = d.getTime() - this.startTime;var e = f.round(this.x),
            k = f.round(this.y),
            h = f.abs(e - this.startX),
            g = f.abs(k - this.startY);b = 0;var l = "";this.initiated = this.isInTransition = 0;this.endTime = d.getTime();if (!this.resetPosition(this.options.bounceTime)) if (this.scrollTo(e, k), this.moved) {
          if (this._events.flick && 200 > c && 100 > h && 100 > g) this._execEvent("flick");else if (this.options.momentum && 300 > c && (b = this.hasHorizontalScroll ? d.momentum(this.x, this.startX, c, this.maxScrollX, this.options.bounce ? this.wrapperWidth : 0, this.options.deceleration) : { destination: e, duration: 0 }, c = this.hasVerticalScroll ? d.momentum(this.y, this.startY, c, this.maxScrollY, this.options.bounce ? this.wrapperHeight : 0, this.options.deceleration) : { destination: k, duration: 0 }, e = b.destination, k = c.destination, b = f.max(b.duration, c.duration), this.isInTransition = 1), this.options.snap && (this.currentPage = l = this._nearestSnap(e, k), b = this.options.snapSpeed || f.max(f.max(f.min(f.abs(e - l.x), 1E3), f.min(f.abs(k - l.y), 1E3)), 300), e = l.x, k = l.y, this.directionY = this.directionX = 0, l = this.options.bounceEasing), e != this.x || k != this.y) {
            if (0 < e || e < this.maxScrollX || 0 < k || k < this.maxScrollY) l = d.ease.quadratic;this.scrollTo(e, k, b, l);
          } else this._execEvent("scrollEnd");
        } else this.options.tap && d.tap(a, this.options.tap), this.options.click && d.click(a), this._execEvent("scrollCancel");
      }
    }, _resize: function _resize() {
      var a = this;clearTimeout(this.resizeTimeout);this.resizeTimeout = setTimeout(function () {
        a.refresh();
      }, this.options.resizePolling);
    }, resetPosition: function resetPosition(a) {
      var b = this.x,
          c = this.y;!this.hasHorizontalScroll || 0 < this.x ? b = 0 : this.x < this.maxScrollX && (b = this.maxScrollX);!this.hasVerticalScroll || 0 < this.y ? c = 0 : this.y < this.maxScrollY && (c = this.maxScrollY);if (b == this.x && c == this.y) return !1;this.scrollTo(b, c, a || 0, this.options.bounceEasing);return !0;
    }, disable: function disable() {
      this.enabled = !1;
    }, enable: function enable() {
      this.enabled = !0;
    }, refresh: function refresh() {
      this.wrapperWidth = this.wrapper.clientWidth;this.wrapperHeight = this.wrapper.clientHeight;this.scrollerWidth = this.scroller.offsetWidth;this.scrollerHeight = this.scroller.offsetHeight;this.maxScrollX = this.wrapperWidth - this.scrollerWidth;this.maxScrollY = this.wrapperHeight - this.scrollerHeight;
      this.hasHorizontalScroll = this.options.scrollX && 0 > this.maxScrollX;this.hasVerticalScroll = this.options.scrollY && 0 > this.maxScrollY;this.hasHorizontalScroll || (this.maxScrollX = 0, this.scrollerWidth = this.wrapperWidth);this.hasVerticalScroll || (this.maxScrollY = 0, this.scrollerHeight = this.wrapperHeight);this.directionY = this.directionX = this.endTime = 0;this.wrapperOffset = d.offset(this.wrapper);this._execEvent("refresh");this.resetPosition();
    }, on: function on(a, b) {
      this._events[a] || (this._events[a] = []);this._events[a].push(b);
    },
    off: function off(a, b) {
      if (this._events[a]) {
        var c = this._events[a].indexOf(b);-1 < c && this._events[a].splice(c, 1);
      }
    }, _execEvent: function _execEvent(a) {
      if (this._events[a]) {
        var b = 0,
            c = this._events[a].length;if (c) for (; b < c; b++) {
          this._events[a][b].apply(this, [].slice.call(arguments, 1));
        }
      }
    }, scrollBy: function scrollBy(a, b, c, e) {
      a = this.x + a;b = this.y + b;this.scrollTo(a, b, c || 0, e);
    }, scrollTo: function scrollTo(a, b, c, e) {
      e = e || d.ease.circular;this.isInTransition = this.options.useTransition && 0 < c;var f = this.options.useTransition && e.style;!c || f ? (f && (this._transitionTimingFunction(e.style), this._transitionTime(c)), this._translate(a, b)) : this._animate(a, b, c, e.fn);
    }, scrollToElement: function scrollToElement(a, b, c, e, k) {
      if (a = a.nodeType ? a : this.scroller.querySelector(a)) {
        var h = d.offset(a);h.left -= this.wrapperOffset.left;h.top -= this.wrapperOffset.top;!0 === c && (c = f.round(a.offsetWidth / 2 - this.wrapper.offsetWidth / 2));!0 === e && (e = f.round(a.offsetHeight / 2 - this.wrapper.offsetHeight / 2));h.left -= c || 0;h.top -= e || 0;h.left = 0 < h.left ? 0 : h.left < this.maxScrollX ? this.maxScrollX : h.left;h.top = 0 < h.top ? 0 : h.top < this.maxScrollY ? this.maxScrollY : h.top;b = void 0 === b || null === b || "auto" === b ? f.max(f.abs(this.x - h.left), f.abs(this.y - h.top)) : b;this.scrollTo(h.left, h.top, b, k);
      }
    }, _transitionTime: function _transitionTime(a) {
      if (this.options.useTransition) {
        a = a || 0;var b = d.style.transitionDuration;if (b) {
          this.scrollerStyle[b] = a + "ms";if (!a && d.isBadAndroid) {
            this.scrollerStyle[b] = "0.0001ms";var c = this;t(function () {
              "0.0001ms" === c.scrollerStyle[b] && (c.scrollerStyle[b] = "0s");
            });
          }if (this.indicators) for (var e = this.indicators.length; e--;) {
            this.indicators[e].transitionTime(a);
          }
        }
      }
    }, _transitionTimingFunction: function _transitionTimingFunction(a) {
      this.scrollerStyle[d.style.transitionTimingFunction] = a;if (this.indicators) for (var b = this.indicators.length; b--;) {
        this.indicators[b].transitionTimingFunction(a);
      }
    }, _translate: function _translate(a, b) {
      this.options.useTransform ? this.scrollerStyle[d.style.transform] = "translate(" + a + "px," + b + "px)" + this.translateZ : (a = f.round(a), b = f.round(b), this.scrollerStyle.left = a + "px", this.scrollerStyle.top = b + "px");this.x = a;this.y = b;if (this.indicators) for (var c = this.indicators.length; c--;) {
        this.indicators[c].updatePosition();
      }
    }, _initEvents: function _initEvents(a) {
      a = a ? d.removeEvent : d.addEvent;var b = this.options.bindToWrapper ? this.wrapper : g;a(g, "orientationchange", this);a(g, "resize", this);this.options.click && a(this.wrapper, "click", this, !0);this.options.disableMouse || (a(this.wrapper, "mousedown", this), a(b, "mousemove", this), a(b, "mousecancel", this), a(b, "mouseup", this));d.hasPointer && !this.options.disablePointer && (a(this.wrapper, d.prefixPointerEvent("pointerdown"), this), a(b, d.prefixPointerEvent("pointermove"), this), a(b, d.prefixPointerEvent("pointercancel"), this), a(b, d.prefixPointerEvent("pointerup"), this));d.hasTouch && !this.options.disableTouch && (a(this.wrapper, "touchstart", this), a(b, "touchmove", this), a(b, "touchcancel", this), a(b, "touchend", this));a(this.scroller, "transitionend", this);a(this.scroller, "webkitTransitionEnd", this);a(this.scroller, "oTransitionEnd", this);a(this.scroller, "MSTransitionEnd", this);
    }, getComputedPosition: function getComputedPosition() {
      var a = g.getComputedStyle(this.scroller, null),
          b;this.options.useTransform ? (a = a[d.style.transform].split(")")[0].split(", "), b = +(a[12] || a[4]), a = +(a[13] || a[5])) : (b = +a.left.replace(/[^-\d.]/g, ""), a = +a.top.replace(/[^-\d.]/g, ""));return { x: b, y: a };
    }, _initIndicators: function _initIndicators() {
      function a(a) {
        if (f.indicators) for (var b = f.indicators.length; b--;) {
          a.call(f.indicators[b]);
        }
      }var b = this.options.interactiveScrollbars,
          c = "string" != typeof this.options.scrollbars,
          e = [],
          d,
          f = this;this.indicators = [];this.options.scrollbars && (this.options.scrollY && (d = { el: u("v", b, this.options.scrollbars), interactive: b, defaultScrollbars: !0, customStyle: c, resize: this.options.resizeScrollbars, shrink: this.options.shrinkScrollbars,
        fade: this.options.fadeScrollbars, listenX: !1 }, this.wrapper.appendChild(d.el), e.push(d)), this.options.scrollX && (d = { el: u("h", b, this.options.scrollbars), interactive: b, defaultScrollbars: !0, customStyle: c, resize: this.options.resizeScrollbars, shrink: this.options.shrinkScrollbars, fade: this.options.fadeScrollbars, listenY: !1 }, this.wrapper.appendChild(d.el), e.push(d)));this.options.indicators && (e = e.concat(this.options.indicators));for (b = e.length; b--;) {
        this.indicators.push(new v(this, e[b]));
      }this.options.fadeScrollbars && (this.on("scrollEnd", function () {
        a(function () {
          this.fade();
        });
      }), this.on("scrollCancel", function () {
        a(function () {
          this.fade();
        });
      }), this.on("scrollStart", function () {
        a(function () {
          this.fade(1);
        });
      }), this.on("beforeScrollStart", function () {
        a(function () {
          this.fade(1, !0);
        });
      }));this.on("refresh", function () {
        a(function () {
          this.refresh();
        });
      });this.on("destroy", function () {
        a(function () {
          this.destroy();
        });delete this.indicators;
      });
    }, _initWheel: function _initWheel() {
      d.addEvent(this.wrapper, "wheel", this);d.addEvent(this.wrapper, "mousewheel", this);d.addEvent(this.wrapper, "DOMMouseScroll", this);this.on("destroy", function () {
        clearTimeout(this.wheelTimeout);this.wheelTimeout = null;d.removeEvent(this.wrapper, "wheel", this);d.removeEvent(this.wrapper, "mousewheel", this);d.removeEvent(this.wrapper, "DOMMouseScroll", this);
      });
    }, _wheel: function _wheel(a) {
      if (this.enabled) {
        var b,
            c,
            e,
            d = this;void 0 === this.wheelTimeout && d._execEvent("scrollStart");clearTimeout(this.wheelTimeout);this.wheelTimeout = setTimeout(function () {
          d.options.snap || d._execEvent("scrollEnd");d.wheelTimeout = void 0;
        }, 400);if ("deltaX" in a) 1 === a.deltaMode ? (b = -a.deltaX * this.options.mouseWheelSpeed, a = -a.deltaY * this.options.mouseWheelSpeed) : (b = -a.deltaX, a = -a.deltaY);else if ("wheelDeltaX" in a) b = a.wheelDeltaX / 120 * this.options.mouseWheelSpeed, a = a.wheelDeltaY / 120 * this.options.mouseWheelSpeed;else if ("wheelDelta" in a) b = a = a.wheelDelta / 120 * this.options.mouseWheelSpeed;else if ("detail" in a) b = a = -a.detail / 3 * this.options.mouseWheelSpeed;else return;b *= this.options.invertWheelDirection;a *= this.options.invertWheelDirection;
        this.hasVerticalScroll || (b = a, a = 0);this.options.snap ? (c = this.currentPage.pageX, e = this.currentPage.pageY, 0 < b ? c-- : 0 > b && c++, 0 < a ? e-- : 0 > a && e++, this.goToPage(c, e)) : (c = this.x + f.round(this.hasHorizontalScroll ? b : 0), e = this.y + f.round(this.hasVerticalScroll ? a : 0), this.directionX = 0 < b ? -1 : 0 > b ? 1 : 0, this.directionY = 0 < a ? -1 : 0 > a ? 1 : 0, 0 < c ? c = 0 : c < this.maxScrollX && (c = this.maxScrollX), 0 < e ? e = 0 : e < this.maxScrollY && (e = this.maxScrollY), this.scrollTo(c, e, 0));
      }
    }, _initSnap: function _initSnap() {
      this.currentPage = {};"string" == typeof this.options.snap && (this.options.snap = this.scroller.querySelectorAll(this.options.snap));this.on("refresh", function () {
        var a = 0,
            b,
            c = 0,
            e,
            d,
            g,
            n = 0,
            l;e = this.options.snapStepX || this.wrapperWidth;var m = this.options.snapStepY || this.wrapperHeight;this.pages = [];if (this.wrapperWidth && this.wrapperHeight && this.scrollerWidth && this.scrollerHeight) {
          if (!0 === this.options.snap) for (d = f.round(e / 2), g = f.round(m / 2); n > -this.scrollerWidth;) {
            this.pages[a] = [];for (l = b = 0; l > -this.scrollerHeight;) {
              this.pages[a][b] = { x: f.max(n, this.maxScrollX), y: f.max(l, this.maxScrollY), width: e, height: m, cx: n - d, cy: l - g }, l -= m, b++;
            }n -= e;a++;
          } else for (m = this.options.snap, b = m.length, e = -1; a < b; a++) {
            if (0 === a || m[a].offsetLeft <= m[a - 1].offsetLeft) c = 0, e++;this.pages[c] || (this.pages[c] = []);n = f.max(-m[a].offsetLeft, this.maxScrollX);l = f.max(-m[a].offsetTop, this.maxScrollY);d = n - f.round(m[a].offsetWidth / 2);g = l - f.round(m[a].offsetHeight / 2);this.pages[c][e] = { x: n, y: l, width: m[a].offsetWidth, height: m[a].offsetHeight, cx: d, cy: g };n > this.maxScrollX && c++;
          }this.goToPage(this.currentPage.pageX || 0, this.currentPage.pageY || 0, 0);0 === this.options.snapThreshold % 1 ? this.snapThresholdY = this.snapThresholdX = this.options.snapThreshold : (this.snapThresholdX = f.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].width * this.options.snapThreshold), this.snapThresholdY = f.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].height * this.options.snapThreshold));
        }
      });this.on("flick", function () {
        var a = this.options.snapSpeed || f.max(f.max(f.min(f.abs(this.x - this.startX), 1E3), f.min(f.abs(this.y - this.startY), 1E3)), 300);this.goToPage(this.currentPage.pageX + this.directionX, this.currentPage.pageY + this.directionY, a);
      });
    }, _nearestSnap: function _nearestSnap(a, b) {
      if (!this.pages.length) return { x: 0, y: 0, pageX: 0, pageY: 0 };var c = 0,
          e = this.pages.length,
          d = 0;if (f.abs(a - this.absStartX) < this.snapThresholdX && f.abs(b - this.absStartY) < this.snapThresholdY) return this.currentPage;0 < a ? a = 0 : a < this.maxScrollX && (a = this.maxScrollX);0 < b ? b = 0 : b < this.maxScrollY && (b = this.maxScrollY);for (; c < e; c++) {
        if (a >= this.pages[c][0].cx) {
          a = this.pages[c][0].x;
          break;
        }
      }for (e = this.pages[c].length; d < e; d++) {
        if (b >= this.pages[0][d].cy) {
          b = this.pages[0][d].y;break;
        }
      }c == this.currentPage.pageX && (c += this.directionX, 0 > c ? c = 0 : c >= this.pages.length && (c = this.pages.length - 1), a = this.pages[c][0].x);d == this.currentPage.pageY && (d += this.directionY, 0 > d ? d = 0 : d >= this.pages[0].length && (d = this.pages[0].length - 1), b = this.pages[0][d].y);return { x: a, y: b, pageX: c, pageY: d };
    }, goToPage: function goToPage(a, b, c, d) {
      d = d || this.options.bounceEasing;a >= this.pages.length ? a = this.pages.length - 1 : 0 > a && (a = 0);b >= this.pages[a].length ? b = this.pages[a].length - 1 : 0 > b && (b = 0);var g = this.pages[a][b].x,
          h = this.pages[a][b].y;c = void 0 === c ? this.options.snapSpeed || f.max(f.max(f.min(f.abs(g - this.x), 1E3), f.min(f.abs(h - this.y), 1E3)), 300) : c;this.currentPage = { x: g, y: h, pageX: a, pageY: b };this.scrollTo(g, h, c, d);
    }, next: function next(a, b) {
      var c = this.currentPage.pageX,
          d = this.currentPage.pageY;c++;c >= this.pages.length && this.hasVerticalScroll && (c = 0, d++);this.goToPage(c, d, a, b);
    }, prev: function prev(a, b) {
      var c = this.currentPage.pageX,
          d = this.currentPage.pageY;c--;0 > c && this.hasVerticalScroll && (c = 0, d--);this.goToPage(c, d, a, b);
    }, _initKeys: function _initKeys(a) {
      a = { pageUp: 33, pageDown: 34, end: 35, home: 36, left: 37, up: 38, right: 39, down: 40 };var b;if ("object" == _typeof(this.options.keyBindings)) for (b in this.options.keyBindings) {
        "string" == typeof this.options.keyBindings[b] && (this.options.keyBindings[b] = this.options.keyBindings[b].toUpperCase().charCodeAt(0));
      } else this.options.keyBindings = {};for (b in a) {
        this.options.keyBindings[b] = this.options.keyBindings[b] || a[b];
      }d.addEvent(g, "keydown", this);this.on("destroy", function () {
        d.removeEvent(g, "keydown", this);
      });
    }, _key: function _key(a) {
      if (this.enabled) {
        var b = this.options.snap,
            c = b ? this.currentPage.pageX : this.x,
            e = b ? this.currentPage.pageY : this.y,
            g = d.getTime(),
            h = this.keyTime || 0,
            n;this.options.useTransition && this.isInTransition && (n = this.getComputedPosition(), this._translate(f.round(n.x), f.round(n.y)), this.isInTransition = !1);this.keyAcceleration = 200 > g - h ? f.min(this.keyAcceleration + .25, 50) : 0;switch (a.keyCode) {case this.options.keyBindings.pageUp:
            this.hasHorizontalScroll && !this.hasVerticalScroll ? c += b ? 1 : this.wrapperWidth : e += b ? 1 : this.wrapperHeight;break;case this.options.keyBindings.pageDown:
            this.hasHorizontalScroll && !this.hasVerticalScroll ? c -= b ? 1 : this.wrapperWidth : e -= b ? 1 : this.wrapperHeight;break;case this.options.keyBindings.end:
            c = b ? this.pages.length - 1 : this.maxScrollX;e = b ? this.pages[0].length - 1 : this.maxScrollY;break;case this.options.keyBindings.home:
            e = c = 0;break;case this.options.keyBindings.left:
            c += b ? -1 : 5 + this.keyAcceleration >> 0;break;case this.options.keyBindings.up:
            e += b ? 1 : 5 + this.keyAcceleration >> 0;break;case this.options.keyBindings.right:
            c -= b ? -1 : 5 + this.keyAcceleration >> 0;break;case this.options.keyBindings.down:
            e -= b ? 1 : 5 + this.keyAcceleration >> 0;break;default:
            return;}b ? this.goToPage(c, e) : (0 < c ? this.keyAcceleration = c = 0 : c < this.maxScrollX && (c = this.maxScrollX, this.keyAcceleration = 0), 0 < e ? this.keyAcceleration = e = 0 : e < this.maxScrollY && (e = this.maxScrollY, this.keyAcceleration = 0), this.scrollTo(c, e, 0), this.keyTime = g);
      }
    }, _animate: function _animate(a, b, c, e) {
      function f() {
        var r = d.getTime(),
            p;r >= q ? (g.isAnimating = !1, g._translate(a, b), g.resetPosition(g.options.bounceTime) || g._execEvent("scrollEnd")) : (r = (r - m) / c, p = e(r), r = (a - n) * p + n, p = (b - l) * p + l, g._translate(r, p), g.isAnimating && t(f));
      }var g = this,
          n = this.x,
          l = this.y,
          m = d.getTime(),
          q = m + c;this.isAnimating = !0;f();
    }, handleEvent: function handleEvent(a) {
      switch (a.type) {case "touchstart":case "pointerdown":case "MSPointerDown":case "mousedown":
          this._start(a);break;case "touchmove":case "pointermove":case "MSPointerMove":case "mousemove":
          this._move(a);break;case "touchend":case "pointerup":case "MSPointerUp":case "mouseup":case "touchcancel":case "pointercancel":case "MSPointerCancel":case "mousecancel":
          this._end(a);
          break;case "orientationchange":case "resize":
          this._resize();break;case "transitionend":case "webkitTransitionEnd":case "oTransitionEnd":case "MSTransitionEnd":
          this._transitionEnd(a);break;case "wheel":case "DOMMouseScroll":case "mousewheel":
          this._wheel(a);break;case "keydown":
          this._key(a);break;case "click":
          this.enabled && !a._constructed && (a.preventDefault(), a.stopPropagation());}
    } };v.prototype = { handleEvent: function handleEvent(a) {
      switch (a.type) {case "touchstart":case "pointerdown":case "MSPointerDown":case "mousedown":
          this._start(a);
          break;case "touchmove":case "pointermove":case "MSPointerMove":case "mousemove":
          this._move(a);break;case "touchend":case "pointerup":case "MSPointerUp":case "mouseup":case "touchcancel":case "pointercancel":case "MSPointerCancel":case "mousecancel":
          this._end(a);}
    }, destroy: function destroy() {
      this.options.fadeScrollbars && (clearTimeout(this.fadeTimeout), this.fadeTimeout = null);this.options.interactive && (d.removeEvent(this.indicator, "touchstart", this), d.removeEvent(this.indicator, d.prefixPointerEvent("pointerdown"), this), d.removeEvent(this.indicator, "mousedown", this), d.removeEvent(g, "touchmove", this), d.removeEvent(g, d.prefixPointerEvent("pointermove"), this), d.removeEvent(g, "mousemove", this), d.removeEvent(g, "touchend", this), d.removeEvent(g, d.prefixPointerEvent("pointerup"), this), d.removeEvent(g, "mouseup", this));this.options.defaultScrollbars && this.wrapper.parentNode.removeChild(this.wrapper);
    }, _start: function _start(a) {
      var b = a.touches ? a.touches[0] : a;a.preventDefault();a.stopPropagation();this.transitionTime();this.initiated = !0;this.moved = !1;this.lastPointX = b.pageX;this.lastPointY = b.pageY;this.startTime = d.getTime();this.options.disableTouch || d.addEvent(g, "touchmove", this);this.options.disablePointer || d.addEvent(g, d.prefixPointerEvent("pointermove"), this);this.options.disableMouse || d.addEvent(g, "mousemove", this);this.scroller._execEvent("beforeScrollStart");
    }, _move: function _move(a) {
      var b = a.touches ? a.touches[0] : a,
          c,
          e;d.getTime();this.moved || this.scroller._execEvent("scrollStart");this.moved = !0;c = b.pageX - this.lastPointX;this.lastPointX = b.pageX;e = b.pageY - this.lastPointY;this.lastPointY = b.pageY;this._pos(this.x + c, this.y + e);a.preventDefault();a.stopPropagation();
    }, _end: function _end(a) {
      if (this.initiated) {
        this.initiated = !1;a.preventDefault();a.stopPropagation();d.removeEvent(g, "touchmove", this);d.removeEvent(g, d.prefixPointerEvent("pointermove"), this);d.removeEvent(g, "mousemove", this);if (this.scroller.options.snap) {
          a = this.scroller._nearestSnap(this.scroller.x, this.scroller.y);var b = this.options.snapSpeed || f.max(f.max(f.min(f.abs(this.scroller.x - a.x), 1E3), f.min(f.abs(this.scroller.y - a.y), 1E3)), 300);if (this.scroller.x != a.x || this.scroller.y != a.y) this.scroller.directionX = 0, this.scroller.directionY = 0, this.scroller.currentPage = a, this.scroller.scrollTo(a.x, a.y, b, this.scroller.options.bounceEasing);
        }this.moved && this.scroller._execEvent("scrollEnd");
      }
    }, transitionTime: function transitionTime(a) {
      a = a || 0;var b = d.style.transitionDuration;if (b && (this.indicatorStyle[b] = a + "ms", !a && d.isBadAndroid)) {
        this.indicatorStyle[b] = "0.0001ms";var c = this;t(function () {
          "0.0001ms" === c.indicatorStyle[b] && (c.indicatorStyle[b] = "0s");
        });
      }
    }, transitionTimingFunction: function transitionTimingFunction(a) {
      this.indicatorStyle[d.style.transitionTimingFunction] = a;
    }, refresh: function refresh() {
      this.transitionTime();this.indicatorStyle.display = this.options.listenX && !this.options.listenY ? this.scroller.hasHorizontalScroll ? "block" : "none" : this.options.listenY && !this.options.listenX ? this.scroller.hasVerticalScroll ? "block" : "none" : this.scroller.hasHorizontalScroll || this.scroller.hasVerticalScroll ? "block" : "none";this.scroller.hasHorizontalScroll && this.scroller.hasVerticalScroll ? (d.addClass(this.wrapper, "iScrollBothScrollbars"), d.removeClass(this.wrapper, "iScrollLoneScrollbar"), this.options.defaultScrollbars && this.options.customStyle && (this.options.listenX ? this.wrapper.style.right = "8px" : this.wrapper.style.bottom = "8px")) : (d.removeClass(this.wrapper, "iScrollBothScrollbars"), d.addClass(this.wrapper, "iScrollLoneScrollbar"), this.options.defaultScrollbars && this.options.customStyle && (this.options.listenX ? this.wrapper.style.right = "2px" : this.wrapper.style.bottom = "2px"));this.options.listenX && (this.wrapperWidth = this.wrapper.clientWidth, this.options.resize ? (this.indicatorWidth = f.max(f.round(this.wrapperWidth * this.wrapperWidth / (this.scroller.scrollerWidth || this.wrapperWidth || 1)), 8), this.indicatorStyle.width = this.indicatorWidth + "px") : this.indicatorWidth = this.indicator.clientWidth, this.maxPosX = this.wrapperWidth - this.indicatorWidth, "clip" == this.options.shrink ? (this.minBoundaryX = -this.indicatorWidth + 8, this.maxBoundaryX = this.wrapperWidth - 8) : (this.minBoundaryX = 0, this.maxBoundaryX = this.maxPosX), this.sizeRatioX = this.options.speedRatioX || this.scroller.maxScrollX && this.maxPosX / this.scroller.maxScrollX);this.options.listenY && (this.wrapperHeight = this.wrapper.clientHeight, this.options.resize ? (this.indicatorHeight = f.max(f.round(this.wrapperHeight * this.wrapperHeight / (this.scroller.scrollerHeight || this.wrapperHeight || 1)), 8), this.indicatorStyle.height = this.indicatorHeight + "px") : this.indicatorHeight = this.indicator.clientHeight, this.maxPosY = this.wrapperHeight - this.indicatorHeight, "clip" == this.options.shrink ? (this.minBoundaryY = -this.indicatorHeight + 8, this.maxBoundaryY = this.wrapperHeight - 8) : (this.minBoundaryY = 0, this.maxBoundaryY = this.maxPosY), this.maxPosY = this.wrapperHeight - this.indicatorHeight, this.sizeRatioY = this.options.speedRatioY || this.scroller.maxScrollY && this.maxPosY / this.scroller.maxScrollY);this.updatePosition();
    }, updatePosition: function updatePosition() {
      var a = this.options.listenX && f.round(this.sizeRatioX * this.scroller.x) || 0,
          b = this.options.listenY && f.round(this.sizeRatioY * this.scroller.y) || 0;this.options.ignoreBoundaries || (a < this.minBoundaryX ? ("scale" == this.options.shrink && (this.width = f.max(this.indicatorWidth + a, 8), this.indicatorStyle.width = this.width + "px"), a = this.minBoundaryX) : a > this.maxBoundaryX ? "scale" == this.options.shrink ? (this.width = f.max(this.indicatorWidth - (a - this.maxPosX), 8), this.indicatorStyle.width = this.width + "px", a = this.maxPosX + this.indicatorWidth - this.width) : a = this.maxBoundaryX : "scale" == this.options.shrink && this.width != this.indicatorWidth && (this.width = this.indicatorWidth, this.indicatorStyle.width = this.width + "px"), b < this.minBoundaryY ? ("scale" == this.options.shrink && (this.height = f.max(this.indicatorHeight + 3 * b, 8), this.indicatorStyle.height = this.height + "px"), b = this.minBoundaryY) : b > this.maxBoundaryY ? "scale" == this.options.shrink ? (this.height = f.max(this.indicatorHeight - 3 * (b - this.maxPosY), 8), this.indicatorStyle.height = this.height + "px", b = this.maxPosY + this.indicatorHeight - this.height) : b = this.maxBoundaryY : "scale" == this.options.shrink && this.height != this.indicatorHeight && (this.height = this.indicatorHeight, this.indicatorStyle.height = this.height + "px"));this.x = a;this.y = b;this.scroller.options.useTransform ? this.indicatorStyle[d.style.transform] = "translate(" + a + "px," + b + "px)" + this.scroller.translateZ : (this.indicatorStyle.left = a + "px", this.indicatorStyle.top = b + "px");
    }, _pos: function _pos(a, b) {
      0 > a ? a = 0 : a > this.maxPosX && (a = this.maxPosX);0 > b ? b = 0 : b > this.maxPosY && (b = this.maxPosY);a = this.options.listenX ? f.round(a / this.sizeRatioX) : this.scroller.x;b = this.options.listenY ? f.round(b / this.sizeRatioY) : this.scroller.y;this.scroller.scrollTo(a, b);
    }, fade: function fade(a, b) {
      if (!b || this.visible) {
        clearTimeout(this.fadeTimeout);this.fadeTimeout = null;var c = a ? 0 : 300;this.wrapperStyle[d.style.transitionDuration] = (a ? 250 : 500) + "ms";this.fadeTimeout = setTimeout(function (a) {
          this.wrapperStyle.opacity = a;this.visible = +a;
        }.bind(this, a ? "1" : "0"), c);
      }
    } };p.utils = d;"undefined" != typeof module && module.exports ? module.exports = p :  true ? !(__WEBPACK_AMD_DEFINE_RESULT__ = function () {
    return p;
  }.call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : g.IScroll = p;
})(window, document, Math);

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.6.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
!function (a) {
  "use strict";
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(0)], __WEBPACK_AMD_DEFINE_FACTORY__ = (a),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "undefined" != typeof exports ? module.exports = a(require("jquery")) : a(jQuery);
}(function (a) {
  "use strict";
  var b = window.Slick || {};b = function () {
    function c(c, d) {
      var f,
          e = this;e.defaults = { accessibility: !0, adaptiveHeight: !1, appendArrows: a(c), appendDots: a(c), arrows: !0, asNavFor: null, prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>', nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>', autoplay: !1, autoplaySpeed: 3e3, centerMode: !1, centerPadding: "50px", cssEase: "ease", customPaging: function customPaging(b, c) {
          return a('<button type="button" data-role="none" role="button" tabindex="0" />').text(c + 1);
        }, dots: !1, dotsClass: "slick-dots", draggable: !0, easing: "linear", edgeFriction: .35, fade: !1, focusOnSelect: !1, infinite: !0, initialSlide: 0, lazyLoad: "ondemand", mobileFirst: !1, pauseOnHover: !0, pauseOnFocus: !0, pauseOnDotsHover: !1, respondTo: "window", responsive: null, rows: 1, rtl: !1, slide: "", slidesPerRow: 1, slidesToShow: 1, slidesToScroll: 1, speed: 500, swipe: !0, swipeToSlide: !1, touchMove: !0, touchThreshold: 5, useCSS: !0, useTransform: !0, variableWidth: !1, vertical: !1, verticalSwiping: !1, waitForAnimate: !0, zIndex: 1e3 }, e.initials = { animating: !1, dragging: !1, autoPlayTimer: null, currentDirection: 0, currentLeft: null, currentSlide: 0, direction: 1, $dots: null, listWidth: null, listHeight: null, loadIndex: 0, $nextArrow: null, $prevArrow: null, slideCount: null, slideWidth: null, $slideTrack: null, $slides: null, sliding: !1, slideOffset: 0, swipeLeft: null, $list: null, touchObject: {}, transformsEnabled: !1, unslicked: !1 }, a.extend(e, e.initials), e.activeBreakpoint = null, e.animType = null, e.animProp = null, e.breakpoints = [], e.breakpointSettings = [], e.cssTransitions = !1, e.focussed = !1, e.interrupted = !1, e.hidden = "hidden", e.paused = !0, e.positionProp = null, e.respondTo = null, e.rowCount = 1, e.shouldClick = !0, e.$slider = a(c), e.$slidesCache = null, e.transformType = null, e.transitionType = null, e.visibilityChange = "visibilitychange", e.windowWidth = 0, e.windowTimer = null, f = a(c).data("slick") || {}, e.options = a.extend({}, e.defaults, d, f), e.currentSlide = e.options.initialSlide, e.originalSettings = e.options, "undefined" != typeof document.mozHidden ? (e.hidden = "mozHidden", e.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.webkitHidden && (e.hidden = "webkitHidden", e.visibilityChange = "webkitvisibilitychange"), e.autoPlay = a.proxy(e.autoPlay, e), e.autoPlayClear = a.proxy(e.autoPlayClear, e), e.autoPlayIterator = a.proxy(e.autoPlayIterator, e), e.changeSlide = a.proxy(e.changeSlide, e), e.clickHandler = a.proxy(e.clickHandler, e), e.selectHandler = a.proxy(e.selectHandler, e), e.setPosition = a.proxy(e.setPosition, e), e.swipeHandler = a.proxy(e.swipeHandler, e), e.dragHandler = a.proxy(e.dragHandler, e), e.keyHandler = a.proxy(e.keyHandler, e), e.instanceUid = b++, e.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, e.registerBreakpoints(), e.init(!0);
    }var b = 0;return c;
  }(), b.prototype.activateADA = function () {
    var a = this;a.$slideTrack.find(".slick-active").attr({ "aria-hidden": "false" }).find("a, input, button, select").attr({ tabindex: "0" });
  }, b.prototype.addSlide = b.prototype.slickAdd = function (b, c, d) {
    var e = this;if ("boolean" == typeof c) d = c, c = null;else if (0 > c || c >= e.slideCount) return !1;e.unload(), "number" == typeof c ? 0 === c && 0 === e.$slides.length ? a(b).appendTo(e.$slideTrack) : d ? a(b).insertBefore(e.$slides.eq(c)) : a(b).insertAfter(e.$slides.eq(c)) : d === !0 ? a(b).prependTo(e.$slideTrack) : a(b).appendTo(e.$slideTrack), e.$slides = e.$slideTrack.children(this.options.slide), e.$slideTrack.children(this.options.slide).detach(), e.$slideTrack.append(e.$slides), e.$slides.each(function (b, c) {
      a(c).attr("data-slick-index", b);
    }), e.$slidesCache = e.$slides, e.reinit();
  }, b.prototype.animateHeight = function () {
    var a = this;if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
      var b = a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.animate({ height: b }, a.options.speed);
    }
  }, b.prototype.animateSlide = function (b, c) {
    var d = {},
        e = this;e.animateHeight(), e.options.rtl === !0 && e.options.vertical === !1 && (b = -b), e.transformsEnabled === !1 ? e.options.vertical === !1 ? e.$slideTrack.animate({ left: b }, e.options.speed, e.options.easing, c) : e.$slideTrack.animate({ top: b }, e.options.speed, e.options.easing, c) : e.cssTransitions === !1 ? (e.options.rtl === !0 && (e.currentLeft = -e.currentLeft), a({ animStart: e.currentLeft }).animate({ animStart: b }, { duration: e.options.speed, easing: e.options.easing, step: function step(a) {
        a = Math.ceil(a), e.options.vertical === !1 ? (d[e.animType] = "translate(" + a + "px, 0px)", e.$slideTrack.css(d)) : (d[e.animType] = "translate(0px," + a + "px)", e.$slideTrack.css(d));
      }, complete: function complete() {
        c && c.call();
      } })) : (e.applyTransition(), b = Math.ceil(b), e.options.vertical === !1 ? d[e.animType] = "translate3d(" + b + "px, 0px, 0px)" : d[e.animType] = "translate3d(0px," + b + "px, 0px)", e.$slideTrack.css(d), c && setTimeout(function () {
      e.disableTransition(), c.call();
    }, e.options.speed));
  }, b.prototype.getNavTarget = function () {
    var b = this,
        c = b.options.asNavFor;return c && null !== c && (c = a(c).not(b.$slider)), c;
  }, b.prototype.asNavFor = function (b) {
    var c = this,
        d = c.getNavTarget();null !== d && "object" == (typeof d === "undefined" ? "undefined" : _typeof(d)) && d.each(function () {
      var c = a(this).slick("getSlick");c.unslicked || c.slideHandler(b, !0);
    });
  }, b.prototype.applyTransition = function (a) {
    var b = this,
        c = {};b.options.fade === !1 ? c[b.transitionType] = b.transformType + " " + b.options.speed + "ms " + b.options.cssEase : c[b.transitionType] = "opacity " + b.options.speed + "ms " + b.options.cssEase, b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c);
  }, b.prototype.autoPlay = function () {
    var a = this;a.autoPlayClear(), a.slideCount > a.options.slidesToShow && (a.autoPlayTimer = setInterval(a.autoPlayIterator, a.options.autoplaySpeed));
  }, b.prototype.autoPlayClear = function () {
    var a = this;a.autoPlayTimer && clearInterval(a.autoPlayTimer);
  }, b.prototype.autoPlayIterator = function () {
    var a = this,
        b = a.currentSlide + a.options.slidesToScroll;a.paused || a.interrupted || a.focussed || (a.options.infinite === !1 && (1 === a.direction && a.currentSlide + 1 === a.slideCount - 1 ? a.direction = 0 : 0 === a.direction && (b = a.currentSlide - a.options.slidesToScroll, a.currentSlide - 1 === 0 && (a.direction = 1))), a.slideHandler(b));
  }, b.prototype.buildArrows = function () {
    var b = this;b.options.arrows === !0 && (b.$prevArrow = a(b.options.prevArrow).addClass("slick-arrow"), b.$nextArrow = a(b.options.nextArrow).addClass("slick-arrow"), b.slideCount > b.options.slidesToShow ? (b.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), b.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.prependTo(b.options.appendArrows), b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.appendTo(b.options.appendArrows), b.options.infinite !== !0 && b.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : b.$prevArrow.add(b.$nextArrow).addClass("slick-hidden").attr({ "aria-disabled": "true", tabindex: "-1" }));
  }, b.prototype.buildDots = function () {
    var c,
        d,
        b = this;if (b.options.dots === !0 && b.slideCount > b.options.slidesToShow) {
      for (b.$slider.addClass("slick-dotted"), d = a("<ul />").addClass(b.options.dotsClass), c = 0; c <= b.getDotCount(); c += 1) {
        d.append(a("<li />").append(b.options.customPaging.call(this, b, c)));
      }b.$dots = d.appendTo(b.options.appendDots), b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false");
    }
  }, b.prototype.buildOut = function () {
    var b = this;b.$slides = b.$slider.children(b.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), b.slideCount = b.$slides.length, b.$slides.each(function (b, c) {
      a(c).attr("data-slick-index", b).data("originalStyling", a(c).attr("style") || "");
    }), b.$slider.addClass("slick-slider"), b.$slideTrack = 0 === b.slideCount ? a('<div class="slick-track"/>').appendTo(b.$slider) : b.$slides.wrapAll('<div class="slick-track"/>').parent(), b.$list = b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), b.$slideTrack.css("opacity", 0), (b.options.centerMode === !0 || b.options.swipeToSlide === !0) && (b.options.slidesToScroll = 1), a("img[data-lazy]", b.$slider).not("[src]").addClass("slick-loading"), b.setupInfinite(), b.buildArrows(), b.buildDots(), b.updateDots(), b.setSlideClasses("number" == typeof b.currentSlide ? b.currentSlide : 0), b.options.draggable === !0 && b.$list.addClass("draggable");
  }, b.prototype.buildRows = function () {
    var b,
        c,
        d,
        e,
        f,
        g,
        h,
        a = this;if (e = document.createDocumentFragment(), g = a.$slider.children(), a.options.rows > 1) {
      for (h = a.options.slidesPerRow * a.options.rows, f = Math.ceil(g.length / h), b = 0; f > b; b++) {
        var i = document.createElement("div");for (c = 0; c < a.options.rows; c++) {
          var j = document.createElement("div");for (d = 0; d < a.options.slidesPerRow; d++) {
            var k = b * h + (c * a.options.slidesPerRow + d);g.get(k) && j.appendChild(g.get(k));
          }i.appendChild(j);
        }e.appendChild(i);
      }a.$slider.empty().append(e), a.$slider.children().children().children().css({ width: 100 / a.options.slidesPerRow + "%", display: "inline-block" });
    }
  }, b.prototype.checkResponsive = function (b, c) {
    var e,
        f,
        g,
        d = this,
        h = !1,
        i = d.$slider.width(),
        j = window.innerWidth || a(window).width();if ("window" === d.respondTo ? g = j : "slider" === d.respondTo ? g = i : "min" === d.respondTo && (g = Math.min(j, i)), d.options.responsive && d.options.responsive.length && null !== d.options.responsive) {
      f = null;for (e in d.breakpoints) {
        d.breakpoints.hasOwnProperty(e) && (d.originalSettings.mobileFirst === !1 ? g < d.breakpoints[e] && (f = d.breakpoints[e]) : g > d.breakpoints[e] && (f = d.breakpoints[e]));
      }null !== f ? null !== d.activeBreakpoint ? (f !== d.activeBreakpoint || c) && (d.activeBreakpoint = f, "unslick" === d.breakpointSettings[f] ? d.unslick(f) : (d.options = a.extend({}, d.originalSettings, d.breakpointSettings[f]), b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b)), h = f) : (d.activeBreakpoint = f, "unslick" === d.breakpointSettings[f] ? d.unslick(f) : (d.options = a.extend({}, d.originalSettings, d.breakpointSettings[f]), b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b)), h = f) : null !== d.activeBreakpoint && (d.activeBreakpoint = null, d.options = d.originalSettings, b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b), h = f), b || h === !1 || d.$slider.trigger("breakpoint", [d, h]);
    }
  }, b.prototype.changeSlide = function (b, c) {
    var f,
        g,
        h,
        d = this,
        e = a(b.currentTarget);switch (e.is("a") && b.preventDefault(), e.is("li") || (e = e.closest("li")), h = d.slideCount % d.options.slidesToScroll !== 0, f = h ? 0 : (d.slideCount - d.currentSlide) % d.options.slidesToScroll, b.data.message) {case "previous":
        g = 0 === f ? d.options.slidesToScroll : d.options.slidesToShow - f, d.slideCount > d.options.slidesToShow && d.slideHandler(d.currentSlide - g, !1, c);break;case "next":
        g = 0 === f ? d.options.slidesToScroll : f, d.slideCount > d.options.slidesToShow && d.slideHandler(d.currentSlide + g, !1, c);break;case "index":
        var i = 0 === b.data.index ? 0 : b.data.index || e.index() * d.options.slidesToScroll;d.slideHandler(d.checkNavigable(i), !1, c), e.children().trigger("focus");break;default:
        return;}
  }, b.prototype.checkNavigable = function (a) {
    var c,
        d,
        b = this;if (c = b.getNavigableIndexes(), d = 0, a > c[c.length - 1]) a = c[c.length - 1];else for (var e in c) {
      if (a < c[e]) {
        a = d;break;
      }d = c[e];
    }return a;
  }, b.prototype.cleanUpEvents = function () {
    var b = this;b.options.dots && null !== b.$dots && a("li", b.$dots).off("click.slick", b.changeSlide).off("mouseenter.slick", a.proxy(b.interrupt, b, !0)).off("mouseleave.slick", a.proxy(b.interrupt, b, !1)), b.$slider.off("focus.slick blur.slick"), b.options.arrows === !0 && b.slideCount > b.options.slidesToShow && (b.$prevArrow && b.$prevArrow.off("click.slick", b.changeSlide), b.$nextArrow && b.$nextArrow.off("click.slick", b.changeSlide)), b.$list.off("touchstart.slick mousedown.slick", b.swipeHandler), b.$list.off("touchmove.slick mousemove.slick", b.swipeHandler), b.$list.off("touchend.slick mouseup.slick", b.swipeHandler), b.$list.off("touchcancel.slick mouseleave.slick", b.swipeHandler), b.$list.off("click.slick", b.clickHandler), a(document).off(b.visibilityChange, b.visibility), b.cleanUpSlideEvents(), b.options.accessibility === !0 && b.$list.off("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().off("click.slick", b.selectHandler), a(window).off("orientationchange.slick.slick-" + b.instanceUid, b.orientationChange), a(window).off("resize.slick.slick-" + b.instanceUid, b.resize), a("[draggable!=true]", b.$slideTrack).off("dragstart", b.preventDefault), a(window).off("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).off("ready.slick.slick-" + b.instanceUid, b.setPosition);
  }, b.prototype.cleanUpSlideEvents = function () {
    var b = this;b.$list.off("mouseenter.slick", a.proxy(b.interrupt, b, !0)), b.$list.off("mouseleave.slick", a.proxy(b.interrupt, b, !1));
  }, b.prototype.cleanUpRows = function () {
    var b,
        a = this;a.options.rows > 1 && (b = a.$slides.children().children(), b.removeAttr("style"), a.$slider.empty().append(b));
  }, b.prototype.clickHandler = function (a) {
    var b = this;b.shouldClick === !1 && (a.stopImmediatePropagation(), a.stopPropagation(), a.preventDefault());
  }, b.prototype.destroy = function (b) {
    var c = this;c.autoPlayClear(), c.touchObject = {}, c.cleanUpEvents(), a(".slick-cloned", c.$slider).detach(), c.$dots && c.$dots.remove(), c.$prevArrow && c.$prevArrow.length && (c.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), c.htmlExpr.test(c.options.prevArrow) && c.$prevArrow.remove()), c.$nextArrow && c.$nextArrow.length && (c.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), c.htmlExpr.test(c.options.nextArrow) && c.$nextArrow.remove()), c.$slides && (c.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
      a(this).attr("style", a(this).data("originalStyling"));
    }), c.$slideTrack.children(this.options.slide).detach(), c.$slideTrack.detach(), c.$list.detach(), c.$slider.append(c.$slides)), c.cleanUpRows(), c.$slider.removeClass("slick-slider"), c.$slider.removeClass("slick-initialized"), c.$slider.removeClass("slick-dotted"), c.unslicked = !0, b || c.$slider.trigger("destroy", [c]);
  }, b.prototype.disableTransition = function (a) {
    var b = this,
        c = {};c[b.transitionType] = "", b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c);
  }, b.prototype.fadeSlide = function (a, b) {
    var c = this;c.cssTransitions === !1 ? (c.$slides.eq(a).css({ zIndex: c.options.zIndex }), c.$slides.eq(a).animate({ opacity: 1 }, c.options.speed, c.options.easing, b)) : (c.applyTransition(a), c.$slides.eq(a).css({ opacity: 1, zIndex: c.options.zIndex }), b && setTimeout(function () {
      c.disableTransition(a), b.call();
    }, c.options.speed));
  }, b.prototype.fadeSlideOut = function (a) {
    var b = this;b.cssTransitions === !1 ? b.$slides.eq(a).animate({ opacity: 0, zIndex: b.options.zIndex - 2 }, b.options.speed, b.options.easing) : (b.applyTransition(a), b.$slides.eq(a).css({ opacity: 0, zIndex: b.options.zIndex - 2 }));
  }, b.prototype.filterSlides = b.prototype.slickFilter = function (a) {
    var b = this;null !== a && (b.$slidesCache = b.$slides, b.unload(), b.$slideTrack.children(this.options.slide).detach(), b.$slidesCache.filter(a).appendTo(b.$slideTrack), b.reinit());
  }, b.prototype.focusHandler = function () {
    var b = this;b.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function (c) {
      c.stopImmediatePropagation();var d = a(this);setTimeout(function () {
        b.options.pauseOnFocus && (b.focussed = d.is(":focus"), b.autoPlay());
      }, 0);
    });
  }, b.prototype.getCurrent = b.prototype.slickCurrentSlide = function () {
    var a = this;return a.currentSlide;
  }, b.prototype.getDotCount = function () {
    var a = this,
        b = 0,
        c = 0,
        d = 0;if (a.options.infinite === !0) for (; b < a.slideCount;) {
      ++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
    } else if (a.options.centerMode === !0) d = a.slideCount;else if (a.options.asNavFor) for (; b < a.slideCount;) {
      ++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
    } else d = 1 + Math.ceil((a.slideCount - a.options.slidesToShow) / a.options.slidesToScroll);return d - 1;
  }, b.prototype.getLeft = function (a) {
    var c,
        d,
        f,
        b = this,
        e = 0;return b.slideOffset = 0, d = b.$slides.first().outerHeight(!0), b.options.infinite === !0 ? (b.slideCount > b.options.slidesToShow && (b.slideOffset = b.slideWidth * b.options.slidesToShow * -1, e = d * b.options.slidesToShow * -1), b.slideCount % b.options.slidesToScroll !== 0 && a + b.options.slidesToScroll > b.slideCount && b.slideCount > b.options.slidesToShow && (a > b.slideCount ? (b.slideOffset = (b.options.slidesToShow - (a - b.slideCount)) * b.slideWidth * -1, e = (b.options.slidesToShow - (a - b.slideCount)) * d * -1) : (b.slideOffset = b.slideCount % b.options.slidesToScroll * b.slideWidth * -1, e = b.slideCount % b.options.slidesToScroll * d * -1))) : a + b.options.slidesToShow > b.slideCount && (b.slideOffset = (a + b.options.slidesToShow - b.slideCount) * b.slideWidth, e = (a + b.options.slidesToShow - b.slideCount) * d), b.slideCount <= b.options.slidesToShow && (b.slideOffset = 0, e = 0), b.options.centerMode === !0 && b.options.infinite === !0 ? b.slideOffset += b.slideWidth * Math.floor(b.options.slidesToShow / 2) - b.slideWidth : b.options.centerMode === !0 && (b.slideOffset = 0, b.slideOffset += b.slideWidth * Math.floor(b.options.slidesToShow / 2)), c = b.options.vertical === !1 ? a * b.slideWidth * -1 + b.slideOffset : a * d * -1 + e, b.options.variableWidth === !0 && (f = b.slideCount <= b.options.slidesToShow || b.options.infinite === !1 ? b.$slideTrack.children(".slick-slide").eq(a) : b.$slideTrack.children(".slick-slide").eq(a + b.options.slidesToShow), c = b.options.rtl === !0 ? f[0] ? -1 * (b.$slideTrack.width() - f[0].offsetLeft - f.width()) : 0 : f[0] ? -1 * f[0].offsetLeft : 0, b.options.centerMode === !0 && (f = b.slideCount <= b.options.slidesToShow || b.options.infinite === !1 ? b.$slideTrack.children(".slick-slide").eq(a) : b.$slideTrack.children(".slick-slide").eq(a + b.options.slidesToShow + 1), c = b.options.rtl === !0 ? f[0] ? -1 * (b.$slideTrack.width() - f[0].offsetLeft - f.width()) : 0 : f[0] ? -1 * f[0].offsetLeft : 0, c += (b.$list.width() - f.outerWidth()) / 2)), c;
  }, b.prototype.getOption = b.prototype.slickGetOption = function (a) {
    var b = this;return b.options[a];
  }, b.prototype.getNavigableIndexes = function () {
    var e,
        a = this,
        b = 0,
        c = 0,
        d = [];for (a.options.infinite === !1 ? e = a.slideCount : (b = -1 * a.options.slidesToScroll, c = -1 * a.options.slidesToScroll, e = 2 * a.slideCount); e > b;) {
      d.push(b), b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
    }return d;
  }, b.prototype.getSlick = function () {
    return this;
  }, b.prototype.getSlideCount = function () {
    var c,
        d,
        e,
        b = this;return e = b.options.centerMode === !0 ? b.slideWidth * Math.floor(b.options.slidesToShow / 2) : 0, b.options.swipeToSlide === !0 ? (b.$slideTrack.find(".slick-slide").each(function (c, f) {
      return f.offsetLeft - e + a(f).outerWidth() / 2 > -1 * b.swipeLeft ? (d = f, !1) : void 0;
    }), c = Math.abs(a(d).attr("data-slick-index") - b.currentSlide) || 1) : b.options.slidesToScroll;
  }, b.prototype.goTo = b.prototype.slickGoTo = function (a, b) {
    var c = this;c.changeSlide({ data: { message: "index", index: parseInt(a) } }, b);
  }, b.prototype.init = function (b) {
    var c = this;a(c.$slider).hasClass("slick-initialized") || (a(c.$slider).addClass("slick-initialized"), c.buildRows(), c.buildOut(), c.setProps(), c.startLoad(), c.loadSlider(), c.initializeEvents(), c.updateArrows(), c.updateDots(), c.checkResponsive(!0), c.focusHandler()), b && c.$slider.trigger("init", [c]), c.options.accessibility === !0 && c.initADA(), c.options.autoplay && (c.paused = !1, c.autoPlay());
  }, b.prototype.initADA = function () {
    var b = this;b.$slides.add(b.$slideTrack.find(".slick-cloned")).attr({ "aria-hidden": "true", tabindex: "-1" }).find("a, input, button, select").attr({ tabindex: "-1" }), b.$slideTrack.attr("role", "listbox"), b.$slides.not(b.$slideTrack.find(".slick-cloned")).each(function (c) {
      a(this).attr({ role: "option", "aria-describedby": "slick-slide" + b.instanceUid + c });
    }), null !== b.$dots && b.$dots.attr("role", "tablist").find("li").each(function (c) {
      a(this).attr({ role: "presentation", "aria-selected": "false", "aria-controls": "navigation" + b.instanceUid + c, id: "slick-slide" + b.instanceUid + c });
    }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), b.activateADA();
  }, b.prototype.initArrowEvents = function () {
    var a = this;a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.off("click.slick").on("click.slick", { message: "previous" }, a.changeSlide), a.$nextArrow.off("click.slick").on("click.slick", { message: "next" }, a.changeSlide));
  }, b.prototype.initDotEvents = function () {
    var b = this;b.options.dots === !0 && b.slideCount > b.options.slidesToShow && a("li", b.$dots).on("click.slick", { message: "index" }, b.changeSlide), b.options.dots === !0 && b.options.pauseOnDotsHover === !0 && a("li", b.$dots).on("mouseenter.slick", a.proxy(b.interrupt, b, !0)).on("mouseleave.slick", a.proxy(b.interrupt, b, !1));
  }, b.prototype.initSlideEvents = function () {
    var b = this;b.options.pauseOnHover && (b.$list.on("mouseenter.slick", a.proxy(b.interrupt, b, !0)), b.$list.on("mouseleave.slick", a.proxy(b.interrupt, b, !1)));
  }, b.prototype.initializeEvents = function () {
    var b = this;b.initArrowEvents(), b.initDotEvents(), b.initSlideEvents(), b.$list.on("touchstart.slick mousedown.slick", { action: "start" }, b.swipeHandler), b.$list.on("touchmove.slick mousemove.slick", { action: "move" }, b.swipeHandler), b.$list.on("touchend.slick mouseup.slick", { action: "end" }, b.swipeHandler), b.$list.on("touchcancel.slick mouseleave.slick", { action: "end" }, b.swipeHandler), b.$list.on("click.slick", b.clickHandler), a(document).on(b.visibilityChange, a.proxy(b.visibility, b)), b.options.accessibility === !0 && b.$list.on("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().on("click.slick", b.selectHandler), a(window).on("orientationchange.slick.slick-" + b.instanceUid, a.proxy(b.orientationChange, b)), a(window).on("resize.slick.slick-" + b.instanceUid, a.proxy(b.resize, b)), a("[draggable!=true]", b.$slideTrack).on("dragstart", b.preventDefault), a(window).on("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).on("ready.slick.slick-" + b.instanceUid, b.setPosition);
  }, b.prototype.initUI = function () {
    var a = this;a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.show(), a.$nextArrow.show()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.show();
  }, b.prototype.keyHandler = function (a) {
    var b = this;a.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === a.keyCode && b.options.accessibility === !0 ? b.changeSlide({ data: { message: b.options.rtl === !0 ? "next" : "previous" } }) : 39 === a.keyCode && b.options.accessibility === !0 && b.changeSlide({ data: { message: b.options.rtl === !0 ? "previous" : "next" } }));
  }, b.prototype.lazyLoad = function () {
    function g(c) {
      a("img[data-lazy]", c).each(function () {
        var c = a(this),
            d = a(this).attr("data-lazy"),
            e = document.createElement("img");e.onload = function () {
          c.animate({ opacity: 0 }, 100, function () {
            c.attr("src", d).animate({ opacity: 1 }, 200, function () {
              c.removeAttr("data-lazy").removeClass("slick-loading");
            }), b.$slider.trigger("lazyLoaded", [b, c, d]);
          });
        }, e.onerror = function () {
          c.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), b.$slider.trigger("lazyLoadError", [b, c, d]);
        }, e.src = d;
      });
    }var c,
        d,
        e,
        f,
        b = this;b.options.centerMode === !0 ? b.options.infinite === !0 ? (e = b.currentSlide + (b.options.slidesToShow / 2 + 1), f = e + b.options.slidesToShow + 2) : (e = Math.max(0, b.currentSlide - (b.options.slidesToShow / 2 + 1)), f = 2 + (b.options.slidesToShow / 2 + 1) + b.currentSlide) : (e = b.options.infinite ? b.options.slidesToShow + b.currentSlide : b.currentSlide, f = Math.ceil(e + b.options.slidesToShow), b.options.fade === !0 && (e > 0 && e--, f <= b.slideCount && f++)), c = b.$slider.find(".slick-slide").slice(e, f), g(c), b.slideCount <= b.options.slidesToShow ? (d = b.$slider.find(".slick-slide"), g(d)) : b.currentSlide >= b.slideCount - b.options.slidesToShow ? (d = b.$slider.find(".slick-cloned").slice(0, b.options.slidesToShow), g(d)) : 0 === b.currentSlide && (d = b.$slider.find(".slick-cloned").slice(-1 * b.options.slidesToShow), g(d));
  }, b.prototype.loadSlider = function () {
    var a = this;a.setPosition(), a.$slideTrack.css({ opacity: 1 }), a.$slider.removeClass("slick-loading"), a.initUI(), "progressive" === a.options.lazyLoad && a.progressiveLazyLoad();
  }, b.prototype.next = b.prototype.slickNext = function () {
    var a = this;a.changeSlide({ data: { message: "next" } });
  }, b.prototype.orientationChange = function () {
    var a = this;a.checkResponsive(), a.setPosition();
  }, b.prototype.pause = b.prototype.slickPause = function () {
    var a = this;a.autoPlayClear(), a.paused = !0;
  }, b.prototype.play = b.prototype.slickPlay = function () {
    var a = this;a.autoPlay(), a.options.autoplay = !0, a.paused = !1, a.focussed = !1, a.interrupted = !1;
  }, b.prototype.postSlide = function (a) {
    var b = this;b.unslicked || (b.$slider.trigger("afterChange", [b, a]), b.animating = !1, b.setPosition(), b.swipeLeft = null, b.options.autoplay && b.autoPlay(), b.options.accessibility === !0 && b.initADA());
  }, b.prototype.prev = b.prototype.slickPrev = function () {
    var a = this;a.changeSlide({ data: { message: "previous" } });
  }, b.prototype.preventDefault = function (a) {
    a.preventDefault();
  }, b.prototype.progressiveLazyLoad = function (b) {
    b = b || 1;var e,
        f,
        g,
        c = this,
        d = a("img[data-lazy]", c.$slider);d.length ? (e = d.first(), f = e.attr("data-lazy"), g = document.createElement("img"), g.onload = function () {
      e.attr("src", f).removeAttr("data-lazy").removeClass("slick-loading"), c.options.adaptiveHeight === !0 && c.setPosition(), c.$slider.trigger("lazyLoaded", [c, e, f]), c.progressiveLazyLoad();
    }, g.onerror = function () {
      3 > b ? setTimeout(function () {
        c.progressiveLazyLoad(b + 1);
      }, 500) : (e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), c.$slider.trigger("lazyLoadError", [c, e, f]), c.progressiveLazyLoad());
    }, g.src = f) : c.$slider.trigger("allImagesLoaded", [c]);
  }, b.prototype.refresh = function (b) {
    var d,
        e,
        c = this;e = c.slideCount - c.options.slidesToShow, !c.options.infinite && c.currentSlide > e && (c.currentSlide = e), c.slideCount <= c.options.slidesToShow && (c.currentSlide = 0), d = c.currentSlide, c.destroy(!0), a.extend(c, c.initials, { currentSlide: d }), c.init(), b || c.changeSlide({ data: { message: "index", index: d } }, !1);
  }, b.prototype.registerBreakpoints = function () {
    var c,
        d,
        e,
        b = this,
        f = b.options.responsive || null;if ("array" === a.type(f) && f.length) {
      b.respondTo = b.options.respondTo || "window";for (c in f) {
        if (e = b.breakpoints.length - 1, d = f[c].breakpoint, f.hasOwnProperty(c)) {
          for (; e >= 0;) {
            b.breakpoints[e] && b.breakpoints[e] === d && b.breakpoints.splice(e, 1), e--;
          }b.breakpoints.push(d), b.breakpointSettings[d] = f[c].settings;
        }
      }b.breakpoints.sort(function (a, c) {
        return b.options.mobileFirst ? a - c : c - a;
      });
    }
  }, b.prototype.reinit = function () {
    var b = this;b.$slides = b.$slideTrack.children(b.options.slide).addClass("slick-slide"), b.slideCount = b.$slides.length, b.currentSlide >= b.slideCount && 0 !== b.currentSlide && (b.currentSlide = b.currentSlide - b.options.slidesToScroll), b.slideCount <= b.options.slidesToShow && (b.currentSlide = 0), b.registerBreakpoints(), b.setProps(), b.setupInfinite(), b.buildArrows(), b.updateArrows(), b.initArrowEvents(), b.buildDots(), b.updateDots(), b.initDotEvents(), b.cleanUpSlideEvents(), b.initSlideEvents(), b.checkResponsive(!1, !0), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().on("click.slick", b.selectHandler), b.setSlideClasses("number" == typeof b.currentSlide ? b.currentSlide : 0), b.setPosition(), b.focusHandler(), b.paused = !b.options.autoplay, b.autoPlay(), b.$slider.trigger("reInit", [b]);
  }, b.prototype.resize = function () {
    var b = this;a(window).width() !== b.windowWidth && (clearTimeout(b.windowDelay), b.windowDelay = window.setTimeout(function () {
      b.windowWidth = a(window).width(), b.checkResponsive(), b.unslicked || b.setPosition();
    }, 50));
  }, b.prototype.removeSlide = b.prototype.slickRemove = function (a, b, c) {
    var d = this;return "boolean" == typeof a ? (b = a, a = b === !0 ? 0 : d.slideCount - 1) : a = b === !0 ? --a : a, d.slideCount < 1 || 0 > a || a > d.slideCount - 1 ? !1 : (d.unload(), c === !0 ? d.$slideTrack.children().remove() : d.$slideTrack.children(this.options.slide).eq(a).remove(), d.$slides = d.$slideTrack.children(this.options.slide), d.$slideTrack.children(this.options.slide).detach(), d.$slideTrack.append(d.$slides), d.$slidesCache = d.$slides, void d.reinit());
  }, b.prototype.setCSS = function (a) {
    var d,
        e,
        b = this,
        c = {};b.options.rtl === !0 && (a = -a), d = "left" == b.positionProp ? Math.ceil(a) + "px" : "0px", e = "top" == b.positionProp ? Math.ceil(a) + "px" : "0px", c[b.positionProp] = a, b.transformsEnabled === !1 ? b.$slideTrack.css(c) : (c = {}, b.cssTransitions === !1 ? (c[b.animType] = "translate(" + d + ", " + e + ")", b.$slideTrack.css(c)) : (c[b.animType] = "translate3d(" + d + ", " + e + ", 0px)", b.$slideTrack.css(c)));
  }, b.prototype.setDimensions = function () {
    var a = this;a.options.vertical === !1 ? a.options.centerMode === !0 && a.$list.css({ padding: "0px " + a.options.centerPadding }) : (a.$list.height(a.$slides.first().outerHeight(!0) * a.options.slidesToShow), a.options.centerMode === !0 && a.$list.css({ padding: a.options.centerPadding + " 0px" })), a.listWidth = a.$list.width(), a.listHeight = a.$list.height(), a.options.vertical === !1 && a.options.variableWidth === !1 ? (a.slideWidth = Math.ceil(a.listWidth / a.options.slidesToShow), a.$slideTrack.width(Math.ceil(a.slideWidth * a.$slideTrack.children(".slick-slide").length))) : a.options.variableWidth === !0 ? a.$slideTrack.width(5e3 * a.slideCount) : (a.slideWidth = Math.ceil(a.listWidth), a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0) * a.$slideTrack.children(".slick-slide").length)));var b = a.$slides.first().outerWidth(!0) - a.$slides.first().width();a.options.variableWidth === !1 && a.$slideTrack.children(".slick-slide").width(a.slideWidth - b);
  }, b.prototype.setFade = function () {
    var c,
        b = this;b.$slides.each(function (d, e) {
      c = b.slideWidth * d * -1, b.options.rtl === !0 ? a(e).css({ position: "relative", right: c, top: 0, zIndex: b.options.zIndex - 2, opacity: 0 }) : a(e).css({ position: "relative", left: c, top: 0, zIndex: b.options.zIndex - 2, opacity: 0 });
    }), b.$slides.eq(b.currentSlide).css({ zIndex: b.options.zIndex - 1, opacity: 1 });
  }, b.prototype.setHeight = function () {
    var a = this;if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
      var b = a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.css("height", b);
    }
  }, b.prototype.setOption = b.prototype.slickSetOption = function () {
    var c,
        d,
        e,
        f,
        h,
        b = this,
        g = !1;if ("object" === a.type(arguments[0]) ? (e = arguments[0], g = arguments[1], h = "multiple") : "string" === a.type(arguments[0]) && (e = arguments[0], f = arguments[1], g = arguments[2], "responsive" === arguments[0] && "array" === a.type(arguments[1]) ? h = "responsive" : "undefined" != typeof arguments[1] && (h = "single")), "single" === h) b.options[e] = f;else if ("multiple" === h) a.each(e, function (a, c) {
      b.options[a] = c;
    });else if ("responsive" === h) for (d in f) {
      if ("array" !== a.type(b.options.responsive)) b.options.responsive = [f[d]];else {
        for (c = b.options.responsive.length - 1; c >= 0;) {
          b.options.responsive[c].breakpoint === f[d].breakpoint && b.options.responsive.splice(c, 1), c--;
        }b.options.responsive.push(f[d]);
      }
    }g && (b.unload(), b.reinit());
  }, b.prototype.setPosition = function () {
    var a = this;a.setDimensions(), a.setHeight(), a.options.fade === !1 ? a.setCSS(a.getLeft(a.currentSlide)) : a.setFade(), a.$slider.trigger("setPosition", [a]);
  }, b.prototype.setProps = function () {
    var a = this,
        b = document.body.style;a.positionProp = a.options.vertical === !0 ? "top" : "left", "top" === a.positionProp ? a.$slider.addClass("slick-vertical") : a.$slider.removeClass("slick-vertical"), (void 0 !== b.WebkitTransition || void 0 !== b.MozTransition || void 0 !== b.msTransition) && a.options.useCSS === !0 && (a.cssTransitions = !0), a.options.fade && ("number" == typeof a.options.zIndex ? a.options.zIndex < 3 && (a.options.zIndex = 3) : a.options.zIndex = a.defaults.zIndex), void 0 !== b.OTransform && (a.animType = "OTransform", a.transformType = "-o-transform", a.transitionType = "OTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.MozTransform && (a.animType = "MozTransform", a.transformType = "-moz-transform", a.transitionType = "MozTransition", void 0 === b.perspectiveProperty && void 0 === b.MozPerspective && (a.animType = !1)), void 0 !== b.webkitTransform && (a.animType = "webkitTransform", a.transformType = "-webkit-transform", a.transitionType = "webkitTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.msTransform && (a.animType = "msTransform", a.transformType = "-ms-transform", a.transitionType = "msTransition", void 0 === b.msTransform && (a.animType = !1)), void 0 !== b.transform && a.animType !== !1 && (a.animType = "transform", a.transformType = "transform", a.transitionType = "transition"), a.transformsEnabled = a.options.useTransform && null !== a.animType && a.animType !== !1;
  }, b.prototype.setSlideClasses = function (a) {
    var c,
        d,
        e,
        f,
        b = this;d = b.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), b.$slides.eq(a).addClass("slick-current"), b.options.centerMode === !0 ? (c = Math.floor(b.options.slidesToShow / 2), b.options.infinite === !0 && (a >= c && a <= b.slideCount - 1 - c ? b.$slides.slice(a - c, a + c + 1).addClass("slick-active").attr("aria-hidden", "false") : (e = b.options.slidesToShow + a, d.slice(e - c + 1, e + c + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === a ? d.eq(d.length - 1 - b.options.slidesToShow).addClass("slick-center") : a === b.slideCount - 1 && d.eq(b.options.slidesToShow).addClass("slick-center")), b.$slides.eq(a).addClass("slick-center")) : a >= 0 && a <= b.slideCount - b.options.slidesToShow ? b.$slides.slice(a, a + b.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : d.length <= b.options.slidesToShow ? d.addClass("slick-active").attr("aria-hidden", "false") : (f = b.slideCount % b.options.slidesToShow, e = b.options.infinite === !0 ? b.options.slidesToShow + a : a, b.options.slidesToShow == b.options.slidesToScroll && b.slideCount - a < b.options.slidesToShow ? d.slice(e - (b.options.slidesToShow - f), e + f).addClass("slick-active").attr("aria-hidden", "false") : d.slice(e, e + b.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" === b.options.lazyLoad && b.lazyLoad();
  }, b.prototype.setupInfinite = function () {
    var c,
        d,
        e,
        b = this;if (b.options.fade === !0 && (b.options.centerMode = !1), b.options.infinite === !0 && b.options.fade === !1 && (d = null, b.slideCount > b.options.slidesToShow)) {
      for (e = b.options.centerMode === !0 ? b.options.slidesToShow + 1 : b.options.slidesToShow, c = b.slideCount; c > b.slideCount - e; c -= 1) {
        d = c - 1, a(b.$slides[d]).clone(!0).attr("id", "").attr("data-slick-index", d - b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");
      }for (c = 0; e > c; c += 1) {
        d = c, a(b.$slides[d]).clone(!0).attr("id", "").attr("data-slick-index", d + b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");
      }b.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
        a(this).attr("id", "");
      });
    }
  }, b.prototype.interrupt = function (a) {
    var b = this;a || b.autoPlay(), b.interrupted = a;
  }, b.prototype.selectHandler = function (b) {
    var c = this,
        d = a(b.target).is(".slick-slide") ? a(b.target) : a(b.target).parents(".slick-slide"),
        e = parseInt(d.attr("data-slick-index"));return e || (e = 0), c.slideCount <= c.options.slidesToShow ? (c.setSlideClasses(e), void c.asNavFor(e)) : void c.slideHandler(e);
  }, b.prototype.slideHandler = function (a, b, c) {
    var d,
        e,
        f,
        g,
        j,
        h = null,
        i = this;return b = b || !1, i.animating === !0 && i.options.waitForAnimate === !0 || i.options.fade === !0 && i.currentSlide === a || i.slideCount <= i.options.slidesToShow ? void 0 : (b === !1 && i.asNavFor(a), d = a, h = i.getLeft(d), g = i.getLeft(i.currentSlide), i.currentLeft = null === i.swipeLeft ? g : i.swipeLeft, i.options.infinite === !1 && i.options.centerMode === !1 && (0 > a || a > i.getDotCount() * i.options.slidesToScroll) ? void (i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function () {
      i.postSlide(d);
    }) : i.postSlide(d))) : i.options.infinite === !1 && i.options.centerMode === !0 && (0 > a || a > i.slideCount - i.options.slidesToScroll) ? void (i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function () {
      i.postSlide(d);
    }) : i.postSlide(d))) : (i.options.autoplay && clearInterval(i.autoPlayTimer), e = 0 > d ? i.slideCount % i.options.slidesToScroll !== 0 ? i.slideCount - i.slideCount % i.options.slidesToScroll : i.slideCount + d : d >= i.slideCount ? i.slideCount % i.options.slidesToScroll !== 0 ? 0 : d - i.slideCount : d, i.animating = !0, i.$slider.trigger("beforeChange", [i, i.currentSlide, e]), f = i.currentSlide, i.currentSlide = e, i.setSlideClasses(i.currentSlide), i.options.asNavFor && (j = i.getNavTarget(), j = j.slick("getSlick"), j.slideCount <= j.options.slidesToShow && j.setSlideClasses(i.currentSlide)), i.updateDots(), i.updateArrows(), i.options.fade === !0 ? (c !== !0 ? (i.fadeSlideOut(f), i.fadeSlide(e, function () {
      i.postSlide(e);
    })) : i.postSlide(e), void i.animateHeight()) : void (c !== !0 ? i.animateSlide(h, function () {
      i.postSlide(e);
    }) : i.postSlide(e))));
  }, b.prototype.startLoad = function () {
    var a = this;a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.hide(), a.$nextArrow.hide()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.hide(), a.$slider.addClass("slick-loading");
  }, b.prototype.swipeDirection = function () {
    var a,
        b,
        c,
        d,
        e = this;return a = e.touchObject.startX - e.touchObject.curX, b = e.touchObject.startY - e.touchObject.curY, c = Math.atan2(b, a), d = Math.round(180 * c / Math.PI), 0 > d && (d = 360 - Math.abs(d)), 45 >= d && d >= 0 ? e.options.rtl === !1 ? "left" : "right" : 360 >= d && d >= 315 ? e.options.rtl === !1 ? "left" : "right" : d >= 135 && 225 >= d ? e.options.rtl === !1 ? "right" : "left" : e.options.verticalSwiping === !0 ? d >= 35 && 135 >= d ? "down" : "up" : "vertical";
  }, b.prototype.swipeEnd = function (a) {
    var c,
        d,
        b = this;if (b.dragging = !1, b.interrupted = !1, b.shouldClick = b.touchObject.swipeLength > 10 ? !1 : !0, void 0 === b.touchObject.curX) return !1;if (b.touchObject.edgeHit === !0 && b.$slider.trigger("edge", [b, b.swipeDirection()]), b.touchObject.swipeLength >= b.touchObject.minSwipe) {
      switch (d = b.swipeDirection()) {case "left":case "down":
          c = b.options.swipeToSlide ? b.checkNavigable(b.currentSlide + b.getSlideCount()) : b.currentSlide + b.getSlideCount(), b.currentDirection = 0;break;case "right":case "up":
          c = b.options.swipeToSlide ? b.checkNavigable(b.currentSlide - b.getSlideCount()) : b.currentSlide - b.getSlideCount(), b.currentDirection = 1;}"vertical" != d && (b.slideHandler(c), b.touchObject = {}, b.$slider.trigger("swipe", [b, d]));
    } else b.touchObject.startX !== b.touchObject.curX && (b.slideHandler(b.currentSlide), b.touchObject = {});
  }, b.prototype.swipeHandler = function (a) {
    var b = this;if (!(b.options.swipe === !1 || "ontouchend" in document && b.options.swipe === !1 || b.options.draggable === !1 && -1 !== a.type.indexOf("mouse"))) switch (b.touchObject.fingerCount = a.originalEvent && void 0 !== a.originalEvent.touches ? a.originalEvent.touches.length : 1, b.touchObject.minSwipe = b.listWidth / b.options.touchThreshold, b.options.verticalSwiping === !0 && (b.touchObject.minSwipe = b.listHeight / b.options.touchThreshold), a.data.action) {case "start":
        b.swipeStart(a);break;case "move":
        b.swipeMove(a);break;case "end":
        b.swipeEnd(a);}
  }, b.prototype.swipeMove = function (a) {
    var d,
        e,
        f,
        g,
        h,
        b = this;return h = void 0 !== a.originalEvent ? a.originalEvent.touches : null, !b.dragging || h && 1 !== h.length ? !1 : (d = b.getLeft(b.currentSlide), b.touchObject.curX = void 0 !== h ? h[0].pageX : a.clientX, b.touchObject.curY = void 0 !== h ? h[0].pageY : a.clientY, b.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(b.touchObject.curX - b.touchObject.startX, 2))), b.options.verticalSwiping === !0 && (b.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(b.touchObject.curY - b.touchObject.startY, 2)))), e = b.swipeDirection(), "vertical" !== e ? (void 0 !== a.originalEvent && b.touchObject.swipeLength > 4 && a.preventDefault(), g = (b.options.rtl === !1 ? 1 : -1) * (b.touchObject.curX > b.touchObject.startX ? 1 : -1), b.options.verticalSwiping === !0 && (g = b.touchObject.curY > b.touchObject.startY ? 1 : -1), f = b.touchObject.swipeLength, b.touchObject.edgeHit = !1, b.options.infinite === !1 && (0 === b.currentSlide && "right" === e || b.currentSlide >= b.getDotCount() && "left" === e) && (f = b.touchObject.swipeLength * b.options.edgeFriction, b.touchObject.edgeHit = !0), b.options.vertical === !1 ? b.swipeLeft = d + f * g : b.swipeLeft = d + f * (b.$list.height() / b.listWidth) * g, b.options.verticalSwiping === !0 && (b.swipeLeft = d + f * g), b.options.fade === !0 || b.options.touchMove === !1 ? !1 : b.animating === !0 ? (b.swipeLeft = null, !1) : void b.setCSS(b.swipeLeft)) : void 0);
  }, b.prototype.swipeStart = function (a) {
    var c,
        b = this;return b.interrupted = !0, 1 !== b.touchObject.fingerCount || b.slideCount <= b.options.slidesToShow ? (b.touchObject = {}, !1) : (void 0 !== a.originalEvent && void 0 !== a.originalEvent.touches && (c = a.originalEvent.touches[0]), b.touchObject.startX = b.touchObject.curX = void 0 !== c ? c.pageX : a.clientX, b.touchObject.startY = b.touchObject.curY = void 0 !== c ? c.pageY : a.clientY, void (b.dragging = !0));
  }, b.prototype.unfilterSlides = b.prototype.slickUnfilter = function () {
    var a = this;null !== a.$slidesCache && (a.unload(), a.$slideTrack.children(this.options.slide).detach(), a.$slidesCache.appendTo(a.$slideTrack), a.reinit());
  }, b.prototype.unload = function () {
    var b = this;a(".slick-cloned", b.$slider).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.remove(), b.$nextArrow && b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.remove(), b.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "");
  }, b.prototype.unslick = function (a) {
    var b = this;b.$slider.trigger("unslick", [b, a]), b.destroy();
  }, b.prototype.updateArrows = function () {
    var b,
        a = this;b = Math.floor(a.options.slidesToShow / 2), a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && !a.options.infinite && (a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === a.currentSlide ? (a.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : a.currentSlide >= a.slideCount - a.options.slidesToShow && a.options.centerMode === !1 ? (a.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : a.currentSlide >= a.slideCount - 1 && a.options.centerMode === !0 && (a.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")));
  }, b.prototype.updateDots = function () {
    var a = this;null !== a.$dots && (a.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), a.$dots.find("li").eq(Math.floor(a.currentSlide / a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"));
  }, b.prototype.visibility = function () {
    var a = this;a.options.autoplay && (document[a.hidden] ? a.interrupted = !0 : a.interrupted = !1);
  }, a.fn.slick = function () {
    var f,
        g,
        a = this,
        c = arguments[0],
        d = Array.prototype.slice.call(arguments, 1),
        e = a.length;for (f = 0; e > f; f++) {
      if ("object" == (typeof c === "undefined" ? "undefined" : _typeof(c)) || "undefined" == typeof c ? a[f].slick = new b(a[f], c) : g = a[f].slick[c].apply(a[f].slick, d), "undefined" != typeof g) return g;
    }return a;
  };
});

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(3);
__webpack_require__(4);
module.exports = __webpack_require__(5);


/***/ })
/******/ ]);