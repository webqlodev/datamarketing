<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
use DB;
use App\Click;
use App\Contact;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $var['message'] = $request->input('message');

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $var['ip'] = $ip;

        return view('microsite', $var);
    }

    public function upload(Request $request)
    {
        $path = $request->file->store('public/uploaded');
        return $path;
    }

    public function contactUs(Request $request) {

        $this->validate($request, [
            'name' => 'required|max:255',
            'contact' => 'required|numeric',
            'company' => 'required|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required',
            'file' => 'nullable|file|max:10000',
        ]);

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->contact = $request->contact;
        $contact->company = $request->company;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->save();

        // Send email

        Mail::send('emails.contactmail', ['contact' => $contact], function($mail) use ($contact) {
            $mail->from('no-reply@webqlo.com.my', 'Webqlo Digital Marketing');
            $mail->to($contact->email, $contact->name);
            $mail->subject('Webqlo Received Your Message "'.$contact->message.'"');
        });

        Mail::send('emails.receivedcontact', ['contact' => $contact], function($mail) use ($request) {
            $mail->from('no-reply@webqlo.com.my', 'Webqlo Digital Marketing Auto Email');
            $mail->to('hello@webqlo.com', 'Digital Marketing');
            $mail->subject('New Contact From Webqlo Sale Microsite');
            $mail->attach( storage_path('app/' . $request->uploaded) );
        });

        return array('status' => 1);
    }

    public function calculateBudget()
    {
        $click = new Click();

        $click->location = 'calculateBudget';
        $click->save();

        DB::table('calculate')->insert([
            'industry' => $_GET['industry'],
            'objective' => $_GET['objective'],
            'ip_address' => $_GET['ip'],
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        return view('budget', ['budget' => $_GET['calc'] ]);
    }

    public function caseStudy($case)
    {
        $click = new Click();

        $click->location = 'caseStudy' . ucwords($case);
        $click->save();

        return view('case-study.' . $case);
    }

    public function homepage()
    {
        $click = new Click();

        $click->location = 'headerLogo';
        $click->save();

        return redirect('http://www.webqlo.com.my/');
    }

    public function facebook()
    {
        $click = new Click();

        $click->location = 'facebook';
        $click->save();

        return redirect('https://www.facebook.com/webqlo');
    }
}
