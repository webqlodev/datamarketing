<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use Excel;
use App\Click;
use App\Contact;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('admin.dashboard');
    }

    public function dashboard()
    {
        $clickSummary = [
            'headerLogo' => Click::where('location', 'headerLogo')->count(),
            'calculateBudget' => Click::where('location', 'calculateBudget')->count(),
            'caseStudyShiseido' => Click::where('location', 'caseStudyShiseido')->count(),
            'caseStudyHbct' => Click::where('location', 'caseStudyHbct')->count(),
            'caseStudySr' => Click::where('location', 'caseStudySr')->count(),
            'caseStudyRl' => Click::where('location', 'caseStudyRl')->count(),
            'caseStudyTinge' => Click::where('location', 'caseStudyTinge')->count(),
            'facebook' => Click::where('location', 'facebook')->count(),
            'total' => Click::count(),
        ];

        return view('admin.dashboard', ['clickSummary' => $clickSummary]);
    }

    public function clicks(Request $request)
    {
        $startDate = date( 'Y-m-d', strtotime($request->startDate) );
        $endDate = date( 'Y-m-d', strtotime($request->endDate) );

        $result = [
            'headerLogo' => [],
            'calculateBudget' => [],
            'caseStudyShiseido' => [],
            'caseStudyHbct' => [],
            'caseStudySr' => [],
            'caseStudyRl' => [],
            'caseStudyTinge' => [],
        ];

        $currentDate = $startDate;

        while (true) {
            $result['headerLogo'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->where('location', 'headerLogo')
                    ->count()
            ];

            $result['calculateBudget'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->where('location', 'calculateBudget')
                    ->count()
            ];

            $result['caseStudyShiseido'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->where('location', 'caseStudyShiseido')
                    ->count()
            ];

            $result['caseStudyHbct'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->where('location', 'caseStudyHbct')
                    ->count()
            ];

            $result['caseStudySr'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->where('location', 'caseStudySr')
                    ->count()
            ];

            $result['caseStudyRl'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->where('location', 'caseStudyRl')
                    ->count()
            ];

            $result['caseStudyTinge'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->where('location', 'caseStudyTinge')
                    ->count()
            ];

            $result['facebook'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->where('location', 'facebook')
                    ->count()
            ];

            $result['total'][] = [
                $currentDate,
                Click::whereDate('created_at', $currentDate)
                    ->count()
            ];

            if ($currentDate == $endDate) {
                break;
            } else {
                $currentDate = date( 'Y-m-d', strtotime($currentDate) + (24*60*60) );
            }
        }

        return $result;
    }

    public function exportClicks(Request $request)
    {
        $startDate = date( 'Y-m-d', strtotime($request->startDate) );
        $endDate = date( 'Y-m-d', strtotime($request->endDate) );

        $fileName = 'webqlo_clicks_from_';
        $fileName .= date( 'j_M_Y', strtotime($request->startDate) ) . '_to_';
        $fileName .= date( 'j_M_Y', strtotime($request->endDate) );

        Excel::create($fileName, function ($excel) use ($startDate, $endDate) {
            $excel->sheet('reservations', function ($sheet) use ($startDate, $endDate) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Date',
                    'Header Logo',
                    'Calculate Budget',
                    'Case Study (Shiseido)',
                    'Case Study (HBCT)',
                    'Case Study (Secret Recipe)',
                    'Case Study (Red Lobster)',
                    'Case Study (Tinge)',
                    'Facebook',
                    'Total Clicks',
                ]);

                $currentDate = $startDate;

                while (true) {
                    $rowIndex++;

                    $sheet->row($rowIndex, [
                        date( 'j M Y', strtotime($currentDate) ),
                        Click::whereDate('created_at', $currentDate)
                            ->where('location', 'headerLogo')
                            ->count(),
                        Click::whereDate('created_at', $currentDate)
                            ->where('location', 'calculateBudget')
                            ->count(),
                        Click::whereDate('created_at', $currentDate)
                            ->where('location', 'caseStudyShiseido')
                            ->count(),
                        Click::whereDate('created_at', $currentDate)
                            ->where('location', 'caseStudyHbct')
                            ->count(),
                        Click::whereDate('created_at', $currentDate)
                            ->where('location', 'caseStudySr')
                            ->count(),
                        Click::whereDate('created_at', $currentDate)
                            ->where('location', 'caseStudyRl')
                            ->count(),
                        Click::whereDate('created_at', $currentDate)
                            ->where('location', 'caseStudyTinge')
                            ->count(),
                        Click::whereDate('created_at', $currentDate)
                            ->where('location', 'facebook')
                            ->count(),
                        Click::whereDate('created_at', $currentDate)
                            ->count(),
                    ]);

                    if ($currentDate == $endDate) {
                        break;
                    } else {
                        $currentDate = date( 'Y-m-d', strtotime($currentDate) + (24*60*60) );
                    }
                }

                $sheet->setAutoSize(true);
            });
        })->download('xls');
    }

    public function contacts(Request $request)
    {
        $contactModel = Contact::orderBy('created_at', 'desc');

        if ($request->startDate && $request->endDate) {
            $startDate = date( 'Y-m-d', strtotime($request->startDate) );
            $endDate = date( 'Y-m-d', strtotime($request->endDate) );

            $contactModel->whereDate('created_at', '>=', $startDate);
            $contactModel->whereDate('created_at', '<=', $endDate);
        }

        return Datatables::of( $contactModel->get() )->make(true);
    }

    public function exportContacts(Request $request)
    {
        $contactModel = Contact::orderBy('created_at', 'asc');
        $fileName = 'webqlo_contacts';

        if ($request->startDate && $request->endDate) {
            $startDate = date( 'Y-m-d', strtotime($request->startDate) );
            $endDate = date( 'Y-m-d', strtotime($request->endDate) );

            $fileName .= '_from_' . date( 'j_M_Y', strtotime($request->startDate) );
            $fileName .= '_to_' . date( 'j_M_Y', strtotime($request->endDate) );

            $contactModel->whereDate('created_at', '>=', $startDate);
            $contactModel->whereDate('created_at', '<=', $endDate);
        }

        $contacts = $contactModel->get();

        Excel::create($fileName, function ($excel) use ($contacts) {
            $excel->sheet('reservations', function ($sheet) use ($contacts) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Submitted On',
                    'Name',
                    'Company',
                    'Phone',
                    'Email',
                    'Message',
                ]);

                foreach ($contacts as $key => $value) {
                    $rowIndex++;

                    $sheet->row($rowIndex, [
                        date( 'j M Y', strtotime($value->created_at) ),
                        $value->name,
                        $value->company,
                        $value->contact,
                        $value->email,
                        $value->message,
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xls');
    }
}
