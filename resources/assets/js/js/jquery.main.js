$(document).ready(function() {
    Dropzone.autoDiscover = false;
	// $('#fullpage').fullpage({
	// 	sectionsColor: ['#111', '#111', '#FFF', '#FFF', '#FFF', '#FFF', '#13171a'],
	// 	slidesNavigation: true,
	// 	navigation: true,
	// 	keyboardScrolling: true,
 //        scrollingSpeed: 700,
 //        touchSensitivity: 15,
	// 	navigationPosition: 'right',
	// 	navigationTooltips: ['About Us', 'How & Why', 'Solution', 'Solution', 'Solution', 'Solution', 'Case Study', 'Client', 'Contact Us'],
	// 	normalScrollElements: '.jquery-modal, .case-wrap, .budget-body, .form-group',
	// 	afterLoad: function(anchorLink, index) {
	// 		//GA Tracking
	// 		gtag('send', 'pageview', { 'page': anchorLink });
	// 	}
 //    });

    $('.case-study-slides').slick({
        centerMode: true,
        appendArrows: $('.case-study-arrows'),
        prevArrow: '<button type="button" class="slick-prev">&larr;<span>More Cases</span></button>',
        nextArrow: '<button type="button" class="slick-next"><span>More Cases</span>&rarr;</button>',
        slidesToShow: 3,
        variableWidth: true
    });

    $('.test-testimonial').slick({
        centerMode: true,
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    });

    var dropZone = new Dropzone('.dropzone', {
        url: '/upload',
        maxFilesize: 10,
        maxFiles: 1,
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        acceptedFiles: 'application/msword, application/pdf',
        hiddenInputContainer: '.contact-form',
        dictDefaultMessage: '<i class="fa fa-upload" style="font-size: 32px;"></i><br />Got docs? Drag &amp; drop \'em here.',
        dictFallbackMessage: 'Got docs? Click to upload.',
        dictFallbackText: null,
        dictInvalidFileType: 'Only supports DOC, and PDF.',
        dictFileTooBig: 'Maximum file size of 10MB allowed.'
    });

    dropZone.on('success', function (file, response) {
        $('.uploaded').val(response);
        this.disable();
    });

    dropZone.on('error', function (file, errorMessage) {
        this.disable();
    });

    dropZone.on('removedfile', function () {
        $('.uploaded').val('');
        this.enable();
    });

    $('.modal-btn').click(function (event) {
        if ( $(this).hasClass('case-study-btn') ) {
            var ajaxLink = $(this).attr('data-ajax');

            $.get(ajaxLink, function (modalHtml) {
                var result = $(modalHtml).appendTo('body').modal({
                    clickClose: false,
                    closeText: '&bigotimes;',
                    escapeClose: false
                });

				var url = ajaxLink;
				var array = url.split('/');
				var lastSegment = array[array.length-1];
				var lastSecondSegment = array[array.length-2];

				//GA Tracking
    			gtag('send', 'pageview', { 'page': lastSecondSegment+'/'+lastSegment });

                $('body').one($.modal.AFTER_CLOSE, function () {
                    location.hash = 'case-study';
                });
            });
        } else if ( $(this).hasClass('contact-form-btn') ) {
            event.preventDefault();

            $('body').one($.modal.OPEN, function () {
                var modalDropZone = new Dropzone('.contact-modal .dropzone', {
                    url: '/upload',
                    maxFilesize: 10,
                    maxFiles: 1,
                    addRemoveLinks: true,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    acceptedFiles: 'application/msword, application/pdf',
                    hiddenInputContainer: '.contact-form',
                    dictDefaultMessage: '<i class="fa fa-upload" style="font-size: 32px;"></i><br />Got docs? Drag &amp; drop \'em here.',
                    dictFallbackMessage: 'Got docs? Click to upload.',
                    dictFallbackText: null,
                    dictInvalidFileType: 'Only supports DOC, and PDF.',
                    dictFileTooBig: 'Maximum file size of 10MB allowed.'
                });

                modalDropZone.on('success', function (file, response) {
                    $('.contact-modal .uploaded').val(response);
                    this.disable();
                });

                modalDropZone.on('error', function (file, errorMessage) {
                    this.disable();
                });

                modalDropZone.on('removedfile', function () {
                    $('.contact-modal .uploaded').val('');
                    this.enable();
                });
            });

            $(this).modal({
                clickClose: false,
                closeText: '&bigotimes;',
                escapeClose: false,
                modalClass: 'modal contact-modal'
            });
        }
    });

	$('body').on($.modal.CLOSE, function (event, modal) {
        // Re-enable fullPage.js scrolling
        // $.fn.fullpage.setAllowScrolling(true);
        // $.fn.fullpage.setKeyboardScrolling(true);

		$('.ind, .obj').val('default');
		$('.ind, .obj').selectpicker('refresh');

		$('.lc, .bd').val('');
    });

    $('body').on('click', '.case-study-modal .modal-nav a', function (event) {
        var ajaxLink = $(this).attr('data-ajax');

        $('.modal').scrollTop(0);

        $('.case-study-modal').empty();

        // Change the content of the case study modal
        $.get(ajaxLink, function (modalHtml) {
            $('.case-study-modal').html( $(modalHtml).html() + '<a href="#close-modal" rel="modal:close" class="close-modal ">&bigotimes;</a>' );

            var url = ajaxLink;
            var array = url.split('/');
            var lastSegment = array[array.length-1];
            var lastSecondSegment = array[array.length-2];

            //GA Tracking
            gtag('send', 'pageview', { 'page': lastSecondSegment+'/'+lastSegment });
        });
    });

    $('body').on('input', '.numeric', function (event) {
        // Only allow numbers for this input field
    	this.value = this.value.replace(/[^0-9]/g, '');
    });

    $('body').on('click', '.submitContact', function() {
        // Check if the form is from modal or body
        if ( $(this).parents('.jquery-modal').length ) {
            var val = new FormData( $('.postContact')[1] );
        } else {
            var val = new FormData( $('.postContact')[0] );
        }

    	var url = "contact-us";
    	var sEmail = $('input[name="email"]').val();

        $.ajax({
            method: 'POST',
            url: url,
            data: val,
            processData: false,
            contentType: false,
            cache: false,
            success: function(r){
                if (r.status == '1') {
        			// $('#thanks').modal('show');
        			$(".postContact")[0].reset();
                    dropZone.removeAllFiles();
    				 //GA Tracking
    				gtag('send', 'event', 'Form', 'Submission');
    				window.location = "thanks";
                }
            },
            error: function(er) {
                // Show validation error results
                if(er.responseJSON.name) {
                    $('.name').empty();
                    var err = '<div class="text-danger">Name is required</div>';
                    $('.name').append(err);
                    $('.name').show(err);
                } else {
                    $('.name').hide();
        		}

        		if(er.responseJSON.contact) {
                    $('.contact').empty();
                    var err = '<div class="text-danger">Contact is required</div>';
                    $('.contact').append(err);
                    $('.contact').show(err);
        		} else {
                    $('.contact').hide();
        		}

                if(er.responseJSON.company) {
                    $('.company').empty();
                    var err = '<div class="text-danger">Company is required</div>';
                    $('.company').append(err);
                    $('.company').show(err);
                } else {
                    $('.company').hide();
        		}

    			if(er.responseJSON.message) {
                    $('.message').empty();
                    var err = '<div class="text-danger">Message is required</div>';
                    $('.message').append(err);
                    $('.message').show(err);
                }  else {
                    $('.message').hide();
        		}

        		function validateEmail(sEmail) {
        			var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        			if (filter.test(sEmail)) {
        				return true;
        			} else {
        				return false;
        			}
        		}

        		if(er.responseJSON.email) {
                    $('.email').empty();
                    var err = '<div class="text-danger">Email is required</div>';
                    $('.email').append(err);
                    $('.email').show(err);
                } else if( !validateEmail(sEmail) ) {
                    $('.email').empty();
                    var err = '<div class="text-danger">This must be a valid email address</div>';
                    $('.email').append(err);
                    $('.email').show(err);
                }  else {
                    $('.email').hide();
        		}
        	}
        });
    });

    $('body').on('keyup', "input[data-type='number']", function(event){
		// Skip for arrow keys
		if(event.which >= 37 && event.which <= 40){
			event.preventDefault();
		}

        // Add comma to every third places
		var $this = $(this);
		var num = $this.val().replace(/,/gi, '');
		var num2 = num.split(/(?=(?:\d{3})+$)/).join(',');

		$this.val(num2);
	});

    $('.cta-btn').on('click', function() {
        event.preventDefault();

        if ($(window).width() > 991) {
            $('html, body').animate({ scrollTop: $('.section[data-anchor="contact-us"]').offset().top }, 500);
        } else {
            $('#modal-contact-btn').click();
        }
            
    });
});