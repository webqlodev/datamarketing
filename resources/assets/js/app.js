
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');
require('./js/bootstrap.min.js');
require('./js/scrolloverflow.min.js');
require('./js/jquery.fullpage.js');
//require('./js/jquery.fullpage.extensions.min.js');
require('./js/jquery.easings.min.js');
require('./js/jquery.modal.min.js');
require('./js/jquery.animateNumber.min.js');
require('./js/jquery.easings.min.js');
//require('./js/jquery.cardslider.min.js');
require('./js/slick.min.js');
window.Dropzone  = require('./js/dropzone.min.js');
//require('./js/ben.js');
require('./js/bootstrap-select.min.js');
window.bodymovin = require('./js/bodymovin.js');
require('./js/jquery.svg.js');
require('./js/jquery.main.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });
