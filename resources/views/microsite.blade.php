@extends('layout')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <style>
    	body{color:#f7f7f7;background-color:#111; font-display:fallback;}
    </style>
@stop
@section('content')
<div class="fixed-logo md-show">
	<div class="logo-wrap">
		<a href="{{ route('homepage') }}" target="_blank">
			<img src="{{url('img/webqlo-2018-retina.png')}}" class="img-responsive" alt="Webqlo Logo">
		</a>
	</div>
</div>
<div class="block-logo text-center md-hide">
	<div class="logo-wrap">
		<a href="{{ route('homepage') }}" target="_blank">
			<img src="{{url('img/webqlo-logo.png')}}" class="img-responsive" alt="Webqlo Logo">
		</a>
	</div>
</div>
		
<div id="fullpage">
	<div class="section" data-anchor="about-us" data-tooltip="About Us">
        <!-- <div class="dig-pad top-page-bg lazy-bg" data-bg="url({{env('APP_URL')}}img/header_image.jpg)"> -->
        <div class="dig-pad top-page-bg lazy-bg" data-bg="">
			<div class="dig-in">
                <div class="row">
    				<div class="text-center case-pad">
    					<!-- <div class="gothambook-28 title-letter-spacing" style="padding: 0px 0px 20px;">
    						"We help businesses drive <br>impactful digital performance in any industry"
    					</div> -->
        				<!-- <div class="latosemibold-100 title-letter-spacing text-upper">
        					We believe <span class="show-block"><br></span>Digital Marketing<br><span class="hide-block"> can grow your <span class="show-block"><br></span><span class="latoheavy-100">brand.</span></span>
						</div> -->
						<div class="avenirheavy-100 title-letter-spacing text-upper">
        					We believe<span class="sm-hide-block"> </span><span class="show-block"><br></span>Digital Marketing<span class="show-block"><br></span><span class="hide-block">can grow your brand.</span>
						</div>
						<div class="video-boundary-container p-t-1">
        					<div class="video-container">
	        					<iframe width="560" height="315" src="https://www.youtube.com/embed/hOI1g8PVSpU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	        				</div>
        				</div>
						<div class="p-t-1 top-text-container">
        					<!-- <p class="gothambook-28">We strategically develop the most <span class="hide-block"> effective and connected experiences by</span> <span class="hide-block">combining data, analytics & creative solutions to </span><span class="hide-block">consistency deliver top performance for our clients.</span></p> -->
        					<p class="avenirbook-28">We strategically develop the most effective and connected experiences by combining data, analytics & creative solutions to consistently deliver top performance for our clients.</p>
        				</div>
        				<div class="divider"></div>
        				<div class="p-b-1 top-text-container">
        					<!-- <p class="gothambook-28">We are an award-winning digital marketing team that <span class="hide-block"> specializes in <span class="gothambold-28">Data-driven & Performance Marketing</span></p> -->
        					<p class="avenirbook-28">We are an award-winning digital marketing team that specializes in Data-driven & Performance Marketing</p>
        				</div>
        				
	        				
        				<div class="btn-container">
        					<button id="after-video-cta" class=" btn cta-btn avenirbook-28">Contact Us Now!</button>
        				</div>
        			</div>
                </div>
        		<div class="fixed-pageno">01 <span class="blacky">-09</span></div>
            </div>
        </div>
    </div>
	<div class="section" data-anchor="how-why" data-tooltip="About Us">
        <!-- <div class="dig-pad second-page-bg lazy-bg" data-bg="url({{env('APP_URL')}}img/textured-bg01.jpg)"> -->
    	<div class="dig-pad second-page-bg lazy-bg" data-bg="">
			<div class="dig-in">
				<div class="container vcenter-container">
					<div class="text-center">
						<div class="avenirheavy-100 title-letter-spacing text-upper">
        					What We Do
						</div>
						<!-- <div class="row gothambook-24-i text-left"> -->
						<div class="row avenirbook-10 text-center">
							<!-- <div class="col-xs-12 col-md-4">
								<div class="latosemibold-i-80 slogan-title">Enable</div>
								<div class="divider-longer"></div>
								<p class="gothambook-26">Social Media Analytics that allows <span class="hide-block">you to think and plan ahead of </span><span class="hide-block">your competitors.</span></p>
							</div>
							<div class="col-xs-12 col-md-4">
								<div class="latosemibold-i-80 slogan-title">Empower</div>
								<div class="divider-longer"></div>
								<p class="gothambook-26">Boost your content marketing <span class="hide-block">strategy and tactical campaigns.</span></p>
							</div>
							<div class="col-xs-12 col-md-4">
								<div class="latosemibold-i-80 slogan-title">Enrich</div>
								<div class="divider-longer"></div>
								<p class="gothambook-26">Performance Marketing through <span class="hide-block">Digital Strategy and KPI</span> <span class="hide-block">review.</span></p>
							</div> -->
							<div class="col-xs-12 col-sm-6 p-h-1 m-t-30 what-we-do-container">
								<div class="what-we-do-icon-holder">
									<img class="what-we-do-icon" src="{{env('APP_URL')}}img/icons/wm.png">
								</div>
								<div class="what-we-do-title avenirheavy-30 theme-blue-text">
									Web Marketing
								</div>
								<!-- <div class="what-we-do-subtitle italic">
									Do you think your website is past its sell-by date?
								</div> -->
								<div class="what-we-do-des">
									We build & craft optimized sites to outperform their peers
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 p-h-1 m-t-30 what-we-do-container">
								<div class="what-we-do-icon-holder">
									<img class="what-we-do-icon" src="{{env('APP_URL')}}img/icons/cm.png">
								</div>
								
								<div class="what-we-do-title avenirheavy-30 theme-blue-text">
									Content Marketing
								</div>
								<!-- <div class="what-we-do-subtitle italic">
									Audience love stunning creativity
								</div> -->
								<div class="what-we-do-des">
									We craft beautiful content that connects your audience with your brand.
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 p-h-1 m-t-30 what-we-do-container">
								<div class="what-we-do-icon-holder">
									<img class="what-we-do-icon" src="{{env('APP_URL')}}img/icons/pm.png">
								</div>
								<div class="what-we-do-title avenirheavy-30 theme-blue-text">
									Performance Marketing
								</div>
								<!-- <div class="what-we-do-subtitle italic">
									Outperform your competitors
								</div> -->
								<div class="what-we-do-des">
									We ensure every digital campaign is measured & executed well to hit your KPI.
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 p-h-1 m-t-30 what-we-do-container">
								<div class="what-we-do-icon-holder">
									<img class="what-we-do-icon" src="{{env('APP_URL')}}img/icons/sma.png">
								</div>
								<div class="what-we-do-title avenirheavy-30 theme-blue-text">
									Social Media Analytics
								</div>
								<!-- <div class="what-we-do-subtitle italic">
									Uncover opportunities & overcome challenges
								</div> -->
								<div class="what-we-do-des">
									We equip you with big data technology that gives you an unfair foresight over your competitors.
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fixed-pageno">02 <span class="blacky">-09</span></div>
			</div>
		</div>
    </div>
	<!-- <div class="section" data-anchor="solution1" data-tooltip="About Us">
        <div class="dig-pad black-bg">
			<div class="dig-in">
				<div class="container vcenter-container">
					<div class="row">
						<div class="col-md-6 col-sm-6 pad-left-1">
                            <div id="wd">
                            </div>
						</div>
						<div class="col-md-6 col-sm-6 pad-left">
							<div class="latosemibold-65 title-letter-spacing">Website Development</div>
							<div class="left-divider"></div>
							<div class="text-left gothambook-24-i">
								Provide innovative <span class="hide-block">designs and enhancements that will</span><span class="hide-block"> revolutionize your brand's Digital game and</span><span class="hide-block"> boost online brand performance.</span>
							</div>
						</div>
					</div>
				</div>
				<div class="fixed-pageno">03 <span class="blacky">-09</span></div>
			</div>
		</div>
    </div> -->
	<!-- <div class="section" data-anchor="solution2" data-tooltip="About Us">
        <div class="dig-pad black-bg">
			<div class="dig-in">
				<div class="container vcenter-container">
					<div class="row">
						<div class="col-xs-12 visible-xs">
                            <div id="da">
                            </div>
						</div>
						<div class="col-md-6 col-sm-6 pad-left">
							<div class="latosemibold-65 title-letter-spacing">Digital Advertising</div>
							<div class="left-divider"></div>
							<div class="text-left gothambook-24-i">
								The world is diving head first into more <span class="hide-block">automated future and we enable our partners</span><span class="hide-block"> to embrace this once in a lifetime</span><span class="hide-block"> technological revolution with our strategic</span><span class="hide-block"> executions.</span>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 hidden-xs">
                            <div id="da2">
                            </div>
						</div>
					</div>
				</div>
				<div class="fixed-pageno">04 <span class="blacky">-09</span></div>
			</div>
		</div>
    </div> -->
	<!-- <div class="section" data-anchor="solution3" data-tooltip="About Us">
        <div class="dig-pad black-bg">
			<div class="dig-in">
				<div class="container vcenter-container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
                            <div id="cc">
                            </div>
						</div>
						<div class="col-md-6 col-sm-6 pad-left">
							<div class="latosemibold-65 title-letter-spacing">Creative Content</div>
							<div class="left-divider"></div>
							<div class="text-left gothambook-24-i">
								We create meaningful stories and <span class="hide-block">well-crafted creative content to be highly</span><span class="hide-block"> engaging and tastefully original.</span>
							</div>
						</div>
					</div>
				</div>
				<div class="fixed-pageno">05 <span class="blacky">-09</span></div>
			</div>
		</div>
    </div> -->
    <div class="section" data-anchor="our-work" data-tooltip="About Us">
    	<div class="dig-pad black-bg">
    		<div class="dig-in">
    			<div class="container vcenter-container">
    				<div class="text-center">
    					<div class="avenirheavy-100 title-letter-spacing text-upper">
        					Our Work
						</div>
						<div class="row avenirmedium-10">
							<div class="col-md-4 our-work-container">
								<div class="our-work-imgbtn-container relative">
									<div class="our-work-img-container animate-shadow relative">
										<a href="http://datamarketing.webqlo.com.my/" target="_blank">
											<img src="{{env('APP_URL')}}img/our-work/shiseido-white-lucent.jpg" class="our-work-img">
											<!-- <button class="btn our-work-explore-btn text-upper latosemibold-10 absolute">Explore</button> -->
											<!-- <a href="http://datamarketing.webqlo.com.my/" target="_blank" class="btn our-work-explore-btn text-upper avenirmedium-10 absolute">Explore</a> -->
											<div class="btn our-work-explore-btn text-upper avenirmedium-10 absolute">Explore</div>
										</a>
											
									</div>
									<div class="our-work-excel-container absolute">
										<img src="{{env('APP_URL')}}img/icons/award.png" class="our-work-excel-img">
										<div class="our-work-excel-des theme-gold-text text-left our-work-excel-text">
											Marketing Excellence Award
											<br>
											(Gold in Data-Driven Marketing)
										</div>
									</div>
								</div>
								<div class="our-work-data-container text-center">
									<div class="our-work-data-brand-text">
										<div class="our-work-data-title avenirheavy-30">
											Shiseido
										</div>
										<div class="our-work-data-subtitle">
											Social Media Marketing
										</div>
									</div>
									<div class="our-work-engage our-work-data-item">
										<div class="our-work-data-rate theme-blue-text avenirheavy-20">
											+141%
										</div>
										<div class="our-work-data-text">
											Engagement Rate
										</div>
									</div>
									<div class="our-work-ctr our-work-data-item">
										<div class="our-work-data-rate theme-blue-text avenirheavy-20">
											+160%
										</div>
										<div class="our-work-data-text">
											CTR (Click Thru Rate)
										</div>
									</div>
									<div class="our-work-cpl our-work-data-item">
										<div class="our-work-data-rate theme-blue-text avenirheavy-20">
											-86%
										</div>
										<div class="our-work-data-text">
											CPL (Cost Per Lead)
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 our-work-container">
								<div class="our-work-imgbtn-container relative">
									<div class="our-work-img-container relative">
										<a href="http://datamarketing.webqlo.com.my/" target="_blank">
											<img src="{{env('APP_URL')}}img/our-work/rt_july-two.jpg" class="our-work-img">
											<!-- <button class="btn our-work-explore-btn text-upper latosemibold-10 absolute">Explore</button> -->
											<!-- <a href="http://datamarketing.webqlo.com.my/" target="_blank" class="btn our-work-explore-btn text-upper avenirmedium-10 absolute">Explore</a> -->
											<div class="btn our-work-explore-btn text-upper avenirmedium-10 absolute">Explore</div>
										</a>
									</div>
								</div>
								<div class="our-work-data-container text-center">
									<div class="our-work-data-brand-text">
										<div class="our-work-data-title avenirheavy-30">
											Rakuten Trade
										</div>
										<div class="our-work-data-subtitle">
											Increase Account Opening
										</div>
									</div>
									<div class="our-work-engage our-work-data-item">
										<div class="our-work-data-rate theme-blue-text avenirheavy-20">
											+61%
										</div>
										<div class="our-work-data-text">
											Engagement Rate
										</div>
									</div>
									<div class="our-work-ctr our-work-data-item">
										<div class="our-work-data-rate theme-blue-text avenirheavy-20">
											+4%
										</div>
										<div class="our-work-data-text">
											CTR (Click Thru Rate)
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 our-work-container">
								<div class="our-work-imgbtn-container relative">
									<div class="our-work-img-container relative">
										<a href="http://datamarketing.webqlo.com.my/" target="_blank">
											<img src="{{env('APP_URL')}}img/our-work/igtaiwanexcellence-jan-26.jpg" class="our-work-img">
											<!-- <button class="btn our-work-explore-btn text-upper latosemibold-10 absolute">Explore</button> -->
											<!-- <a href="http://datamarketing.webqlo.com.my/" target="_blank" class="btn our-work-explore-btn text-upper avenirmedium-10 absolute">Explore</a> -->
											<div class="btn our-work-explore-btn text-upper avenirmedium-10 absolute">Explore</div>
										</a>
											
									</div>
								</div>
								<div class="our-work-data-container text-center">
									<div class="our-work-data-brand-text">
										<div class="our-work-data-title avenirheavy-30">
											TAIWAN EXCELLENCE
										</div>
										<div class="our-work-data-subtitle">
											Increase Brand Awareness
										</div>
									</div>
									<div class="our-work-engage our-work-data-item">
										<div class="our-work-data-rate theme-blue-text avenirheavy-20">
											2,794,160
										</div>
										<div class="our-work-data-text">
											Clicks
										</div>
									</div>
									<div class="our-work-ctr our-work-data-item">
										<div class="our-work-data-rate theme-blue-text avenirheavy-20">
											+22%
										</div>
										<div class="our-work-data-text">
											CTR (Click Thru Rate)
										</div>
									</div>
								</div>
							</div>
						</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="section" data-anchor="testimonials" data-tooltip="About Us">
    	<div class="dig-pad black-bg">
    		<div class="dig-in">
    			<div class="container vcenter-container">
    				<div class="text-center">
    					<div class="avenirmedium-30 title-letter-spacing">
        					Send us your enquiry and 
        					<br>we'll be in touch.
						</div>
						<div class="btn-container">
        					<button id="after-our-work-cta" class=" btn cta-btn avenirbook-28">Send Enquiry Now!</button>
        				</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="section" data-anchor="testimonials" data-tooltip="About Us">
    	<div class="dig-pad black-bg">
    		<div class="dig-in">
    			<div class="container vcenter-container">
    				<div class="text-center">
    					<div class="avenirheavy-100 title-letter-spacing text-upper">
        					Testimonials
						</div>
						<!-- <div class='carousel-product d-none'>
		                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							  
							  <ol class="carousel-indicators">
							    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
							    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
							  </ol>

							  <div class="carousel-inner theme-black-text text-left" role="listbox">
							    <div class="item active">
							      <div class="item-container">
							      	<div class="testimonial-container">
							      		<p class="testimonial-brand-name">
							      			Shiseido
							      		</p>
							      		<p class="testimonial-position">
							      			Manager, Digital Marketing & CRM
							      		</p>
							      		<p class="testimonial-des">
							      			"Webqlo is more than just a digital performance agency to us. They have helped our brand reach greater heights in terms of growing our audience on our digital platforms by targeting the right audience and increasing traffic, and providing us informative data to drive business performances to stay ahead of our competitors."
							      		</p>
							      	</div>
							  	  </div>
							      <div class="carousel-caption">
							      </div>
							    </div>
							    <div class="item">
							      <div class="item-container">
							      	<div class="testimonial-container">
							      		<p class="testimonial-brand-name">
							      			NARS
							      		</p>
							      		<p class="testimonial-position">
							      			General Manager
							      		</p>
							      		<p class="testimonial-des">
							      			"We've achieved great results thanks to Adqlo's data-driven marketing. Our online and social performance have improved tremendously thanks to their team's dedication and analytic methods. The team understands the brand needs and is always working with us to strengthen our presence online."
							      		</p>
							      	</div>
							      </div>
							      <div class="carousel-caption">
							      </div>
							    </div>
							    <div class="item">
							      <div class="item-container">
							      	<div class="testimonial-container">
							      		<p class="testimonial-brand-name">
							      			Laura Mercier
							      		</p>
							      		<p class="testimonial-position">
							      			Deputy General Manager
							      		</p>
							      		<p class="testimonial-des">
							      			"Webqlo eases my mind. A reliable & experience in digital marketing with excellent insights. Strong in performance marketing and creative in web development & content creation. The team is dedicated and efficient."
							      		</p>
							      	</div>
							      </div>
							      <div class="carousel-caption">
							      </div>
							    </div>
							  </div>

							  
							  <a class="left carousel-control" id="carousel-control-left-a" href="#carousel-example-generic" role="button" data-slide="prev">
							    <i class="fa fa-arrow-left theme-black-text"></i>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" id="carousel-control-right-a" href="#carousel-example-generic" role="button" data-slide="next">
							    
							    <i class="fa fa-arrow-right theme-black-text"></i>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
                		</div> -->

                		<div class="test-testimonial theme-black-text">
                			<div class="slick-slide-item text-left">
                				<div class="testimonial-container">
						      		<p class="testimonial-brand-name">
						      			Shiseido
						      		</p>
						      		<p class="testimonial-position">
						      			Manager, Digital Marketing & CRM
						      		</p>
						      		<p class="testimonial-des">
						      			"Webqlo is more than just a digital performance agency to us. They have helped our brand reach greater heights in terms of growing our audience on our digital platforms by targeting the right audience and increasing traffic, and providing us informative data to drive business performances to stay ahead of our competitors."
						      		</p>
						      	</div>
                			</div>
	                			
					      	<div class="slick-slide-item text-left">
					      		<div class="testimonial-container">
						      		<p class="testimonial-brand-name">
						      			NARS
						      		</p>
						      		<p class="testimonial-position">
						      			General Manager
						      		</p>
						      		<p class="testimonial-des">
						      			"We've achieved great results thanks to Adqlo's data-driven marketing. Our online and social performance have improved tremendously thanks to their team's dedication and analytic methods. The team understands the brand needs and is always working with us to strengthen our presence online."
						      		</p>
						      	</div>
					      	</div>

					      	<div class="slick-slide-item text-left">
					      		<div class="testimonial-container">
						      		<p class="testimonial-brand-name">
						      			Laura Mercier
						      		</p>
						      		<p class="testimonial-position">
						      			Deputy General Manager
						      		</p>
						      		<p class="testimonial-des">
						      			"Webqlo eases my mind. A reliable & experience in digital marketing with excellent insights. Strong in performance marketing and creative in web development & content creation. The team is dedicated and efficient."
						      		</p>
						      	</div>
					      	</div>
						      	
					  	  </div>
                		</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- <div class="section" data-anchor="solution4" data-tooltip="About Us">
        <div class="dig-pad black-bg">
			<div class="dig-in">
				<div class="container vcenter-container">
					<div class="row">
						<div class="col-xs-12 visible-xs">
                            <div id="sma">
                            </div>
						</div>
						<div class="col-md-6 col-sm-6 pad-left">
							<div class="latosemibold-65 title-letter-spacing">Social Media Analytics</div>
							<div class="left-divider"></div>
							<div class="text-left gothambook-24-i">
							Equip your team with big data technology to have the upper hand in social media with strategic foresight, competitor analysis, and meaningful social data. Understand how big data can influence your position in the digital landscape and uncover the strengths and weaknesses of your brand with the help of our experts.
							</div>
						</div>
						<div class="col-md-6 col-sm-6 hidden-xs">
                            <div id="sma2">
                            </div>
						</div>
					</div>
				</div>
				<div class="fixed-pageno">06 <span class="blacky">-09</span></div>
			</div>
		</div>
    </div> -->
	<!-- <div class="section" data-anchor="case-study" data-tooltip="About Us">
        <div class="dig-pad black-bg relative">
			<div class="dig-in">
				<div class="container pro-con">
                    <div class="row">
						<div class="text-center case-pad">
	        				<div class="lts-square-nml" style="opacity: 0;"><p> </p></div>
	        				<div class="case-study-title"><p> </p></div>
	        				<div class="case-study-title"><p> </p></div>
	        			</div>
	        			<div class="case-study-slides title-letter-spacing">
	                        <div class="slide-item">
	                            <a href="#case-study-shiseido" class="lazy-bg modal-btn case-study-btn" data-ajax="{{ route('casestudy', ['case' => 'shiseido']) }}"
	                            	 data-bg="url({{ asset('/img/newcasestudy/shiseido-portfolio-640.jpg')}})">
	                                <div class="latoheavy-40 case-content">Digital Advertising</div>
	                                <p class="gothambook-24 case-content">Embrace Digital enhancements and technological revolution.</p>
	                            </a>
							</div>
							
							<div class="slide-item">
	                            <a href="#case-study-adqlo" class="lazy-bg modal-btn case-study-btn" data-ajax="{{ route('casestudy', ['case' => 'adqlo']) }}" data-bg="url({{ asset('/img/newcasestudy/adqlo-portfolio-640.jpg') }})">
	                                <div class="latoheavy-40 case-content">Social Media Analytics</div>
	                                <p class="gothambook-24 case-content">Get Insights to improve your Engagement & Conversion on Social Media.</p>
	                            </a>
							</div>
							<div class="slide-item">
	                            <a href="#case-study-tinge" class="lazy-bg modal-btn case-study-btn" data-ajax="{{ route('casestudy', ['case' => 'tinge']) }}" data-bg="url({{ asset('/img/newcasestudy/tinge-portfolio-640.jpg') }})">
	                                <div class="latoheavy-40 case-content">Creative Content</div>
	                                <p class="gothambook-24 case-content">Create meaningful stories that go a long way.</p>
	                            </a>
	                        </div>

	                        <div class="slide-item">
	                            <a href="#case-study-wendys" class="lazy-bg modal-btn case-study-btn" data-ajax="{{ route('casestudy', ['case' => 'wendys']) }}" data-bg="url({{ asset('/img/newcasestudy/wendys-portfolio-640.jpg') }})">
	                                <div class="latoheavy-40 case-content ">Website Development</div>
	                                <p class="gothambook-24 case-content">Provide enhancements that will revolutionize your brand.</p>
	                            </a>
							</div>
	                    </div>
	                </div>
                </div>
				<div class="fixed-pageno">07 <span class="blacky">-09</span></div>
				<div class="case-study-arrows"></div>
			</div>
		</div>
    </div> -->
    <div class="section" data-anchor="client" data-tooltip="About Us">
        <div class="dig-pad black-bg">
			<div class="dig-in">
				<div class="avenirheavy-100 title-letter-spacing text-upper text-center">
					Our Clients
				</div>
				<div class="row">
					<div class="col-xs-12 text-center p-xs-0">
						@php
							$clients = array(
							"Canon"=>"Canon.png",
							"HBCT"=>"HBCT.png",
							"Imagine your Korea"=>"imginekorea.png",
							"Laura Mercier"=>"lauramercier.png",
							"Maybank"=>"maybank.png",
							"NARS"=>"NARS.png",
							"Rakuten Trade"=>"RT.png",
							"Shiseido"=>"shiseido.png",
							"Spritzer"=>"spritzer.png",
							"Secret Recipe"=>"SR.png",
							"Taiwan Excellent"=>"Taiwan-Excellent.png",
							 );
						@endphp
						<div class="row">
							@foreach($clients as $client=>$client_logo)
								@if($client == 'Taiwan Excellent')
									<div class="col-xs-6 col-xs-offset-3 col-md-3 col-md-offset-0 p-xs-0">
										<img data-src="{{env('APP_URL')}}/img/client_logos/{{$client_logo}}" alt="Client Logo {{$client}}" class="img-fluid lazy-logo" />
									</div>
								@else
									<div class="col-xs-6 col-md-3 p-xs-0">
										<img data-src="{{env('APP_URL')}}/img/client_logos/{{$client_logo}}" alt="Client Logo {{$client}}" class="img-fluid lazy-logo" />
									</div>
								@endif
							<!-- <div class="col-xs-6 col-md-3 p-xs-0">
								<img data-src="{{env('APP_URL')}}/img/client_logos/{{$client_logo}}" alt="Client Logo {{$client}}" class="img-fluid lazy-logo" />
							</div> -->
							@endforeach
						</div>
					</div>
				</div>
				<div class="fixed-pageno">08 <span class="blacky">-09</span></div>
			</div>
		</div>
	</div>
	<div class="section" data-anchor="contact-us" data-tooltip="About Us">
		<div class="col-md-6 form-pad-left">
			<div class="left-form-bg lazy-bg" data-bg="url({{env('APP_URL')}}img/contact-left-bg.jpg)">
				<div class="form-pad-left">
					<div class="left-in text-center">
						<div class="contact-title latosemibold-90">
							Go Digital
						</div>
						<div class="contact-stitle gothambook-50">
							Let's get started!
						</div>
						<div class="contact-items gothambook-24 text-left">
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2">
									<i class="fa fa-envelope-o fa-2x"></i>
								</div>
								<div class="col-md-10 col-sm-10 col-xs-10">
									<p class="contact-item">
										enquiry@webqlo.com.my
									</p>
								</div>
							</div>

							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2">
									<i class="fa fa-phone fa-2x"></i>
								</div>
								<div class="col-md-10 col-sm-10 col-xs-10">
									<p class="contact-item">
										<a href="tel:0377347247">+603-7734 7247</a>
									</p>
								</div>
							</div>

							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2">
									<i class="fa fa-map-o fa-2x"></i>
								</div>
								<div class="col-md-10 col-sm-10 col-xs-10">
									<p class="contact-item">
										B-2-5, Pacific Place Commercial Center, <br />
										Jalan PJU 1A/4, Ara Damansara, <br />
										47301 Petaling Jaya, Selangor.
									</p>
								</div>
							</div>
						</div>

						<div class="hidden-md hidden-lg small-contact-btn">
							<a id="modal-contact-btn" href="{{ route('contactform') }}" class="modal-btn contact-form-btn lts-btn lts-btn-light hvr-sweep-to-right">Contact Us <img src="{{ asset('/img/rightarrow-light.png') }}" alt="" class="right-arrow" /></a>
						</div>
					</div> <!-- left in -->
				</div>
			</div>
		</div> <!-- 6 -->
		<div class="col-md-6 form-pad-right hidden-xs hidden-sm">
			<div class="right-form-bg">
				<div class="form-pad-right">
					<div class="right-in">
						@include('contact-form')
					</div>
				</div>
			</div>
		</div> <!-- 6 -->
		<div class="fixed-pageno">END</div>
    </div>
</div>
<!-- <a href="{{ route('facebook') }}" aria-label="Facebook page" rel="facebook" class="fixed-fb text-center" target="_blank">
	<div class="fb-wrap">
		<i class="fa fa-facebook fa-cus"></i>
	</div>
</a> -->
{{--<div id="error" class="hidden">
    @include('thanks')
</div>--}}
@stop
@section('script')
<script>
	$(function(){
		var myLazyLoad = new LazyLoad({
		    elements_selector: ".lazy-bg",
		    threshold:500
		});
		var myLazyLoadLogo = new LazyLoad({
		    elements_selector: ".lazy-logo",
		    threshold:500
		});
	});
	
	var animation_path = null;//'{{ env('APP_URL') }}js';
</script>
@stop