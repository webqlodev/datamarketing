@extends('layout')
@section('style')
<div class="flex-center position-ref full-height">
    <div class="case-study-items">
        <div class="case-study-item">
            <a href="{{ route('casestudy', ['case' => 'shiseido']) }}" class="modal-btn case-study-btn">
                <img src="{{ asset('img/shiseido-portfolio-640.jpg') }}" class="img-responsive img-circle case-study-img" />
            </a>
        </div>

        <div class="case-study-item">
            <a href="{{ route('casestudy', ['case' => 'hbct']) }}" class="modal-btn case-study-btn">
                <img src="{{ asset('img/hbct-portfolio-640.jpg') }}" class="img-responsive img-circle case-study-img" />
            </a>
        </div>

        <div class="case-study-item">
            <a href="{{ route('casestudy', ['case' => 'sr']) }}" class="modal-btn case-study-btn">
                <img src="{{ asset('img/sr-portfolio-640.jpg') }}" class="img-responsive img-circle case-study-img" />
            </a>
        </div>
    </div>
</div>
@stop