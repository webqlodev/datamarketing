<form method="POST" action="">
    {{ csrf_field() }}
    <div class="my-cardslider cardslider">
        <div class="text-center syp">
            <div class="lts-square-nml i5">Start Your Project</div>
            <div class="budget-title" style="margin-top:15px;">
                <div>Experience what we can do for you here!</div>
            </div>
        </div>
        <ul>
            <li>
                <div class="wrappie case-study-title">
                    <div class="pro-mar">What's your industry?</div>
                    <input type="hidden" name="ip_address" value="{{$ip}}">
                    <div class="form-group">
                        <select class="form-control selectpicker" name="industry" title="Select Your Industry">
                            <option value="fnb">F&amp;B</option>
                            <option value="beauty">Beauty</option>
                            <option value="travel">Travel</option>
                            <option value="property">Property</option>
                            <option value="others">Others</option>
                        </select>
                    </div>
                    <input class="form-control pro-ipt oth hide" name="other" placeholder="Fill in your industry">
                    <div class="pro-mar">
                        <a slide="1" class="next-slide btn btn-pro hvr-sweep-to-right"><img src="{{url('img/right-row.png')}}" width="50"></a>
                    </div>
                </div>
            </li>
            <li>
                <div class="wrappie case-study-title">
                    <div class="pro-mar">Where's your customer?</div>
                    <div class="form-group">
                        <input class="form-control pro-ipt" name="location" placeholder="Ex. Kuala Lumpur">
                    </div>
                    <div class="pro-mar">
                        <a class="previous-slide btn btn-pro hvr-sweep-to-left"><img src="{{url('img/left-row.png')}}" width="50"></a>
                        <a slide="2" class="next-slide btn btn-pro hvr-sweep-to-right"><img src="{{url('img/right-row.png')}}" width="50"></a>
                    </div>
                </div>
            </li>
            <li>
                <div class="wrappie case-study-title">
                    <div class="pro-mar">What's your target?</div>
                    <div class="form-group">
                        <select class="form-control selectpicker" name="objective" title="Select Your Objective">
                            <option value="awareness">Grow Your Audience</option>
                            <option value="boost">Boost Your Sales</option>
                        </select>
                    </div>
                    <div class="pro-mar">
                        <a class="previous-slide btn btn-pro hvr-sweep-to-left"><img src="{{url('img/left-row.png')}}" width="50"></a>
                        <a slide="3" class="next-slide btn btn-pro hvr-sweep-to-right"><img src="{{url('img/right-row.png')}}" width="50"></a>
                    </div>
                </div>
            </li>
            <li>
                <div class="wrappie case-study-title">
                    <div class="pro-mar">Your budget?</div>
                    <div class="form-group">
                        <input class="form-control pro-ipt" name="budget" placeholder="What's Your Budget? Ex. 1500" data-type="number">
                    </div>
                    <div class="pro-mar">
                        <a class="previous-slide btn btn-pro hvr-sweep-to-left"><img src="{{url('img/left-row.png')}}" width="50"></a>
                        <a class="modal-btn budget-btn btn btn-result hvr-sweep-to-right" href="#">SUBMIT</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</form>
