<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- <base href="http://www.webqlo.com.my/digital-marketing/public"> -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Webqlo Digital Marketing | Award-Winning Digital Marketing Agency</title>
        <link rel="icon" href="https://www.webqlo.com.my/wp-content/uploads/2018/10/favicon-32x32.png?x19803" type="image/png">
        <meta name="description" content="Award-Winning Digital Marketing Agency Specializes In Data-Driven & Performance Marketing. Content Strategy. Digital Campaign. Lead Generation. Online Ads. Facebook Ads. Google Ads" >
        <meta name="keywords" content="webqlo, malaysia, digital marketing, advertising company, advertising agency malaysia, ad agency malaysia, marketing agency, digital marketing agency, digital agency, social media marketing, facebook marketing"/>
        <meta property="og:url"           content="https://www.webqlo.com.my/digital-marketing" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Webqlo Digital Marketing | Award-Winning Digital Marketing Agency" />
        <meta property="og:description"   content="Award-Winning Digital Marketing Agency Specializes In Data-Driven & Performance Marketing. Content Strategy. Digital Campaign. Lead Generation. Online Ads. Facebook Ads. Google Ads" />
        <meta property="og:image"         content="{{url('img/')}}" />

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-96572083-2"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-96572083-2');
        </script> -->

        <!-- Tag Manager -->
        <!-- Google Tag Manager -->
        <!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NGFXDNK');</script> -->
        <!-- End Google Tag Manager -->

        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" /> 
        <link rel="stylesheet" href="{{ asset('css/jquery.fullpage.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/jquery.modal.css') }}" />
        {{--<link rel="stylesheet" href="{{ asset('css/cardslider.css') }}" />--}}
        <link rel="stylesheet" href="{{ asset('css/slick.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/hover-min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css') }}" />
        {{--<link rel="stylesheet" href="{{ asset('css/ben.css') }}" />--}}
        -->

        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/font.css') }}" async/> 
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
        @yield('style')
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGFXDNK"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
        <!-- End Google Tag Manager (noscript) -->
        @yield('content')

        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
        <script src="{{ asset('js/jquery-3.2.0.min.js') }}" ></script>
        <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@10.20.1/dist/lazyload.min.js"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
        <!-- <script src="{{ asset('js/scrolloverflow.min.js') }}" ></script>
        <script src="{{ asset('js/jquery.fullpage.js') }}" ></script>
        {{--<script src="{{ asset('js/jquery.fullpage.extensions.min.js') }}"></script>--}}
        <script src="{{ asset('js/jquery.easings.min.js') }}" ></script>
        <script src="{{ asset('js/jquery.modal.min.js') }}" ></script>
        <script src="{{ asset('js/jquery.animateNumber.min.js') }}" ></script>
        <script src="{{ asset('js/jquery.easings.min.js') }}" ></script>
        {{--<script src="{{ asset('js/jquery.cardslider.min.js') }}" ></script>--}}
        <script src="{{ asset('js/slick.min.js') }}" ></script>
        <script src="{{ asset('js/dropzone.min.js') }}"></script>
        {{--<script src="{{ asset('js/ben.js') }}"></script>--}}
        <script src="{{ asset('js/bootstrap-select.min.js') }}" ></script>
        <script src="{{ asset('/js/bodymovin.js') }}" ></script>
        <script src="{{ asset('js/jquery.svg.js') }}" async></script>
        <script src="{{ asset('js/jquery.main.js') }}" sync></script> -->
        <script src="{{ asset('js/app.js') }}" async></script>
        @yield('script')
    </body>
</html>
