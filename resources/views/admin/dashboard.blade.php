<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome, Webqlo!</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <div class="navbar-brand">
                    <img src="{{ asset('img/logo.png') }}" class="img-responsive" style="max-height: 100%" />
                </div>
            </div>

            <form method="post" action="{{ route('logout') }}" class="navbar-form navbar-right">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default">Logout</button>
            </form>
        </div>
    </nav>

    <div class="container" style="color:#2d2d2d">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Clicks Report</strong>
                    </div>

                    <div class="panel-body">
                        <form method="post" action="{{ route('admin.export.clicks') }}" target="_blank" id="clicks_date" class="form-inline" style="margin-bottom: 20px">
                            <div class="form-group">
                                <input type="text" name="startDate" value="{{ date( 'j M Y', strtotime('-14 days') ) }}" class="form-control datetimepicker" readonly />
                            </div>

                            <div class="form-group">
                                &rarr;
                            </div>

                            <div class="form-group">
                                <input type="text" name="endDate" value="{{ date('j M Y') }}" class="form-control datetimepicker" readonly />
                            </div>

                            <button type="button" id="make_clicks_chart_button" class="btn btn-default">Set Dates</button>
                            <button type="submit" class="btn btn-primary">Export to XLS</button>
                            {{ csrf_field() }}
                        </form>

                        <div id="clicks_chart" style="height: 400px"></div>
                    </div>

                    <div class="panel-footer">
                        <h4>Cumulative Clicks Since Live</h4>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Header Logo</th>
                                        <th>Calculate Budget</th>
                                        <th>Case Study (Shiseido)</th>
                                        <th>Case Study (HBCT)</th>
                                        <th>Case Study (Secret Recipe)</th>
                                        <th>Case Study (Red Lobster)</th>
                                        <th>Case Study (Tinge)</th>
                                        <th>Facebook</th>
                                        <th>Total Clicks</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>{{ $clickSummary['headerLogo'] }}</td>
                                        <td>{{ $clickSummary['calculateBudget'] }}</td>
                                        <td>{{ $clickSummary['caseStudyShiseido'] }}</td>
                                        <td>{{ $clickSummary['caseStudyHbct'] }}</td>
                                        <td>{{ $clickSummary['caseStudySr'] }}</td>
                                        <td>{{ $clickSummary['caseStudyRl'] }}</td>
                                        <td>{{ $clickSummary['caseStudyTinge'] }}</td>
                                        <td>{{ $clickSummary['facebook'] }}</td>
                                        <td>{{ $clickSummary['total'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Received Contacts</strong>
                    </div>

                    <div class="panel-body">
                        <form method="post" action="{{ route('admin.export.contacts') }}" target="_blank" id="contacts_date" class="form-inline" style="margin-bottom: 20px">
                            <div class="form-group">
                                <input type="text" name="startDate" class="form-control datetimepicker" readonly />
                            </div>

                            <div class="form-group">
                                &rarr;
                            </div>

                            <div class="form-group">
                                <input type="text" name="endDate" class="form-control datetimepicker" readonly />
                            </div>

                            <button type="button" id="make_contacts_table_button" class="btn btn-default">Set Dates</button>
                            <button type="button" id="reset_contacts_table_button" class="btn btn-default">Clear Dates</button>
                            <button type="submit" class="btn btn-primary">Export to XLS</button>
                            {{ csrf_field() }}
                        </form>

                        <div class="table-responsive">
                            <table id="contacts_table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Submitted On</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Message</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/jquery-3.2.0.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/echarts.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            // Init chart and table

            var clicksChart = echarts.init( $('#clicks_chart')[0] );

            clicksChart.setOption({
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: [
                        'Header Logo',
                        'Calculate Budget',
                        'Case Study (Shiseido)',
                        'Case Study (HBCT)',
                        'Case Study (Secret Recipe)',
                        'Case Study (Red Lobster)',
                        'Case Study (Tinge)',
                        'Facebook',
                        'Total Clicks'
                    ]
                },
                grid: {
                    left: '3%',
                    right: '5%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    type: 'time',
                    name: 'Date',
                    boundaryGap: false,
                    axisLabel: {
                        formatter: function (value, index) {
                            return moment(value).format('D MMM YYYY');
                        }
                    }
                },
                yAxis: {
                    type: 'value'
                }
            });

            var contactsTable = $('#contacts_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('admin.contacts') }}',
                    data: function (data) {
                        data.startDate = $('#contacts_date').find('input[name="startDate"]').val();
                        data.endDate = $('#contacts_date').find('input[name="endDate"]').val();
                    }
                },
                columns: [
                    {
                        data: 'created_at',
                        render: function (data, type, row, meta) {
                            return moment(data).format('D MMM YYYY h:mm a');
                        }
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'company'
                    },
                    {
                        data: 'contact'
                    },
                    {
                        data: 'email'
                    },
                    {
                        data: 'message'
                    }
                ]
            });

            // Datepickers

            $('.datetimepicker').datetimepicker({
                format: 'D MMM YYYY',
                ignoreReadonly: true
            });

            // Buttons

            $('#make_clicks_chart_button').click(function () {
                var dateStatus = checkDates( $(this).parent() );

                if (dateStatus) {
                    makeClicksChart();
                }
            });

            $('#make_contacts_table_button').click(function () {
                var dateStatus = checkDates( $(this).parent() );

                if (dateStatus) {
                    contactsTable.draw();
                }
            })

            $('#reset_contacts_table_button').click(function () {
                $(this).parent().find('input').val('');
                contactsTable.draw();
            });

            $('#clicks_date, #contacts_date').submit(function () {
                var dateStatus = checkDates( $(this) );

                if (dateStatus) {
                    return true;
                } else {
                    return false
                }
            });

            // Autorun

            makeClicksChart();

            // Functions

            function checkDates(jObj) {
                jObj.find('.date-error').remove();
                jObj.find('.form-group').removeClass('has-error');

                var startDate = jObj.find('input[name="startDate"]').val(),
                    endDate = jObj.find('input[name="endDate"]').val();

                if ( moment(startDate).isAfter(endDate) ) {
                    jObj.append('<p class="form-control-static text-danger date-error"><i class="fa fa-warning"></i> START DATE must be before END DATE</p>');

                    jObj.find('.form-group').addClass('has-error');

                    return false;
                } else {
                    return true;
                }
            }

            function makeClicksChart() {
                $.get(
                    '{{ route('admin.clicks') }}',
                    $('#clicks_date').serialize()
                ).done(function (result) {
                    clicksChart.setOption({
                        series: [
                            {
                                type: 'line',
                                name: 'Header Logo',
                                data: result.headerLogo
                            },
                            {
                                type: 'line',
                                name: 'Calculate Budget',
                                data: result.calculateBudget
                            },
                            {
                                type: 'line',
                                name: 'Case Study (Shiseido)',
                                data: result.caseStudyShiseido
                            },
                            {
                                type: 'line',
                                name: 'Case Study (HBCT)',
                                data: result.caseStudyHbct
                            },
                            {
                                type: 'line',
                                name: 'Case Study (Secret Recipe)',
                                data: result.caseStudySr
                            },
                            {
                                type: 'line',
                                name: 'Case Study (Red Lobster)',
                                data: result.caseStudyRl
                            },
                            {
                                type: 'line',
                                name: 'Case Study (Tinge)',
                                data: result.caseStudyTinge
                            },
                            {
                                type: 'line',
                                name: 'Facebook',
                                data: result.facebook
                            },
                            {
                                type: 'line',
                                name: 'Total Clicks',
                                data: result.total
                            }
                        ]
                    });
                });
            }
        });
    </script>
</body>
</html>
