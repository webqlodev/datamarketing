<div class="modal case-study-modal">
    <div class="modal-img" style="height: 320px; background: url({{ asset('img/sr-cover.jpg') }}) center/cover no-repeat;"></div>

    <div class="modal-body">
        <div class="body-featured" style="background-color: #e2292b; color: #fff;">
            <h1>Secret Recipe</h1>
            <h4>Website Creation &plus; Social Media Management</h4>
            <hr style="border-top: solid 1px #fff; width: 200px; margin-left: 0;" />
            <p>
                Bringing an iconic brand to life. Building a lasting home for Secret Recipe &amp; more!
            </p>
        </div>

        <div class="body-main">
            <h2>Facebook Management</h2>
            <p>
                We took our vision a step further by hitting +98% Fan Like and +552% monthly engagement, establishing a strong influential presence and a mouthwatering following, keeping fans hungry for more Secret Recipe!
            </p>
        </div>
    </div>

    <div class="modal-stats">
        <div class="roi-items text-center">
            <div class="row">
                <div class="col-sm-3 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +98%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-like-dark.png')}}" width="31">
                            </div>

                            <div class="note-subtitle">
                                Fan Like
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +217%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-impression-dark.png')}}" width="33">
                            </div>

                            <div class="note-subtitle">
                                Impressions
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +552%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                MONTHLY
                            </div>

                            <div class="note-subtitle">
                                Engagement
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +35%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-ctr-dark.png')}}" width="23">
                            </div>

                            <div class="note-subtitle">
                                Click Thru Rate (CTR) Achieved
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-social">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('img/sr-01.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/sr-02.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/sr-03.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>

            <div class="col-md-6">
                <img src="{{ asset('img/sr-04.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/sr-05.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/sr-06.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>
        </div>
    </div>

    <div class="modal-nav">
        <div class="row">
            <a href="#case-study-hbct" class="col-xs-6 nav-prev" data-ajax="{{ route('casestudy', ['case' => 'hbct']) }}">
                <div class="nav-arrow">
                    &larr;
                </div>

                <div class="nav-body">
                    <h5>Hokkaido Baked Cheese Tart</h5>
                    <h6>Social Media, Digital Campaign</h6>
                </div>
            </a>

            <a href="#case-study-shiseido" class="col-xs-6 nav-next" data-ajax="{{ route('casestudy', ['case' => 'shiseido']) }}">
                <div class="nav-arrow">
                    &rarr;
                </div>

                <div class="nav-body">
                    <h5>Shiseido</h5>
                    <h6>Tactical Campaign</h6>
                </div>
            </a>
        </div>
    </div>
</div>
