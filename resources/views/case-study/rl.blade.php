<div class="modal case-study-modal">
    <div class="modal-img" style="height: 320px; background: url({{ asset('img/rl-cover.jpg') }}) center/cover no-repeat;"></div>

    <div class="modal-body">
        <div class="body-featured" style="background-color: #bd1206; color: #fff;">
            <h1>Red Lobster</h1>
            <h4>Tactical Campaign</h4>
            <hr style="border-top: solid 1px #fff; width: 200px; margin-left: 0;" />
            <p>
                Sailing the high seas with Red Lobster. Famed for serving the freshest seafood, our biggest challenge was to improve overall footfall by delivering a fresh perspective with every mouth-watering promotion.
            </p>
        </div>

        <div class="body-main">
            <h2>Tactical Campaign</h2>
            <p>
                Our result? An amazing 25% footfall conversion using only social media platforms! We’ve pumped up fans’ excitement using teaser ads, driving fans wild as they guess the next upcoming promotion!
            </p>
        </div>
    </div>

    <div class="modal-stats">
        <div class="roi-items text-center">
            <div class="row">
                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            147%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                ROI
                            </div>

                            <div class="note-subtitle">
                                Engagement Achieved
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            3.11%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-ctr-dark.png')}}" width="23">
                            </div>

                            <div class="note-subtitle">
                                Click Thru Rate (CTR) Achieved
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            25%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-footfall-dark.png')}}" width="44">
                            </div>
                            <div class="note-subtitle">
                                Foot Fall Conversion
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-social">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('img/rl-01.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/rl-02.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/rl-03.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>

            <div class="col-md-6">
                <img src="{{ asset('img/rl-04.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/rl-05.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/rl-06.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>
        </div>
    </div>

    <div class="modal-nav">
        <div class="row">
            <a href="#case-study-shiseido" class="col-xs-6 nav-prev" data-ajax="{{ route('casestudy', ['case' => 'shiseido']) }}">
                <div class="nav-arrow">
                    &larr;
                </div>

                <div class="nav-body">
                    <h5>Shiseido</h5>
                    <h6>Tactical Campaign</h6>
                </div>
            </a>

            <a href="#case-study-tinge" class="col-xs-6 nav-next" data-ajax="{{ route('casestudy', ['case' => 'tinge']) }}">
                <div class="nav-arrow">
                    &rarr;
                </div>

                <div class="nav-body">
                    <h5>Spritzer - Tinge</h5>
                    <h6>Facebook Management</h6>
                </div>
            </a>
        </div>
    </div>
</div>
