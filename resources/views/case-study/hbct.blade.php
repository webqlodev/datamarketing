<div class="modal case-study-modal">
    <div class="modal-img" style="height: 320px; background: url({{ asset('img/hbct-cover.jpg') }}) center/cover no-repeat;"></div>

    <div class="modal-body">
        <div class="body-featured" style="background-color: #f6ea4a; color: #000;">
            <h1>Hokkaido Baked Cheese Tart</h1>
            <h4>Social Media &plus; Digital Campaign</h4>
            <hr style="border-top: solid 1px #000; width: 200px; margin-left: 0;" />
            <p>
                Appointed back when Hokkaido Baked Cheese Tart (HBCT) first opened in Klang Valley. Our goal was simple. A great cheese tart is best served fresh and that’s what we did by making it the talk of the town and going viral in one month!
            </p>
        </div>

        <div class="body-main">
            <h2>Social Media Management</h2>
            <p>
                Supporting HBCT’s ambitious growth with 152%+ Fan Likes monthly throughout the region using just only social media. Driving thousands to their store with Free Tart Redemption, creating lasting impression, making HBCT top of mind when it comes to cheese &amp; tarts!
            </p>
        </div>
    </div>

    <div class="modal-stats">
        <div class="roi-items text-center">
            <div class="row">
                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +152%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                MONTHLY
                            </div>

                            <div class="note-subtitle">
                                Fan Like
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +150%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                MONTHLY
                            </div>

                            <div class="note-subtitle">
                                Impressions
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            5.15%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-ctr-dark.png')}}" width="23">
                            </div>

                            <div class="note-subtitle">
                                Click Thru Rate (CTR) Achieved
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-social">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('img/hbct-01.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/hbct-02.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/hbct-03.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/hbct-04.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>

            <div class="col-md-6">
                <img src="{{ asset('img/hbct-05.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/hbct-06.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/hbct-07.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>
        </div>
    </div>

    <div class="modal-nav">
        <div class="row">
            <a href="#case-study-tinge" class="col-xs-6 nav-prev" data-ajax="{{ route('casestudy', ['case' => 'tinge']) }}">
                <div class="nav-arrow">
                    &larr;
                </div>

                <div class="nav-body">
                    <h5>Spritzer - Tinge</h5>
                    <h6>Facebook Management</h6>
                </div>
            </a>

            <a href="#case-study-sr" class="col-xs-6 nav-next" data-ajax="{{ route('casestudy', ['case' => 'sr']) }}">
                <div class="nav-arrow">
                    &rarr;
                </div>

                <div class="nav-body">
                    <h5>Secret Recipe</h5>
                    <h6>Website Creation, Social Media Management</h6>
                </div>
            </a>
        </div>
    </div>
</div>
