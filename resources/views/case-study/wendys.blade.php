<div class="modal case-study-modal">
    <div class="modal-img" style="height: 320px; background: url({{ asset('img/wendys_casestudy/wendys-cover.png') }}) center/cover no-repeat;"></div>

    <div class="modal-body">
        <div class="body-featured" style="background-color: #ce1223; color: #fff;">
            <h1>Wendy's</h1>
            <h4>Tactical Campaign</h4>
            <hr style="border-top: solid 1px #fff; width: 200px; margin-left: 0;" />
            <p>
                To driving registrations for free burger redemption
            </p>
        </div>

        <div class="body-main">
            <h2>Tactical Campaign</h2>
            <p>
                We have garnered 23% registration conversion of every click (Higher than estimated 15% conversion rate) and conclusively 27% conversion of actual redemption throughout the campaign!
            </p>
        </div>
    </div>

    <div class="modal-stats">
        <div class="roi-items text-center">
            <div class="row">
                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            1,311,466
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-impression-dark.png')}}" width="33">
                            </div>

                            <div class="note-subtitle">
                                Impressions
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            27,001
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                MONTHLY
                            </div>

                            <div class="note-subtitle">
                                Engagement
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            8%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-arrow-dark.png')}}" width="23">
                            </div>

                            <div class="note-subtitle">
                                Conversion
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-social">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('img/wendys_casestudy/wendys-01.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/wendys_casestudy/wendys-02.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/wendys_casestudy/wendys-03.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/wendys_casestudy/wendys-04.png') }}" alt="Social testimonial" class="img-responsive" />
            </div>

            <div class="col-md-6">
                <img src="{{ asset('img/wendys_casestudy/wendys-05.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/wendys_casestudy/wendys-06.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/wendys_casestudy/wendys-07.png') }}" alt="Social testimonial" class="img-responsive" />
            </div>
        </div>
    </div>

    <div class="modal-nav">
        <div class="row">
            <a href="#case-study-tinge" class="col-xs-6 nav-prev" data-ajax="{{ route('casestudy', ['case' => 'tinge']) }}">
                <div class="nav-arrow">
                    &larr;
                </div>

                <div class="nav-body">
                    <h5>Spritzer - Tinge</h5>
                    <h6>Creative Content</h6>
                </div>
            </a>

            <a href="#case-study-shiseido" class="col-xs-6 nav-next" data-ajax="{{ route('casestudy', ['case' => 'shiseido']) }}">
                <div class="nav-arrow">
                    &rarr;
                </div>

                <div class="nav-body">
                    <h5>Shiseido</h5>
                    <h6>Digital Advertising</h6>
                </div>
            </a>
        </div>
    </div>
</div>
