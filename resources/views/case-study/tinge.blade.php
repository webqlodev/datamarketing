<div class="modal case-study-modal">
    <div class="modal-img" style="height: 320px; background: url({{ asset('img/tinge-cover.jpg') }}) center/cover no-repeat;"></div>

    <div class="modal-body">
        <div class="body-featured" style="background-color: #1ba91b; color: #000;">
            <h1>Spritzer- Tinge</h1>
            <h4>Social Media Management</h4>
            <hr style="border-top: solid 1px #000; width: 200px; margin-left: 0;" />
            <p>
                Making a splash on social media! More than just mineral water, our challenge was to exceed the expectation of Tinge’s savvy social media fans with engaging content that keeps them coming back for more!
            </p>
        </div>

        <div class="body-main">
            <h2>Facebook Management</h2>
            <p>
                Over a year, we’ve hit an impressive +387% yearly engagement and +148% Yearly Fan Like! Successfully positioning Tinge at the forefront when it comes to mineral water, fun &amp; good times. Get some fun with Tinge today, it’s amazing.
            </p>
        </div>
    </div>

    <div class="modal-stats">
        <div class="roi-items text-center">
            <div class="row">
                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +148%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                YEARLY
                            </div>

                            <div class="note-subtitle">
                                Fan Like
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +610%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                YEARLY
                            </div>

                            <div class="note-subtitle">
                                Impressions
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +378%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                YEARLY
                            </div>
                            <div class="note-subtitle">
                                Engagement
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-social">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('img/tinge-01.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/tinge-02.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/tinge-03.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>

            <div class="col-md-6">
                <img src="{{ asset('img/tinge-04.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/tinge-05.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/tinge-06.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>
        </div>
    </div>

    <div class="modal-nav">
        <div class="row">
            <a href="#case-study-adqlo" class="col-xs-6 nav-prev" data-ajax="{{ route('casestudy', ['case' => 'adqlo']) }}">
                <div class="nav-arrow">
                    &larr;
                </div>

                <div class="nav-body">
                    <h5>Adqlo</h5>
                    <h6>Social Media Analytics</h6>
                </div>
            </a>

            <a href="#case-study-wendys" class="col-xs-6 nav-next" data-ajax="{{ route('casestudy', ['case' => 'wendys']) }}">
                <div class="nav-arrow">
                    &rarr;
                </div>

                <div class="nav-body">
                    <h5>Wendy's</h5>
                    <h6>Website Development</h6>
                </div>
            </a>
        </div>
    </div>
</div>
