<div class="modal case-study-modal">
    <div class="modal-img" style="height: 320px; background: url({{ asset('img/shiseido-cover.jpg') }}) center/cover no-repeat;"></div>

    <div class="modal-body">
        <div class="body-featured" style="background-color: #c80f2e; color: #fff;">
            <h1>Shiseido</h1>
            <h4>Tactical Campaign</h4>
            <hr style="border-top: solid 1px #fff; width: 200px; margin-left: 0;" />
            <p>
                We have revolutionized how beauty is shared in the 21st century, all while staying true to Shiseido’s 140 years of beauty heritage.
            </p>
        </div>

        <div class="body-main">
            <h2>Tactical Campaign</h2>
            <p>
                With over 222% in ROI engagement &amp; a mind-blowing 30% conversion rate, our campaign for Shiseido has redefined how the brand engage and expand its loyal following.
            </p>
        </div>
    </div>

    <div class="modal-stats">
        <div class="roi-items text-center">
            <div class="row">
                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            222%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                ROI
                            </div>

                            <div class="note-subtitle">
                                Engagement Achieved
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            12%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-ctr-dark.png')}}" width="23">
                            </div>

                            <div class="note-subtitle">
                                Click Thru Rate (CTR) Achieved
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            30%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-arrow-dark.png')}}" width="38">
                            </div>
                            <div class="note-subtitle">
                                Conversion
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-social">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('img/shiseido-01.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/shiseido-02.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/shiseido-03.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>

            <div class="col-md-6">
                <img src="{{ asset('img/shiseido-04.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/shiseido-05.jpg') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/shiseido-06.jpg') }}" alt="Social testimonial" class="img-responsive" />
            </div>
        </div>
    </div>

    <div class="modal-nav">
        <div class="row">
            <a href="#case-study-wendys" class="col-xs-6 nav-prev" data-ajax="{{ route('casestudy', ['case' => 'wendys']) }}">
                <div class="nav-arrow">
                    &larr;
                </div>

                <div class="nav-body">
                    <h5>Wendy's</h5>
                    <h6>Website Development</h6>
                </div>
            </a>

            <a href="#case-study-adqlo" class="col-xs-6 nav-next" data-ajax="{{ route('casestudy', ['case' => 'adqlo']) }}">
                <div class="nav-arrow">
                    &rarr;
                </div>

                <div class="nav-body">
                    <h5>Adqlo</h5>
                    <h6>Social Media Analytics</h6>
                </div>
            </a>
        </div>
    </div>
</div>
