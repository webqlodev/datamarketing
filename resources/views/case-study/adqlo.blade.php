<div class="modal case-study-modal">
    <div class="modal-img hidden-xs" style="height: 320px; background: url({{ asset('img/adqlo_casestudy/adqlo-cover.png') }}) center/cover no-repeat;"></div>
    <div class="modal-img visible-xs" style="height: 320px; background: url({{ asset('img/adqlo_casestudy/adqlo-cover-mobile.png') }}) center/cover no-repeat;"></div>

    <div class="modal-body">
        <div class="body-featured" style="background-color: #6ACBDC; color: #000;">
            <h1>Adqlo</h1>
            <h4>Social Media Analytics</h4>
            <hr style="border-top: solid 1px #000; width: 200px; margin-left: 0;" />
            <p>
            Understand your audience, launch impactful campaigns and craft effective content with the help of critical social media insights to achieve your business goals.
            </p>
        </div>

        <div class="body-main">
            <h2>Gain tactical foresight with social media analytics</h2>
            <p>
             Analyze, measure and drive you campaign with meaningful social media analytics to ensure the optimum result.
            </p>
        </div>
    </div>

    {{--<div class="modal-stats">
        <div class="roi-items text-center">
            <div class="row">
                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +152%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                MONTHLY
                            </div>

                            <div class="note-subtitle">
                                Fan Like
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            +150%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title">
                                MONTHLY
                            </div>

                            <div class="note-subtitle">
                                Impressions
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 roi-item">
                    <div class="row">
                        <div class="col-xs-12 item-number">
                            5.15%
                        </div>

                        <div class="col-xs-12 item-note">
                            <div class="note-title text-center">
                                <img src="{{url('img/cs-ctr-dark.png')}}" width="23">
                            </div>

                            <div class="note-subtitle">
                                Click Thru Rate (CTR) Achieved
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}

    <div class="modal-social">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('img/adqlo_casestudy/adqlo-01.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/adqlo_casestudy/adqlo-02.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/adqlo_casestudy/adqlo-03.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/adqlo_casestudy/adqlo-04.png') }}" alt="Social testimonial" class="img-responsive" />
            </div>

            <div class="col-md-6">
                <img src="{{ asset('img/adqlo_casestudy/adqlo-05.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/adqlo_casestudy/adqlo-06.png') }}" alt="Social testimonial" class="img-responsive" />
                <img src="{{ asset('img/adqlo_casestudy/adqlo-07.png') }}" alt="Social testimonial" class="img-responsive" />
            </div>
        </div>
    </div>

    <div class="modal-nav">
        <div class="row">
            <a href="#case-study-shiseido" class="col-xs-6 nav-prev" data-ajax="{{ route('casestudy', ['case' => 'shiseido']) }}">
                <div class="nav-arrow">
                    &larr;
                </div>

                <div class="nav-body">
                    <h5>Shiseido</h5>
                    <h6>Digital Advertising</h6>
                </div>
            </a>

            <a href="#case-study-tinge" class="col-xs-6 nav-next" data-ajax="{{ route('casestudy', ['case' => 'tinge']) }}">
                <div class="nav-arrow">
                    &rarr;
                </div>

                <div class="nav-body">
                    <h5>Spritzer - Tinge</h5>
                    <h6>Creative Content</h6>
                </div>
            </a>
        </div>
    </div>
</div>
