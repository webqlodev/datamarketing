<div class="budget-heading">
    Data-Driven Performance <br><a href="#Budget" rel="modal:close">Recalculate again</a>
</div>

<div class="budget-body">
    <div class="col-sm-5 market-col">
        <div class="col-title">
            Market Benchmark
        </div>

        <div class="col-items">
            <div class="col-item">
                <div class="item-title2">
                    Click-Through Rate (CTR)
                </div>

                <div class="item-number">
                    <span id="market_ctr">0</span> %
                </div>
            </div>

            {{-- <div class="col-item">
                <div class="item-title2">
                    Impression
                </div>

                <div class="item-number">
                    <span id="market_imp">0</span>
                </div>
            </div> --}}

            <div class="col-item">
                <div class="item-title2">
                    Engagement
                </div>

                <div class="item-number">
                    <span id="market_eng">0</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-2 vs-col">
        <div style="font-family: 'Oswald-Medium';">
            VS
        </div>
    </div>

    <div class="col-sm-5 webqlo-col">
        <div class="col-title">
            Webqlo Benchmark
        </div>

        <div class="col-items">
            <div class="col-item">
                <div class="item-title">
                    Click-Through Rate (CTR)
                </div>

                <div class="item-number">
                    <span id="webqlo_ctr">0</span> %
                </div>
            </div>

            {{-- <div class="col-item">
                <div class="item-title">
                    Impression
                </div>

                <div class="item-number">
                    <span id="webqlo_imp">0</span>
                </div>
            </div> --}}

            <div class="col-item">
                <div class="item-title">
                    Engagement
                </div>

                <div class="item-number">
                    <span id="webqlo_eng">0</span>
                </div>
            </div>
        </div>
    </div>
</div>
