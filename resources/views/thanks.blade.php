@extends('layout')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <style>
        body{color:#f7f7f7;background-color:#161616; font-display:fallback;}
    </style>
@stop
@section('content')
{{--<div id="thanks">
    <h4 class="modal-title text-center">
        Thank you! Your message has been received! You’ll hear from us real soon.
    </h4>    
</div>--}}
<div class="fixed-logo">
    <div class="logo-wrap">
        <a href="{{ route('homepage') }}" target="_blank">
            <img src="{{url('img/webqlo-2018-retina.png')}}" class="img-responsive" alt="Webqlo Logo">
        </a>
    </div>
</div>
<div class="container p-t-4">
    <div class="row">
        <div id="thanks" class="text-center col-md-12">
            <div class="gothambook-24-i">
                Thank you! Your message has been received! You’ll hear from us real soon.
                <div class="p-t-1 gothambold-28"><a href="https://www.webqlo.com.my/digital-marketing">Back to Digital Marketing</a></div>
            </div>
        </div>
    </div>  
</div>
@stop 
@section('script')
<script>
    var animation_path = null;
</script>
@endsection