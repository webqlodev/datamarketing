<form method="post" enctype="multipart/form-data" class="contact-form postContact">
    {{ csrf_field() }}
    <div class="form-group">
        <input type="text" class="form-control" placeholder="Name" name="name" />
        <div class="name"></div>
    </div>

    <div class="form-group">
        <input type="text" class="form-control numeric" placeholder="Contact No / HP No" name="contact" />
        <div class="contact"></div>
    </div>

    <div class="form-group">
        <input type="text" class="form-control" placeholder="Company Name" name="company" />
        <div class="company"></div>
    </div>

    <div class="form-group">
        <input type="text" class="form-control" placeholder="Email" name="email" />
        <div class="email"></div>
    </div>

    <div class="form-group">
        <textarea rows="3" class="form-control" placeholder="Message" name="message"></textarea>
        <div class="message"></div>
    </div>

    <div class="form-group">
        <div class="dropzone"></div>
        <div class="file"></div>
        <input type="hidden" class="uploaded" name="uploaded" />
    </div>

    <div class="form-group">
        <button type="button" class="lts-btn lts-btn-light hvr-sweep-to-right submitContact">Submit <img src="{{ asset('/img/rightarrow-light.png') }}" class="right-arrow" /></button>
    </div>
</form>
