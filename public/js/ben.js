// var cardslider = $('.my-cardslider').cardslider({
//     swipe: false,
//     dots: true,
//     nav: false,    
    
// }).data('cardslider');


// $('.next-slide').on('click', function() {
//     cardslider.nextCard(); 
// });

// $('.previous-slide').on('click', function() {
//     cardslider.prevCard(); 
// });

$('select[name="industry"]').change(function() {
    if($(this).val() == 'others') {
        $('.oth').removeClass('hide');
    } else {
        $('.oth').addClass('hide');
    }
});
   
//Modal popup for calculation
$('body').on($.modal.OPEN, function (event, modal) {
    // Disable fullPage.js scrolling
    $.fn.fullpage.setAllowScrolling(false);
    $.fn.fullpage.setKeyboardScrolling(false);
    
    //allow tooltip function in modal
    $('[data-toggle="tooltip"]').tooltip();
    
    if ('ontouchstart' in document.documentElement) { // or whatever "is this a touch device?" test we want to use
        $('body').css('cursor', 'pointer');
    }    
    
    // Add animation to calculated numbers inside budget modal
    if (modal.options.modalClass == 'modal budget-modal') {
    
        var industry = $('[name="industry"]').val(),
            location = $('[name="location"]').val(),
            objective = $('[name="objective"]').val(),            
            budget = parseFloat( $('[name="budget"]').val().replace(/[^\d\.]/g, '') );    
        
        //if($('[name="industry"]').val() == 'others') {
        //    var industry = $('[name="other"]').val();
        //} else {        
        //    var industry = $('[name="industry"]').val();          
        //}    
            
        if( objective == 'awareness' ) {
            
            if( industry == 'fnb' ) {
            
                var comma_separator = $.animateNumber.numberStepFactories.separator(',');
                var fnb_aw_eng = budget/0.4, //webqlo engagement
                    fnb_aw_rea = fnb_aw_eng/0.04; //webqlo reach
                    
                var m_fnb_aw_eng = budget/0.9, //market engagement
                    m_fnb_aw_rea = m_fnb_aw_eng/0.0022; //market reach                
                    
                $('#webqlo_reach').animateNumber({
                    number: fnb_aw_rea,
                    numberStep: comma_separator
                }),
                $('#market_reach').animateNumber({
                    number: m_fnb_aw_rea,
                    numberStep: comma_separator
                });
                
                // Convert CTR to decimal value
                $('#webqlo_ctr').animateNumber({
                    number: 400,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                }),
                $('#market_ctr').animateNumber({
                    number: 22,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                });
                
                $('#webqlo_eng').animateNumber({
                    number: fnb_aw_eng,
                    numberStep: comma_separator
                }),
                $('#market_eng').animateNumber({
                    number: m_fnb_aw_eng,
                    numberStep: comma_separator
                });
                
            } else if ( industry == 'beauty' ) {
                
                var comma_separator = $.animateNumber.numberStepFactories.separator(',');
                
                var beauty_aw_eng = budget/0.3, //webqlo engagement
                    beauty_aw_rea = beauty_aw_eng/0.03; //webqlo reach

                var m_beauty_aw_eng = budget/1.6, //market engagement
                    m_beauty_aw_rea = m_beauty_aw_eng/0.043; //market reach
                    
                $('#webqlo_reach').animateNumber({
                    number: beauty_aw_rea,
                    numberStep: comma_separator
                }),
                $('#market_reach').animateNumber({
                    number: m_beauty_aw_rea,
                    numberStep: comma_separator
                });
                
                // Convert CTR to decimal value
                $('#webqlo_ctr').animateNumber({
                    number: 300,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                }),
                $('#market_ctr').animateNumber({
                    number: 43,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                });
                
                $('#webqlo_eng').animateNumber({
                    number: beauty_aw_eng,
                    numberStep: comma_separator
                }),
                $('#market_eng').animateNumber({
                    number: m_beauty_aw_eng,
                    numberStep: comma_separator
                });                
                
            } else if ( industry == 'travel' || industry == 'property' ) {
                
                var comma_separator = $.animateNumber.numberStepFactories.separator(',');
                
                var mix_aw_eng = budget/0.4, //webqlo engagement
                    mix_aw_rea = mix_aw_eng/0.03; //webqlo reach
                    
                var m_mix_aw_eng = budget/1.5, //market engagement
                    m_mix_aw_rea = m_mix_aw_eng/0.026; //market reach                    
                    
                $('#webqlo_reach').animateNumber({
                    number: mix_aw_rea,
                    numberStep: comma_separator
                }),
                $('#market_reach').animateNumber({
                    number: m_mix_aw_rea,
                    numberStep: comma_separator
                });
                
                // Convert CTR to decimal value
                $('#webqlo_ctr').animateNumber({
                    number: 300,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                }),
                $('#market_ctr').animateNumber({
                    number: 26,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                });
                
                $('#webqlo_eng').animateNumber({
                    number: mix_aw_eng,
                    numberStep: comma_separator
                }),
                $('#market_eng').animateNumber({
                    number: m_mix_aw_eng,
                    numberStep: comma_separator
                });                   
                
            } else if ( industry == 'others' ) {
                
                var comma_separator = $.animateNumber.numberStepFactories.separator(',');
                
                var other_aw_eng = budget/0.4, //webqlo engagement
                    other_aw_rea = other_aw_eng/0.03; //webqlo reach
                    
                var m_other_aw_eng = budget/1.4, //market engagement
                    m_other_aw_rea = m_other_aw_eng/0.047; //market reach                    
                    
                $('#webqlo_reach').animateNumber({
                    number: other_aw_rea,
                    numberStep: comma_separator
                }),
                $('#market_reach').animateNumber({
                    number: m_other_aw_rea,
                    numberStep: comma_separator
                });
                
                // Convert CTR to decimal value
                $('#webqlo_ctr').animateNumber({
                    number: 300,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                }),
                $('#market_ctr').animateNumber({
                    number: 47,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                });
                
                $('#webqlo_eng').animateNumber({
                    number: other_aw_eng,
                    numberStep: comma_separator
                }),
                $('#market_eng').animateNumber({
                    number: m_other_aw_eng,
                    numberStep: comma_separator
                });
                
            }
            
        } else if ( objective == 'boost' ) {
            
            if( industry == 'fnb' ) {
            
                var comma_separator = $.animateNumber.numberStepFactories.separator(',');
            
                var fnb_bo_eng = budget/0.4,
                    fnb_bo_rea = fnb_bo_eng/0.25;
                    
                var m_fnb_bo_eng = budget/0.9,
                    m_fnb_bo_rea = m_fnb_bo_eng/0.0022;                    
                    
                $('#webqlo_reach').animateNumber({
                    number: fnb_bo_rea,
                    numberStep: comma_separator
                }),
                $('#market_reach').animateNumber({
                    number: m_fnb_bo_rea,
                    numberStep: comma_separator
                });
                
                // Convert CTR to decimal value
                $('#webqlo_ctr').animateNumber({
                    number: 250,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                }),
                $('#market_ctr').animateNumber({
                    number: 22,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                });
                
                $('#webqlo_eng').animateNumber({
                    number: fnb_bo_eng,
                    numberStep: comma_separator
                }),
                $('#market_eng').animateNumber({
                    number: m_fnb_bo_eng,
                    numberStep: comma_separator
                });
                
            } else if ( industry == 'beauty' ) {
            
                var comma_separator = $.animateNumber.numberStepFactories.separator(',');
            
                var beauty_bo_eng = budget/0.4,
                    beauty_bo_rea = beauty_bo_eng/0.02;

                var m_beauty_bo_eng = budget/1.6,
                    m_beauty_bo_rea = m_beauty_bo_eng/0.0043;                    
                    
                    
                $('#webqlo_reach').animateNumber({
                    number: beauty_bo_rea,
                    numberStep: comma_separator
                }),
                $('#market_reach').animateNumber({
                    number: m_beauty_bo_rea,
                    numberStep: comma_separator
                });
                
                // Convert CTR to decimal value
                $('#webqlo_ctr').animateNumber({
                    number: 200,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                }),
                $('#market_ctr').animateNumber({
                    number: 43,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                });
                
                $('#webqlo_eng').animateNumber({
                    number: beauty_bo_eng,
                    numberStep: comma_separator
                }),
                $('#market_eng').animateNumber({
                    number: m_beauty_bo_eng,
                    numberStep: comma_separator
                });
            
            } else if ( industry == 'travel' || industry == 'property' ) {
                
                var comma_separator = $.animateNumber.numberStepFactories.separator(',');
                
                var mix_bo_eng = budget/0.5, 
                    mix_bo_rea = mix_bo_eng/0.02;
                    
                var m_mix_bo_eng = budget/1.5, 
                    m_mix_bo_rea = m_mix_bo_eng/0.026;
                    
                $('#webqlo_reach').animateNumber({
                    number: mix_bo_rea,
                    numberStep: comma_separator
                }),
                $('#market_reach').animateNumber({
                    number: m_mix_bo_rea,
                    numberStep: comma_separator
                });
                
                // Convert CTR to decimal value
                $('#webqlo_ctr').animateNumber({
                    number: 200,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                }),
                $('#market_ctr').animateNumber({
                    number: 26,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                });
                
                $('#webqlo_eng').animateNumber({
                    number: mix_bo_eng,
                    numberStep: comma_separator
                }),
                $('#market_eng').animateNumber({
                    number: m_mix_bo_eng,
                    numberStep: comma_separator
                });                   
                
            } else if ( industry == 'others' ) {
                
                var comma_separator = $.animateNumber.numberStepFactories.separator(',');
                
                var other_aw_eng = budget/0.5,
                    other_aw_rea = other_aw_eng/0.02;
                    
                var m_other_aw_eng = budget/1.4,
                    m_other_aw_rea = m_other_aw_eng/0.047;
                    
                $('#webqlo_reach').animateNumber({
                    number: other_aw_rea,
                    numberStep: comma_separator
                }),
                $('#market_reach').animateNumber({
                    number: m_other_aw_rea,
                    numberStep: comma_separator
                });
                
                // Convert CTR to decimal value
                $('#webqlo_ctr').animateNumber({
                    number: 200,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                }),
                $('#market_ctr').animateNumber({
                    number: 47,
                    numberStep: function(now, tween) {
                        var decimal_places = 2,
                            decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places),
                            floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
    
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
    
                        target.text(floored_number);
                    }
                });
                
                $('#webqlo_eng').animateNumber({
                    number: other_aw_eng,
                    numberStep: comma_separator
                }),
                $('#market_eng').animateNumber({
                    number: m_other_aw_eng,
                    numberStep: comma_separator
                });                
                
            }
        }
    }
});