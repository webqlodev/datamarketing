<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('microsite');
//});

Route::get('/', 'HomeController@index');

Route::get('/contactform', function () {
    return view('contact-form');
})->name('contactform');

Route::get('/budget', 'HomeController@calculateBudget')->name('budget');

Route::get('/case-study/{case}', 'HomeController@caseStudy')->name('casestudy');

Route::get('/homepage', 'HomeController@homepage')->name('homepage');

Route::get('/facebook', 'HomeController@facebook')->name('facebook');

Route::post('/contact-us', 'HomeController@contactUs');

Route::get('/thanks', function () {
    return view('thanks');
});

Route::group(['prefix' => 'admin'], function (){
    Route::get('/', 'AdminController@index');

    // Authentication Routes...
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    // Admin Routes...
    Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('/clicks', 'AdminController@clicks')->name('admin.clicks');
    Route::post('/export-clicks', 'AdminController@exportClicks')->name('admin.export.clicks');
    Route::get('/contacts', 'AdminController@contacts')->name('admin.contacts');
    Route::post('/export-contacts', 'AdminController@exportContacts')->name('admin.export.contacts');
});
