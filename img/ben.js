var cardslider = $('.my-cardslider').cardslider({
    swipe: false,
    dots: true,
    nav: false,    
    
}).data('cardslider');


$('.next-slide').on('click', function() {
    cardslider.nextCard(); 
});

$('.previous-slide').on('click', function() {
    cardslider.prevCard(); 
});

$('select[name="industry"]').change(function() {
    if($(this).val() == 'others') {
        $('.oth').removeClass('hide');
    } else {
        $('.oth').addClass('hide');
    }
});